﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PulseWeb.API;
using PulseWeb.Utils;
using PulseWeb.Utils.Security;
using PulseWeb.Utils.Database;
using System.Web.Security;
using PulseWeb.Model;
using Telerik.Web.UI;

namespace PulseWeb.Account
{
    public partial class LoginPage : BasePage
    {

        //string mDemoUser = "demologin";
        //string mDemoPW = Crypto.EncryptString("1234");

        //string mTestUser = "Joseph.Bernacki@wchn.org";
        //string mTestPW = Crypto.EncryptString("joe");

        //string mTestUser = "arlow.farrell@enovatemedical.com";
        //string mTestPW = Crypto.EncryptString("arlow");

        //string mTestUser = "demo@stingermedical.com";
        //string mTestPW = Crypto.EncryptString("StingerSales");

        // St. Thomas
        //string mTestUser = "lauren.demo@enovatemedical.com";
        //string mTestPW = Crypto.EncryptString("demo");

        // CustomerID 1
        //string mTestUser = "demo@castdemo.com";
        //string mTestPW = "rpiqgUg9nPXiIJRuGpkLTw==";

        // Baylor
#if DEBUG
        string mTestUser = "DemoUser@enovatemedical.com";
        string mTestPW = "YkpLkpmQ2gPQBHOMiqUsgg=="; //	"BijanLama007"
#endif

//clinton.chafin@enovatemedical.com



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string queryString = Request.QueryString.ToString().ToLower();
                if (queryString == "logout")
                {
                    Logout();
                }
                else
                {

#if DEBUG
                    Login(mTestUser,mTestPW);
#endif

                }
                this.modalPopup.Visible = false;
                this.modalPopup.Modal = false;
                this.modalPopup.CenterIfModal = true;

                if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                {
                    txtUserName.Text = Request.Cookies["UserName"].Value;
                    txtPassword.Attributes["value"] = Request.Cookies["Password"].Value;
                }
                roundedTwo.Checked = Request.Cookies["RememberPW"] != null && Request.Cookies["RememberPW"].Value != "" ? Convert.ToBoolean(Request.Cookies["RememberPW"].Value) : false;

            }

        }

        protected void Logout()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            Session.Clear();
        }

        protected void Login(string userName, string password)
        {
            try
            {
                if (roundedTwo.Checked)
                {
                    Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(830);
                    Response.Cookies["Password"].Expires = DateTime.Now.AddDays(830);
                    Response.Cookies["RememberPW"].Expires = DateTime.Now.AddDays(830);

                }
                else
                {
                    Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies["RememberPW"].Expires = DateTime.Now.AddDays(-1);
                }
                Response.Cookies["UserName"].Value = txtUserName.Text.Trim();
                Response.Cookies["Password"].Value = txtPassword.Text.Trim();
                Response.Cookies["RememberPW"].Value = roundedTwo.Checked.ToString();

                if (userName == "sales.demo@enovatemedical.com" && password == "pulse2016" || password == "FOIKeSNKxZ4xHwVHmdD0XQ==")
                {
                    userName = "sales.demo@enovatemedical.com";
                    password = "pulse2016";
                    //PulseSession.Instance.StartNewSession("Joseph.Bernacki@wchn.org", Crypto.EncryptString("joe"));
                    PulseSession.Instance.StartNewSession("sales.demo@enovatemedical.com", Crypto.EncryptString("pulse2016"));
                }
                else
                {
                    PulseSession.Instance.StartNewSession(userName, password);
                    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                {
               
                    enovateDB.Query("update [dbo].[User] set LastLoginDateUTC = '" + DateTime.UtcNow.ToString() + "' where username = '" + PulseSession.Instance.User.UserName.ToString() + "'" );

                }
                }
                if (PulseSession.Instance.User.IDSite == PulseWeb.Model.Site.ENOVATE_HQ_CUSTOMER_ID)
                {
                    this.modalPopup.Visible = true;
                    this.modalPopup.Modal = true;
                    this.modalPopup.CenterIfModal = true;
                }
                else
                {
                    RedirectToPage();
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.PopUpMessage("Login Failed: " + ex.Message);
            }
        }
   
        private void RedirectToPage()
        {
            if (Request.QueryString["redir"] != null)
            {
                string redir = HttpUtility.UrlDecode(Request.QueryString.ToString());
                redir = redir.Replace("redir=", "");
                Response.Redirect(redir);
            }
            else
            {
                Response.Redirect("~/FacilityGraphsNew.aspx");
            }

        }

        protected void BtnCreateNewUser(object sender, EventArgs e)
        {
            Response.Redirect("~/AddUser.aspx");
        }

        protected void OnClickLogin(object sender, EventArgs e)
        {
            try
           {
               if (txtPassword.Text.Length > 0 && txtUserName.Text.Length > 0)
               {
                   Login(this.txtUserName.Text, Crypto.EncryptString(txtPassword.Text));
               }
               else
               {
                   ErrorHandler.PopUpMessage("Please enter username and password.");
               }
            }
            catch (Exception ex)
            {
                ErrorHandler.PopUpMessage("Login Failed: " + ex.Message);
            }
            //"Joseph.Bernacki@wchn.org"

            //LabelLogin.Text = "Welcome back, " + (Login2.FindControl("UserName") as TextBox).Text;
        }

        protected void ddlSite_PreRender(object sender, EventArgs e)
        {
            RadComboBox combo = (sender as RadComboBox);
            if (combo != null)
            {
                combo.Items.Clear();
                string curFacility = api.SessionUser.IDSite.ToString();
                using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                {
                    //var facilities = enovateDB.GetAllCustomers();
                    var facilities = enovateDB.GetAllCustomersBySiteOrSfAcctOwnerID(PulseSession.Instance.User.SFAcctOwnerID,PulseSession.Instance.User.IDSite,  PulseSession.Instance.User);
                    foreach (Dictionary<string, string> cur in facilities)
                    {
                        string rowId = Utils.Misc.SafeGetKey(cur, "IDCustomer");
                        combo.Items.Add(new RadComboBoxItem { Text = Utils.Misc.SafeGetKey(cur, "CustomerName"), Value = rowId, Selected = rowId == curFacility ? true : false });
                    }
                }
                combo.Sort = RadComboBoxSort.Ascending;
                combo.SortItems();
            }

        }

        protected void OnSelectFacilityDoneClicked(object sender, EventArgs e)
        {
            try
            {
                if (this.ddlSite != null)
                {
                    if (this.ddlSite.SelectedValue != null)
                    {
                        api.SessionUser.IDSite = Convert.ToInt32(this.ddlSite.SelectedValue);
                        // This will prevent this user from saving themselves with a 'fake' buid
                        //api.SessionUser.IsAliasBUID = true;
                        this.modalPopup.Modal = false;
                        // re-initialize the user
                        PulseSession.Instance.InitializeSessionUser();

                        RedirectToPage();
                    }
                }
                else
                {
                    ErrorHandler.PopUpMessage("No Site or facility associated with this User, please contact Technical Services.");
                }
            }
            catch (Exception ex)
            {
                  ErrorHandler.PopUpMessage("No Site or facility associated with this User, please contact Technical Services.");
            }
        }

        protected void ddlSite_DataBinding(object sender, EventArgs e)
        {
            RadComboBox combo = (sender as RadComboBox);
            if (combo != null)
            {
                using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                {
                    combo.DataSource = enovateDB.GetAllCustomersBySiteOrSfAcctOwnerID(PulseSession.Instance.User.SFAcctOwnerID,PulseSession.Instance.User.IDSite,PulseSession.Instance.User);
                    combo.DataValueField = "IDCustomer";
                    combo.DataValueField = "CustomerName";
                }

            }
        }


        protected void ForgotPW_Click(object sender, ImageClickEventArgs e)
        {
            string t = string.Empty;
            string email = txtUserName.Text.ToLower().Replace("'", "''").Trim();
            if (email.Contains("@"))
            {
                using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                {
                    enovateDB.SendForgotPWEmail(email);
                }
            }
            else
            {
                ErrorHandler.PopUpMessage("Please enter a valid email address, then try again.");
            }
        }
    }
}