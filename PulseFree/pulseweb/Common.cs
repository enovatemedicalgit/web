﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;

namespace PulseWeb
{
    public class Common
    {
        public static string Proper(string val)
        {
            string newval = val.ToLower();
            if (newval.Length > 1)
            {
                newval = newval.Substring(0, 1).ToUpper() + newval.Substring(1, newval.Length - 1);
            }
            return newval;
        }

        public static string GetDataSourceName()
        {
            String ConnString = ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(ConnString);
            return conn.Database.ToString();
            //return conn.DataSource.ToString();
        }

        public static string PadQuote(string val)
        {
            return val.Replace("'", "''");
        }

        public static DateTime LocalToUTC(DateTime value)
        {
            int OffsetMinutes_Standard = Convert.ToInt16(HttpContext.Current.Request.Cookies["OffsetMinutes_Standard"].Value);
            int OffsetMinutes_DaylightSavings = Convert.ToInt16(HttpContext.Current.Request.Cookies["OffsetMinutes_DaylightSavings"].Value);
            int CurrentOffset;
            if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
            {
                CurrentOffset = OffsetMinutes_DaylightSavings;
            }
            else
            {
                CurrentOffset = OffsetMinutes_Standard;
            }

            if (CurrentOffset > 0)
            {
                CurrentOffset = 0 - CurrentOffset;
            }
            else
            {
                CurrentOffset = 0 + Convert.ToInt16(Math.Abs(CurrentOffset));
            }
            return value.AddMinutes(CurrentOffset);
        }

        public static DataTable GetDataTable(string query)
        {
            String ConnString = ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString;
            SqlConnection conn = new SqlConnection(ConnString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand(query, conn);

            DataTable myDataTable = new DataTable();

            conn.Open();
            try
            {
                adapter.Fill(myDataTable);
            }
            catch (Exception x)
            {

            }
            finally
            {
                conn.Close();
            }

            return myDataTable;
        }
 
        public static void PopUpMessage(string argMessage)
        {
            string startupScript = "<script>alert('" + argMessage + "');</script>";
            Page ThisPage = HttpContext.Current.Handler as Page;
            ScriptManager.RegisterStartupScript(ThisPage, ThisPage.GetType(), "startup", startupScript, false);
        }        

        public static DateTime LocalDateTime(DateTime value)
        {
            int OffsetMinutes_Standard = Convert.ToInt16(HttpContext.Current.Request.Cookies["OffsetMinutes_Standard"].Value ?? "0") ;
            int OffsetMinutes_DaylightSavings = Convert.ToInt16(HttpContext.Current.Request.Cookies["OffsetMinutes_DaylightSavings"].Value ?? "0");
            int CurrentOffset;
            if (value.Year < 2000)
            {
                return value;
            }
            if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
            {
                CurrentOffset = OffsetMinutes_DaylightSavings;
            }
            else
            {
                CurrentOffset = OffsetMinutes_Standard;
            }
            return value.AddMinutes(CurrentOffset);
        }
 
        public static string WrapInParensIfNotNull(string value)
        {
            if (value != null && value != string.Empty)
            {
                return "(" + value + ")";
            }
            else
            {
                return value;
            }
        }

        public static string HoursAndMinutes(string TotalMinutes)
        {
            if (TotalMinutes == "0" || TotalMinutes == string.Empty)
            {
                return "0:00";
            }
            else
            {
                int iTotalMinutes = Convert.ToInt32(TotalMinutes);
                int iHours = iTotalMinutes / 60;
                int iMinutes = iTotalMinutes % 60;
                string sMinutes = string.Format("{0:00}", iMinutes).ToString();
                return iHours.ToString() + ":" + sMinutes;
            }
        }

        public static int GetDefaultSearchDurationHours(bool IsStinger)
        {
            return IsStinger == true ? -24 : -24 * 4;
            //if (IsStinger == true)
            //{
            //    return -24;
            //}
            //else
            //{
            //    return -24 * 7;
            //}
        }

        public static Control FindControlRecursively(Control Root, string Id)
        {
            if (Root.ID == Id)
                return Root;

            foreach (Control Ctl in Root.Controls)
            {
                Control FoundCtl = FindControlRecursively(Ctl, Id);
                if (FoundCtl != null)
                    return FoundCtl;
            }

            return null;
        }

        public static string CookieOrElse(string CookieName, string DefaultValue)
        {
            return HttpContext.Current.Request.Cookies[CookieName] == null ? DefaultValue : HttpContext.Current.Request.Cookies[CookieName].Value;
        }

        public static string GetMeasuredVoltage(string MeasuredVoltage)
        {
            return MeasuredVoltage == string.Empty ? string.Empty : (Convert.ToDouble(MeasuredVoltage) * 0.001).ToString();
        }

        public static string GetPerformanceRatingByDevice(int FCC, string BattSN)
        {
            if (BattSN.Length < 7)
            {
                return "0%";
            }

            int ampHours = 0;
            string retval = string.Empty;
            string prefix = BattSN.Substring(0, 7);

            switch (prefix)
            {
                case "3118013": ampHours = 23760;
                    break;
                case "3118513": ampHours = 23760;
                    break;
                case "3118313": ampHours = 23760;
                    break;
                case "3118113": ampHours = 32000;
                    break;
                default:
                    ampHours = 23760;
                    break;
            }

            if (FCC > ampHours)
            {
                retval = "100%";
            }
            else if (FCC > 0)
            {
                retval = (Convert.ToDecimal(FCC) / ampHours).ToString("#0.##%");
            }
            else
            {
                retval = "0%";
            }

            return retval;
        }

        public static string GetOrCreateDepartmentID(string BusinessUnitID, string Description)
        {
            string retval = "0";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("spGetOrCreateDepartmentID", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("BusinessUnitID", BusinessUnitID);
                    cmd.Parameters.AddWithValue("Description", Description);
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        retval = dr["ROW_ID"].ToString();
                    }
                    dr = null;
                }
                finally
                {
                    conn.Close();
                }
                return retval;
            }
        }

        // Reallocates an array with a new size, and copies the contents
        // of the old array to the new array.
        // Arguments:
        //   oldArray  the old array, to be reallocated.
        //   newSize   the new array size.
        // Returns     A new array with the same contents.

        public static System.Array ResizeArray(System.Array oldArray, int newSize)
        {
            int oldSize = oldArray.Length;
            System.Type elementType = oldArray.GetType().GetElementType();
            System.Array newArray = System.Array.CreateInstance(elementType, newSize);
            int preserveLength = System.Math.Min(oldSize, newSize);
            if (preserveLength > 0)
                System.Array.Copy(oldArray, newArray, preserveLength);
            return newArray;

            /*
            Example:
            int[] a = { 1, 2, 3 };
            a = (int[])ResizeArray(a, 5);
            a[3] = 4;
            a[4] = 5;
            for (int i = 0; i < a.Length; i++)
                System.Console.WriteLine(a[i]);
            */
        }

     
    }
}