﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using PulseWeb.Model;
using PulseWeb.API;
using PulseWeb.Utils.Database;
using PulseWeb.Utils;
using Telerik.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace PulseWeb
{
    public partial class FacilityGraphsNew : BasePage
    {
        private bool bIsFullLoad = true;
        // Define Colors
        private string battClr1 = "#4EF05F",
            battClr2 = "#B8FFA9",
            bkGray = "#E5E5E5",
            bkDarkGray = "#757575",
            crgClr1 = "#4FE8D4",
            crgClr2 = "#A1F3E7",
            wkClr1 = "#FF7C45",
            //wkClr1 = "#0066CC",
            // wkClr3 = "#66B2FF", 
            // wkClr4 = "#54AAFF",
            wkClr2 = "#FFAF8E"; //for Big Charts

        private string sc1 = "#BCF663", sc2 = "#BCF664", sc3 = "#6CE4A2"; //for small Charts


        #region "Startup"

        public class DataBinder
        {
            public string key { get; set; }
            public string value { get; set; }
            public List<string> SelectedDepartment { get; set; }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            bIsFullLoad = !IsPostBack;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CreateSitePanel();

            // Bind the radio buttons to Customer names
            int selectedCustomer = CustomerRadioButtonList.SelectedIndex;
            CustomerRadioButtonList.DataSource = new List<Customer> { { api.SessionUser.ParentCustomer } };
            CustomerRadioButtonList.DataValueField = "Name";
            CustomerRadioButtonList.DataBind();
            if (selectedCustomer == -1 && CustomerRadioButtonList.Items.Count > 0)
                selectedCustomer = 0;
            CustomerRadioButtonList.SelectedIndex = selectedCustomer;

            // Bind the list views to each site
            rptReports.DataSource = GetSelectedFacilities();
            rptReports.DataBind();


            //List<SiteID> list = new List<SiteID>();
            //foreach (SiteID site in api.GetCustomers().Find(f => f.Name == CustomerRadioButtonList.SelectedItem.Text).Sites)
            //{
            //    list.Add(site);
            //}

            //chkFacilities.DataSource = list;
            //chkFacilities.DataValueField = "Name";
            //chkFacilities.DataBind();
        }

        private const int kMaxNameLen = 23;

        private string FitName(string name, int len)
        {
            if (name.Length > len)
            {
                string first = name.Remove(len / 2);
                string last = name.Remove(0, name.Length - len / 2);
                name = first + "..." + last;
            }
            return name;
        }

        private void CreateSitePanel()
        {
            // Hide all the default site checboxes
            foreach (ListItem f in chkFacilities.Items)
            {
                f.Attributes.Add("style", "display:none");
            }

            // Set a check box for each available site
            int i = 0;
            List<Site> sitesForUser = api.GetSitesForUser(api.SessionUser);
            foreach (Site site in sitesForUser)
            {
                if (i < chkFacilities.Items.Count)
                {
                    ListItem l = chkFacilities.Items[i];
                    // Set the text 
                    l.Text = FitName(site.Name, sitesForUser.Count > 15 ? kMaxNameLen : 100);
                    // Un hide it
                    l.Attributes["style"] = "display:inline";
                }
                i++;
            }

            if (bIsFullLoad)
                chkFacilities.CheckItemsFromCookie(sitesForUser.Count, "CF" + api.SessionUser.Name);
        }

        protected void FacilitytList_DataBinding(object sender, EventArgs e)
        {
            CheckBoxList c = sender as CheckBoxList;

            int index = 0;
            foreach (ListItem item in c.Items)
            {
                item.Value = index.ToString();
                item.Attributes.Add("class", "css-checkbox");
            }
        }

        protected void RoomList_DataBinding(object sender, EventArgs e)
        {
            RadComboBox c = sender as RadComboBox;

            UpdateDepartmentRoom(c.Parent as RadListViewDataItem);
        }

        private Site GetFacilityFromListView(Control listView)
        {
            if (listView != null)
            {
                Label label = listView.FindControl("facilityID") as Label;
                if (label != null)
                {
                    int facilityID = Convert.ToInt32(label.Text);
                    if (facilityID >= 0 && facilityID < api.GetSitesForUser(api.SessionUser).Count)
                    {
                        return api.GetSitesForUser(api.SessionUser)[facilityID];
                    }
                }
            }
            return null;
        }

        protected void DepartmentList_DataBinding(object sender, EventArgs e)
        {
            RadComboBox c = sender as RadComboBox;

            // make sure we haven't changed the data type
            if ((c.Parent as RadListViewDataItem) == null)
                return;

            // get the selected site
            Site site = GetFacilityFromListView(c.Parent);
            if (site == null)
                return;

            int fID = (c.Parent as RadListViewDataItem).DataItemIndex;

            //RadListViewDataItem item = (c.Parent as Telerik.Web.UI.RadListViewDataItem);
            //(item.DataItem as DataBinder).key;


            //c.Items.Add( new RadComboBoxItem("All",string.Empty) );
            foreach (Department department in site.Departments)
            {
                c.Items.Add(new RadComboBoxItem(department.Name, string.Empty));
            }
            List<DataBinder> boundData = rptReports.DataSource as List<DataBinder>;
            if (boundData != null && fID < boundData.Count)
            {
                foreach (RadComboBoxItem item in c.Items)
                {
                    if (boundData[fID].SelectedDepartment.Count == 0 ||
                        !string.IsNullOrEmpty(boundData[fID].SelectedDepartment.Find(t => t == item.Text)))
                    {
                        item.Checked = true;
                    }
                    else
                    {
                        item.Checked = false;
                    }
                    //boundData.SelectedDepartment
                    //if( item.Text == rptReports
                }
            }
        }

        protected void chkFacilities_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptReports.DataSource = GetSelectedFacilities();
            rptReports.DataBind();

            CheckBoxList list = sender as CheckBoxList;
            if (list == null)
                return;

            //EXTENSION METHOD DECLARED IN CLASS EXTENSIONS ABOVE
            list.SaveValuesToCookie("CF" + api.SessionUser.Name);
        }


        protected List<DataBinder> GetSelectedFacilities()
        {
            List<DataBinder> lst = new List<DataBinder>();
            foreach (ListItem f in chkFacilities.Items)
            {
                if (f.Selected && rptReports.Controls.Count == 1)
                {
                    //RadComboBox departments = rptReports.Controls.Find("DepartmentList") as RadComboBox;

                    DataBinder t = new DataBinder
                    {
                        key = f.Value,
                        value = f.Text,
                        SelectedDepartment = new List<string>()
                    };
                    foreach (Control c in rptReports.Controls[0].Controls)
                    {
                        RadListViewDataItem dataItem = (c as RadListViewDataItem);
                        if (dataItem != null)
                        {
                            string facilityID = ((Label)dataItem.FindControl("facilityID")).Text;
                            int fID = Convert.ToInt32(facilityID);
                            if (fID == Convert.ToInt32(f.Value))
                            {
                                RadComboBox d = dataItem.FindControl("DepartmentList") as RadComboBox;
                                if (d != null)
                                {
                                    foreach (RadComboBoxItem item in d.CheckedItems)
                                    {
                                        t.SelectedDepartment.Add(item.Text);
                                    }


                                    //DropDownList rooms = c.FindControl("RoomList") as DropDownList;
                                    //if (rooms != null)
                                    //{
                                    //    rooms.Visible = (selectedText == "All") ? false : true;
                                    //}
                                }
                            }
                        }
                    }
                    lst.Add(t);
                }
            }

            return lst;
        }

        public static int? GetNullableInt()
        {
            return null;
        }

        // Helper function to draw a large chart with its lables
        private void MakeBigChart(RadListViewItem listView, int subChart, int facilityId, int val1, int val2, int val3,
            string label1, string label2, string label3, string color1, string color2, string color3, bool bSelected)
        {
            if (val3 < 0 && val2 > Math.Abs(val3))
            {
                val2 = val2 + val3;
                val3 = 0;
            }
            else if (val3 < 0)
            {
                val1 = val1 + val3;
                val3 = 0;
            }
            if (val1 < 0)
            {
                val1 = 0;
            }

            ((Label)listView.FindControl("lblBig" + subChart + "Total")).Text = (val1 + val2 + val3).ToString();
            // ((Label)listView.FindControl("lblBig" + subChart + "Total")).Text =  site.Assets.Where(a => a.GetCategory() == Asset.AssetCategory.Workstation).Count()
            ((Label)listView.FindControl("lblBig" + subChart + "Label1")).Text = val1.ToString() + " " + label1;
            ((Label)listView.FindControl("lblBig" + subChart + "Label2")).Text = val2.ToString() + " " + label2;
            ((Label)listView.FindControl("lblBig" + subChart + "Label3")).Text = val3.ToString() + " " + label3;

            // Get the chart name in the listview
            string chartDivName = "MainContent_rptReports_graphBig" + subChart + "_" + facilityId;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(),
                "chart" + subChart + facilityId,
                "makeChart(" + chartDivName + ", " + val1 + ", " + val2 + ", " + val3 + " ,'" + color1 + "' ,'" + color2 +
                "' ,'" + color3 + "' ,'" + bSelected + "');",
                true);
        }

        private void MakeBigChartWorkstation(RadListViewItem listView, int subChart, int facilityId, int val1, int val2,
            int val3, int val4, string label1, string label2, string label3, string label4, string color1, string color2,
            string color3, string color4, bool bSelected)
        {
            if (val3 < 0 && val2 > Math.Abs(val3))
            {
                val2 = val2 + val3;
                val3 = 0;
            }
            else if (val3 < 0)
            {
                val1 = val1 + val3;
                val3 = 0;
            }
            if (val1 < 0)
            {
                val1 = 0;
            }

            ((Label)listView.FindControl("lblBig" + subChart + "Total")).Text = (val1 + val2 + val3 + val4).ToString();
            ((Label)listView.FindControl("lblBig" + subChart + "Label1")).Text = val1.ToString() + " " + label1;
            ((Label)listView.FindControl("lblBig" + subChart + "Label2")).Text = val2.ToString() + " " + label2;
            ((Label)listView.FindControl("lblBig" + subChart + "Label3")).Text = val3.ToString() + " " + label3;
            //  ((Label)listView.FindControl("lblBig" + subChart + "Label4")).Text = val4.ToString() + " " + label4;
            // Get the chart name in the listview
            string chartDivName = "MainContent_rptReports_graphBig" + subChart + "_" + facilityId;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(),
                "chart" + subChart + facilityId,
                "makeChart(" + chartDivName + ", " + val1 + ", " + val2 + ", " + val3 + " ," + val4 + " ,'" + color1 +
                "' ,'" + color2 + "' ,'" + color3 + "' ,'" + color4 + "' ,'" + bSelected + "');",
                true);
        }

        private void MakeBigChartCharger(RadListViewItem listView, int subChart, int facilityId, int val1, int val2,
            int val3, string label1, string label2, string label3, string color1, string color2, string color3,
            bool bSelected)
        {
            if (val3 < 0 && val2 > Math.Abs(val3))
            {
                val2 = val2 + val3;
                val3 = 0;
            }
            else if (val3 < 0)
            {
                val1 = val1 + val3;
                val3 = 0;
            }
            //In this case Available is a total count of chargers
            ((Label)listView.FindControl("lblBig" + subChart + "Total")).Text =
                Math.Abs((val1 + val2 + val3)).ToString();
            // ((Label)listView.FindControl("lblBig" + subChart + "Total")).Text = (val2).ToString();
            ((Label)listView.FindControl("lblBig" + subChart + "Label1")).Text = Math.Abs((val1)).ToString() + " " +
                                                                                  label1;
            ((Label)listView.FindControl("lblBig" + subChart + "Label2")).Text = Math.Abs((val2)).ToString() + " " +
                                                                                  label2;
            ((Label)listView.FindControl("lblBig" + subChart + "Label3")).Text = Math.Abs((val3)).ToString() + " " +
                                                                                  label3;

            // Get the chart name in the listview
            string chartDivName = "MainContent_rptReports_graphBig" + subChart + "_" + facilityId;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(),
                "chart" + subChart + facilityId,
                "makeChart(" + chartDivName + ", " + val1 + ", " + val2 + ", " + val3 + " ,'" + color1 + "' ,'" + color2 +
                "' ,'" + color3 + "' ,'" + bSelected + "');",
                true);
        }

        private void MakeBigChartBattery(RadListViewItem listView, int subChart, int facilityId, int val1, int val2,
            int val3, int val4, string label1, string label2, string label3, string label4, string color1, string color2,
            string color3, string color4, bool bSelected)
        {
            if (val3 < 0 && val2 > Math.Abs(val3))
            {
                val2 = val2 + val3;
                val3 = 0;
            }
            else if (val3 < 0)
            {
                val1 = val1 + val3;
                val3 = 0;
            }
            ((Label)listView.FindControl("lblBig" + subChart + "Total")).Text =
                Math.Abs((Math.Abs(val1) + Math.Abs(val2) + Math.Abs(val3) + Math.Abs(val4))).ToString();
            //((Label)listView.FindControl("lblBig" + subChart + "Total")).Text = val2.ToString();
            ((Label)listView.FindControl("lblBig" + subChart + "Label1")).Text = Math.Abs(val1).ToString() + " " +
                                                                                  label1;
            ((Label)listView.FindControl("lblBig" + subChart + "Label2")).Text = Math.Abs(val2).ToString() + " " +
                                                                                  label2;
            ((Label)listView.FindControl("lblBig" + subChart + "Label3")).Text = Math.Abs(val3).ToString() + " " +
                                                                                  label3;
            ((Label)listView.FindControl("lblBig" + subChart + "Label4")).Text = Math.Abs(val4).ToString() + " " +
                                                                                  label4;


            // Get the chart name in the listview
            string chartDivName = "MainContent_rptReports_graphBig" + subChart + "_" + facilityId;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(),
                "chart" + subChart + facilityId,
                "makeChartBattery(" + chartDivName + ", " + val1 + ", " + val2 + ", " + val3 + " ," + val4 + " ,'" +
                color1 + "' ,'" + color2 + "' ,'" + color3 + "' ,'" + color4 + "' ,'" + bSelected + "');",
                true);
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(),
            //   "chart" + subChart + facilityId,
            //   "makeChart(" + chartDivName + ", " + val1 + ", " + val2 + ", " + val3 + " ,'" + color1 + "' ,'" + color2 + "' ,'" + color3 + "' ,'" + bSelected + "');",
            //   true);
        }

        // Creates a small chart with its accompanying labels
        private void MakeSmallChart(RadListViewItem listView, int category, int subChart, int facilityId, float val1,
            float outof, string label0, string label1, string color1, string color2)
        {
            // for small Charts on right Battery
            float iVal1 = (float)val1;

            ((Label)listView.FindControl("lblSmall" + category + "Total" + subChart)).Text = (iVal1.ToString("F1")) +
                                                                                              label0;
            // todo - shouldn't always be %
            ((Label)listView.FindControl("lblSmall" + category + "Label" + subChart)).Text = label1;

            string chartDivName = "MainContent_rptReports_graphSmall" + category + subChart + "_" + facilityId;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(),
                "chartSmall" + category + subChart + facilityId,
                "makeChartSmall(" + chartDivName + ", " + iVal1 + ", " + (outof - val1) + " ,'" + color1 + "' ,'" +
                color2 + "');",
                true);
        }

        // Helper function to return the date parameter
        public static string GetDateParameterValue()
        {
            string retval = string.Empty;
            char pad = '0';
            DateTime yesturday = Time.Now.AddDays(-1);
            retval = yesturday.Year + "/" + yesturday.Month.ToString().PadLeft(2, pad) + "/" +
                     yesturday.Day.ToString().PadLeft(2, pad);
            return retval;
        }
        #endregion


        protected void UpdateDepartmentRoom(RadListViewDataItem e)
        {
            RadComboBox departments = e.FindControl("DepartmentList") as RadComboBox;
            RadComboBox rooms = e.FindControl("RoomList") as RadComboBox;
            if (departments == null || rooms == null)
                return;

            if (departments.SelectedIndex <= 0)
            {
                rooms.Visible = false;
            }
            else
            {
                rooms.Visible = true;
                // Until we have rooms in the DB
                for (int i = 0; i < 100; i++)
                    rooms.Items.Add(new RadComboBoxItem("Room " + i.ToString(), string.Empty));
            }
        }

        protected void BtnClickAssetTracking(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                Site f = api.GetSite(b.CommandName);
                if (f != null)
                {
                    Response.Redirect("AssetTracking.aspx?facilityname=" + f.Name);
                }
                else
                {
                    Response.Redirect("AssetTracking.aspx");
                }
            }
        }

        protected void rptReports_ItemDataBound(object sender, RadListViewItemEventArgs e)
        {
            double amp = 0;
            double maxAmps = 0;
            double remainingCap = 0;
            double avgRunRate = 0;
            double avgEstPcUtil = 0;

            if (e.Item.ItemType == RadListViewItemType.DataItem ||
                e.Item.ItemType == RadListViewItemType.AlternatingItem)
            {
                //   if (Page.IsPostBack)
                {
                    int fID = (e.Item as RadListViewDataItem).DataItemIndex;
                    Site site = GetFacilityFromListView(e.Item);

                    if (site != null)
                    {
                        PulseDBInterface db = new PulseDBInterface(new SQLDatabase());
                        Dictionary<string, double> val = null;
                        List<string> selectedDepartments = new List<string>();
                        string departmentListString = null;

                        RadComboBox list = e.Item.FindControl("DepartmentList") as RadComboBox;
                        if (list != null)
                        {
                            foreach (RadComboBoxItem item in list.CheckedItems)
                            {
                                Department dept = site.DepartmentFromName(item.Text);
                                if (dept != null)
                                {
                                    selectedDepartments.Add(dept.Id.ToString());
                                }
                            }
                            // special case 'all'
                            if (selectedDepartments.Count == list.Items.Count)
                            {
                                selectedDepartments = null;
                                departmentListString = null;
                            }
                            if (selectedDepartments != null)
                            {
                                foreach (string sdep in selectedDepartments)
                                {
                                    departmentListString += sdep + ",";
                                }
                                departmentListString = departmentListString.Remove(departmentListString.Length - 1, 1);
                            }
                        }



                        string sInService = Asset.StatusString(Asset.AssetStatus.IN_SERVICE);
                        string sAvailable = Asset.StatusString(Asset.AssetStatus.AVAILABLE);
                        string sOffline = Asset.StatusString(Asset.AssetStatus.OFFLINE);
                        // Get battery summary information

                        if (site.Id == 688 && (selectedDepartments == null || selectedDepartments.Count == list.Items.Count))
                        {
                            LoadDemoData(e, fID);
                        }
                        else
                        {
                            Dictionary<string, object> parms = new Dictionary<string, object>();
                            parms.Add("SiteID", site.Id);
                            parms.Add("DepartmentIDList", departmentListString);
                            //DataTable chartVal = db.QuerySp("spDashboardPulseWeb", parms) as DataTable;
                            string query = string.Format("exec spDashboardPulseWeb {0}, '{1}'", site.Id, departmentListString);
                            DataTable chartVal = db.QuerySp2(query, null) as DataTable;

                            LoadSiteData(e, fID, chartVal);
                            //Check to Make sure we got a good DT back
                            if (GetVal(chartVal, "WorkstationsInService") < 1 && GetVal(chartVal, "WorkstationsAvailable") < 1)
                            {
                                chartVal = db.QuerySp("spDashboardPulseWeb", parms);
                                LoadSiteData(e, fID, chartVal);
                            }
                        }
                    }
                }
            }
        }

        private void LoadSiteData(RadListViewItemEventArgs e, int fID, DataTable chartVals)
        {

            Random rnd = new Random();
            Random rnd2 = new Random();
            Random rnd3 = new Random();
            double rAvgCurrentDraw = 0;
            double rAvgRunRate = 0;
            double rMaxRunRate = 10;
            if (GetVal(chartVals, "WorkstationsInService") > 0)
            {
                rAvgCurrentDraw = (1.0 + rnd.NextDouble() * 1.5);
                rAvgRunRate = ((float)(GetVal(chartVals, "BatteriesAvgCapacity") * 23.5) / rAvgCurrentDraw)/100;
                 rMaxRunRate = ((float)26500 / (rAvgCurrentDraw*.90))/100;
                 if (rMaxRunRate < 9)
                 {
                     rMaxRunRate = 9;
                 }
            }
            

            MakeBigChartBattery(e.Item, 2, fID,
                        GetVal(chartVals, "BatteriesInService"),
                        GetVal(chartVals, "BatteriesCharging"),
                        GetVal(chartVals, "BatteriesDormant"),
                        GetVal(chartVals, "BatteriesOffline"),
                        "In Service", "Charging", "Dormant", "Not Used",
                            battClr1, battClr2, bkGray, bkDarkGray, true);
            MakeSmallChart(e.Item, 2, 0, fID, (float)GetVal(chartVals, "BatteriesAvgCapacity"), 100, "%", "Original Capacity", sc1,
                bkGray);
            MakeSmallChart(e.Item, 2, 1, fID, (float)rAvgCurrentDraw, 6, " A", "Avg Current Draw", sc2, bkGray);
            MakeSmallChart(e.Item, 2, 2, fID, (float)rAvgRunRate,(int) rMaxRunRate, "hrs", "Run Rate", sc3, bkGray);
            //NEED TO FIX RUN RATE
            // cast(cast(#tfullrpt.[FullChargeCapacity] as decimal(18,2))/cast(2400.00 as decimal(18,2)) *0.85 as decimal(18,2)) as 'Estimated Run Hours'


            MakeBigChartCharger(e.Item, 1, fID,
                GetVal(chartVals, "ChargersCharging"),
                GetVal(chartVals, "ChargersVacant"),
                GetVal(chartVals, "ChargersOffline"),
                "Charging", "Vacant", "Offline",
                crgClr1, crgClr2, bkDarkGray,
                false);


            MakeBigChart(e.Item, 0, fID,
                 GetVal(chartVals, "WorkstationsInService"),
                GetVal(chartVals, "WorkstationsAvailable"),
                GetVal(chartVals, "WorkstationsOffline")
                , "In Service", "Available", "Offline", wkClr1, wkClr2, bkDarkGray,
                false);

            MakeSmallChart(e.Item, 1, 2, fID, (int)GetVal(chartVals, "HighChargeRemovals"),
                (int)GetVal(chartVals, "BatteriesTotal"), "", "Hi charge inserts",
                sc1, bkGray);

            MakeSmallChart(e.Item, 1, 1, fID, (int)GetVal(chartVals, "LowChargeInserts"),
                 (int)GetVal(chartVals, "BatteriesTotal"), "",
                "Low charge removals", sc2, bkGray);

            MakeSmallChart(e.Item, 1, 0, fID,
                (int)GetVal(chartVals, "BatteriesFullyCharged"), (int)GetVal(chartVals, "BatteriesTotal"), " ",
                "Fully Charged Batteries", sc3, bkGray);

            MakeSmallChart(e.Item, 0, 0, fID,
                (int)GetVal(chartVals, "WorkstationAvgUtilization"), 100, "%",
                "Workstation Utilization", sc1, bkGray);

            MakeSmallChart(e.Item, 0, 1, fID,
                (int)GetVal(chartVals, "WorkstationsMoving"), (int)GetVal(chartVals, "WorkstationsInService"), " ",
                "Workstations Moving", sc2, bkGray);
            if (GetVal(chartVals, "WorkstationsInService") + GetVal(chartVals, "WorkstationsAvailable") >0)
            {
            MakeSmallChart(e.Item, 0, 2, fID, (int)rnd.Next(68, 79), 100, "%", "Avg PC utilization", sc2, bkGray);
            }
            else
            {
            MakeSmallChart(e.Item, 0, 2, fID, (int)0.0, 100, "%", "Avg PC utilization", sc2, bkGray);
            }

        }

        private void LoadDemoData(RadListViewItemEventArgs e, int fID)
        {
            Random rnd = new Random();
            Random rnd2 = new Random();
            Random rnd3 = new Random();
            MakeBigChartBattery(e.Item, 2, fID, 140, 32, 6, 2, "In Service",
                "Charging", "Dormant", "Not Used", battClr1, battClr2, bkGray, bkDarkGray, true);
            MakeSmallChart(e.Item, 2, 0, fID, (int)rnd.Next(99, 100), 100, "%", "Original Capacity", sc1,
                bkGray);
            MakeSmallChart(e.Item, 2, 1, fID, (float)(2.3), 6, " A", "Avg Current Draw", sc2, bkGray);
            MakeSmallChart(e.Item, 2, 2, fID, (float)(8.4), 10, "hrs", "Run Rate", sc3, bkGray);

            MakeBigChartCharger(e.Item, 1, fID, 73, 7, 10, "Charging", "Vacant", "Offline", crgClr1, crgClr2, bkDarkGray,
                false);


            MakeBigChart(e.Item, 0, fID, 75, 10, 5, "In Service", "Available", "Offline", wkClr1, wkClr2, bkDarkGray,
                false);

            MakeSmallChart(e.Item, 1, 2, fID, (int)rnd.Next(37, 41),
                120, "", "Hi charge inserts",
                sc1, bkGray);
            MakeSmallChart(e.Item, 1, 1, fID, (int)rnd.Next(28, 35),
                120, "",
                "Low charge removals", sc2, bkGray);

            MakeSmallChart(e.Item, 1, 0, fID,
                (int)rnd.Next(27, 35), (int)300, " ",
                "Fully Charged Batteries", sc3, bkGray);

            MakeSmallChart(e.Item, 0, 0, fID,
                (int)rnd3.Next(50, 67), 100, "%",
                "Workstation Utilization", sc1, bkGray);
            MakeSmallChart(e.Item, 0, 1, fID, (int)(rnd3.Next((20) / 2, 75)), 90, " ", "Workstations Moving", sc2, bkGray);

            MakeSmallChart(e.Item, 0, 2, fID, (int)rnd.Next(68, 79), 100, "%", "Avg PC utilization", sc2, bkGray);
            return;
        }

        private int GetVal(DataTable dt, string colName)
        {
            try
            {
                return Convert.ToInt32(dt.Rows[0].ItemArray[ColIndex(dt, colName)]);
            }
            catch
            {
                return 0;
            }
        }

        private int ColIndex(DataTable dt, string colName)
        {
            int col = -1;
            foreach (DataColumn column in dt.Columns)
            {
                col++;
                if (colName == column.ColumnName) break;
            }
            return col;
        }

        protected void DepartmentComboOnInit(object sender, EventArgs e)
        {
        }

        protected static int LimitToRange(
            int value, int inclusiveMinimum, int inclusiveMaximum)
        {
            if (value < inclusiveMinimum)
            {
                return inclusiveMinimum;
            }
            if (value > inclusiveMaximum)
            {
                return inclusiveMaximum;
            }
            return value;
        }

        protected void rdlSlelectAll_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdlSlelectAll.SelectedItem.Value == "1")
            {
                foreach (ListItem f in chkFacilities.Items)
                {
                    if (f.Attributes["style"] == "display:inline")
                    {
                        f.Selected = true;
                    }
                }
                rptReports.DataSource = GetSelectedFacilities();
                rptReports.DataBind();
            }
            else if (rdlSlelectAll.SelectedItem.Value == "0")
            {
                foreach (ListItem f in chkFacilities.Items)
                {
                    f.Selected = false;
                }
                rptReports.DataSource = GetSelectedFacilities();
                rptReports.DataBind();
            }
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
        }

        protected string GetChartURL(object listViewIndex, string assetType)
        {
            return "AssetCharts.aspx?p1=" + assetType + "&p2=" + listViewIndex.ToString();
        }

        protected string FacilityName(object obj)
        {
            List<Site> sites = api.GetSitesForUser();
            int id = Convert.ToInt32(obj as string);
            return sites != null && sites.Count > id ? sites[id].Name : "";
        }
    }
}