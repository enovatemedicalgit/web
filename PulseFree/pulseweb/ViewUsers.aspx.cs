﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using PulseWeb.API;
using PulseWeb.Model;
using PulseWeb.Utils;

namespace PulseWeb
{

    public partial class ViewUsers : BasePage
    {
        // Only able to edit the following:
        // Asset#
        // Department
        // No longer in use check
        //                    cmdText = string.Format("update Device set SerialNumber = '{1}', InvoiceNumber = '{2}', PartNumber = '{3}', Description = '{4}', IDSite = {5}, DeviceTypeID = {6}, AssetNumber = '{7}', BusinessUnitDepartmentID = {8}, Notes = '{9}', StingerNotes = '{10}', ModifiedUserID = {11}, ModifiedDateUTC = getutcdate(), Model = '{12}', Retired = {13} where ROW_ID = {0}", ROW_ID, SerialNumber, InvoiceNumber, PartNumber, Description, IDSite, DeviceTypeID, AssetNumber, DepartmentID, Notes, StingerNotes, CASTUserID, Model, Retired);
        PulseUser mUserBeingEdited = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PulseSession.Instance.SelectedSite = PulseWeb.API.api.SessionUser.Sites[0];
            RadGrid1.DataSource = null;
            RadGrid1.Rebind();
            }
            // MDJ TODO fix this
            

        }

        protected void RadGrid1_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = api.GetCoUsers().Select(p => new
            {
                FirstName = p.FirstName,
                LastName = p.LastName,
                Username = p.UserName,
                PrimaryPhone = p.Phone1,
                SecondaryPhone = p.Phone2,
                EMail = p.Email,
                Password = p.EncryptedPassword,
                Title = p.Title,
                    POCEmail = p.POCEmail,
                AccessRights = p.AccessRights,
                Site = p.IsHQUser ? p.ParentCustomer.Name : api.GetSite(p.IDSite).Name,
            } );
        }


        protected void Button1_Click(object sender, EventArgs e)
        
        {
            RadGrid1.MasterTableView.ExportToCSV();
        }

        protected void RadGrid1_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            GridBoundColumn boundColumn = e.Column as GridBoundColumn;
            if (boundColumn != null )
            {
                boundColumn.ReadOnly = false;
                switch (e.Column.UniqueName)
                {
                    case "POCEmail": e.Column.HeaderText = "Site/IDN POC"; break;
                    case "EMail": e.Column.Visible = false; break;
                    case "SecondaryPhone": e.Column.Visible = false; break;
                    default: break;
                }
            }
        }


        Control FindControlRecursive(Control parent, string name)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.ID == name)
                    return c;
                Control ctl = FindControl(c, name);
                if (ctl != null)
                    return ctl;
            }
            return null;

        }
        protected void rgrdParent_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch(e.CommandName)
            {
                case "PerformCancel":
                {
                    RadGrid rg = (sender as RadGrid);
                    rg.MasterTableView.IsItemInserted = false;
                    rg.MasterTableView.ClearEditItems();    // this line required for closing when editing an existing item
                    rg.MasterTableView.Rebind();

                }
                break;
                case "Edit":
                {

                    int index = e.Item.ItemIndex;
                    if (index < e.Item.OwnerTableView.DataKeyValues.Count)
                    {
                        var rowId = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"];
                        mUserBeingEdited = api.GetCoUsers().Find(a => a.UserName == (string)rowId);
                    }

                }
                break;
               case "PerformDelete":
                {

                    int index = e.Item.ItemIndex;
                    if (index < e.Item.OwnerTableView.DataKeyValues.Count)
                    {
                        var username = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"];
                         api.DeleteCoUser((string)username);
                    }
                      RadGrid rg = (sender as RadGrid);
                    rg.MasterTableView.IsItemInserted = false;
                    rg.MasterTableView.ClearEditItems();    // this line required for closing when editing an existing item
                    rg.MasterTableView.Rebind();
                       PulseSession.Instance.User.CoUsers = null;        
                api.GetCoUsers();
                }
                break;
                case   "InitInsert":
            //  UserDetails userControl = e.Item.FindControl(GridEditFormItem.EditFormUserControlID) as UserDetails;
                 Response.Redirect("AddUser.aspx");
              //if (userControl != null)
              //{
              //    if (userControl.ValidateUpdateUserInfo(true))
              //    {
              //        PulseUser user = new PulseUser();
              //        userControl.UpdateUserFromForm(user);

              //        using (PulseDBInterface enovateDB = new PulseDBInterface())
              //        {
              //            enovateDB.AddNewUser(user);
              //            api.GetCoUsers().Add(user);
              //        }
              //    }
              //}
              break;
                case "Import":
                {
                    break;
                }
                case "RebindGrid":
                {
                        PulseSession.Instance.User.CoUsers = null;        
                api.GetCoUsers();
                    break;
                }
            }
        }

        protected void SiteCombo_PreRender(object sender, EventArgs e)
        {
            RadComboBox drop = (sender as RadComboBox);
            if( drop != null )
            {
                foreach( Site site in PulseWeb.API.api.SessionUser.Sites )
                {
                    drop.Items.Add(new RadComboBoxItem { Text = site.Name, Value = site.Id.ToString(), Selected = site == PulseSession.Instance.SelectedSite ? true : false });
                }
            }

        }

        

        protected void rgrdParent_UpdateCommand(object source, GridCommandEventArgs e)
        {

            UserDetails userControl = e.Item.FindControl(GridEditFormItem.EditFormUserControlID) as UserDetails;

            if (userControl != null)
            {
                int index = e.Item.ItemIndex;
                // Is this an update?
                if (index >= 0 && index < e.Item.OwnerTableView.DataKeyValues.Count)
                {
                    if (userControl.ValidateUpdateUserInfo(false) )
                    {
                        var rowId = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"];
                        PulseUser user = api.GetCoUsers().Find(a => a.UserName == (string)rowId);
                       
                      user =  userControl.UpdateUserFromForm(user);
               

                        using (PulseDBInterface enovateDB = new PulseDBInterface())
                        {
                            enovateDB.UpdateExistingUser(user);
                        }

                    }
                }
                e.Item.OwnerTableView.ClearEditItems();
                e.Item.OwnerTableView.Rebind();
            }
            //Wizard wiz = (item.FindControl("Wizard1") as FormTemplate);
            //RadComboBox rcboDept = wiz.FindControl("rcboDept") as RadComboBox;
            //CheckBox chkRetired = wiz.FindControl("chkRetired") as CheckBox;
            //string AssetNumber = (wiz.FindControl("txtAssetNumber") as TextBox).Text;

            //cmdText = string.Format("update Device set SerialNumber = '{1}', InvoiceNumber = '{2}', PartNumber = '{3}', Description = '{4}', IDSite = {5}, DeviceTypeID = {6}, AssetNumber = '{7}', BusinessUnitDepartmentID = {8}, Notes = '{9}', StingerNotes = '{10}', ModifiedUserID = {11}, ModifiedDateUTC = getutcdate(), Model = '{12}', Retired = {13} where ROW_ID = {0}", ROW_ID, SerialNumber, InvoiceNumber, PartNumber, Description, IDSite, DeviceTypeID, AssetNumber, DepartmentID, Notes, StingerNotes, CASTUserID, Model, Retired);

        }
          private string GetControlText(GridItem item, string ctlName)
        {
            TextBox tb = item.FindControl(ctlName) as TextBox;
            return (tb != null) ? tb.Text :  "";
        }
        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            try
            {
                this.RadGrid1.MasterTableView.GetColumn("AccessRights").Visible = false;
                this.RadGrid1.MasterTableView.GetColumn("Password").Visible = false;
            }
            catch (Exception ex)
            {

            }
            // Hid edit/add buttons if the user doesn't have proper priveledges
            if (!PulseWeb.API.api.SessionUser.HasRight(UserAccessRights.CAN_MAINTAIN_USERS) && PulseWeb.API.api.SessionUser.UserName != "arlow.farrell@enovatemedical.com" && PulseWeb.API.api.SessionUser.UserName != "sales.demo@enovatemedical.com" && PulseWeb.API.api.SessionUser.SFAcctOwnerID.Length < 3 && PulseWeb.API.api.SessionUser.UserTypeID != 3) 
            {
                Control addBtn = this.FindControlRecursive(this.RadGrid1, "LinkButton2");
                if( addBtn != null)
                {
                    addBtn.Visible = false;
                   // this.RadGrid1.MasterTableView.GetColumnSafe("linkbuttonedit1").Visible = false;
                //    this.RadGrid1.MasterTableView.GetColumnSafe("EditCommandColumn1").Visible = false;
                    this.RadGrid1.MasterTableView.Columns[1].Visible = false;
                }

                foreach (GridDataItem dataItem in RadGrid1.Items)
                {
                    Control ctl = dataItem.FindControl("linkbuttonedit1");
                    if (ctl != null && ctl.Parent != null)
                    {
                        ctl.Parent.Visible = false;
                    }
                }
            }


        }

        struct AccessRightsStruct
        {
            public string FieldName { get; set; }
            public string FieldValue { get; set; }
        };

		protected void RadGrid1_InsertCommand(object source, GridCommandEventArgs e)
        {
                // Is this a new user?
            UserDetails userControl = e.Item.FindControl(GridEditFormItem.EditFormUserControlID) as UserDetails;

            if (userControl != null)
            {
                if (userControl.ValidateUpdateUserInfo(true) )
                {
                    PulseUser user = new PulseUser();
                    userControl.UpdateUserFromForm(user);

                    using (PulseDBInterface enovateDB = new PulseDBInterface())
                    {
                        enovateDB.AddNewUser(user);
                        api.GetCoUsers().Add(user);
                    }

                }
                else 
                {
                    e.Canceled = true;
                }
            }
        }

        protected void rgrdUsers_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditableItem && e.Item.IsInEditMode )
            {
                UserDetails userControl = e.Item.FindControl(GridEditFormItem.EditFormUserControlID) as UserDetails;
                if (userControl != null && e.Item.ItemIndex >= 0 && e.Item.ItemIndex < e.Item.OwnerTableView.DataKeyValues.Count)
                {
                    var rowId = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["UserName"];
                    mUserBeingEdited = api.GetCoUsers().Find(a => a.UserName == (string)rowId);
                    userControl.EditUser = mUserBeingEdited;
                }

                List<AccessRightsStruct> items = new List<AccessRightsStruct>();


                items.Add(new AccessRightsStruct { FieldName = UserAccessRights.CAN_LOGIN_TO_CAST.ToString(), FieldValue = "Log into Pulse" });
                items.Add(new AccessRightsStruct { FieldName = UserAccessRights.CAN_MAINTAIN_DEVICES.ToString(), FieldValue = "Maintain devices and facilities" });
                items.Add(new AccessRightsStruct { FieldName = UserAccessRights.CAN_MAINTAIN_USERS.ToString(), FieldValue = "Maintain users" });
                //items.Add(new AccessRightsStruct { FieldName = UserAccessRights.CAN_MAINTAIN_FACILITY_NETWORKS.ToString(), FieldValue = "Maintain SiteID Networks" });



                CheckBoxList cbl = userControl.FindControl("UserOptionsCheck") as CheckBoxList;

                cbl.DataValueField = "FieldName";
                cbl.DataTextField = "FieldValue";
                cbl.DataSource = items;
                cbl.DataBind();
            }
        }


    }
}   