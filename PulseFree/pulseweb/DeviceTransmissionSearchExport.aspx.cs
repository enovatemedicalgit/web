﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Data;
using PulseWeb.SearchObjects;

namespace PulseWeb
{
    public partial class DeviceTransmissionSearchExport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        void LoadData()
        {
            PulseSearch search = new PulseSearch();
            var msg = "";

            try
            {
                search = (PulseSearch)Session["PulseSearchInfo"];
            }
            catch { }

            if (string.IsNullOrEmpty(search.SqlCmdText))
            {
                msg = "Search data was missing. Please close this window and try your search again before attempting download.";
            }
            else
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = new SqlCommand(search.SqlCmdText, conn);
                    DataTable dt = new DataTable();

                    try
                    {
                        conn.Open();
                        da.Fill(dt);

                        formatSearchData(ref dt, search.DisplayType);
                        formatDisplayGrid(search);
                        grdDisplay.DataSource = dt;
                        grdDisplay.DataBind();
                    }
                    catch (Exception ex)
                    {
                        msg = string.Format("Search failed with the following error: {0}.", ex.ToString());
                    }
                }
            }


            if (msg.Length > 0)
            {
                grdDisplay.Visible = false;
                divDisplay.Visible = false;

                divError.InnerText = msg;
                divError.Visible = true;
            }
            else
            {
                grdDisplay.Visible = true;
                divDisplay.Visible = true;

                divError.InnerText = "";
                divError.Visible = false;

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.BufferOutput = true;

                var fn = string.Format("Pulse_SearchResults_{0}.xls", DateTime.UtcNow.ToString("yyyyMMddHHmm"));
                Response.AddHeader("Content-Disposition", string.Format("inline; filename={0}", fn));

                Response.ContentType = "application/vnd.ms-excel";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                Response.Charset = "";
                this.EnableViewState = false;

                var sw = new System.IO.StringWriter();
                var hw = new System.Web.UI.HtmlTextWriter(sw);

                grdDisplay.RenderControl(hw);

                Response.Write(@"<style> td { mso-number-format:\@; } </style> ");
                Response.Write(sw.ToString());
                Response.End();

            }
        }

        public void formatSearchData(ref DataTable dt, DisplayType dType)
        {


            if (dType == DisplayType.Session)
            {
                dt.Columns.Add(new DataColumn("SessLen", typeof(string)));

                foreach (DataRow dr in dt.Rows)
                {
                    var st = dr["StartDateUTC"].ToString();
                    dr["StartDateUTC"] = Common.LocalDateTime(Convert.ToDateTime(st)).ToString();
                    var et = dr["EndDateUTC"].ToString();
                    dr["EndDateUTC"] = Common.LocalDateTime(Convert.ToDateTime(et)).ToString();
                    var hm = dr["SessionLengthMinutes"].ToString();
                    dr["SessLen"] = Common.HoursAndMinutes(hm).ToString();
                }
            }
            else
            {
                dt.Columns.Add(new DataColumn("Cap", typeof(string)));

                foreach (DataRow dr in dt.Rows)
                {
                    var cd = dr["CreatedDateUTC"].ToString();
                    dr["CreatedDateUTC"] = Common.LocalDateTime(Convert.ToDateTime(cd)).ToString();
                    var ed = dr["ExaminedDateUTC"].ToString();
                    if (String.IsNullOrEmpty(ed) == true)
                    {
                        ed = DateTime.MinValue.Date.ToString();
                    }

                    dr["ExaminedDateUTC"] = Common.LocalDateTime(Convert.ToDateTime(ed)).ToString();

                    var cap = dr["FullChargeCapacity"].ToString();
                    var battSN = dr["BatterySerialNumber"].ToString();
                    dr["Cap"] = Common.GetPerformanceRatingByDevice(Convert.ToInt32(cap), battSN).ToString();

                    var mv = dr["MeasuredVoltage"].ToString();
                    dr["MeasuredVoltage"] = Common.GetMeasuredVoltage(mv).ToString();


                    var v = string.IsNullOrEmpty(dr["Voltage"].ToString()) ? "0" : dr["Voltage"].ToString();
                    dr["Voltage"] = Convert.ToDouble(v) * 0.001;

                    var a = string.IsNullOrEmpty(dr["Amps"].ToString()) ? "0" : dr["Amps"].ToString();
                    dr["Amps"] = Convert.ToDouble(a) * 0.001;

                    var vc1 = string.IsNullOrEmpty(dr["VoltageCell1"].ToString()) ? "0" : dr["VoltageCell1"].ToString();
                    dr["VoltageCell1"] = Convert.ToDouble(vc1) * 0.001;

                    var vc2 = string.IsNullOrEmpty(dr["VoltageCell2"].ToString()) ? "0" : dr["VoltageCell2"].ToString();
                    dr["VoltageCell2"] = Convert.ToDouble(vc2) * 0.001;

                    var vc3 = string.IsNullOrEmpty(dr["VoltageCell3"].ToString()) ? "0" : dr["VoltageCell3"].ToString();
                    dr["VoltageCell3"] = Convert.ToDouble(vc3) * 0.001;

                    var dc1bv = string.IsNullOrEmpty(dr["DCUnit1BVolts"].ToString()) ? "0" : dr["DCUnit1BVolts"].ToString();
                    dr["DCUnit1BVolts"] = Convert.ToDouble(dc1bv) * 0.001;

                    var dc1bc = string.IsNullOrEmpty(dr["DCUnit1BCurrent"].ToString()) ? "0" : dr["DCUnit1BCurrent"].ToString();
                    dr["DCUnit1BCurrent"] = Convert.ToDouble(dc1bc) * 0.001;

                    var dc1av = string.IsNullOrEmpty(dr["DCUnit1AVolts"].ToString()) ? "0" : dr["DCUnit1AVolts"].ToString();
                    dr["DCUnit1AVolts"] = Convert.ToDouble(dc1av) * 0.001;

                    var dc1ac = string.IsNullOrEmpty(dr["DCUnit1ACurrent"].ToString()) ? "0" : dr["DCUnit1ACurrent"].ToString();
                    dr["DCUnit1ACurrent"] = Convert.ToDouble(dc1ac) * 0.001;

                    var dc2bv = string.IsNullOrEmpty(dr["DCUnit2BVolts"].ToString()) ? "0" : dr["DCUnit2BVolts"].ToString();
                    dr["DCUnit2BVolts"] = Convert.ToDouble(dc2bv) * 0.001;

                    var dc2bc = string.IsNullOrEmpty(dr["DCUnit2BCurrent"].ToString()) ? "0" : dr["DCUnit2BCurrent"].ToString();
                    dr["DCUnit2BCurrent"] = Convert.ToDouble(dc2bc) * 0.001;

                    var dc2av = string.IsNullOrEmpty(dr["DCUnit2AVolts"].ToString()) ? "0" : dr["DCUnit2AVolts"].ToString();
                    dr["DCUnit2AVolts"] = Convert.ToDouble(dc2av) * 0.001;

                    var dc2ac = string.IsNullOrEmpty(dr["DCUnit2ACurrent"].ToString()) ? "0" : dr["DCUnit2ACurrent"].ToString();
                    dr["DCUnit2ACurrent"] = Convert.ToDouble(dc2ac) * 0.001;
                }
            }
        }

        public void formatDisplayGrid(PulseSearch search)
        {
            GridView grid = grdDisplay;
            if (search != null && search.Columns.Count > 0)
            {
                grid.AutoGenerateColumns = false;

                var cols = search.Columns.Select(x => x).OrderBy(x => x.DisplayOrder);
                foreach (var col in cols)
                {
                    BoundField field = new BoundField();
                    field.DataField = col.DataName;
                    field.HeaderText = col.DisplayName;
                    grid.Columns.Add(field);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
    }
}