﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewAssets.aspx.cs" Inherits="PulseWeb.ViewAssets" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/viewAssets.css" rel="stylesheet" type="text/css" />
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="MainContent">

     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="false" />
    <telerik:RadFormDecorator runat="server" DecorationZoneID="demo" EnableRoundedCorners="true" DecoratedControls="All" />
    <div id="view-assets" class="view-assets-container no-bg">
        <asp:Label runat="server" Font-Size="Large" ID="AssetTypeLabel" ForeColor="#333333">&nbsp;&nbsp;Workstations</asp:Label>
        <br />
        <div id="demo" class="demo-container no-bg" style="zoom: 90%">
            <telerik:RadGrid ID="RadGrid1" runat="server"
                Skin="Metro" CssClass="RadGrid_Rounded"
                OnNeedDataSource="RadGrid1_NeedDataSource"
                AllowPaging="True"
                MasterTableView-AllowSorting="true"
                AllowAutomaticUpdates="True"
                AllowAutomaticDeletes="True"
                AllowSorting="True"
                GroupPanelPosition="Top"
                GroupingSettings-CaseSensitive="false"
                PageSize="80"
                ClientSettings-Resizing-AllowColumnResize="true"
                ClientSettings-AllowColumnsReorder="true"
                ClientSettings-Resizing-ResizeGridOnColumnResize="true"
                OnItemCommand="rgrdParent_ItemCommand"
                OnUpdateCommand="rgrdParent_UpdateCommand"
                OnPreRender="RadGrid1_PreRender"
                OnColumnCreated="RadGrid1_ColumnCreated" AllowFilteringByColumn="True" OnItemDataBound="RadGrid1_ItemDataBound" FilterType="Combined" ShowGroupPanel="True">
                <PagerStyle AlwaysVisible="true" />

                <GroupingSettings CaseSensitive="False"></GroupingSettings>

                <ExportSettings ExportOnlyData="true"  IgnorePaging="true" Csv-EnableBomHeader="true"   OpenInNewWindow="true">
                    <Excel Format="ExcelML" />
                </ExportSettings>

                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" AllowDragToGroup="True">
                    <Selecting AllowRowSelect="True"></Selecting>
                    <ClientEvents OnPopUpShowing="PopUpShowing" />

<Resizing AllowColumnResize="True" ResizeGridOnColumnResize="True"></Resizing>

                    <Animation AllowColumnReorderAnimation="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="True"
                    CommandItemDisplay="Top"
                  
                    AllowSorting="true"
                    DataKeyNames="IDAsset"
                    EditMode="PopUp"
                    EditFormSettings-PopUpSettings-Height="700"
                    EditFormSettings-PopUpSettings-Width="520"
                    Width="100%">

                    <PagerStyle AlwaysVisible="True"></PagerStyle>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px;">
                            &nbsp;&nbsp;
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="ExportToExcel"><img style="border:0px;vertical-align:middle;" alt="" src="Images/Excel_XLSX.png"/>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                        </div>
                    </CommandItemTemplate>
                    <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridTemplateColumn Resizable="true" HeaderStyle-Width="55px" UniqueName="gtcEdit" AllowFiltering="false">
                            <ItemTemplate>
                                <asp:Image runat="server" ID="edit1" ImageUrl="/Images/Edit.png" AlternateText="Edit" Width="12px" Height="12px" />
                                <asp:LinkButton runat="server" ID="linkbuttonedit1" Text="Edit" CommandName="Edit"></asp:LinkButton>
                            </ItemTemplate>

<HeaderStyle Width="55px"></HeaderStyle>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn UniqueName="gtcFind" HeaderStyle-Width="55px" AllowFiltering="false">
                            <ItemTemplate>
                                <asp:Image runat="server" ID="find1" ImageUrl="/Images/locate.png" AlternateText="Find" Width="12px" Height="12px" />
                                <asp:LinkButton runat="server" ID="findbuttonedit1" Text="Find" CommandName="Find"></asp:LinkButton>
                            </ItemTemplate>

<HeaderStyle Width="55px"></HeaderStyle>
                        </telerik:GridTemplateColumn>
                   <%--     <telerik:GridBoundColumn UniqueName="SiteName" DataField="SiteName" HeaderText="Facility Name" HeaderStyle-Width="160px">
                            <FilterTemplate>
                                <telerik:RadComboBox ID="RadComboBox1" runat="server"></telerik:RadComboBox>
                            </FilterTemplate>
                        </telerik:GridBoundColumn>--%>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionFormatString="Edit device">
                        <EditColumn ButtonType="ImageButton" />
                        <PopUpSettings Modal="true" />
                        <FormTemplate>

                            <asp:Panel runat="server" ID="panel2" OnPreRender="panel2_PreRender" CssClass="ViewDepartments_Panel" Height="650" Width="400">
                                <div class="df_dept_container">
                                    <div class="df_serial_label">
                                        <asp:Label ID="Label1" runat="server" Text="Serial Number"></asp:Label>
                                    </div>
                                    <div class="df_serial_input">
                                        <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" Text='<%#Eval("SerialNumber") %>' Width="296px"></asp:TextBox>
                                    </div>
                                    <div class="df_facility_label">Facility </div>
                                    <div class="df_facility_combo">
                                        <telerik:RadDropDownList ID="ddlSite" runat="server" OnSelectedIndexChanged="ddlSite_SelectedIndexChanged" DataValueField="IDSite" DataTextField="SiteDescription" Width="296" OnPreRender="SiteCombo_PreRender" EnableEmbeddedSkins="true">
                                        </telerik:RadDropDownList>
                                    </div>

                                    <div class="df_description_label">
                                        <asp:Label ID="Label2" runat="server" Text="Asset Number"></asp:Label>
                                    </div>
                                    <div class="df_description_input">
                                        <asp:TextBox ID="TextBox2" runat="server" Text='<%#Eval("AssetNumber") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_wing_label">
                                        <asp:Label ID="lblWing" runat="server" Text="Wing"></asp:Label>
                                    </div>
                                    <div class="df_wing_input">
                                        <asp:TextBox ID="txtWing" runat="server" Text='<%#Eval("Wing") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_floor_label">
                                        <asp:Label ID="lblFloor" runat="server" Text="Floor"></asp:Label>
                                    </div>
                                    <div class="df_floor_input">
                                        <asp:TextBox ID="txtFloor" runat="server" Text='<%#Eval("Floor") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_Monitor_label">
                                        <asp:Label ID="lblMonitor" runat="server" Text="Monitor"> </asp:Label>
                                    </div>
                                    <div class="df_Monitor_input">
                                        <asp:TextBox ID="txtMonitor" runat="server" Text='<%#Eval("DCMonitorSerial") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_AIO_label">
                                        <asp:Label ID="lblAIO" runat="server" Text="AIO"></asp:Label>
                                    </div>
                                    <div class="df_AIO_input">
                                        <asp:TextBox ID="txtAIO" runat="server" Text='<%#Eval("AIOSerial") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_Workstation_label">
                                        <asp:Label ID="lblWorkstation" runat="server" Text="Workstation"></asp:Label>
                                    </div>
                                    <div class="df_Workstation_input">
                                        <asp:TextBox ID="txtWorkstation" runat="server" Text='<%#Eval("CartSerial") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_Notes_label">
                                        <asp:Label ID="lblNotes" runat="server" Text="Notes"></asp:Label>
                                    </div>
                                    <div class="df_Notes_input">
                                        <asp:TextBox ID="txtNotes" runat="server" Text='<%#Eval("Notes") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_Other_label">
                                        <asp:Label ID="lblOther" runat="server" Text="Other"></asp:Label>
                                    </div>
                                    <div class="df_Other_input">
                                        <asp:TextBox ID="txtOther" runat="server" Text='<%#Eval("Other") %>' Width="294px"></asp:TextBox>
                                    </div>
                                    <div class="df_dept_label">Department Name</div>
                                    <div class="df_dept_name">
                                        <telerik:RadDropDownList ID="rcboDept" runat="server" Text='<%#Eval("DepartmentId") %>'
                                            Width="296px" DropDownHeight="156px" DropDownWidth="296px" MaxLength="50" EmptyMessage=""
                                            HighlightTemplatedItems="true" EnableLoadOnDemand="true" Filter="StartsWith"
                                            MarkFirstMatch="true" OnPreRender="DepartmentCombo_ItemsRequested">
                                        </telerik:RadDropDownList>
                                    </div>
                                    <div></div>
                                </div>


                                <div>
                                    <div></div>
                                    <asp:Label runat="server" Font-Bold="true" Text="Configure Mobius Powersystem"></asp:Label>
                                </div>
                                <div>
                                    <asp:Label runat="server" Font-Italic="true" Text="Connect software is not installed on this workstation"></asp:Label>
                                </div>

                                <div>
                                    <div></div>
                                    <asp:Label ID="Label99" runat="server" Font-Bold="true" Text="Wireless SSID"></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="TextBox33" runat="server" ReadOnly="false" Text=' ' Width="296px"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:Label ID="Label33" Font-Bold="true" runat="server" Text="Key"></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtSSIDPass" runat="server" ReadOnly="false" Text=' ' Width="296px"></asp:TextBox>
                                </div>
                                <div>Security Type</div>
                                <div>
                                    <telerik:RadDropDownList ID="rdcbAuth" runat="server" Text="WPA2 Personal"
                                        Width="296px" DropDownWidth="296px" MaxHeight="100" MaxLength="50" EmptyMessage="WPA2 Personal"
                                        HighlightTemplatedItems="true" EnableLoadOnDemand="true" Filter="StartsWith"
                                        MarkFirstMatch="true" Height="100px">

                                        <Items>
                                            <telerik:DropDownListItem runat="server" Text="WPA2-Personal" Value="4" Selected="True" />
                                            <telerik:DropDownListItem runat="server" Text="WPA-Personal" Value="2" />
                                            <telerik:DropDownListItem runat="server" Text="WEP" Value="1" />
                                        </Items>
                                    </telerik:RadDropDownList>
                                </div>
                                <div></div>
                                <div class="">
                                    <%-- --%>
                                    <asp:Button ID="Button4" runat="server" Text="Save To Workstation" CommandName="PerformWifiSave" CausesValidation="false" />
                                </div>
                                    <div class="">
                                    <%-- --%>
                                    <asp:Button ID="Button1" runat="server" Text="Save To All Workstations" CommandName="PerformWifiSave2" CausesValidation="false" />
                                </div>
                                <div></div>
                                <div class="df_dept_btn_save">
                                    <%-- --%>
                                    <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Update" />
                                </div>
                                <div class="df_dept_btn_cancel">
                                    <%-- --%>
                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CommandName="PerformCancel" CausesValidation="false" />
                                </div>

                            </asp:Panel>
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <CommandItemStyle BorderStyle="Solid" />
                <PagerStyle AlwaysVisible="true" />
                <FilterMenu AppendDataBoundItems="True">
                </FilterMenu>
            </telerik:RadGrid>
        </div>
    </div>
</asp:Content>
