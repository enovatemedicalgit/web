﻿using PulseWeb.API;
using PulseWeb.API.RTLS;
using PulseWeb.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace PulseWeb
{
    public class Test2
    {
        public string SerialNumber { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public int GetSelectedSiteId()
        {
            if( PulseSession.Instance.SelectedSite != null )
            {
                return PulseSession.Instance.SelectedSite.Id;
            }
            return 0;
        }

        public List<PulseWeb.Model.Site.AssetRTLSUpdate> GetUpdatedRTLSAssets(int siteId,int departmentId)
        {
            Site site = PulseGlobals.GetSite(siteId);
            // create a list of moved assets
            return site.UpdatedAssets();
        }

        [WebMethod(EnableSession = true)]
        public List<PulseWeb.Model.Site.AssetJSON> GetAllAssets()
        {
            RTLS.InitAssetLocations(PulseSession.Instance.SelectedSite);
            return PulseSession.Instance.SelectedSite.AssetsToJson();
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public void SetDepartment(int department)
        {
            PulseSession.Instance.SelectedDepartmentIndex = department;
        }

        [WebMethod(EnableSession = true)]
        public List<Test2> GetAutocomplete()
        {
            Site site = PulseSession.Instance.SelectedSite;
            if (api.SessionUser != null && site != null && site.IsRTLSEnabled())
            {
                var js = new JavaScriptSerializer();


                //Response.ContentType = "application/json";
                //Context.Response.Clear();
                //Context.Response.ContentType = "application/json";
                List<Test2> test = new List<Test2>();
                foreach (Asset asset in site.Assets)
                {
                    test.Add(new Test2 { Name = asset.Description, SerialNumber = asset.SerialNumber });
                }
                //Context.Response.Write(js.Serialize(api.GetPulseAssetsForUser()));
                //Context.Response.Write(js.Serialize(test));
                return test;
            }
            return null;
        }
        [WebMethod(EnableSession = true)]
        public List<Department> GetDepartmentsxxx()
        {
            return PulseSession.Instance.SelectedSite.Departments;
        }

        [WebMethod]
        public List<Department> GetDepartments()
        {
            List<Department> test = new List<Department> {
                new Department{ Name = "Floor 1",Id = 1},
                new Department{ Name = "Pediatrics",Id = 2},
            };
            return test;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public void GetAssets(string assetType)
        {
            var js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(api.GetAssetsForUser()));
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public void UpdateAsset(string models)
        {
            var js = new JavaScriptSerializer();
            Asset a = js.Deserialize<Asset>(models);
            if (a != null)
            {
                UpdateAsset(a);
            }

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public void CreateAsset(string models)
        {
            var js = new JavaScriptSerializer();
            Asset a = js.Deserialize<Asset>(models);
            if( a != null )
            {
                UpdateAsset(a);
            }
        }

        private void UpdateAsset(Asset a)
        {
            Asset cur = api.GetAssetsForUser().Find(asset => asset.SerialNumber == a.SerialNumber);
            if( cur != null )
            {
                cur.Update(a);
            }
            else
            {
                api.GetSite(a.SiteID).Assets.Add(a);
            }
        }
    }
}
