﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Script.Serialization;
using System.Diagnostics;
using PulseWeb.API;
using PulseWeb.Model;
using PulseWeb.API.RTLS;

namespace PulseWeb
{

    public partial class AssetTracking : BasePage
    {
        public AssetTracking()
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)

            string name = Request.QueryString["facilityname"];
            if (!string.IsNullOrEmpty(name))
            {
                PulseSession.Instance.SelectedSite = api.GetSite(name);
            }
            else
            {
                PulseSession.Instance.SelectedSite = api.SessionUser.Sites.Count > 0 ? api.SessionUser.Sites[0] : null;
            }
        }

    }
}