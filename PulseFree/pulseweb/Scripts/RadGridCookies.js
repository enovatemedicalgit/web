﻿function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function NameThatCookie(columnname) {
    return "rgrdParent_" + columnname + "_Display";
}

function TestMe() {
    alert("TestMe");
}
 function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportToExcel") >= 0) {
                    args.set_enableAjax(false);
                }
                if (args.get_eventTarget().indexOf("ExportToExcel") >= 0) {
                    args.set_enableAjax(false);
                }
            }
function ColumnShowing(sender, eventArgs) {
    var columnname = eventArgs.get_gridColumn().get_element().innerText;
    var cookiename = NameThatCookie(columnname);
    //alert("ColumnShowing");
    //setCookie(cookiename, "true", 365);
    SaveUserSetting(cookiename, "true");
}
function ColumnHiding(sender, eventArgs) {
    var columnname = eventArgs.get_gridColumn().get_element().innerText;
    var cookiename = NameThatCookie(columnname);
    //alert("ColumnHiding");
    //setCookie(cookiename, "false", 365);
    SaveUserSetting(cookiename, "false");
}

function SaveUserSetting(_SettingKey, _SettingValue) {
    //alert("SettingKey=" + _SettingKey);
    $.post("savesetting.aspx?SettingKey=" + _SettingKey + "&SettingValue=" + _SettingValue);
}