﻿$(document).ready(function () {

    var assetMoveProxy = $.connection.moveAssetHub;   // the generated client-side hub proxy
    // start connection on a different port
    var url = '/signalr';

    $.connection.hub.start().done(function () {

        $.connection.hub.logging = true;
        $.connection.hub.error(function () {
            console.error('An error occurred with the hub connection.');
        });

        $.ajax({
            type: "POST",
            url: 'Service.asmx/GetSelectedSiteId',
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                assetMoveProxy.server.registerClientFacility(response.d);
            },
            error: function (e) {
                $("#divResult").html("WebSerivce unreachable");
            }
        });

        

    });

    assetMoveProxy.client.updateAsset = function (obj) {
        assetController.destroyFloor();
        $.fn.initAssetsCallback(obj);

    };

    var assetController = new AssetController();
    var imgURL = "Images/";
    var imgExt = ".png";
    var errorStyle = "5px solid #F00";


    $.fn.initAssetsCallback = function (obj) {

        if (obj != null) {

            assetController.createFloorAssets(obj);

            // We need to re-scale each time we add/update asset positions
            if (panzoom.settings.onZoom)
                panzoom.settings.onZoom(panzoom.settings.zoom);
        }
    }

    $.fn.filterByTypeClick = function (element, type) {
        assetController.addTypeFilter(type);
    }

    $.fn.filterOneClick = function (element) {
        assetController.setSingleAssetFilter(element);
    }

    //
    // AssetController
    //
    function AssetController() {
        var self = this;
        this.div = null;
        this.assets = null;
        this.startTime = null;
        this.ticks = 0;
        this.filter = new Array();
        this.filteredAssetSerial = "";
        this.div = document.getElementsByClassName("panzoom-inner")[0];

        this.findAsset = function (value, type) {
            if (this.assets != null) {
                for (var i = 0; i < this.assets.length; i++) {
                    switch (type) {
                        case "SerialNumber": if (this.assets[i].serialNumber === value) return this.assets[i]; break;
                        case "Name": if (this.assets[i].description === value) return this.assets[i]; break;
                    }
                }

            }
            return null;
        }

        this.destroyFloor = function () {

            // Remove all previous asset divs
            if (this.div != null) {
                while (this.div.firstChild) {
                    this.div.removeChild(this.div.firstChild);
                }
            }
        }


        this.createFloorAssets = function (obj) {

            self.assets = new Array(obj.length);
            for (var i = 0; i != obj.length; ++i) {
                var asset = new Asset();
                asset.create(i, obj[i]);

                self.div.appendChild(asset.img);
                self.assets[i] = asset;
            }
            self.filterAssets();
        }


        this.setSingleAssetFilter = function (oneToFilter) {
            if (oneToFilter != null) {
                self.filteredAssetSerial = oneToFilter.serialNumber;
            }
            else {
                self.filteredAssetSerial = "";
            }
            self.filterAssets();
        }

        this.addTypeFilter = function (byType) {
            if (self.filter == null)
                self.filter = new Array(byType);
            else {
                // is it already in the list?
                var index = self.filter.indexOf(byType);
                if (index != -1) {
                    self.filter.splice(index, 1)
                }
                else {
                    self.filter.push(byType);
                }
            }
            self.filterAssets();
        }

        this.filterAssets = function (byType) {
            if (self.assets != null) {
                for (var i = 0; i != self.assets.length; ++i) {
                    var asset = self.assets[i];
                    var visibility = "hidden"; // assume hidden

                    // First we filter by current department
                    if (asset.departmentIndex == panzoom.settings.floor) {
                        // Is the cur asset the one being searched for?
                        if (self.filteredAssetSerial == "" || asset.serialNumber == self.filteredAssetSerial)
                            visibility = "visible";

                        // Does the cur asset meet the hidden criteria
                        for (var j = 0; j < self.filter.length; j++) {
                            if (asset.type == self.filter[j]) {
                                visibility = "hidden";
                            }
                        }
                    }
                    asset.img.style.visibility = visibility;
                }

            }

        }

        this.startTimer = function () {
            self.startTime = new Date().getTime();
            self.timer();
        }

        this.stopTimer = function () {
            self.startTime = null;
        }



        this.timer = function () {
            if (self.startTime) {
                setTimeout(self.timer, 10);

                self.ticks += 10;
                if (self.ticks > 1500) {
                    //ServerUpdateAssets("update", "2");
                    recreateFloor(panzoom.settings.floor);
                    self.ticks = 0;
                }
                if (self.assets) {
                    for (var i = 0; i < self.assets.length; i++) {
                        self.assets[i].perFrameUpdate(10);
                    }
                }
            }
        }
    }


    //
    // Asset
    //
    function Asset() {
        this.value = 0;
        this.state = 'healthy';
        this.img = null;
        this.x = 0;
        this.y = 0;
        this.type = "unknown";
        this.serialNumber = "";
        this.description = "";
        this.departmentIndex = "";
        // interpolate
        this.startX;
        this.startY;
        this.goalX;
        this.goalY;
        this.goalTime;
        this.iterTime;
        this.isInterpolating = false;
        var self = this;


        this.update = function (x, y, state) {

            if (this.isInterpolating && Math.abs(this.goalX - x) < 2 && Math.abs(this.goalY - y) < 2)
                return;

            this.startX = this.x;
            this.startY = this.y;
            this.goalX = x;
            this.goalY = y;
            this.goalTime = 3000;
            this.iterTime = 0;
            this.isInterpolating = true;
            if (Math.abs(x - this.x) < 5 && Math.abs(y - this.y) < 5) {
                this.iterTime = this.goalTime;
            }
            //this.x = x;
            //this.y = y;
            //this.img.style.left = this.x  + "px";
            //this.img.style.top = this.y  + "px";
        }


        this.perFrameUpdate = function (tick) {

            if (this.isInterpolating) {
                this.iterTime += tick;
                if (this.iterTime >= this.goalTime) {
                    this.isInterpolating = 0;
                    this.x = this.goalX;
                    this.y = this.goalY;
                    this.img.style.left = this.x + "px";
                    this.img.style.top = this.y + "px";
                }
                else {
                    var pcnt = (this.iterTime / this.goalTime);
                    this.x = (this.startX + (this.goalX - this.startX) * pcnt + .5) | 0;
                    this.y = (this.startY + (this.goalY - this.startY) * pcnt + .5) | 0;
                    this.img.style.left = this.x + "px";
                    this.img.style.top = this.y + "px";
                }
            }

        }


        this.create = function (row, obj) {
            var img = document.createElement("a");

            this.x = obj.x;
            this.y = obj.y;
            this.type = obj.type;
            this.serialNumber = obj.serialNumber;
            this.description = obj.description;
            this.departmentIndex = obj.departmentIndex;

            img.id = "asset" + row;
            img.className = "icon location-pin";
            img.setAttribute("data-x", this.x);
            img.setAttribute("data-y", this.y);
            img.setAttribute("data-id", obj.imgName);
            img.style.backgroundColor = obj.color;
            // pre-load the image
            var image = new Image();
            image.src = "/Images/RTLS/" + obj.imgName;

            if (obj.state != "Healthy") {
                img.style.width = "6px";
                img.style.height = "6px";
                img.style.boxShadow = "0 0 0 5px rgba(255,0,0,255)";
            }
            else {
                img.style.width = "12px";
                img.style.height = "12px";
            }

            //img.src = imgURL + obj.image + imgExt;
            //img.onmouseover = mouseOver;
            //img.onmouseleave = mouseOut;
            img.style.border = "none";
            img.style.borderRadius = "50%";
            img.title = "<b>" + obj.description + "</b><br>" + "Status: " + obj.state;
            if (this.type != "EMPLOYEE" && this.type != "PATIENT") {
                img.title += "<br>" + "Serial number: " + this.serialNumber;
            }
            else {
                img.title += "<br>Blood pressure: " + obj.bloodPressure;
                img.title += "<br>Heartrate: " + obj.heartRate;
                img.title += "<br>Temperature: " + obj.temperature;
            }
            this.img = img;
        }

    }


    //
    // Initialize the floor asset icons
    //
    function recreateFloor(floor) {

        // Destroy the previous floor if it exists
        assetController.destroyFloor();
        serverInitAssets();
    }

    function serverInitAssets() {
        $.ajax({
            type: "POST",
            url: 'Service.asmx/GetAllAssets',
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $.fn.initAssetsCallback(response.d);
            },
            error: function (e) {
                $("#divResult").html("WebSerivce unreachable");
            }
        });
    }

    //assetController.startTimer();
    // Autocomplete
    $('#autoComplete').kendoAutoComplete({
        minLength: 1,
        dataTextField: "Name",
        placeholder: "Type to search...",
        change: function (e) {
            var value = this.value();
            if (value == "") {
                $.fn.clearSearch();
            }
            // Use the value of the widget
        },
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Service.asmx/GetAutocomplete",
                    dataType: "json",
                }
            },
            schema: {
                data: "d"
            }
        })
    });

    // Searcy-by dropdown
    $("#searchbyCombo").kendoDropDownList({
        dataTextField: "display",
        dataValueField: "fieldName",
        index: 0,
        change: onChangeSearchBy,
        dataSource: [{
            fieldName: 'Name',
            display: 'Asset Name'
        }, {
            fieldName: 'SerialNumber',
            display: 'Serial Number'
        }],
    });

    function onChangeSearchBy() {
        var me = $("#autoComplete").data("kendoAutoComplete");
        var dataItem = $("#searchbyCombo").val();

        me.value("");
        me.dataSource.filter([]); //clear filters
        me.element.attr("data-text-field", dataItem);
        me.setOptions({ dataTextField: dataItem });
    }


    // Tool tip
    var tooltip = $("#panzoomContainer").kendoTooltip({
        filter: "a",
        width: 240,
        content: kendo.template($("#hover-template").html()),
        position: "top",
        showOn: "click",
    }).data("kendoTooltip");

    //$("#panzoomContainer").find("a").click(false);


    //
    // Search Window
    //
    var searchWindow = $("#searchWindow");

    $('#searchWindow').keypress(function (e) {
        if (e.keyCode == $.ui.keyCode.ENTER) {
            searchWindow.data("kendoWindow").close();
        }
    });

    $.fn.clickSearch = function () {
        searchWindow.data("kendoWindow").center();
        searchWindow.data("kendoWindow").open();
    };

    $.fn.clearSearch = function () {
        $.fn.filterOneClick(null);
        $("#searh_results").val("");
    }

    $.fn.departmentChange = function (department) {
        $.post("Service.asmx/SetDepartment",
        {
            department: department,
        });
    }


    var handleUserInput = function () {
        var userinput = ($("#autoComplete").val());
        var asset = assetController.findAsset(userinput, $("#searchbyCombo").val());

        if (asset != null) {
            var dropdown = $("#departments").data("kendoDropDownList");
            if ($("#departments").val() != asset.areaId) {
                dropdown.select(function (dataItem) {
                    return dataItem.Id == asset.areaId;
                });
                onChange();
            }
            $("#searh_results").val(asset.description);
        }
        $.fn.filterOneClick(asset);
    }

    if (!searchWindow.data("kendoWindow")) {
        searchWindow.kendoWindow({
            actions: ["Close"],
            animation: {
                open: {
                    effects: "slideIn:down fadeIn",
                    duration: 500
                },
                close: {
                    effects: "slide:up fadeOut",
                    duration: 500
                }
            },
            minWidth: 300,
            modal: true,
            resizable: false,
            title: "Find an asset",
            visible: false,
            close: handleUserInput,
            cancel: handleUserInput,
            activate: function () {
                $("#autoComplete").data("kendoAutoComplete").value("");
                $("#autoComplete").focus();
            }
        });
    }

    // Department dropdown
    $("#departments").kendoDropDownList({
        dataTextField: "Name",
        dataValueField: "Id",
        index: 0,
        change: onChange,
        dataBound: onBound,
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Service.asmx/GetDepartments",
                    dataType: "json",
                }
            },
            schema: {
                data:"d"
            }
        })
    });


    function onChange() {
        var value = $("#departments").val();

        pulse.panzoom.setFloor(value);
        recreateFloor(value);

        $(this).departmentChange(value);

    };

    function onBound(e) {
        //var dept = getSelectedDepartment();
        //$("#departments").data("kendoDropDownList").select(dept);
        onChange();
    };
});
