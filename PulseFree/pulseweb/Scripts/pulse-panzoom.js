﻿var panzoom = extend(pulse, 'pulse.panzoom');

// default settings
panzoom.settings = {
  zoom: 1,
  floorImagesPath: '/images/departments/dept-',
  floor: 3,
  getWidth: function() {
    return panzoom.settings.zoom * panzoom.settings.scale1Width;
  }
};

panzoom.init = function (settings) {
  $.extend(panzoom.settings, settings);
  panzoom.setFloor(panzoom.settings.floor);
  
  // Preload images for easy transition, could pass these variables in with settings object
  // with a preloadImages boolean option.
  //var image = new Image();
  //var minFloor = 1;
  //var maxFloor = 5;
  //for (var i=minFloor; i<=maxFloor; i++)
  //  image.src = panzoom.settings.floorImagesPath + 'dept-' + i + '.jpg';
};

panzoom.setFloor = function(floor) {
  panzoom.settings.floor = floor;
  var inner = panzoom.settings.container.find('.panzoom-inner');

  inner.fadeOut('slow', function() {
    inner.css('background-image', "url('" + panzoom.settings.floorImagesPath + floor + ".jpg')");
    inner.fadeIn('slow');
  });
  
  var image = new Image();
  image.src = panzoom.settings.floorImagesPath + panzoom.settings.floor + '.jpg';
  image.onload = function () {
    panzoom.settings.scale1Width = this.width;
    panzoom.settings.scale1Height = this.height;
    panzoom.setZoom(panzoom.settings.zoom);
  };
};

panzoom.setZoom = function (zoom) {
  var startWidth = panzoom.settings.getWidth();

  panzoom.settings.zoom = zoom;
  var inner = panzoom.settings.container.find('.panzoom-inner');
  var width = Math.round(panzoom.settings.scale1Width * zoom);
  var height = Math.round(panzoom.settings.scale1Height * zoom);
  inner.css('width', width + 'px');
  inner.css('height', height + 'px');
  inner.css('background-size', width + 'px ' + height + 'px');

  // move the image half the scale amount so the focal point is centered
  var requiredOffset = (startWidth - width) / 2;
  inner.css('left', (inner.position().left + requiredOffset) + 'px');

  // call the callback function if supplied
  if (panzoom.settings.onZoom)
    panzoom.settings.onZoom(panzoom.settings.zoom);

  panzoom.initImageDrag();
};

panzoom.initImageDrag = function () {
  var container = panzoom.settings.container;
  var containerOffset = container.offset();
  var inner = container.find('.panzoom-inner');
  var containment = {
    top: 0,
    right: 0,
    bottom: 2000,
    left: 0
  };
  var imageWidth = inner.width();
  var containerWidth = container.width();
  if (imageWidth > containerWidth) {
    containment.left = containerOffset.left - (imageWidth - containerWidth);
    containment.right = containerOffset.left;
  } else {
    containment.left = containerOffset.left;
    containment.right = containerWidth - imageWidth + containerOffset.left;
  }

  var imageHeight = inner.height();
  var containerHeight = container.height();
  if (imageHeight > containerHeight) {
    containment.top = containerOffset.top - (imageHeight - containerHeight);
    containment.bottom = containerOffset.top;
  } else {
    containment.top = containerOffset.top;
    containment.bottom = containerHeight - imageHeight + containerOffset.top;
  }


  // In case we are resizing, make sure image is still within containment bounds:
  var imageOffset = inner.offset();
  
  // for x-axis
  if (imageOffset.left > containerOffset.left && imageOffset.left + imageWidth > containerOffset.left + containerWidth) {
    if (imageWidth > containerWidth)
      inner.css('left', '0');
    else
      inner.css('left', (containerWidth - imageWidth) + 'px');
  }
  if (imageOffset.left < containerOffset.left && imageOffset.left + imageWidth < containerOffset.left + containerWidth) {
    inner.css('left', '0');
  }

  // for y-axis
  if (imageOffset.top > containerOffset.top && imageOffset.top + imageHeight > containerOffset.top + containerWidth) {
    if (imageHeight > containerHeight)
      inner.css('top', '0');
    else
      inner.css('top', (containerHeight - imageHeight) + 'px');
  }
  if (imageOffset.top < containerOffset.top && imageOffset.top + imageHeight < containerOffset.top + containerHeight) {
    inner.css('top', '0');
  }


  inner.draggable({
    containment: [containment.left, containment.top, containment.right, containment.bottom],
    scroll: false,
    distance: 10
  });
};