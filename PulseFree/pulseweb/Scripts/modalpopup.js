﻿(function (global, undefined) {
    var thunk = {};

    function togglePopupModality() {
        var wnd = getModalWindow();
        wnd.set_modal(!wnd.get_modal());

        if (!wnd.get_modal()) {
            document.documentElement.focus();
        }
    }
    function showDialogInitially() {
        var wnd = getModalWindow();
        if (wnd != null )
            wnd.show();
        Sys.Application.remove_load(showDialogInitially);
    }
    Sys.Application.add_load(showDialogInitially);

    function getModalWindow() { return $find(thunk.modalWindowID); }

    global.$modalWindowThunk = thunk;
    global.togglePopupModality = togglePopupModality;
})(window);