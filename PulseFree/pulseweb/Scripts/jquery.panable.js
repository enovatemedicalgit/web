﻿(function ($) {
  $.fn.panable = function (callback) {
    var image = $(this);
    var width = image.width();
    var height = parseInt(image.data('height'));
    

    // chrome issue workaround, pagewidth is not correct unless overflow set to always show scroll bar. Effecting the image offset value.
    //if ($(window).height() < $(document).height())
    //  $('body').css('overflow', 'visible');

    var containerOffset = image.offset();
    var x1 = containerOffset.left;
    var y1 = containerOffset.top - image.height() + height;
    var x2 = x1;
    var y2 = containerOffset.top;

    image.css('position', 'absolute');
    var initialPosition = (image.height() - height) / 2 * -1;
    image.css('top', initialPosition + 'px');
    image.wrap("<div class='croppable-container' style='position: relative;overflow:hidden;width:" + width + "px;height:" + height + "px;' />");

    var actionWrapper = $("<div style='position:relative;top:0px;'></div>");

    var cancel = $("<input id='btnCropCancel' type='button' class='btn btn-danger' value='Cancel' />");
    cancel.click(function () {
      $('#cropoverlay').remove();
      $('.croppable-container').remove();
      $('#uxHeaderPreviewWrapper').show();
    });

    var save = $("<input type='button' class='btn btn-default' value='Save'/>");
    save.click(function () {
      var imageWidth = image.outerWidth();
      var headerWidth = 1170;
      var scaledOffset = parseInt(image.css('top')) * -1;
      var actualOffset = Math.round(headerWidth / imageWidth * scaledOffset);
      $.post("/asset/saveheader",
					{
					  modalSiteId: $('#SiteId').val(),
					  url: image.attr('src'),
					  y: actualOffset,
					  height: $('.croppable-container').outerHeight()
					},
					function (data) {
					  $('#cropoverlay').remove();
					  $('.croppable-container').remove();
					  var wrapperDiv = $('#uxHeaderPreviewWrapper');
					  wrapperDiv.css('background-image', "url('" + data + '?' + new Date().getTime() + "')");
					  wrapperDiv.show();

					  if (callback)
					    callback();

					  //$('.header-image').css('background-color', '');
					  //$('.header-image').css('background-image', "url('" + data + '?' + new Date().getTime() + "')");
					  //$('.header-image').show();
					}
				);
    });

    actionWrapper.append(save);
    actionWrapper.append(cancel);

    $('.croppable-container').after('<div id="cropoverlay" ></div>');
    $('#cropoverlay').on('click', function () {
      $('#btnCropCancel').trigger('click');
    });

    image.draggable({
      containment: [x1, y1, x2, y2],
      axis: "y"
    });
    image.after(actionWrapper);
    var actionsLeftOffset = width - save.outerWidth() - cancel.outerWidth();
    actionWrapper.css('left', actionsLeftOffset + 'px');
  };
})(jQuery);
