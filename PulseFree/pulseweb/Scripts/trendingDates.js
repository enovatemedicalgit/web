﻿
/*************************

file used in:
trendingutilization.aspx

**************************/

var datetimeStart = document.getElementById("<%= dtStart.ClientID %>");
var datetimeEnd = document.getElementById("<%= dtEnd.ClientID %>");

var bDateSetByUser = false;
var DefaultDate_HourlyUtilization_Start = document.getElementById("<%= dtStart.ClientID %>_dateInput_text").value;
var DefaultDate_HourlyUtilization_End = document.getElementById("<%= dtEnd.ClientID %>_dateInput_text").value;
var DefaultDate_Other_Start = new Date();
var DefaultDate_Other_End = DefaultDate_HourlyUtilization_End;

function MakeNoMoreDateAssumptions() {
    bDateSetByUser = true;
}
function HelpWithDefaultDates(el) {
    var dtp = $find("<%=dtStart.ClientID %>");
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    var yesterday = new Date(today - 1000 * 60 * 60 * 24);
    var lastweekDate = new Date(today - 1000 * 60 * 60 * 24 * 7);
    if (bDateSetByUser == false) {

        switch (el.value) {
            case "HourlyUtilization":
                dtp.set_selectedDate(yesterday);
                bDateSetByUser = false;
                break;
            case "ChargeLevel":
                dtp.set_selectedDate(yesterday);
                bDateSetByUser = false;
                break;
            default:
                dtp.set_selectedDate(lastweekDate);
                bDateSetByUser = false;
                break;
        }
    }
}