﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PulseWeb.API;
using System.Text.RegularExpressions;

using PulseWeb.Utils;
using PulseWeb.Utils.Security;
using PulseWeb.Utils.Database;
using PulseWeb.Model;

namespace PulseWeb.Account
{
    class UserValidationException : Exception
    {
        public TextBox mBadTextBox = null;

        public UserValidationException(TextBox box, string message)
            : base(message)
        {
            mBadTextBox = box;
        }
    }

    public partial class AddUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string temp = Request.UrlReferrer != null ? Request.UrlReferrer.ToString() : "";
                int index = temp.IndexOf("redir=");
                if (index > 0)
                {
                    HttpContext.Current.Session["RedirectAddUser"] = temp.Substring(index + 6);
                }
            }

        }

        protected void ShowUserMessage(TextBox txtBox,string message)
        {
            ErrorHandler.PopUpMessage(message);
            txtBox.Focus();
        }

        private void ValidateNotNull(TextBox txtBox,string errorMessage)
        {
            if (String.IsNullOrEmpty(txtBox.Text))
            {
                throw new UserValidationException(txtBox, "A valid " + errorMessage + " is needed");
            }
        }

        private void ValidEmail(TextBox mail)
        {
            ValidateNotNull(mail, "Email");
            if (!Regex.IsMatch(mail.Text, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                throw new UserValidationException(mail,"Invalid e-mail format");
            }
            PulseDBInterface db = new PulseDBInterface(new SQLDatabase());
            if (db.DoesEmailAlreadyExist(mail.Text))
            {
                throw new UserValidationException(mail,string.Format("The email address: {0} is already in use.", mail.Text));
            }

        }

        private bool ValidateUserInfo()
        {
            
            try
            {
                ValidateNotNull(this.FirstName, "First Name");
                ValidateNotNull(this.LastName, "Last Name");
                ValidEmail(this.EmailText);
                ValidateNotNull(this.Password, "Password");
            }
            catch (UserValidationException ex)
            {
                ShowUserMessage( ex.mBadTextBox, ex.Message );
                return false;
            }

            return true;
        }
        protected void BtnClose(object sender, EventArgs e)
        {
            if (Request.QueryString["redir"] != null)
            {
                Response.Redirect(Request.QueryString["redir"].ToString());
            }
            else
            {
                string redirect = HttpContext.Current.Session["RedirectAddUser"] as string;
                if (!string.IsNullOrEmpty(redirect))
                {
                    Response.Redirect(redirect);
                }
            }
            Response.Redirect("ViewUsers.aspx");
        }

        public PulseUser UpdateUserFromForm(PulseUser user)
        {
            user.UserName = this.EmailText.Text;
            user.FirstName = this.FirstName.Text;
            user.LastName = this.LastName.Text;
            user.Title = this.TitleText.Text;
            user.Phone1 = this.HomePhoneBox.Text;
            user.Phone2 = this.OtherPhoneBox.Text;
            user.EncryptedPassword = Crypto.EncryptString(this.Password.Text);
            user.Email = this.EmailText.Text;
            user.IDSite = 1;
            user.Id = -1;                // Will be set when added to the DB

            UserAccessRights rights = 0;
            foreach (ListItem item in this.UserOptionsCheck.Items)
            {
                rights = AccessRightsHelper.AddRight(rights, item.Value, item.Selected ? "1" : "0");
            }
            user.AccessRights = rights;

            return user;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            if (ValidateUserInfo())
            {
                PulseUser user = UpdateUserFromForm(new PulseUser());
                api.AddUser(user);
               
            }
            Response.Redirect("ViewUsers.aspx");
        }

    }
}