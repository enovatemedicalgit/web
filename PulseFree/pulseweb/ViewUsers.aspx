﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewUsers.aspx.cs" Inherits="PulseWeb.ViewUsers" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content runat="server" ContentPlaceHolderID="head"> 
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/adduser.css" />
    <link rel="stylesheet" type="text/css" href="css/customRadioButton.css" />

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">

            <script type="text/javascript">
                function mngRequestStarted(ajaxManager, eventArgs) {
                    var n = eventArgs.EventTarget.search("LinkButton3");
                    if ( n >= 0 ){
                        eventArgs.EnableAjax = false;
                    }
                }
            </script>

   <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true">
        <Windows>
            <telerik:RadWindow ID="RadWindow1" runat="server" ShowContentDuringLoad="false" Width="400px"
                Height="400px" Title="Import Users from an Active Directory file" Behaviors="Default" AutoSize="true" AutoSizeBehaviors="Width,Height">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

    <telerik:RadAjaxManager ID="RadAjaxManager2" runat="server" ClientEvents-OnRequestStart="mngRequestStarted">
        <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadGrid1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
            <ClientEvents OnRequestStart="mngRequestStarted" />
         </telerik:RadAjaxManager>

    <telerik:RadFormDecorator runat="server" DecorationZoneID="demo" EnableRoundedCorners="true" DecoratedControls="All" />
    <div id="view-assets" class="view-assets-container no-bg">
        <asp:Label runat="server" Font-Size="Large" ID="AssetTypeLabel" ForeColor="#333333" > </asp:Label>
        <br />
        <div id="demo" class="demo-container no-bg">
            <telerik:RadGrid ID="RadGrid1" runat="server" EnableEmbeddedSkins="true" 
                Skin="Metro" CssClass="RadGrid_Rounded"
                OnNeedDataSource="RadGrid1_NeedDataSource" 
                AllowPaging="True" 
                AllowSorting="True" 
                GroupPanelPosition="Top" 
                AutoGenerateEditColumn="false" 
                PageSize="40"
                OnItemCreated="rgrdUsers_ItemCreated"
                OnItemCommand="rgrdParent_ItemCommand" 
                OnUpdateCommand="rgrdParent_UpdateCommand"
                OnInsertCommand="RadGrid1_InsertCommand" 
                OnPrerender = "RadGrid1_PreRender"
                OnColumnCreated="RadGrid1_ColumnCreated" AllowFilteringByColumn="True" >

                <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                        <Excel Format="ExcelML" />
                    </ExportSettings>
                <PagerStyle AlwaysVisible="true" />

                <MasterTableView AutoGenerateColumns="True"  
                                  CommandItemDisplay="Top"
                                    AllowSorting="true"
                                DataKeyNames="UserName" >
      <CommandItemTemplate>

                    <div style="padding: 2px 2px;">

                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="InitInsert" Visible='<%# !RadGrid1.MasterTableView.IsItemInserted %>'><img style="border:0px;vertical-align:middle;" alt="" src="Images/AddRecord.png"/>&nbsp;&nbsp;Add new</asp:LinkButton>&nbsp;&nbsp;

                        <asp:LinkButton ID="LinkButton1" runat="server"  OnClientClick="openRadWin()" Visible="false" ><img style="border:0px;vertical-align:middle;" alt="" src="Images/ImportFile.png"/>Import</asp:LinkButton>&nbsp;&nbsp;

                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="RebindGrid"><img style="border:0px;vertical-align:middle;" alt="" src="Images/Refresh.png"/>&nbsp;&nbsp;Refresh users list</asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="ExportToExcel"><img style="border:0px;vertical-align:middle;" alt="" src="Images/Excel_XLSX.png"/>&nbsp;&nbsp;Export to Excel</asp:LinkButton>

                    </div>
                </CommandItemTemplate>
                 <CommandItemSettings ShowExportToExcelButton="False" ShowAddNewRecordButton="True" AddNewRecordText="Add new user"/>
                <Columns>
					<telerik:GridTemplateColumn  HeaderStyle-Width="50px" AllowFiltering="false">
						<ItemTemplate>
							<asp:Image runat="server" ID="edit1" ImageUrl="/Images/Edit.png" AlternateText="Edit" Width="12px" Height="12px"/>
							<asp:LinkButton runat="server" Width="12px" ID="linkbuttonedit1" Text="Edit" CommandName="Edit"></asp:LinkButton>
						</ItemTemplate>
					</telerik:GridTemplateColumn>

                </Columns>
				    <EditFormSettings  UserControlName="UserDetails.ascx" EditFormType="WebUserControl">
					    <EditColumn  HeaderStyle-Width="50px" UniqueName="EditCommandColumn1">
					    </EditColumn>
				    </EditFormSettings>

                </MasterTableView>
            </telerik:RadGrid>
        </div>
        </div>

    <script>
        (function (global, undefined) {
            var widthTextBox, heightTextBox, leftTextBox, topTextBox;

            function widthTextBox_load(sender, args) {
                widthTextBox = sender;
            }
            function heightTextBox_load(sender, args) {
                heightTextBox = sender;
            }
            function leftTextBox_load(sender, args) {
                leftTextBox = sender;
            }
            function topTextBox_load(sender, args) {
                topTextBox = sender;
            }

            function openRadWin() {
                //var width = widthTextBox.get_value();
                //var height = heightTextBox.get_value();
                //var left = leftTextBox.get_value();
                //var top = topTextBox.get_value();
                var wndw = radopen("http://" + window.location.host + "/importUsers/importUserTab.aspx", "RadWindow1", 0, 0, 0, 0);
                wndw.center();
            }

            function openPopUp() {
                window.open("http://www.microsoft.com", "WindowPopup", "width=400px, height=400px, resizable");
            }

            global.openRadWin = openRadWin;
            global.openPopUp = openPopUp;
            global.widthTextBox_load = widthTextBox_load;
            global.heightTextBox_load = heightTextBox_load;
            global.leftTextBox_load = leftTextBox_load;
            global.topTextBox_load = topTextBox_load;
        })(window);
    </script>

</asp:Content>
