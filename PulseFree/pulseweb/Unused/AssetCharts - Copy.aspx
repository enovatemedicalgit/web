﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssetCharts.aspx.cs" Inherits="PulseWeb.AssetCharts" %>
<asp:Chart ID="ChartHourlyFacility" runat="server" Width="600px" Height="320px" EnableViewState="true" 
    BackColor="#f3f3f3" Visible="true" CssClass="Chart1" RenderType="BinaryStreaming" >
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1"  BackColor="Transparent">
            <AxisX><LabelStyle ForeColor="Gray" /></AxisX>
            <AxisY><LabelStyle ForeColor="Gray" /></AxisY>
        </asp:ChartArea>
    </ChartAreas>
    <Titles>
        <asp:Title Name="criteria" Text="" ForeColor="Gray"></asp:Title>
    </Titles>
    <Legends>
        <asp:Legend Alignment="Center" TitleAlignment="Center" Docking="Bottom" BackColor="Transparent" ForeColor="Gray"></asp:Legend>
    </Legends>
</asp:Chart>

