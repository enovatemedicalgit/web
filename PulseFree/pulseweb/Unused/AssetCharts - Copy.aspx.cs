﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using PulseWeb.Model;
using System.Collections.Specialized;
using PulseWeb.API;

namespace PulseWeb
{
    public partial class AssetCharts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NameValueCollection qscoll = HttpUtility.ParseQueryString(Request.QueryString.ToString());

            Asset.AssetCategory category;
            string LabelText = null;
            //if( qscoll.AllKeys.con)
            switch (qscoll["p1"])
            {
                case "chargers": category = Asset.AssetCategory.CHARGER; LabelText = "Chargers"; break;
                case "batteries": category = Asset.AssetCategory.BATTERY; LabelText = "Batteries"; break;
                case "workstations": category = Asset.AssetCategory.WORKSTATION; LabelText = "Daily Workstation Movement"; break;
                // internal error
                default: return;
            }
            // get the site
            Site site = null;
            int siteID = Convert.ToInt32(qscoll["p2"]);
            if (siteID >= 0 && siteID < api.GetSitesForUser(api.SessionUser).Count)
            {
                site = api.GetSitesForUser(api.SessionUser)[siteID];
            }

            if(site != null )
            {
                BindUtilizationChart(this.ChartHourlyFacility, site, category, LabelText);
            }
        }
        protected void BindUtilizationChart(Chart ChartHourlyHQ, Site site, Asset.AssetCategory category,string LabelText)
        {
            if (ChartHourlyHQ != null)
            {
                DateTime datetimeStart = Utils.Time.Today.AddDays(-7);
                DateTime datetimeEnd = Utils.Time.Today;
                // Workstation, Charger, Battery
                Color[] colors = new Color[] { Color.FromArgb(255, 255, 192, 167), Color.FromArgb(255, 79, 232, 212), Color.FromArgb(255, 80, 240, 94) };

                string ChartLegendTitle = "";
                // when user is with a site, not tied directly to the HQ:

                DataTable dtHourly = new DataTable();

                // MDJ Todo - migrate this to the PulseDB model
                using (PulseDBInterface enovateDB = new PulseDBInterface())
                {
                    var db = enovateDB.Query(string.Format("select AvgRunRate,Date FROM fnDailyUtilizationByHQ('{0}', '{1}', {2}) where SiteID={3}", datetimeStart, datetimeEnd, site.CustomerID, site.Id));


                    ChartHourlyHQ.Series.Clear();

                    string SelectedChartType = LabelText;

                    ChartHourlyHQ.Titles["criteria"].Text = Environment.NewLine + SelectedChartType + " " + Environment.NewLine + datetimeStart.DayOfWeek.ToString() + " " + datetimeStart.ToShortDateString() + "-"
                        + datetimeEnd.DayOfWeek.ToString() + " " + datetimeEnd.ToShortDateString();

                    ChartHourlyHQ.Legends[0].Title = ChartLegendTitle;

                    // setup series for departments
                    Series s1 = new Series();
                 //   Series s2 = new Series();
                    s1.LegendText = "";// site.Name;
                    s1.ChartType = SeriesChartType.ThreeLineBreak;
                    s1.ChartArea = "ChartArea1";
                    //s2.LegendText = "";// site.Name;
                    //s2.ChartType = SeriesChartType.StackedColumn;
                    //s2.ChartArea = "ChartArea1";

                    s1.YValueType = ChartValueType.Double;
                    s1.YValueMembers = "AvgRunRate";

                    s1.XValueType = ChartValueType.String;
                    s1.XValueMember = "Date";

                    //s2.YValueType = ChartValueType.Double;
                    //s2.YValueMembers = "Utilization";
                    //s2.XValueType = ChartValueType.String;
                    //s2.XValueMember = "Date";

                    s1.Color = colors[(int)category];
                   // s1.Color = colors[3];
                    s1.CustomProperties = "PointWidth=0.7"; 
                    //s2.Color = colors[2];
                    //s2.CustomProperties = "PointWidth=0.5"; 
                    ChartHourlyHQ.Series.Add(s1);
                    // ChartHourlyHQ.Series.Add(s2);
                    ChartArea area = ChartHourlyHQ.ChartAreas["ChartArea1"];
                    area.AxisX.MajorGrid.Enabled = false;
                    area.AxisX.Title = "Date";
                    area.AxisY.MajorGrid.Enabled = false;
                    area.AxisY.Title = "Movements per day";
                    area.Area3DStyle.Enable3D = true;

                    ChartHourlyHQ.Legends["Legend1"].Enabled = false;

                    ChartHourlyHQ.DataSource = db.Select(x => new
                    {
                        AvgRunRate = Utils.Misc.SafeGetKey(x, "AvgRunRate"),
                        Date = Utils.Misc.SafeShortDate(Utils.Misc.SafeGetKey(x, "Date")),
                    }).ToList();

                    ChartHourlyHQ.DataBind();
                    //lblMessage.Text = "";
                    ChartHourlyHQ.Visible = true;
                }

            }

        }

        protected string GetHQSQL(Site site, string UtilizationInterval, DateTime datetimeStart, DateTime datetimeEnd)
        {
            // UtilizationInterval parameter/value must either be "Daily" or "Hourly"

            string sSELECT = "SELECT [Date], case datename(weekday, [Date]) when 'Sunday' then 'Su' when 'Monday' then 'M' when 'Tuesday' then 'Tu' when 'Wednesday' then 'W' when 'Thursday' then 'Th' when 'Friday' then 'F' when 'Saturday' then 'Sa' end as WeekdayAbbr, ";
            string sFROM = "FROM fnDailyUtilizationByHQ('{0}', '{1}', {2}) ";
            string sGROUPBY = " GROUP BY [Date] ";
            string sWHERE = "WHERE HQID = '{0}' AND [Date] BETWEEN convert(varchar(10), cast('{1}' as datetime), 101) AND convert(varchar(10), cast('{2}' as datetime), 101) ";
            string TrendFieldname = "Utilization";
            if (UtilizationInterval == "Hourly")
            {
                sSELECT += " [DateAndHour], REPLACE(RIGHT(convert(varchar(50), cast([DateAndHour] as datetime), 100),7), ':00', ' ') as TimeOnly, ";

                //sGROUPBY += ", [DateAndHour] ";
                sGROUPBY += ", [DateAndHour], REPLACE(RIGHT(convert(varchar(50), cast([DateAndHour] as datetime), 100),7), ':00', ' ') ";
                sFROM = "FROM fnHourlyUtilizationByHQ('{0}', '{1}', {2}) ";
                sWHERE = "WHERE HQID = '{0}' AND [Date] BETWEEN '{1}' AND '{2}' ";
                TrendFieldname = "HourlyUtilization";
                //datetimeStart = datetimeEnd.AddDays(-2);
            }

            sFROM = string.Format(sFROM, datetimeStart, datetimeEnd, site.CustomerID);

            string AndSelect = "";
            string AndWhere = " AND Description in (";
            string ItemDelimiter = "";
            string AggFunction = "Avg";

            AndSelect += ItemDelimiter + " " + AggFunction + "(CASE WHEN Description = '" + site.Name.Trim().Replace("'", "''").Trim() + "' THEN " + TrendFieldname + " ELSE NULL END) as [" + site.Name.Replace(",", " ").Trim() + "] ";
            AndWhere += ItemDelimiter + "'" + site.Name.Trim().Replace("'", "''").Trim() + "'";
            ItemDelimiter = ", ";

            sSELECT += AndSelect;
            AndWhere += ") ";
            sWHERE += AndWhere;
            sWHERE = string.Format(sWHERE, 696, datetimeStart, datetimeEnd);


            // where clause is not needed for hourly trending because the function in the FROM clause limits selection adequately
            if (UtilizationInterval == "Hourly")
            {
                sWHERE = string.Empty;
            }

            return sSELECT + sFROM + sWHERE + sGROUPBY;

        }
    }
}