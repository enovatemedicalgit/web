﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace PulseWeb
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static readonly Dictionary<int, string> mMessageTable = new Dictionary<int, string>
        {
            // Batteries main info icon
            { 1, @"<ul>
<li>In Service is the count of batteries that are in a workstation. The count considers the past 30 minutes of rolling data. </li>
<li>Charging is the count of batteries in a charging bay. The count considers the past 30 minutes of rolling data.</li>
<li>Dormant Status is the count of batteries that have not been in a workstation or charging bay within the past 2 hours, but have been used at least once in the last 7 days. </li>
<br><br><b>Tip:</b> Batteries in dormant status may not be in a charger or workstation. They also may be within a charger or workstation not reporting.</ul> </li>
<br><li>Not used is the count of batteries that have been in a workstation or charger the past 7 days. 
<br><b>Tip:</b> Batteries with a Not Used status and are likely out of circulation. They also may not have been placed in a charger or workstation that is communicating. After 30 days, a device that hasn't reported to Pulse will be marked as Out Of Service and will not be included in reports or metrics.  </ul>
    </li> </ul> " },

            // Chargers main info icon
            { 2, @"<ul>
<li>Charging is a count of charging bays that are actively charging a battery within the selected departments. This status also indicates that there has been communication from the charger within the past two hours.
<br><br><b>Tip:</b> The charging stations are either 2-bay or 4-bay. This considers all the individual bays available within both charging stations.
<li>Charging count is the count of charging bays that do not have a battery and therefore are not charging and available. A charging bay must have communicated within the past 7 days qualify for this status. 
</li>
<li>Offline Status is a count of charging bays that have not communicated with Pulse within the past 24 Hours. Chargers could lose connection when wireless infrastructure changes, devices are moved to an area with limited connectivity, or if the antenna on the device is no longer functioning properly.  <br> After 30 days, a device that hasn't reported to Pulse will be marked as Out of Service and will not be included in reports or metrics.
</li> </ul> " },

            // Workstatin main info icon
            { 3, @"<ul>
<li>In Service: The count of workstations that are online, moving and powering a PC that is in use. A workstation must have communicated within the past 2 hours to qualify for this status. 
<br><br><b>Tip</b>: Pulse will collect up to 12 informational packets per hour. If half of the packets have been flagged with movement, the workstation is considered moving. Movement is defined as 5 seconds of constant motion. If half of the packets have a power draw of 1.5 amps or greater, the PC is considered to be in use vs. powered but not processing information.</li>
<br><li>Available: The count of workstations that are online but are not moving or powering a PC in use. A workstation must have communicated within the past 2 hours to qualify for this status.</li>
<br><li>Offline: The count of workstations that have not communicated within the past 7 days. 
<br><br><b>Tip:</b> Offline workstations do not have Connect installed on the PC or the wireless card on the power system configured properly. If Connect is installed but the PC is not on the workstation will report as offline. If the wireless card on the power system is configured but not powered by the Mobius battery, and the power system’s internal back-up battery has expired, the workstation will report as offline. <br> After 30 days, a device that hasn't reported to Pulse will be marked as Out Of Service and will not be included in reports or metrics.
If the Mobius battery is expired, the power system will still be energized by the back-up battery and send one packet per 24 hours. This packet will be labeled as a “heartbeat” packet to indicate the workstation is idle and only powered by the back-up battery.</li> </ul> "  },
            // Drilldown 1.1
            { 4, @"Average Capacity level is the average remaining capacity level percentage of all batteries in the selected departments that was last reported to Pulse by the battery. " },
            // Drilldown 1.2
            { 5, @"Average amp draw is a measure of average current being pulled from battery. Amps are a measure of current.  If the amp draw is too high the battery life will be shortened. A good average draw from a workstation is between 1.7-3.0 Amps.  
If you have external devices plugged in such as laptops, phones, scanners, etc. the battery life will be shortened." },
            // Drilldown 1.3
            { 6, @"Average Run Rate is a calculation showing the average run time in a workstation for batteries that have communicated in the last 7 days. Run rate is the estimated time you can expect a battery to last based on your average remaining battery capacity, PC Amp draw, and workflow practices.
<br><br><b>Tip</b> To increase run rate, ensure battery is fully charged before removing from charger and do not replace until remaining time is below 20 minutes. Decreasing monitor brightness will also 
reduce current draw and increase run rate. Please remove laptop batteries to increase run rate if applicable." },
            // Drilldown 2.1
            { 7, @"Fully charged batteries is a count of batteries that are sitting in a charger at 95% charge or greater." },
            // Drilldown 2.2
            { 8, @"Low charge removals are times a battery is removed from a charger when its charge level is below 90%.  Ensuring a battery is fully charged prior to removing it will increase its usage time." },
            // Drilldown 2.3
            //{ 9, @"Average Charger Utilization reviews the amp draw from the charger and estimates the amount of time the charger was being utilized." },
             { 9, @"Hi charge inserts are times a battery is inserted into a charger when its charge level is above 10%.  Ensuring a battery level is below 10% prior to inserting it will increase its usage time." },
            // Drilldown 3.1
            { 10, @"Average Workstation Utilization is the percentage of time online workstations have been moving over the past 7 days." },
            // Drilldown 3.2
           // { 11, @"Hi charge removals are times a battery is removed from a cart when its charge level is still above 10%.  Ensuring a battery level is below 10% prior to removing it will increase its usage time." },
           { 11, @"Workstations Moving shows the number of In Service mobile workstations that have moved in the past two hours." },
            // Drilldown 3.3
            { 12, @"Average PC Utilization reviews the average amp drawn from your batteries while in workstations and estimates the amount of time the PC was being utilized. " },
            // Reports
            { 13, @"This report lists assets reporting any issues withing the last 30 days.  It includes a column showing the status of the issue – Resolved/Unresolved.." },
            { 14, @"This is a report of all batteries whose warranty will expire within the next 6 months." },
            { 15, @"This is a report that lists all batteries for the selected department, the lo charge insert count (number of t and hi charge removal for each battery." },
            { 16, @"This report lists all batteries allocated to a site including the utilization, full charge capacity, cycle count and charge level for each battery." },
            { 17, @"This report lists all workstations allocated to a site including the utilization level for each workstation." },
            { 18, @"This report lists all chargers allocated to a site including the utilization level for each charger." },
            { 19, @"This report lists all assets allocated to a site including its utilization level." },
            { 20, @"This is a report showing which devices in a customer’s fleet are using the most power.  The 'Big Offenders'. " },
            { 21, @"This report lists all offline devices or devices that have not communicated with the back-end for over 2 hours.  It lists the time a communication packet was received and the access points MAC address that sent the packet." },
            { 22, @"This report lists all Workstations allocated to a facility and trending usage data." },
        };
        protected void Page_Load(object sender, EventArgs e)
        {
            string name = Request.QueryString["MessageID"];
            if (!string.IsNullOrEmpty(name)  )
            {
                int id = Convert.ToInt32(name);
                if( mMessageTable.ContainsKey(id) )
                {
                    this.Label1.Text = mMessageTable[id];
                }
            }
        }
   }
}