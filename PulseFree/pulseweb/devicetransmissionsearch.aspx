﻿<%@ Page Title=""  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" ClientIDMode="Predictable" CodeBehind="devicetransmissionsearch.aspx.cs" Inherits="PulseWeb.devicetransmissionsearch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/modules/header.ascx" TagPrefix="uc1" TagName="header" %>


<asp:Content runat="server" ContentPlaceHolderID="head">
    <script language="javascript" type="text/javascript" src="scripts/RadGridCookies.js"></script>
    <link href="css/loading.css" type="text/css" rel="Stylesheet" />
    <link rel="Stylesheet" type="text/css" href="css/trending.css" /> 
<style type="text/css">
        body {
          overflow:auto;
        }
        /*div{
            zoom: 90%;
        }*/
        .Chart1
        {
            position:relative;
            left:0px;
            top:1px;
        }
           .Chart2
        {
            position:relative;
            left:10px;
            top:140px;
        }
        .zoom {
         zoom: .9;
            }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--  <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>  
     <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
           
        </script> 
    </telerik:RadCodeBlock>  --%> 
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxPanel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>                
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlbusinessUnit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkSelect">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgrdParent">
                <UpdatedControls>       
                    <telerik:AjaxUpdatedControl ControlID="rgrdParent" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgrdSessions">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgrdSessions" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>       
        </AjaxSettings>           
    </telerik:RadAjaxManager>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1" ClientEvents-OnRequestStart="onRequestStart" >
        <h1>   Data History</h1>           
        <asp:HiddenField ID="hidEquipmentType" runat="server" />
        <asp:HiddenField ID="hidSource" runat="server" />   
        <div style="height:10px;"></div>  
        <asp:Panel runat="server" ID="Panel1">
        <div style="width:800px;margin:auto;">
            <div class="uiBackShort">
                <div runat="server" ID="decorationZone" >
                    <center>
                        <table cellpadding="6" width="750px">
                            <tr>
                                <td colspan="2" align="right" valign="middle"> 
                                    <span class="Text">Serial #, Asset #, MAC Addr or IP</span>                                                                           
                                    <asp:TextBox ID="txtSearchColumn1" runat="server" Width="185px" Height="18px" Font-Size="X-Small" ></asp:TextBox>
                                </td>
                                <td align="right" valign="bottom">
                                    <span class="Text">Equipment Type</span>
                                </td>
                                <td valign="bottom">
                                    <asp:DropDownList ID="ddlDeviceType" runat="server" Width="111px" Height="22px" >
                                        <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Workstation" Value="Workstation"></asp:ListItem>
                                        <asp:ListItem Text="Charger" Value="Charger"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>  
                                <tr>
                                <td colspan="2" align="right" valign="top">
                                        <div class="df_facility_combo">
                                              <span class="Text">Facility Name</span> 
                                    <telerik:RadDropDownList ID="ddlSite" runat="server"    OnSelectedIndexChanged="ddlSite_SelectedIndexChanged" DataValueField="IDSite" DataTextField="SiteDescription" Width="296" OnPreRender="ddlSites_PreRender" EnableEmbeddedSkins="true" >
                                    </telerik:RadDropDownList>
                                </div>
                                 <%--   <asp:SqlDataSource ID="dsSite" runat="server" ProviderName="System.Data.SqlClient"
                                        SelectCommand="spGetSitesByUserName" SelectCommandType="StoredProcedure"
                                        ConnectionString='<%$ConnectionStrings:PulseConnection%>'>
                                        <SelectParameters>
                                            <asp:SessionParameter Name="IDSite" Type="String" SessionField="IDSite" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>--%>
                                </td>                                   
                                <td align="right" valign="middle">
                                    <span class="Text">Location Info</span>
                                </td>
                                <td>                                                         
                                    <asp:DropDownList 
                                        ID="ddlDeptWFOA" 
                                        runat="server"                                         
                                        Width="111px" 
                                        Height="22px"
                                        AutoPostBack="true"                                        
                                        CssClass="DepartmentName" 
                                        OnSelectedIndexChanged="ddlDeptWFOA_SelectedIndexChanged">
                                        <asp:ListItem Text="All" Value="Choose"></asp:ListItem>
                                        <asp:ListItem Text="Department(s)" Value="Department(s)"></asp:ListItem>
                                        <asp:ListItem Text="Wing(s)" Value="Wing(s)"></asp:ListItem>
                                        <asp:ListItem Text="Floor(s)" Value="Floor(s)"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                    </asp:DropDownList>                                                                     
                                </td>
                               </tr>                               
                                <tr>
                                <td align="right" valign="bottom">
                                    <span class="Text">Start Date</span>
                                    <telerik:RadDateTimePicker                                      
                                        ID="datetimeStart" 
                                        runat="server"                                        
                                        onkeypress="return IgnoreEnterKey(event)">
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td align="right" valign="bottom">
                                    <span class="Text">End Date</span>
                                    <telerik:RadDateTimePicker 
                                        ID="datetimeEnd" 
                                        runat="server"                                                                             
                                        onkeypress="return IgnoreEnterKey(event)"  >
                                        <TimeView CellSpacing="-1">
                                        </TimeView>
                                        <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                        <Calendar EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                                        </Calendar>
                                        <DateInput DateFormat="M/d/yyyy" DisplayDateFormat="M/d/yyyy" LabelWidth="40%" SelectedDate="02/08/2016 01:43:25" value="2/8/2016">
                                            <EmptyMessageStyle Resize="None" />
                                            <ReadOnlyStyle Resize="None" />
                                            <FocusedStyle Resize="None" />
                                            <DisabledStyle Resize="None" />
                                            <InvalidStyle Resize="None" />
                                            <HoveredStyle Resize="None" />
                                            <EnabledStyle Resize="None" />
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                </telerik:RadDateTimePicker>
                                </td>
                                <td align="right" valign="top">
                                    <div style="padding-top:3px;"></div>
                                    <span class="Text">Session Type</span>
                                    </td>
                                <td valign="top">
                                    <asp:DropDownList ID="ddlTransmissionType" runat="server" Width="111px" Height="22px" >
                                        <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Session" Value="Session"></asp:ListItem>
                                        <asp:ListItem Text="NonSession" Value="NonSession"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <hr />
                                    <br />
                                    <asp:Label ID="lblFeedback" CssClass="Feedback" runat="server" Text=""></asp:Label>
                                    <div style="padding: 11px 0 11px 0;display:inline;text-align:left;color:Gray;">   
                                        <asp:CheckBoxList ID="cblDeptWFOA" runat="server" RepeatLayout="Table" CellSpacing="12"
                                            RepeatColumns="4" RepeatDirection="Horizontal" ForeColor="Gray" CellPadding="8" Font-Size="Small">
                                        </asp:CheckBoxList>   
                                                                                   
                                    </div>                   
                                    </td>
                            </tr>
                            <tr>
                                <td align="left"><asp:CheckBox ID="chkSelect" runat="server" Visible="false" Text="UnSelect All" Checked="false" AutoPostBack="true" 
                                    OnCheckedChanged="chkSelect_CheckedChanged"/>
                                </td>
                                <td colspan="4" align="right" valign="middle">   
                                    <telerik:RadButton ID="RadButton1" runat="server" Font-Bold="True" OnClick="btnSearch_Click"  Text="Search Pulse">
                                    </telerik:RadButton>
                                </td>
                            </tr>
                        </table>
                    </center>                   
                </div>               
            </div>
        </div>
 
    <asp:Panel runat="server" ID="pnlGrid" style="background-color:#EEEEEE; margin-top:33px;">
   
        <telerik:RadGrid 
        ID="rgrdParent" 
        runat="server"
        AllowPaging="True" 
        AllowSorting="True"   
        AllowColumnHiding="True"         
        AutoGenerateColumns="False"        
        ClientSettings-ClientEvents-OnColumnShowing="ColumnShowing"
        ClientSettings-ClientEvents-OnColumnHiding="ColumnHiding"             
        OnNeedDataSource="rgrdParent_NeedDataSource"
        OnGroupsChanging="rgrdParent_GroupsChanging"        
        OnItemDataBound="AnyRadGrid_ItemDataBound"         
        OnPreRender="rgrdParent_PreRender"                    
        ShowGroupPanel="True"
        PageSize="96"
        Width="100%" GroupPanelPosition="Top" OnPageIndexChanged="rgrdParent_PageIndexChanged">
        <ClientSettings 
            AllowColumnHide="true" 
            AllowDragToGroup="true"
            AllowColumnsReorder="true"                 
            ReorderColumnsOnClient="true"
            Selecting-AllowRowSelect="true"
            Scrolling-UseStaticHeaders="false"
            Scrolling-AllowScroll="false"  
            Scrolling-ScrollHeight="403">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnColumnHiding="ColumnHiding" OnColumnShowing="ColumnShowing" />
            <Scrolling ScrollHeight="403px" />
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="true" />
        <ExportSettings HideStructureColumns="true" 
            Excel-Format="ExcelML" 
            ExportOnlyData="true" 
            OpenInNewWindow="true" 
            IgnorePaging="true">
            <Excel Format="ExcelML" />
         </ExportSettings>            
        <MasterTableView 
            Width="100%"
            ItemStyle-Wrap="false"
            GroupLoadMode="Client"           
            GroupsDefaultExpanded="false"
            AllowAutomaticInserts="false"                             
            EnableHeaderContextMenu="true">
            <RowIndicatorColumn Visible="False">
            </RowIndicatorColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="Status" HeaderText="Session Status" UniqueName="Status">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DeviceTypeDescription" HeaderText="Device Type" ItemStyle-Width="45px" UniqueName="DeviceTypeDescription">
                    <ItemStyle Width="45px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SiteDescription" HeaderText="Facility" ItemStyle-Width="65px" UniqueName="SiteDescription">
                    <ItemStyle Width="65px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SiteDescription_Radar" HeaderText="Radar Facility" ItemStyle-Width="65px" UniqueName="SiteDescription_Radar">
                    <ItemStyle Width="65px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SiteDescription_Owner" Display="false" HeaderText="Owner Facility" ItemStyle-Width="65px" UniqueName="SiteDescription_Owner" Visible="false">
                    <ItemStyle Width="65px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SiteDescription_AP" HeaderText="AP Facility" ItemStyle-Width="65px" UniqueName="SiteDescription_AP">
                    <ItemStyle Width="65px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Department" HeaderText="Department" ItemStyle-Width="65px" UniqueName="Department">
                    <ItemStyle Width="65px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="true" DataField="CreatedDateUTC" HeaderText="Created" UniqueName="CreatedDate">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DeviceSerialNumber" HeaderText="Dev SN" HeaderTooltip="Device Serial Number" UniqueName="DeviceSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatterySerialNumber" HeaderText="Batt SN" HeaderTooltip="Battery Serial Number" UniqueName="BatterySerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AssetNumber" Display="false" HeaderText="Asset Number" HeaderTooltip="Asset Number" UniqueName="AssetNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Notes" Display="False"  HeaderText="Notes" UniqueName="Notes">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Floor" Display="false" HeaderText="Floor" HeaderTooltip="Floor" UniqueName="Floor">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Wing" Display="false" HeaderText="Wing" HeaderTooltip="Wing" UniqueName="Wing">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Other" Display="false" HeaderText="Other" HeaderTooltip="Other" UniqueName="Other">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AIOSerialNumber" Display="false" HeaderText="PC Serial #" UniqueName="AIOSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CartSerialNumber" Display="false" HeaderText="Cart Serial #" UniqueName="CartSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DCMonitorSerialNumber" Display="false" HeaderText="DC Monitor Serial #" UniqueName="DCMonitorSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ActivityDescription" HeaderText="Activity" UniqueName="ActivityDescription">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DeviceType" HeaderText="Device Type" UniqueName="DeviceType" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay" HeaderText="Bay" UniqueName="Bay">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FullChargeCapacity" HeaderText="FCC" HeaderTooltip="Full Charge Capacity" UniqueName="FullChargeCapacity">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Cap" HeaderText="Capacity" HeaderTooltip="Capacity rating" UniqueName="Performance">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="Voltage" HeaderText="V" HeaderTooltip="Voltage" UniqueName="Voltage">
                    <ItemTemplate>
                        <%#Convert.ToInt32(Eval("Voltage")) * 0.001 %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="MeasuredVoltage" HeaderText="M-V" HeaderTooltip="Measured Voltage" UniqueName="MeasuredVoltage">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="Amps" HeaderText="Amp" UniqueName="Amps">
                    <ItemTemplate>
                        <%#Convert.ToInt32(Eval("Amps")) * 0.001 %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="Temperature" HeaderText="Temp" UniqueName="Temperature">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ChargeLevel" HeaderText="Charge %" UniqueName="ChargeLevel">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CycleCount" HeaderText="Cycle" UniqueName="CycleCount">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MaxCycleCount" HeaderText="Max Cycles" UniqueName="MaxCycleCount">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="VoltageCell1" HeaderText="VCell1" UniqueName="VoltageCell1">
                    <ItemTemplate>
                        <%#Convert.ToInt32(Eval("VoltageCell1")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="VoltageCell2" HeaderText="VCell2" UniqueName="VoltageCell2">
                    <ItemTemplate>
                        <%#Convert.ToInt32(Eval("VoltageCell2")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="VoltageCell3" HeaderText="VCell3" UniqueName="VoltageCell3">
                    <ItemTemplate>
                        <%#Convert.ToInt32(Eval("VoltageCell3")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="FETStatus" HeaderText="FET" HeaderTooltip="FET Status" UniqueName="FETStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RemainingTime" HeaderText="Remain Time" UniqueName="RemainingTime">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatteryName" HeaderText="Batt Name" UniqueName="BatteryName">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Efficiency" HeaderText="Eff" HeaderTooltip="Efficiency" UniqueName="Efficiency">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ControlBoardRevision" HeaderText="Control Rev" HeaderTooltip="Control Revision" UniqueName="ControlBoardRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LCDRevision" HeaderText="LCD Rev" HeaderTooltip="LCD Revision" UniqueName="LCDRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HolsterChargerRevision" HeaderText="Holster Rev" HeaderTooltip="Holster Charger Revision" UniqueName="HolsterChargerRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DCBoardOneRevision" HeaderText="DC1 Rev" HeaderTooltip="DC Board 1 Revision" UniqueName="DCBoardOneRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DCBoardTwoRevision" HeaderText="DC2 Rev" HeaderTooltip="DC Board 2 Revision" UniqueName="DCBoardTwoRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="WifiFirmwareRevision" HeaderText="WiFi Rev" HeaderTooltip="WiFi Firmware Revision" UniqueName="WifiFirmwareRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BayWirelessRevision" HeaderText="Ch WiFi Rev" HeaderTooltip="Charger WiFi Revision" UniqueName="BayWirelessRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay1ChargerRevision" HeaderText="Bay1 Rev" HeaderTooltip="Bay 1 Charger Revision" UniqueName="Bay1ChargerRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay2ChargerRevision" HeaderText="Bay2 Rev" HeaderTooltip="Bay 2 Charger Revision" UniqueName="Bay2ChargerRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay3ChargerRevision" HeaderText="Bay3 Rev" HeaderTooltip="Bay 3 Charger Revision" UniqueName="Bay3ChargerRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay4ChargerRevision" HeaderText="Bay4 Rev" HeaderTooltip="Bay 4 Charger Revision" UniqueName="Bay4ChargerRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardRevision" HeaderText="Med Rev" HeaderTooltip="Med Board Revision" UniqueName="MedBoardRevision">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BackupBatteryVoltage" HeaderText="BUB V" HeaderTooltip="Backup Battery Voltage" UniqueName="BackupBatteryVoltage">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BUBStatusDescription" HeaderText="BUB Status" HeaderTooltip="Backup Battery Status" UniqueName="BackupBatteryStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IsAC" HeaderText="AC" UniqueName="IsAC">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="DCUnit1BVolts" HeaderText="Chnl1V" HeaderTooltip="Channel 1 Volts" UniqueName="DCUnit1BVolts">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit1BVolts")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit1BCurrent" HeaderText="Chnl1C" HeaderTooltip="Channel 1 Current" UniqueName="DCUnit1BCurrent">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit1BCurrent")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit1AVolts" HeaderText="Chnl2V" HeaderTooltip="Channel 2 Volts" UniqueName="DCUnit1AVolts">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit1AVolts")) * 0.001 %>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit1ACurrent" HeaderText="Chnl2C" HeaderTooltip="Channel 2 Current" UniqueName="DCUnit1ACurrent">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit1ACurrent")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit2BVolts" HeaderText="Chnl3V" HeaderTooltip="Channel 3 Volts" UniqueName="DCUnit2BVolts">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit2BVolts")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit2BCurrent" HeaderText="Chnl3C" HeaderTooltip="Channel 3 Current" UniqueName="DCUnit2BCurrent">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit2BCurrent")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit2AVolts" HeaderText="Chnl4V" HeaderTooltip="Channel 4 Volts" UniqueName="DCUnit2AVolts">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit2AVolts")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn DataField="DCUnit2ACurrent" HeaderText="Chnl4C" HeaderTooltip="Channel 4 Current" UniqueName="DCUnit2ACurrent">
                    <ItemTemplate>
                        <%# Convert.ToDouble(Eval("DCUnit2ACurrent")) * 0.001%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="XValue" HeaderText="XValue" UniqueName="XValue">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="XMax" HeaderText="XMax" UniqueName="XMax">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="YValue" HeaderText="YValue" UniqueName="YValue">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="YMax" HeaderText="YMax" UniqueName="YMax">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ZValue" HeaderText="ZValue" UniqueName="ZValue">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ZMax" HeaderText="ZMax" UniqueName="ZMax">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Move" HeaderText="Move" UniqueName="Move">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatteryStatus" HeaderText="BatteryStatus" UniqueName="BatteryStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatteryErrorCode" HeaderText="Battery Err Code" UniqueName="BatteryErrorCode">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SafetyStatus" HeaderText="SafetyStatus" UniqueName="SafetyStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PermanentFailureStatus" HeaderText="PermanentFailureStatus" UniqueName="PermanentFailureStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PermanentFailureAlert" HeaderText="PermanentFailureAlert" UniqueName="PermanentFailureAlert">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatteryChargeStatus" HeaderText="BatteryChargeStatus" UniqueName="BatteryChargeStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatterySafetyAlert" HeaderText="BatterySafetyAlert" UniqueName="BatterySafetyAlert">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatteryOpStatus" HeaderText="BatteryOpStatus" UniqueName="BatteryOpStatus">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatteryMode" HeaderText="BatteryMode" UniqueName="BatteryMode">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DC1Error" HeaderText="DCAError" UniqueName="DC1Error">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DC1Status" HeaderText="DCAStatus" UniqueName="DC1Status">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DC2Error" HeaderText="DCBError" UniqueName="DC2Error">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DC2Status" HeaderText="DCBStatus" UniqueName="DC2Status">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MouseFailureNotification" HeaderText="MouseFailureNotification" UniqueName="MouseFailureNotification">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="KeyboardFailureNotification" HeaderText="KeyboardFailureNotification" UniqueName="KeyboardFailureNotification">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="WindowsShutdownNotification" HeaderText="WindowsShutdownNotification" UniqueName="WindowsShutdownNotification">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LinkQuality" HeaderText="LinkQuality" UniqueName="LinkQuality">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IP" HeaderText="IP" UniqueName="IP">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DeviceMAC" HeaderText="DeviceMAC" UniqueName="DeviceMAC">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="APMAC" HeaderText="APMAC" UniqueName="APMAC">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AccessPointDescription" HeaderText="AP Name" UniqueName="AccessPointDescription">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="WEPKey" HeaderText="WEPKey" UniqueName="WEPKey">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PassCode" HeaderText="PassCode" UniqueName="PassCode">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SSID" HeaderText="SSID" UniqueName="SSID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SparePinSwitch" HeaderText="SparePinSwitch" UniqueName="SparePinSwitch">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ControlBoardSerialNumber" HeaderText="Control SN" HeaderTooltip="Control Board Serial Number" UniqueName="ControlBoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HolsterBoardSerialNumber" HeaderText="Holster SN" HeaderTooltip="Holster Serial Number" UniqueName="HolsterBoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LCDBoardSerialNumber" HeaderText="LCD SN" HeaderTooltip="LCD Board Serial Number" UniqueName="LCDBoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DC1BoardSerialNumber" HeaderText="DC1 SN" HeaderTooltip="DC1 Board Serial Number" UniqueName="DC1BoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DC2BoardSerialNumber" HeaderText="DC2 SN" HeaderTooltip="DC2 Board Serial Number" UniqueName="DC2BoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardSerialNumber" HeaderText="Med SN" HeaderTooltip="Med Board Serial Number" UniqueName="MedBoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BayWirelessBoardSerialNumber" HeaderText="BayWirelessBoardSerialNumber" UniqueName="BayWirelessBoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay1BoardSerialNumber" HeaderText="Bay1BoardSerialNumber" UniqueName="Bay1BoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay2BoardSerialNumber" HeaderText="Bay2BoardSerialNumber" UniqueName="Bay2BoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay3BoardSerialNumber" HeaderText="Bay3BoardSerialNumber" UniqueName="Bay3BoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Bay4BoardSerialNumber" HeaderText="Bay4BoardSerialNumber" UniqueName="Bay4BoardSerialNumber">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardDrawerOpenTime" HeaderText="MedBoardDrawerOpenTime" UniqueName="MedBoardDrawerOpenTime">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardDrawerCount" HeaderText="Draw" HeaderTooltip="Med Board Drawer Count" UniqueName="MedBoardDrawerCount">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardMotorUp" HeaderText="Motor Up" HeaderTooltip="Med Board Motor Up" UniqueName="MedBoardMotorUp">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardMotorDown" HeaderText="Motor Dn" HeaderTooltip="Med Board Motor Down" UniqueName="MedBoardMotorDown">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardUnlockCount" HeaderText="Unlock" HeaderTooltip="Med Board Unlock Count" UniqueName="MedBoardUnlockCount">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardAdminPin" HeaderText="Adm PIN" HeaderTooltip="Med Board Admin PIN" UniqueName="MedBoardAdminPin">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardNarcPin" HeaderText="Narc PIN" HeaderTooltip="Med Board Narc PIN" UniqueName="MedBoardNarcPin">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardUserPin1" HeaderText="User PIN 1" HeaderTooltip="Med Board User PIN 1" UniqueName="MedBoardUserPin1">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardUserPin2" HeaderText="User PIN 2" HeaderTooltip="Med Board User PIN 2" UniqueName="MedBoardUserPin2">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardUserPin3" HeaderText="User PIN 3" HeaderTooltip="Med Board User PIN 3" UniqueName="MedBoardUserPin3">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardUserPin4" HeaderText="User PIN 4" HeaderTooltip="Med Board User PIN 4" UniqueName="MedBoardUserPin4">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedBoardUserPin5" HeaderText="User PIN 5" HeaderTooltip="Med Board User PIN 5" UniqueName="MedBoardUserPin5">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SessionRecordType" HeaderText="SessionRecordType" UniqueName="SessionRecordType">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowFiltering="true" DataField="ExaminedDateUTC" HeaderText="Examined Date" UniqueName="ExaminedDate">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SourceIPAddress" HeaderText="Transmission IP" UniqueName="SourceIPAddress">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="QueryStringID" HeaderText="QueryStringID" UniqueName="QueryStringID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SessionID" HeaderText="SessionID" UniqueName="SessionID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CommandCode" HeaderText="CC" HeaderTooltip="Command Code" UniqueName="CommandCode">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ROW_ID" HeaderText="ROW_ID" UniqueName="ROW_ID">
                </telerik:GridBoundColumn>
            </Columns>
            <ItemStyle Wrap="False" />
        </MasterTableView>         
        <PagerStyle Mode="NextPrevAndNumeric" />
    </telerik:RadGrid>
        <telerik:RadGrid 
        ID="rgrdSessions" 
        runat="server"
        AllowPaging="True" 
        AllowSorting="True" 
        ShowGroupPanel="True" 
        AutoGenerateColumns="False"
        ClientSettings-ClientEvents-OnColumnShowing="ColumnShowing" 
        ClientSettings-ClientEvents-OnColumnHiding="ColumnHiding"
        OnNeedDataSource="rgrdSessions_NeedDataSource"  
        OnItemCommand="rgrdSessions_ItemCommand"      
        OnPreRender="rgrdSessions_PreRender"
        Visible="False"
        PageSize="48"
        Width="100%" GroupPanelPosition="Top">
        <ClientSettings 
            Selecting-AllowRowSelect="true"
            ReorderColumnsOnClient="true" 
            AllowColumnsReorder="true" 
            AllowDragToGroup="true"
            AllowColumnHide="true">
        </ClientSettings>
        <GroupingSettings ShowUnGroupButton="true" />
          <ExportSettings HideStructureColumns="true" 
            Excel-Format="ExcelML" 
            ExportOnlyData="true"
            OpenInNewWindow="true" 
            IgnorePaging="true">
            </ExportSettings>
        <MasterTableView 
            Width="100%"
            TableLayout="Fixed"
            DataKeyNames="ROW_ID"
            ItemStyle-Wrap="false" 
            Summary="Sessions Table"
            AllowAutomaticDeletes="false" 
            AllowAutomaticInserts="false"
            AllowAutomaticUpdates="false"
            GroupsDefaultExpanded="false" 
            EnableHeaderContextMenu="true"             
            EditFormSettings-EditFormType="Template">
            <NestedViewTemplate>
                <asp:Label ID="lblROW_ID" runat="server" Text='<%#Eval("ROW_ID") %>' Visible="false"></asp:Label>
                <telerik:RadGrid 
                    ID="rgrdChild"
                    runat="server" 
                    AllowSorting="true"
                    EnableViewState="false" 
                    AutoGenerateColumns="false" 
                    ClientSettings-ClientEvents-OnColumnShowing="ColumnShowing"
                    ClientSettings-ClientEvents-OnColumnHiding="ColumnHiding" 
                    OnNeedDataSource="rgrdChild_NeedDataSource"                        
                    OnItemDataBound="AnyRadGrid_ItemDataBound"
                    Visible="false"                         
                    PageSize="48" 
                    Width="99%">
                    <ClientSettings 
                        AllowColumnHide="true" 
                        AllowColumnsReorder="true"                             
                        Scrolling-AllowScroll="true" 
                        Scrolling-ScrollHeight="403" 
                        ReorderColumnsOnClient="true"
                        Scrolling-UseStaticHeaders="false"
                        Selecting-AllowRowSelect="true">
                    </ClientSettings>
                    <ExportSettings 
                        HideStructureColumns="true" 
                        OpenInNewWindow="true" 
                        Excel-Format="ExcelML"
                        ExportOnlyData="true"
                        IgnorePaging="true" 
                        FileName="CAST_SearchResults_CompletedSessionsOnly">
                    </ExportSettings>
                    <MasterTableView 
                        Width="100%"
                        TableLayout="Fixed"
                        ItemStyle-Wrap="false" 
                        CommandItemDisplay="None"
                        EnableHeaderContextMenu="true">
                        <Columns>                        
                            <telerik:GridBoundColumn UniqueName="CreatedDate" DataField="CreatedDateUTC" HeaderText="Created" AllowFiltering="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DeviceSerialNumber" DataField="DeviceSerialNumber" HeaderText="Dev SN" HeaderTooltip="Device Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatterySerialNumber" DataField="BatterySerialNumber" HeaderText="Batt SN" HeaderTooltip="Battery Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="AssetNumber" DataField="AssetNumber" HeaderText="Asset Number" HeaderTooltip="Asset Number" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Notes" DataField="Notes" HeaderText="Notes" HeaderTooltip="Notes" Display="true">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Floor" DataField="Floor" HeaderText="Floor" HeaderTooltip="Floor" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Wing" DataField="Wing" HeaderText="Wing" HeaderTooltip="Wing" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Other" DataField="Other" HeaderText="Other" HeaderTooltip="Other" Display="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ActivityDescription" DataField="ActivityDescription" HeaderText="Activity">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DeviceType" DataField="DeviceType" HeaderText="Device Type" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay" DataField="Bay" HeaderText="Bay">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="FullChargeCapacity" DataField="FullChargeCapacity" HeaderText="FCC" HeaderTooltip="Full Charge Capacity">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="Voltage" DataField="Voltage" HeaderText="V" HeaderTooltip="Voltage">
                                <ItemTemplate><%#Convert.ToInt32(Eval("Voltage")) * 0.001 %></ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn UniqueName="MeasuredVoltage" DataField="MeasuredVoltage" HeaderText="M-V" HeaderTooltip="Measured Voltage">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="Amps" DataField="Amps" HeaderText="Amp">
                                <ItemTemplate><%#Convert.ToInt32(Eval("Amps")) * 0.001 %></ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn UniqueName="Temperature" DataField="Temperature" HeaderText="Temp">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ChargeLevel" DataField="ChargeLevel" HeaderText="Charge %">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="CycleCount" DataField="CycleCount" HeaderText="Cycle">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MaxCycleCount" DataField="MaxCycleCount" HeaderText="Max Cycles">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn UniqueName="VoltageCell1" DataField="VoltageCell1" HeaderText="VCell1">
                                <ItemTemplate><%#Convert.ToInt32(Eval("VoltageCell1")) * 0.001%></ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="VoltageCell2" DataField="VoltageCell2" HeaderText="VCell2">
                                <ItemTemplate><%#Convert.ToInt32(Eval("VoltageCell2")) * 0.001%></ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn UniqueName="VoltageCell3" DataField="VoltageCell3" HeaderText="VCell3">
                                <ItemTemplate><%#Convert.ToInt32(Eval("VoltageCell3")) * 0.001%></ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn UniqueName="FETStatus" DataField="FETStatus" HeaderText="FET" HeaderTooltip="FET Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="RemainingTime" DataField="RemainingTime" HeaderText="Remain Time">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatteryName" DataField="BatteryName" HeaderText="Batt Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Efficiency" DataField="Efficiency" HeaderText="Eff" HeaderTooltip="Efficiency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ControlBoardRevision" DataField="ControlBoardRevision" HeaderText="Control Rev" HeaderTooltip="Control Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="LCDRevision" DataField="LCDRevision" HeaderText="LCD Rev" HeaderTooltip="LCD Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="HolsterChargerRevision" DataField="HolsterChargerRevision"  HeaderText="Holster Rev" HeaderTooltip="Holster Charger Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCBoardOneRevision" DataField="DCBoardOneRevision" HeaderText="DC1 Rev" HeaderTooltip="DC Board 1 Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCBoardTwoRevision" DataField="DCBoardTwoRevision" HeaderText="DC2 Rev" HeaderTooltip="DC Board 2 Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="WifiFirmwareRevision" DataField="WifiFirmwareRevision" HeaderText="WiFi Rev" HeaderTooltip="WiFi Firmware Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BayWirelessRevision" DataField="BayWirelessRevision" HeaderText="Ch WiFi Rev" HeaderTooltip="Charger WiFi Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay1ChargerRevision" DataField="Bay1ChargerRevision" HeaderText="Bay1 Rev" HeaderTooltip="Bay 1 Charger Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay2ChargerRevision" DataField="Bay2ChargerRevision" HeaderText="Bay2 Rev" HeaderTooltip="Bay 2 Charger Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay3ChargerRevision" DataField="Bay3ChargerRevision" HeaderText="Bay3 Rev" HeaderTooltip="Bay 3 Charger Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay4ChargerRevision" DataField="Bay4ChargerRevision" HeaderText="Bay4 Rev" HeaderTooltip="Bay 4 Charger Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardRevision" DataField="MedBoardRevision" HeaderText="Med Rev" HeaderTooltip="Med Board Revision">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BackupBatteryVoltage" DataField="BackupBatteryVoltage" HeaderText="BUB V" HeaderTooltip="Backup Battery Voltage">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BackupBatteryStatus" DataField="BUBStatusDescription" HeaderText="BUB Status" HeaderTooltip="Backup Battery Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="IsAC" DataField="IsAC" HeaderText="AC">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn UniqueName="DCUnit1BVolts" DataField="DCUnit1BVolts" HeaderText="Chnl1V" HeaderTooltip="Channel 1 Volts">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit1BCurrent" DataField="DCUnit1BCurrent" HeaderText="Chnl1C" HeaderTooltip="Channel 1 Current">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit1AVolts" DataField="DCUnit1AVolts" HeaderText="Chnl2V" HeaderTooltip="Channel 2 Volts">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit1ACurrent" DataField="DCUnit1ACurrent" HeaderText="Chnl2C" HeaderTooltip="Channel 2 Current">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit2BVolts" DataField="DCUnit2BVolts" HeaderText="Chnl3V" HeaderTooltip="Channel 3 Volts">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit2BCurrent" DataField="DCUnit2BCurrent" HeaderText="Chnl3C" HeaderTooltip="Channel 3 Current">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit2AVolts" DataField="DCUnit2AVolts" HeaderText="Chnl4V" HeaderTooltip="Channel 4 Volts">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DCUnit2ACurrent" DataField="DCUnit2ACurrent" HeaderText="Chnl4C" HeaderTooltip="Channel 4 Current">
                            </telerik:GridBoundColumn>                            
                            <telerik:GridBoundColumn UniqueName="XValue" DataField="XValue" HeaderText="XValue">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="XMax" DataField="XMax" HeaderText="XMax">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="YValue" DataField="YValue" HeaderText="YValue">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="YMax" DataField="YMax" HeaderText="YMax">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ZValue" DataField="ZValue" HeaderText="ZValue">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ZMax" DataField="ZMax" HeaderText="ZMax">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Move" DataField="Move" HeaderText="Move">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatteryStatus" DataField="BatteryStatus" HeaderText="BatteryStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatteryErrorCode" DataField="BatteryErrorCode" HeaderText="Battery Err Code">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="SafetyStatus" DataField="SafetyStatus" HeaderText="SafetyStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="PermanentFailureStatus" DataField="PermanentFailureStatus" HeaderText="PermanentFailureStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="PermanentFailureAlert" DataField="PermanentFailureAlert" HeaderText="PermanentFailureAlert">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatteryChargeStatus" DataField="BatteryChargeStatus" HeaderText="BatteryChargeStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatterySafetyAlert" DataField="BatterySafetyAlert" HeaderText="BatterySafetyAlert">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatteryOpStatus" DataField="BatteryOpStatus" HeaderText="BatteryOpStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BatteryMode" DataField="BatteryMode" HeaderText="BatteryMode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DC1Error" DataField="DC1Error" HeaderText="DCAError">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DC1Status" DataField="DC1Status" HeaderText="DCAStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DC2Error" DataField="DC2Error" HeaderText="DCBError">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DC2Status" DataField="DC2Status" HeaderText="DCBStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MouseFailureNotification" DataField="MouseFailureNotification" HeaderText="MouseFailureNotification">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="KeyboardFailureNotification" DataField="KeyboardFailureNotification" HeaderText="KeyboardFailureNotification">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="WindowsShutdownNotification" DataField="WindowsShutdownNotification" HeaderText="WindowsShutdownNotification">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="LinkQuality" DataField="LinkQuality" HeaderText="LinkQuality">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="IP" DataField="IP" HeaderText="IP">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DeviceMAC" DataField="DeviceMAC" HeaderText="DeviceMAC">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="APMAC" DataField="APMAC" HeaderText="APMAC">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="AccessPointDescription" DataField="AccessPointDescription" HeaderText="AP Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="LastTransmissionStatus" DataField="LastTransmissionStatus" HeaderText="Tran Stat" HeaderTooltip="Last Transmission Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="AuthType" DataField="AuthType" HeaderText="AuthType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ChannelNumber" DataField="ChannelNumber" HeaderText="ChannelNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="PortNumber" DataField="PortNumber" HeaderText="PortNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DHCP" DataField="DHCP" HeaderText="DHCP">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="WEPKey" DataField="WEPKey" HeaderText="WEPKey">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="PassCode" DataField="PassCode" HeaderText="PassCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="StaticIP" DataField="StaticIP" HeaderText="StaticIP">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="SSID" DataField="SSID" HeaderText="SSID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="SparePinSwitch" DataField="SparePinSwitch" HeaderText="SparePinSwitch">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ControlBoardSerialNumber" DataField="ControlBoardSerialNumber" HeaderText="Control SN" HeaderTooltip="Control Board Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="HolsterBoardSerialNumber" DataField="HolsterBoardSerialNumber" HeaderText="Holster SN" HeaderTooltip="Holster Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="LCDBoardSerialNumber" DataField="LCDBoardSerialNumber" HeaderText="LCD SN" HeaderTooltip="LCD Board Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DC1BoardSerialNumber" DataField="DC1BoardSerialNumber" HeaderText="DC1 SN" HeaderTooltip="DC1 Board Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="DC2BoardSerialNumber" DataField="DC2BoardSerialNumber" HeaderText="DC2 SN" HeaderTooltip="DC2 Board Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardSerialNumber" DataField="MedBoardSerialNumber"  HeaderText="Med SN" HeaderTooltip="Med Board Serial Number">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="BayWirelessBoardSerialNumber" DataField="BayWirelessBoardSerialNumber" HeaderText="BayWirelessBoardSerialNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay1BoardSerialNumber" DataField="Bay1BoardSerialNumber" HeaderText="Bay1BoardSerialNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay2BoardSerialNumber" DataField="Bay2BoardSerialNumber" HeaderText="Bay2BoardSerialNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay3BoardSerialNumber" DataField="Bay3BoardSerialNumber" HeaderText="Bay3BoardSerialNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="Bay4BoardSerialNumber" DataField="Bay4BoardSerialNumber" HeaderText="Bay4BoardSerialNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardDrawerOpenTime" DataField="MedBoardDrawerOpenTime" HeaderText="MedBoardDrawerOpenTime">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardDrawerCount" DataField="MedBoardDrawerCount" HeaderText="Draw" HeaderTooltip="Med Board Drawer Count">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardMotorUp" DataField="MedBoardMotorUp" HeaderText="Motor Up" HeaderTooltip="Med Board Motor Up">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardMotorDown" DataField="MedBoardMotorDown" HeaderText="Motor Dn" HeaderTooltip="Med Board Motor Down">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardUnlockCount" DataField="MedBoardUnlockCount" HeaderText="Unlock" HeaderTooltip="Med Board Unlock Count">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardAdminPin" DataField="MedBoardAdminPin" HeaderText="Adm PIN" HeaderTooltip="Med Board Admin PIN">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardNarcPin" DataField="MedBoardNarcPin" HeaderText="Narc PIN" HeaderTooltip="Med Board Narc PIN">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardUserPin1" DataField="MedBoardUserPin1" HeaderText="User PIN 1" HeaderTooltip="Med Board User PIN 1">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardUserPin2" DataField="MedBoardUserPin2" HeaderText="User PIN 2" HeaderTooltip="Med Board User PIN 2">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardUserPin3" DataField="MedBoardUserPin3" HeaderText="User PIN 3" HeaderTooltip="Med Board User PIN 3">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardUserPin4" DataField="MedBoardUserPin4" HeaderText="User PIN 4" HeaderTooltip="Med Board User PIN 4">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="MedBoardUserPin5" DataField="MedBoardUserPin5" HeaderText="User PIN 5" HeaderTooltip="Med Board User PIN 5">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="SessionRecordType" DataField="SessionRecordType" HeaderText="SessionRecordType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ExaminedDate" DataField="ExaminedDateUTC" HeaderText="ExaminedDate">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="QueryStringID" DataField="QueryStringID" HeaderText="QueryStringID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="SessionID" DataField="SessionID" HeaderText="SessionID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="CommandCode" DataField="CommandCode" HeaderText="CC"  HeaderTooltip="Command Code">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn UniqueName="ROW_ID" DataField="ROW_ID" HeaderText="ROW_ID">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </NestedViewTemplate>
            <Columns>
                <telerik:GridBoundColumn UniqueName="ROW_ID" Visible="False" DataField="ROW_ID">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="StartDate" DataField="StartDateUTC" HeaderText="Batt Insert" AllowFiltering="true">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="EndDate" DataField="EndDateUTC" HeaderText="Batt Removed" AllowFiltering="true">                
                </telerik:GridBoundColumn>              
                <telerik:GridBoundColumn UniqueName="DeviceTypeDescription" DataField="DeviceTypeDescription" HeaderText="Type" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="DeviceSerialNumber" DataField="DeviceSerialNumber" HeaderText="Device SN">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="AssetNumber" DataField="AssetNumber" HeaderText="Asset Number" HeaderTooltip="Asset Number" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="BatterySerialNumber" DataField="BatterySerialNumber" HeaderText="Batt SN">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="AIOSerialNumber" DataField="AIOSerialNumber" HeaderText="PC Serial #" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="CartSerialNumber" DataField="CartSerialNumber"  HeaderText="Cart Serial #" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="DCMonitorSerialNumber" DataField="DCMonitorSerialNumber" HeaderText="DC Monitor Serial #" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="AvgAmpDraw" DataField="AvgAmpDraw" HeaderText="Avg Current Draw">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="NumberOfMoves" DataField="NumberOfMoves" HeaderText="Moves">
                </telerik:GridBoundColumn>           
                <telerik:GridBoundColumn UniqueName="SessLen" DataField="SessLen" HeaderText="Duration" AllowFiltering="true">
                </telerik:GridBoundColumn>            
                <telerik:GridBoundColumn UniqueName="StartChargeLevel" DataField="StartChargeLevel" HeaderText="Chg % on insert" HeaderTooltip="Charge level when battery was inserted" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="EndChargeLevel" DataField="EndChargeLevel" HeaderText="Chg % on removal" HeaderTooltip="Charge level when battery was removed" Display="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="BatteryName" DataField="BatteryName" HeaderText="Battery Name" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="SiteDescription" DataField="SiteDescription" HeaderText="Facility">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="Department" DataField="Department" HeaderText="Department">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn UniqueName="PacketCount" DataField="PacketCount" HeaderText="Packets">
                </telerik:GridBoundColumn>
            </Columns>           
        </MasterTableView>           
        <PagerStyle Mode="NextPrevAndNumeric" />
    </telerik:RadGrid>  
    <div id="divPersistSettings">           
        <asp:Button ID="btnPersistSettings" runat="server" Text="Save grid customization" OnClick="btnPersistSettings_Click" Visible="false" />
        <asp:Button ID="btnRemoveCustomization" runat="server" Text="Remove grid customization" OnClick="btnRemoveCustomization_Click" Visible="false" />
         <asp:Button ID="btnExportToExcel" runat="server" Text="Download" OnClick="btnExportToExcel_Click" Visible="false" />
         <asp:Button ID="btnExcelExport" runat="server" Text="Download" Visible="false" />
    </div>
     </asp:Panel>
                </asp:Panel> 
       <%-- <input type="button" title="test" value="test" id="test" onclick="doTest()" visible="false" />--%>
        <script language="javascript" type="text/javascript">
            function doTest() {
                var grid = document.getElementById("ctl00_MainContent_rgrdParent_ctl00");
                var gridrows = grid.getElementsByTagName("tr");
                for (var i = 0; i < gridrows.length; i++) {
                    var fieldpos = 15;
                    if (gridrows[i].getElementsByTagName("td")[fieldpos] != null && gridrows[i].getElementsByTagName("td")[fieldpos].outerText > "4.1") {
                        gridrows[i].getElementsByTagName("td")[fieldpos].className = "highlit";
                    }
                }
            }
            $(document).ready(function () {
                // focus on the first input field or textarea
                $(":input:visible:enabled").each(function () {
                    if (($(this).attr('type') == 'text') && ($(this).is('input'))) {
                        $(this).focus();
                        return false;
                    }
                    if ($(this).is('textarea')) {
                        $(this).focus();
                        return false;
                    }
                });
            });
        </script>
    </telerik:RadAjaxPanel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" InitialDelayTime="0" Transparency="5">
        <div id="progressBackgroundFilter"></div>
        <div id="processMessage">
            <img alt="Loading..." src="images/blue/loading.gif" />
        </div>
    </telerik:RadAjaxLoadingPanel>
   
</asp:Content>
