﻿<%@ Page Language="C#" Inherits="PulseWeb.Master" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }
</script>
      
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Telerik ASP.NET Example</title>
        <style type="text/css">
        .borderCssClass div {
            border: 1px thin blue;
            text-align: center;
        }
            .borderCssClass {}
    </style>
  
    <link href="styles.css" rel="stylesheet" />
    
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="stylesheet" href="//ajax.aspnetcdn.com/ajax/bootstrap/3.2.0/css/bootstrap.min.css" />
  <link href="/css/font-awesome.css" rel="stylesheet" />
  <link href="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/overcast/jquery-ui.css" rel="stylesheet" />
  <link href="/css/site.css" rel="stylesheet" />
  <link href="/css/menu.css" rel="stylesheet" />
  <link href="/css/notifictionmenu.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link href="css/font-awesome.css" rel="stylesheet" />
<link type="text/css" href="Kendo/styles/examples-offline.css" rel="stylesheet"/>
<link type="text/css" href="Kendo/styles/kendo.common.min.css" rel="stylesheet"/>
<link type="text/css" href="Kendo/styles/kendo.rtl.min.css" rel="stylesheet"/>
<link type="text/css" href="Kendo/styles/kendo.default.min.css" rel="stylesheet"/>
    <link rel="shortcut icon" type="image/x-icon" href="/Images/EIcon.ico" />
  <script type="text/javascript" src="/Kendo/js/jquery.min.js"></script>
  <script type="text/javascript" src="kendo/js/kendo.web.min.js"></script>
  <script type="text/javascript" src="/kendo/js/kendo.all.min.js"></script>
  <script type="text/javascript" src="/Scripts/Site.js?p=1"></script>
    <script type="text/javascript" src="scripts.js"></script>
</head>
 <body>
     <form id="form1" runat="server">
         <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
         </telerik:RadStyleSheetManager>
<telerik:RadPageLayout ID="RadPageLayout1" runat="server"  GridType="Fixed" CssClass="borderCssClass" Width="678px" Height="347px" EnableAjaxSkinRendering="False" EnableEmbeddedBaseStylesheet="True" EnableEmbeddedScripts="False" EnableEmbeddedSkins="False" HtmlTag="Div" RegisterWithScriptManager="True" RenderMode="Classic" ShowGrid="True">
 
                 <Rows>
            <telerik:LayoutRow  WrapperHtmlTag="Div">
                <Columns>
                    <telerik:LayoutColumn  Span="8">
                    <asp:Image ID="Image1" ImageAlign="Left" ImageUrl="~/Images/RTLS/EVO-power.png" runat="server" />
              </telerik:LayoutColumn>
                    <telerik:CompositeLayoutColumn Span="4">
                        <Rows>
                            <telerik:LayoutRow  WrapperHtmlTag="Div">
                                <Content>
                        <asp:Label runat="server" >Battery Charge Level: </asp:Label> <telerik:RadLinearGauge  runat="server" ID="RadLinearGauge1" Width="150px" Height="50px">
                <Pointer Shape="BarIndicator" Value="15">
                    <Track Opacity="0.2" />
                </Pointer>
                <Scale Min="-20" Max="50" MajorUnit="10">
                    <Labels Format="{0} degrees" />
                    <Ranges>
                        <telerik:GaugeRange Color="#2a94cb" From="-20" To="5" />
                        <telerik:GaugeRange Color="#8dcb2a " From="5" To="17" />
                        <telerik:GaugeRange Color="#ffc700" From="17" To="27" />
                        <telerik:GaugeRange Color="#ff7a00" From="27" To="35" />
                        <telerik:GaugeRange Color="#c20000" From="35" To="50" />
                    </Ranges>
                </Scale>

                        </telerik:RadLinearGauge>

                                </Content>
                      </telerik:LayoutRow>    
                            <telerik:LayoutRow  WrapperHtmlTag="Div">
                            <Content>additional content 1</Content>
               
                            <Content> additional content 2</Content>

                            </telerik:LayoutRow>
                   </Rows>
                     </telerik:CompositeLayoutColumn>
                     </Columns> 

            </telerik:LayoutRow>
              </Rows>
</telerik:RadPageLayout>   
    
     </form>
    
</body>
</html>