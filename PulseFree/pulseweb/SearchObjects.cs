﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PulseWeb.SearchObjects
{
    public class PulseSearch
    {
        public PulseSearch()
        {
            Columns = new List<DisplayColumn>();
        }

        public List<DisplayColumn> Columns { get; set; }
        public string SqlCmdText { get; set; }
        public DisplayType DisplayType { get; set; }
    }

    public class DisplayColumn
    {
        public string DataName { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
    }

    public enum DisplayType
    {
        Session, All
    }
}