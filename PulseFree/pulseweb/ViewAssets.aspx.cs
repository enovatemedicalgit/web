﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web;
using PulseWeb.API;
using PulseWeb.Model;
using PulseWeb.Utils;

namespace PulseWeb
{
    public partial class ViewAssets : BasePage
    {
        // Only able to edit the following:
        // Asset#
        // Department
        // No longer in use check
        //                    cmdText = string.Format("update Device set SerialNumber = '{1}', InvoiceNumber = '{2}', PartNumber = '{3}', Description = '{4}', IDSite = {5}, DeviceTypeID = {6}, AssetNumber = '{7}', BusinessUnitDepartmentID = {8}, Notes = '{9}', StingerNotes = '{10}', ModifiedUserID = {11}, ModifiedDateUTC = getutcdate(), Model = '{12}', Retired = {13} where ROW_ID = {0}", ROW_ID, SerialNumber, InvoiceNumber, PartNumber, Description, IDSite, DeviceTypeID, AssetNumber, DepartmentID, Notes, StingerNotes, CASTUserID, Model, Retired);
        string AssetType { get { return Session["ManageScreenAssetType"] as string; } set { Session["ManageScreenAssetType"] = value; } }
        Asset selectedAsset = null;
        const string mNoDepartment = "None";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
            string siteName = PulseSession.Instance.User.IsHQUser ? PulseSession.Instance.User.ParentCustomer.Name : api.GetSite(PulseSession.Instance.User.IDSite).Name;
            string param = Request.QueryString.ToString();
            switch (param)
            {
                case "assets": AssetType = param; AssetTypeLabel.Text = "&nbsp;&nbsp;All assets for " + siteName; break;
                case "chargers": AssetType = param; AssetTypeLabel.Text = "&nbsp;&nbsp;Chargers for " + siteName; break;
                case "batteries": AssetType = param; AssetTypeLabel.Text = "&nbsp;&nbsp;Batteries for " + siteName; break;
                case "workstations": AssetType = param; AssetTypeLabel.Text = "&nbsp;&nbsp;Workstations for " + siteName; break;

                default: AssetType = null; break;
            }

            RadGrid1.AllowSorting = true;
            RadGrid1.MasterTableView.AllowSorting = true;
            //  RadGrid1.ClientSettings.Resizing.AllowColumnResize = true;
            RadGrid1.ClientSettings.Resizing.AllowResizeToFit = true;


            //  RadGrid1.ClientSettings.Resizing.ResizeGridOnColumnResize = true;
        }

        protected void RadGrid1_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (this.AssetType == "chargers") { }
            RadGrid1.DataSource = GetDataSource(this.AssetType);
            RadGrid1.AllowSorting = true;

        }


        protected void Button1_Click(object sender, EventArgs e)
        {

            RadGrid1.MasterTableView.ExportToCSV();
        }

        protected void RadGrid1_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            GridBoundColumn boundColumn = e.Column as GridBoundColumn;
            if (boundColumn != null)
            {


                boundColumn.ReadOnly = false;
            }
            //Asset

            switch (e.Column.UniqueName)
            {
                case "gtcEdit": e.Column.Visible = AssetType != "batteries";  e.Column.Resizable = true; e.Column.HeaderStyle.Width = 55; break;
                case "gtcFind": e.Column.Visible = true; e.Column.Resizable = true; e.Column.HeaderStyle.Width = 55; break;
                case "SerialNumber": e.Column.Visible = true; e.Column.Resizable = false; e.OwnerTableView.AllowSorting = true; e.Column.HeaderStyle.Width = 110; e.Column.OrderIndex = 4; break;
                case "AssetNumber": e.Column.Visible = true; e.Column.HeaderStyle.Width = 160; e.Column.OrderIndex = 5; break;
                case "Description": e.Column.Visible = true; e.Column.HeaderStyle.Width = 170; e.Column.OrderIndex = 6; break;
                case "DepartmentName": e.Column.Visible = true; e.Column.HeaderTooltip = "Originally Assigned Department"; e.Column.HeaderStyle.Width = 110; e.Column.OrderIndex = 15; break;
                case "ActualDepartment": e.Column.Visible = true; e.Column.HeaderTooltip = "Department Asset is reporting from"; e.Column.HeaderStyle.Width = 110; e.Column.OrderIndex = 16; break;
                case "CycleCount": e.Column.Visible = AssetType == "batteries"; e.Column.HeaderStyle.Width = 70; e.Column.OrderIndex = 8; break;
                case "Temperature": e.Column.Visible = AssetType == "batteries"; e.Column.HeaderStyle.Width = 70; e.Column.OrderIndex = 9; break;
                case "ChargeLevel": e.Column.Visible = AssetType == "batteries"; e.Column.HeaderStyle.Width = 70; e.Column.OrderIndex = 7; break;
                case "LastPostDateUTC": e.Column.Visible = true; e.Column.HeaderTooltip = "Last Communication from Device to Pulse"; e.Column.HeaderStyle.Width = 140; e.Column.OrderIndex = 12; break;
                case "Floor": e.Column.Visible = true; e.Column.HeaderStyle.Width = 70; e.Column.OrderIndex = 13; break;
                case "Wing": e.Column.Visible = true; e.Column.HeaderStyle.Width = 70; e.Column.OrderIndex = 14; break;
                case "FullChargeCapacity": e.Column.Visible = AssetType == "batteries"; e.Column.HeaderStyle.Width = 85; e.Column.OrderIndex = 10; break;
                case "UtilizationLevel": e.Column.Visible = AssetType == "workstations"; e.Column.HeaderStyle.Width = 35; e.Column.OrderIndex = 18; break;
                case "AIOSerial": e.Column.Visible = AssetType == "workstations"; e.Column.HeaderStyle.Width = 100; e.Column.OrderIndex = 22; break;
                case "DCMonitorSerial": e.Column.Visible = AssetType == "workstations"; e.Column.HeaderStyle.Width = 100; e.Column.OrderIndex = 20; break;
                case "CartSerial": e.Column.Visible = AssetType == "workstations"; e.Column.HeaderStyle.Width = 100; e.Column.OrderIndex = 21; break;
                case "Notes": e.Column.Visible = true; e.Column.Exportable = true; e.Column.HeaderStyle.Width = 130; e.Column.OrderIndex = 19; break;
                case "Other": e.Column.Visible = true; e.Column.HeaderStyle.Width = 130; e.Column.OrderIndex = 17; break;
                case "SiteName": e.Column.Visible = true;  e.Column.HeaderStyle.Width = 130; e.OwnerTableView.AllowSorting = true; e.Column.OrderIndex = 3; break;
                case "IP": e.Column.Visible = true; e.OwnerTableView.AllowSorting = true; e.Column.HeaderTooltip = "LAN IP of Asset"; e.Column.HeaderStyle.Width = 100; e.Column.OrderIndex = 25; break;
                case "SourceIPAddress": e.Column.Visible = true; e.OwnerTableView.AllowSorting = true; e.Column.HeaderTooltip = "WAN/DMZ IP of Network"; e.Column.HeaderStyle.Width = 100; e.Column.OrderIndex = 24; break;
                case "CapacityHealth": e.Column.Visible = AssetType == "batteries"; e.Column.HeaderStyle.Width = 85; e.Column.OrderIndex = 11; break;
                case "Retired": e.Column.Visible = false; e.Column.HeaderStyle.Width = 85; e.Column.HeaderTooltip = "Hasn't reported in Over 30 days"; e.Column.OrderIndex = 26; break;

                default: e.Column.Visible = false; break;
                //RadGrid1.ClientSettings.Resizing.ResizeGridOnColumnResize = true;

            }
            e.Column.FilterControlWidth = Unit.Percentage(70);

            RadGrid1.ClientSettings.AllowColumnsReorder = true;
            RadGrid1.AllowSorting = true;

        }


        Control FindControlRecursive(Control parent, string name)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.ID == name)
                    return c;
                Control ctl = FindControl(c, name);
                if (ctl != null)
                    return ctl;
            }
            return null;

        }
        protected void rgrdParent_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "PerformCancel":
                    {
                        RadGrid rg = (sender as RadGrid);
                        rg.MasterTableView.IsItemInserted = false;
                        rg.MasterTableView.ClearEditItems();    // this line required for closing when editing an existing item
                        rg.MasterTableView.Rebind();

                    }
                    break;
                    case "PerformWifiSave":
                    {
                        int index = e.Item.ItemIndex;
                        if (index < e.Item.OwnerTableView.DataKeyValues.Count && index >= 0)
                        {
                            int rowId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IDAsset"]);
                            Asset asset = api.GetAssetsForUser().Find(a => a.IDAsset == rowId);
                            if (asset != null)
                            {
                                int AuthTypeINT = 0;
                                try {      
                            RadDropDownList c = this.FindControl("rdcbAuth") as RadDropDownList;
                             //   RadComboBox c = e.Item.FindControl("rdcbAuth") as RadComboBox;
                                 AuthTypeINT = Convert.ToInt32(c.SelectedValue);
                                }
                                catch (Exception ex)
                                {
                                    AuthTypeINT = 4;
                                }
                                string SSIDName = (e.Item.FindControl("txtSSIDName") as TextBox).Text;
                                string SSIDPass = (e.Item.FindControl("txtSSIDPass") as TextBox).Text;
                                PulseSession.Instance.SelectedSite = api.GetSite(asset.SiteID);
                                selectedAsset = asset;
                                using (PulseDBInterface enovateDB = new PulseDBInterface())
                                {
                                    if (selectedAsset.SerialNumber != null)
                                    {
                                        enovateDB.Query("insert into dbo.CommandQueue (SerialNumber, CommandText) values ('" + selectedAsset.SerialNumber + "', '778(" + AuthTypeINT.ToString() + ")" + "[" + SSIDName + "]" + "[" + SSIDPass + "]" +  "')");
                 

                                        ErrorHandler.PopUpMessage("Commands Sent. This Workstation will now be commissioned with new wifi credentials.");
                                    }
                                }
                            }
                        }

                    }
                    break;
                case "Edit":
                    {
                        int index = e.Item.ItemIndex;
                        if (index < e.Item.OwnerTableView.DataKeyValues.Count && index >= 0)
                        {
                            int rowId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IDAsset"]);
                            Asset asset = api.GetAssetsForUser().Find(a => a.IDAsset == rowId);
                            if (asset != null)
                            {
                                PulseSession.Instance.SelectedSite = api.GetSite(asset.SiteID);
                                selectedAsset = asset;
                            }
                        }
                        break;
                    }
                case "Find":
                    {
                        int index = e.Item.ItemIndex;
                        if (index < e.Item.OwnerTableView.DataKeyValues.Count && index >= 0)
                        {
                            int rowId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IDAsset"]);
                            Asset asset = api.GetAssetsForUser().Find(a => a.IDAsset == rowId);
                            if (asset != null)
                            {
                                PulseSession.Instance.SelectedSite = api.GetSite(asset.SiteID);
                                selectedAsset = asset;
                                using (PulseDBInterface enovateDB = new PulseDBInterface())
                                {
                                    if (selectedAsset.SerialNumber != null)
                                    {
                                        enovateDB.Query("insert into dbo.CommandQueue (SerialNumber, CommandText) values ('" + selectedAsset.SerialNumber + "', '777')");
                                        enovateDB.Query("insert into dbo.CommandQueue (SerialNumber, CommandText) values ('" + selectedAsset.SerialNumber + "', '301')");

                                        enovateDB.Query("insert into dbo.CommandQueue (SerialNumber, CommandText) values ('" + selectedAsset.SerialNumber + "', '303')");
                                        enovateDB.Query("insert into dbo.CommandQueue (SerialNumber, CommandText) values ('" + selectedAsset.SerialNumber + "', '304')");

                                        ErrorHandler.PopUpMessage("Commands Sent. Look for Blinking LED on " + selectedAsset.Description.ToString() + " " + selectedAsset.AssetNumber + " in the " + selectedAsset.DepartmentName.ToString() + " Department");
                                    }
                                }
                            }
                        }
                        break;
                    }
                case "ExportToExcel":
                    {
                        RadGrid1.MasterTableView.GetColumn("gtcEdit").Visible = false;
                        RadGrid1.MasterTableView.GetColumn("gtcFind").Visible = false;
                    }
                    break;
                case "InitInsert":
                    if (PulseWeb.API.api.SessionUser.Sites.Count > 0)
                    {
                        PulseSession.Instance.SelectedSite = PulseWeb.API.api.SessionUser.Sites[0];
                    }
                    break;
            }
               RadGrid1.MasterTableView.Rebind();
        }
        protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




        protected void SiteCombo_PreRender(object sender, EventArgs e)
        {
            RadDropDownList drop = (sender as RadDropDownList);
            if (drop != null)
            {
                foreach (Site site in PulseWeb.API.api.SessionUser.Sites)
                {
                    drop.Items.Add(new DropDownListItem { Text = site.Name, Value = site.Id.ToString(), Selected = site == PulseSession.Instance.SelectedSite ? true : false });
                }
            }
        }

        protected void DepartmentCombo_ItemsRequested(object sender, EventArgs e)
        {
            RadDropDownList combo = (sender as RadDropDownList);

            if (combo != null)
            {
                string deptName = "";
                if (selectedAsset != null)
                {
                    deptName = selectedAsset.DepartmentName;
                }

                combo.Items.Add(new DropDownListItem(mNoDepartment, "-1"));

                foreach (Department dept in PulseSession.Instance.SelectedSite.Departments)
                {
                    DropDownListItem item = new DropDownListItem(dept.Name, dept.Id.ToString());
                    if (item.Text == deptName)
                    {
                        combo.SelectedItem.Selected = false;
                        combo.ClearSelection();
                        item.Selected = true;
                    }
                    combo.Items.Add(item);
                }
            }

        }

        private string GetControlText(GridItem item, string ctlName)
        {
            TextBox tb = item.FindControl(ctlName) as TextBox;
            return (tb != null) ? tb.Text : "";
        }

        protected void rgrdParent_UpdateCommand(object source, GridCommandEventArgs e)
        {
            // is this an insert?
            //if( e.Item as GridEditFormInsertItem != null )
            //{
            //    Asset asset = new Asset();
            //    RadDropDownList d = e.Item.FindControl("rcboDept") as RadDropDownList;

            //    asset.SerialNumber = GetControlText(e.Item,"TextBox1");
            //    asset.Description = GetControlText(e.Item, "TextBox2");
            //    asset.DepartmentId = Convert.ToInt32(d.SelectedValue);
            //    using (PulseDBInterface enovateDB = new PulseDBInterface())
            //    {
            //        enovateDB.AddAsset(asset);
            //    }

            //    return;
            //}

            RadDropDownList c = e.Item.FindControl("rcboDept") as RadDropDownList;
            RadDropDownList siteCombo = e.Item.FindControl("ddlSite") as RadDropDownList;
            if (c != null) //&& siteCombo != null )
            {
                // get the selected asset
                int index = e.Item.ItemIndex;
                if (index < e.Item.OwnerTableView.DataKeyValues.Count)
                {
                    var rowId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IDAsset"]);
                    Asset asset = api.GetAssetsForUser().Find(a => a.IDAsset == rowId);

                    if (asset != null)
                    {
                        string DepartmentName = c.SelectedText.Trim();
                        //FIX ME
                        Site owningFacility = api.GetSite(asset.SiteID);
                        if (owningFacility != null)
                        {
                            using (PulseDBInterface enovateDB = new PulseDBInterface())
                            {
                                if (DepartmentName != mNoDepartment)
                                {
                                    string s = enovateDB.GetOrCreateDepartmentID(owningFacility.Id.ToString(), DepartmentName);
                                    if (!string.IsNullOrEmpty(s))
                                    {
                                        asset.DepartmentId = Convert.ToInt32(s);
                                        asset.DepartmentName = DepartmentName;
                                    }

                                }
                                else
                                {
                                    asset.DepartmentId = 0;
                                    asset.DepartmentName = "";
                                }

                                asset.SiteID = Convert.ToInt32(owningFacility.Id);
                                asset.SiteID = Convert.ToInt32(siteCombo.SelectedValue.ToString() ?? owningFacility.Id.ToString());
                                asset.AssetNumber = GetControlText(e.Item, "TextBox2");
                                asset.Wing = GetControlText(e.Item, "txtWing");
                                asset.Floor = GetControlText(e.Item, "txtFloor");
                                asset.AIOSerial = GetControlText(e.Item, "txtAIO");
                                asset.DCMonitorSerial = GetControlText(e.Item, "txtMonitor");
                                asset.Notes = GetControlText(e.Item, "txtNotes");
                                asset.CartSerial = GetControlText(e.Item, "txtWorkstation");
                                asset.Other = GetControlText(e.Item, "txtOther");
                                PulseSession.Instance.SelectedSite = owningFacility;
                                enovateDB.UpdateAsset(asset);
                            }
                        }
                    }
                }
                else
                {
                    ErrorHandler.Error("Internal error");
                }


            }
            //Wizard wiz = (item.FindControl("Wizard1") as FormTemplate);
            //RadComboBox rcboDept = wiz.FindControl("rcboDept") as RadComboBox;
            //CheckBox chkRetired = wiz.FindControl("chkRetired") as CheckBox;
            //string AssetNumber = (wiz.FindControl("txtAssetNumber") as TextBox).Text;

            //cmdText = string.Format("update Device set SerialNumber = '{1}', InvoiceNumber = '{2}', PartNumber = '{3}', Description = '{4}', IDSite = {5}, DeviceTypeID = {6}, AssetNumber = '{7}', BusinessUnitDepartmentID = {8}, Notes = '{9}', StingerNotes = '{10}', ModifiedUserID = {11}, ModifiedDateUTC = getutcdate(), Model = '{12}', Retired = {13} where ROW_ID = {0}", ROW_ID, SerialNumber, InvoiceNumber, PartNumber, Description, IDSite, DeviceTypeID, AssetNumber, DepartmentID, Notes, StingerNotes, CASTUserID, Model, Retired);

            e.Item.OwnerTableView.ClearEditItems();
            e.Item.OwnerTableView.Rebind();
        }

        protected void RadDropDownList1_ItemSelected(object sender, DropDownListEventArgs e)
        {
            RadGrid1.DataSource = GetDataSource(e.Text);
            RadGrid1.Rebind();
        }

        protected object GetDataSource(string assetType)
        {
            object dataSource = null;
            switch (assetType)
            {
                case "assets": dataSource = api.GetAssetsForUser(); break;
                case "batteries": dataSource = api.GetAssetsForUser().Where(s => s.GetCategory() == Asset.AssetCategory.BATTERY); break;
                case "workstations": dataSource = api.GetAssetsForUser().Where(s => s.GetCategory() == Asset.AssetCategory.WORKSTATION); break;
                case "chargers": dataSource = api.GetAssetsForUser().Where(s => s.GetCategory() == Asset.AssetCategory.CHARGER); break;
            }
            return dataSource;
        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {

            if (RadGrid1.MasterTableView.GetColumnSafe("Floor") != null)
            {
                RadGrid1.MasterTableView.SwapColumns("Floor", "ChargeLevel");
                RadGrid1.MasterTableView.SwapColumns("Wing", "FullChargeCapacity");
            }
            else
            {
                // ErrorHandler.PopUpMessage("No " + AssetType.ToLower() + " associated with this facility." ) ;
            }


        }

        protected void RadDropDownList1_PreRender(object sender, EventArgs e)
        {
        }

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                TextBox tb = e.Item.FindControl("TextBox1") as TextBox;
                int index = e.Item.ItemIndex;

                if (index < e.Item.OwnerTableView.DataKeyValues.Count && index >= 0)
                {
                    int rowId = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[index]["IDAsset"]);
                    Asset asset = api.GetAssetsForUser().Find(a => a.IDAsset == rowId);
                    if (asset != null)
                    {
                        PulseSession.Instance.SelectedSite = api.GetSite(asset.SiteID);
                        if (tb != null)
                        {
                            tb.Text = asset.SerialNumber;
                        }

                    }
                }
                if (tb != null)
                {
                    // Are we inserting?  If so, enable the text box
                    tb.ReadOnly = e.Item is GridEditFormInsertItem ? false : true;
                }

            }
            //Is it a GridDataItem
                 //if (e.Item is GridFilteringItem)
                //{
                //    GridFilteringItem item = (GridFilteringItem)e.Item;
                //    RadComboBox combo = (RadComboBox)item.FindControl("RadComboBox1");
                //    combo.DataSource = PulseWeb.API.api.SessionUser.Sites;
                //    combo.DataTextField = "SiteDescription";
                //    combo.DataValueField = "Id";
                //}
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem dataBoundItem = e.Item as GridDataItem;

                //Check the formatting condition
                //if (int.Parse(dataBoundItem["Retired"].Text) > 0)
                //{
                if (DateTime.Parse(dataBoundItem["LastPostDateUTC"].Text) < DateTime.Now.AddDays(-30))
                {
                    dataBoundItem["Retired"].ForeColor = System.Drawing.Color.Red;
                    dataBoundItem["LastPostDateUTC"].ForeColor = System.Drawing.Color.Red;
                    dataBoundItem.ForeColor = System.Drawing.Color.Black;
                    dataBoundItem.Font.Bold = true;
                    dataBoundItem.ToolTip = "This asset is retired and is not used in metrics, calculations, or asset counts. No communication in the last 30 days.";
                    dataBoundItem.BackColor = System.Drawing.Color.LightGray;
                    //Customize more...
                }
           
            }
        }
        protected void panel2_PreRender(object sender, EventArgs e)
        {
            Panel epanel = (Panel)FindControlRecursive(RadGrid1, "panel2");
            

            //           e.Item.FindControl("lblMonitor").Visible = AssetType == "workstations";
            //e.Item.FindControl("lblAIO").Visible = AssetType == "workstations";
            //e.Item.FindControl("lblWorkstation").Visible = AssetType == "workstations";
            //      e.Item.FindControl("txtMonitor").Visible = AssetType == "workstations";
            //e.Item.FindControl("txtAIO").Visible = AssetType == "workstations";
            //e.Item.FindControl("txtWorkstation").Visible = AssetType == "workstations";
        }
    }
}