﻿using System;
using System.Web.UI;
using Telerik.Web.UI;

namespace PulseWeb.ImportUsers
{
    static class TabNames
    {
        public struct TabInfo
        {
            public string Name { get; set; }
            public string Image { get; set; }
            public string SelectedImage { get; set; }
            public string DisabledImage { get; set; }
        }

        public static TabInfo[] Infos = new TabInfo[]{
            new TabInfo{ Name="SelectADFile", Image="1_normal.png", SelectedImage = "1_active.png", DisabledImage = "1_disable.png"},
            new TabInfo{ Name="ViewImportedUsers", Image="2_normal.png", SelectedImage = "2_active.png", DisabledImage = "2_disable.png"},
            new TabInfo{ Name="Import", Image="3_normal.png", SelectedImage = "3_active.png", DisabledImage = "3_disable.png"},
        };

        public static TabInfo Get(int index) { return Infos[index];  }
        public static int Count { get { return Infos.Length; } }
    }

    public partial class ImportUserTab : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RadPageView pageView = new RadPageView();
                pageView.ID = TabNames.Get(0).Name;
                RadMultiPage1.PageViews.Add(pageView);

                for (int i = 0; i < TabNames.Infos.Length; i++)
                    AddTab(TabNames.Infos[i], i == 0 ? true : false);
            }
        }

        private void AddTab(TabNames.TabInfo tabInfo, bool enabled)
        {
            RadTab tab = new RadTab(tabInfo.Name);

            tab.Enabled = enabled;
            tab.ImageUrl = @"\Images\" + tabInfo.Image;
            tab.SelectedImageUrl = @"\Images\" + tabInfo.SelectedImage;
            tab.DisabledImageUrl = @"\Images\" + tabInfo.DisabledImage;
            RadTabStrip1.Tabs.Add(tab);
        }
        protected void RadMultiPage1_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            Control pageViewContents = LoadControl(e.PageView.ID + ".ascx");
            pageViewContents.ID = e.PageView.ID + "userControl";
            e.PageView.Controls.Add(pageViewContents);
        }
    }
}
