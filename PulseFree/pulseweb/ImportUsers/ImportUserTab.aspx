﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ImportUserTab.aspx.cs" Inherits="PulseWeb.ImportUsers.ImportUserTab" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns='http://www.w3.org/1999/xhtml'>

<head runat="server">

    <title></title>

    <link rel="stylesheet" href="/css/ImportUserTab.css" type="text/css" />

</head>

<body>

    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />

     <div class="demo-container size-narrow no-bg">
         <telerik:RadTabStrip ID="RadTabStrip1" SelectedIndex="0" runat="server" MultiPageID="RadMultiPage1" Align="Justify"
        Skin="Silk" CssClass="tabStrip" CausesValidation="false">
    </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" OnPageViewCreated="RadMultiPage1_PageViewCreated"
            CssClass="multiPage">
        </telerik:RadMultiPage>
    </div>

    </form>

</body>

    </html>