﻿using PulseWeb.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;


namespace PulseWeb.ImportUsers
{
    public class TabBase : UserControl
    {
        protected int TabIndex { get; set; }

        public virtual void GoToNextTab()
        {
            if (TabIndex < TabNames.Count - 1)
            {
                RadTabStrip tabStrip = (RadTabStrip)this.NamingContainer.FindControl("RadTabStrip1");
                RadTab nextTab = tabStrip.FindTabByText(TabNames.Infos[TabIndex + 1].Name);
                nextTab.Enabled = true;
                nextTab.Selected = true;
            }
        }

        public virtual void GoToNextPageView()
        {
            if (TabIndex < TabNames.Count - 1)
            {
                RadMultiPage multiPage = (RadMultiPage)this.NamingContainer.FindControl("RadMultiPage1");
                RadPageView nextPage = multiPage.FindPageViewByID(TabNames.Get(this.TabIndex + 1).Name);
                if (nextPage == null)
                {
                    nextPage = new RadPageView();
                    nextPage.ID = TabNames.Get(this.TabIndex + 1).Name;
                    multiPage.PageViews.Add(nextPage);
                }
                nextPage.Selected = true;
            }
        }

        public virtual void UpdateNextPageView()
        {
            if (TabIndex < TabNames.Count - 1)
            {
                RadMultiPage multiPage = (RadMultiPage)this.NamingContainer.FindControl("RadMultiPage1");
                RadPageView pageView = multiPage.FindPageViewByID(TabNames.Get(this.TabIndex + 1).Name);
            }
        }

        protected void NextButton_Click(object sender, EventArgs e)
        {
            GoToNextTab();
            GoToNextPageView();
            UpdateNextPageView();
        }

    }


    public partial class SelectADFile : TabBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TabIndex = 0;
        }

        public override void GoToNextTab()
        {
            base.GoToNextTab();
        }

        protected void AsyncUpload1_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            if (RadUpload1.UploadedFiles.Count > 0)
            {
                foreach (Telerik.Web.UI.UploadedFile postedFile in RadUpload1.UploadedFiles)
                {
                    if (!Object.Equals(postedFile, null))
                    {
                        //postedFile.InputStream;
                        if (postedFile.ContentLength > 0)
                        {
                            //GivenNAme,Surname,Name,SamAccountNAme,Description,Department,EmployeeID,Path,Enabled,Password,PasswordNeverExpires

                            using (UserFileParser parser = new UserFileParser(postedFile.InputStream))
                            {
                                List<PulseUser> rowList = parser.CreateUserList();
                                Session["ImportUserList"] = rowList;
                            }
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                    }
                }
            }
        }
    }
}