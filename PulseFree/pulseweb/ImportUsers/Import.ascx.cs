﻿using PulseWeb.API;
using PulseWeb.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PulseWeb.ImportUsers
{
    public partial class Import : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnImportClick(object sender, EventArgs e)
        {
            List<PulseUser> list = Session["ImportUserList"] as List<PulseUser>;
            if( list != null )
            {
                string errors = "";
                using (PulseDBInterface enovateDB = new PulseDBInterface())
                {
                    int numUsersAdded = 0;
                    foreach (PulseUser user in list)
                    {
                        try
                        {
                            // TODO what site should they get added to?
                            user.IDSite = PulseSession.Instance.SelectedSite.CustomerID;
                            enovateDB.AddNewUser(user);
                            api.GetCoUsers().Add(user);
                            numUsersAdded++;
                        }
                        catch(Exception ex)
                        {
                            errors += ex.Message + "\r\n";
                        }
                    }

                    string msg = "Number of users imported: " + numUsersAdded + " Number of users skipped: " + (list.Count - numUsersAdded).ToString();
                    RadWindowManager1.RadAlert(msg, 330, 180, "Import Succeeded!", "alertCallBackFn");
                }
            }
        }
    }
}