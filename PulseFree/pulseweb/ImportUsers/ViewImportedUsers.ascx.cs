﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PulseWeb.ImportUsers
{
    public partial class ViewImportedUsers : TabBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TabIndex = 1;
        }

        protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RadGrid1.DataSource = Session["ImportUserList"];
        }
        protected void RadGrid1_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            switch(e.Column.UniqueName)
            {
                case "EncryptedPassword":
                case "AccessRights":
                case "BusinessUnitID":
                case "Id":
                case "UserName":
                case "Title":
                    e.Column.Visible = false; break;
            }

        }

    }
}