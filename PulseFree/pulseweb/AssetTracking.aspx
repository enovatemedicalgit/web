﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AssetTracking.aspx.cs" Inherits="PulseWeb.AssetTracking" %>


<asp:Content runat="server" ContentPlaceHolderID="head">
    <link href="/css/floorplanner.css" rel="stylesheet" />
  <link href="/css/agilametrics.css" rel="stylesheet" />
    <link href="/kendo/styles/kendo.common.min.css" rel="stylesheet">
    <link href="/kendo/styles/kendo.rtl.min.css" rel="stylesheet">
    <link href="/kendo/styles/kendo.default.min.css" rel="stylesheet">
    <link href="/kendo/styles/kendo.dataviz.min.css" rel="stylesheet">
    <link href="/kendo/styles/kendo.dataviz.default.min.css" rel="stylesheet">
    <link type="text/css" href="css/customRadioButton.css" rel="stylesheet" />
    <style type="text/css">
        .searchRadio label {
            font-weight:normal;
            margin-left:10px;
        }
            .tablex {
              width: 100%;
              max-width: 100%;
              margin-bottom: 20px;
            }
            .tablex > thead > tr > th,
            .tablex > tbody > tr > th,
            .tablex > tfoot > tr > th,
            .tablex > thead > tr > td,
            .tablex > tbody > tr > td,
            .tablex > tfoot > tr > td {
              padding: 4px;
              line-height: 1;
              vertical-align: top;
              border-top: 1px solid transparent;
            }
            .tablex > thead > tr > th {
              vertical-align: bottom;
              border-bottom: 2px solid #ddd;
            }
            .tablex > caption + thead > tr:first-child > th,
            .tablex > colgroup + thead > tr:first-child > th,
            .tablex > thead:first-child > tr:first-child > th,
            .tablex > caption + thead > tr:first-child > td,
            .tablex > colgroup + thead > tr:first-child > td,
            .tablex > thead:first-child > tr:first-child > td {
              border-top: 0;
            }
        .deviceIcon
        {
            position:relative;
            left:8px; 
            top:2px;
        }
    input[type=checkbox] {
        display: none;
    }
    </style>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="container-fluid">
    <div class="row">
      <div class="col-sm-3 col-md-2 sidebar">
        <div class="panel panel-default">
          <table class="table" style="border-color:transparent;">
          </table>
          <table class="tablex"  border="0">
            <tbody>
              <tr>
                  <td style="width:32px;"></td>
                  <td style="width:64px;"></td>
                  <td style="position:relative;left:-8px">Hide</td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/battery.png" alt=""/></td>
                  <td>Battery</td>
                  <td><input id="c1" class="pulseCheckbox" type="checkbox" onchange="$(this).filterByTypeClick(this,'BATTERY')" /> <label for="c1"></label></td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/charger.png" alt=""/></td>
                  <td>Charger</td>
                  <td><input id="c2" class="pulseCheckbox" type="checkbox" onchange="$(this).filterByTypeClick(this,'CHARGER')" /><label for="c2"></label></td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/workstation.png" alt=""/></td>
                  <td>Workstation</td>
                  <td><input id="c3" class="pulseCheckbox" type="checkbox" onchange="$(this).filterByTypeClick(this,'WORKSTATION')"/><label for="c3"></label></td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/employee.png" alt=""/></td>
                  <td>Employee</td>
                  <td><input id="c4" class="pulseCheckbox" type="checkbox" onchange="$(this).filterByTypeClick(this,'EMPLOYEE')" /> <label for="c4"></label></td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/patient.png" alt=""/></td>
                  <td>Patient</td>
                  <td><input id="c5" class="pulseCheckbox" type="checkbox" onchange="$(this).filterByTypeClick(this,'PATIENT')" /> <label for="c5"></label> </td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/hub.png" alt=""/></td>
                  <td>Hub</td>
                  <td><input id="c6" class="pulseCheckbox" type="checkbox" onchange="$(this).filterByTypeClick(this,'HUB')" /> <label for="c6"></label></td>
              </tr>
              <tr>
                  <td><img id="c7" class="deviceIcon" style="top:-2px;" src="Images/offline.png" alt=""/> <label for="c7"></label></td>
                  <td>Offline</td>
                  <td></td>
              </tr>
              <tr>
                  <td><img class="deviceIcon" src="Images/error.png" alt=""/></td>
                  <td>Error</td>
                  <td></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div style="visibility:visible">
            <table>
              <tr>
                  <td style="font-weight:normal;">Search for asset</td>
                  <td></td>
                  <td></td>
              </tr>
              <tr> 
                  <td><input class="k-textbox" type="text" id="searh_results" style="width:130px" readonly="readonly" onclick="$(this).clickSearch()"/></td> 
                  <td><a class="btn" href="#" onclick="$(this).clickSearch()"> <i class="fa fa-search"></i></a></td>
                  <td><a class="btn" href="#" onclick="$(this).clearSearch()"> <i class="fa fa-remove" ></i></a></td>
              </tr>
              <tr><td>&nbsp</td></tr>
            </table>
        </div>
        <div >
            <label style="font-weight:normal;">Select department</label>
        </div>
        <div >
            <input id="departments"  style="width:100%"/>
        </div>
      </div>
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="zoom">
          <a id="btnZoomIn" href="#"><i class="icon icon-plus icon-zoom"></i></a>
          <div id="slider-vertical" style="height:200px;"></div>
          <a id="btnZoomOut" href="#"><i class="icon icon-minus icon-zoom"></i></a>
        </div>
        
        <div id="panzoomContainer">
          <div class="panzoom-inner">
          </div>
        </div>
      </div>
    </div>
  </div>

    <div id="searchWindow">
        <label for="searchbyCombo">Search by:</label>
        <input id="searchbyCombo" style="width:150px;"/>
        <input id="autoComplete" style="width:200px"/>
    </div>


  
    <script type="text/javascript">
    </script>

            <script id="hover-template" type="text/x-kendo-template">
                <img src="/Images/RTLS/#=target.data('id')#" alt="#=target.data('id')#" style="border-style:solid; border-width:1px; border-color:gray;"/>
                <p>#=target.data('title')#</p>
            </script>

  <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.1.min.js"></script>
  <script src="//ajax.aspnetcdn.com/ajax/bootstrap/3.2.0/bootstrap.min.js"></script>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/jquery-ui.min.js"></script>
  
  <script type="text/javascript">
      var pulse = pulse || {};
	</script>
  <script src="scripts/namespace.js"></script>
  <script src="scripts/pulse-panzoom.js?p=0"></script>
  <script src="scripts/AssetTracking.js?p=0"></script>
  <script src="scripts/pulse-panzoom-main.js?p=0">  </script>
    <script src="/kendo/js/kendo.all.min.js"></script>
   
    <script src="Scripts/jquery.signalR-2.2.0.js?p=1" type="text/javascript"></script>
    <script src="/signalr/hubs" type="text/javascript"></script>

</asp:Content>