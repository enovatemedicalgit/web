﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PulseWeb.Source.Utils
{
    public class Vector
    {
        public double[] vectorArray;
        public int Dimension;

        public Vector(double x, double y)
        {
            vectorArray = new double[] { x, y };
            Dimension = vectorArray.GetLength(0);
        }

        public Vector(double x, double y, double z)
        {
            vectorArray = new double[] { x, y, z };
            Dimension = vectorArray.GetLength(0);
        }

        public Vector(double[] Values)
        {
            vectorArray = Values;
            Dimension = vectorArray.GetLength(0);
        }

        public Vector(int dim)
        {
            vectorArray = new double[dim];
            Dimension = dim;
            Zero();
        }


        public void Zero()
        {
            for (int i = 0; i < Dimension; i++)
            {
                vectorArray[i] = 0;
            }
        }

        public double Abs()
        {
            double abs = 0;

            for (int i = 0; i < Dimension; i++)
            {
                abs += vectorArray[i] * vectorArray[i];
            }
            return System.Math.Sqrt(abs);
        }

        public double Dot(Vector vector)
        {
            if (vector.Dimension != Dimension)
            {
                return 0;
            }
            double dotProd = 0;
            for (int i = 0; i < Dimension; i++)
            {
                dotProd += vectorArray[i] * vector.vectorArray[i];
            }
            return dotProd;
        }

        public Vector Scale(double factor)
        {
            double[] values = new double[Dimension];

            for (int i = 0; i < Dimension; i++)
            {
                values[i] = vectorArray[i] * factor;
            }
            return new Vector(values);
        }

        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < Dimension; i++)
            {
                if (i != 0)
                {
                    str += ", ";
                }
                str += vectorArray[i];
            }
            return "(" + str + ")";
        }

        public static Vector operator /(Vector left, double val)
        {
            double[] values = new double[left.Dimension];
            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] / val;
            }
            return (new Vector(values));
        }

        public static Vector operator *(Vector left, double val)
        {
            double[] values = new double[left.Dimension];
            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] * val;
            }
            return (new Vector(values));
        }

        public static Vector operator +(Vector left, Vector right)
        {
            if (left.Dimension != right.Dimension)
            {
                return left;
            }
            double[] values = new double[left.Dimension];

            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] + right.vectorArray[i];
            }
            return (new Vector(values));
        }

        public static Vector operator -(Vector left, Vector right)
        {
            if (left.Dimension != right.Dimension)
            {
                return left;
            }
            double[] values = new double[left.Dimension];

            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] - right.vectorArray[i];
            }
            return (new Vector(values));
        }

        public static Vector operator *(Vector left, Vector right)
        {
            if (left.Dimension != right.Dimension)
            {
                return left;
            }
            double[] values = new double[left.Dimension];

            for (int i = 0; i < left.Dimension; i++)
            {
                values[i] = left.vectorArray[i] * right.vectorArray[i];
            }
            return (new Vector(values));
        }
    }

    public class Point2 : Vector
    {
        public Point2(double x, double y) : base(x, y)
        {
        }

        public double x { get { return vectorArray[0]; } }
        public double y { get { return vectorArray[1]; } }
    }

    public class Point3 : Vector
    {
        public Point3(double x, double y, double z)
            : base(x, y, z)
        {
        }

        public double x { set { vectorArray[0] = value; } get { return vectorArray[0]; } }
        public double y { set { vectorArray[1] = value; } get { return vectorArray[1]; } }
        public double z { set { vectorArray[2] = value; } get { return vectorArray[2]; } }
    }

    class MathLib
    {
        /*
            ex = (P2 - P1) / ‖P2 - P1‖
            i = ex(P3 - P1)
            ey = (P3 - P1 - i · ex) / ‖P3 - P1 - i · ex‖
            d = ‖P2 - P1‖
            j = ey(P3 - P1)
            x = (r12 - r22 + d2) / 2d
            y = (r12 - r32 + i2 + j2) / 2j - ix / j
         */
        public static void TrilateratePosition(Point3 beacon1, Point3 beacon2, Point3 beacon3, out double outX, out double outY)
        {
            Vector p1 = new Vector(beacon1.x, beacon1.y);
            Vector p2 = new Vector(beacon2.x, beacon2.y);
            Vector p3 = (p2 - p1) / (p2 - p1).Abs();

            Vector temp = p2 - p1;
            double d = temp.Abs();
            Vector ex = temp / d;
            Vector i = ex * (p3 - p1);

            temp = (p3 - p1 - i * ex);
            Vector ey = temp / temp.Abs();
            Vector j = ey * (p3 - p1);

            double r1 = beacon1.z;
            double r2 = beacon2.z;
            double r3 = beacon3.z;

            outX = (Sqr(r1) - Sqr(r2) + Sqr(d)) / (2 * d);
            outY = (Sqr(r1) - Sqr(r2) + Sqr(d)) / (2 * d);


        }

        static public void TrilateratePosition3(Point3 beacon1, Point3 beacon2, Point3 beacon3, out double outX, out double outY)
        {
            double dA = beacon1.z;
            double dB = beacon2.z;
            double dC = beacon3.z;


            outX = (((Math.Pow(dA, 2) - Math.Pow(dB, 2)) + (Math.Pow(beacon3.x, 2) - Math.Pow(beacon1.x, 2)) + (Math.Pow(beacon2.y, 2) - Math.Pow(beacon1.y, 2))) * (2 * beacon3.y - 2 * beacon2.y) - ((Math.Pow(dB, 2) - Math.Pow(dC, 2)) + (Math.Pow(beacon3.x, 2) - Math.Pow(beacon3.x, 2)) + (Math.Pow(beacon3.y, 2) - Math.Pow(beacon2.y, 2))) * (2 * beacon2.y - 2 * beacon1.y)) / ((2 * beacon2.x - 2 * beacon3.x) * (2 * beacon2.y - 2 * beacon1.y) - (2 * beacon1.x - 2 * beacon2.x) * (2 * beacon3.y - 2 * beacon2.y));

            outY = ((Math.Pow(dA, 2) - Math.Pow(dB, 2)) + (Math.Pow(beacon3.x, 2) - Math.Pow(beacon1.x, 2)) + (Math.Pow(beacon2.y, 2) - Math.Pow(beacon1.y, 2)) + outX * (2 * beacon1.x - 2 * beacon2.x)) / (2 * beacon2.y - 2 * beacon1.y);
        }

        static public void TrilateratePosition2(Point3 beacon1, Point3 beacon2, Point3 beacon3, out double outX, out double outY)
        {
            double xa = beacon1.x;
            double ya = beacon1.y;
            double xb = beacon2.x;
            double yb = beacon2.y;
            double xc = beacon3.x;
            double yc = beacon3.y;
            double ra = (double)beacon1.z;
            double rb = (double)beacon2.z;
            double rc = (double)beacon3.z;

            double S = (Math.Pow(xc, 2) - Math.Pow(xb, 2) + Math.Pow(yc, 2) - Math.Pow(yb, 2) + Math.Pow(rb, 2) - Math.Pow(rc, 2)) / 20;
            double T = (Math.Pow(xa, 2) - Math.Pow(xb, 2) + Math.Pow(ya, 2) - Math.Pow(yb, 2) + Math.Pow(rb, 2) - Math.Pow(ra, 2)) / 20;
            double y = ((T * (xb - xc)) - (S * (xb - xa))) / (((ya - yb) * (xb - xc)) - ((yc - yb) * (xb - xa)));
            double x = ((y * (ya - yb)) - T) / (xb - xa);

            outX = x;
            outY = y;
        }


        public static double Sqr(double x)
        {
            return x * x;
        }
    }
}