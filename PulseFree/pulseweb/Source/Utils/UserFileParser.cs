﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PulseWeb.Model;

namespace PulseWeb.ImportUsers
{
    class UserFileParser : IDisposable
    {
        const string FirstName = "givenname";
        const string LastName = "surname";
        const string Email = "emailaddress";
        private Stream stream = null;

        public UserFileParser(Stream stream)
        {
            this.stream = stream;
        }

        public UserFileParser()
        {
        }

        internal List<PulseUser> CreateUserList()
        {
            if( this.stream != null )
            {
                List<PulseUser> users = new List<PulseUser>();
                using (TextReader reader = new StreamReader(stream))
                {
                    // Read the header line
                    string line = reader.ReadLine();
                    // get all the tokens defined in our list
                    string[] headers = line.Split(',');

                    line = reader.ReadLine();
                    while (!string.IsNullOrEmpty(line))
                    {
                        string [] tokens = line.Split(',');
                        if (headers.Length != tokens.Length)
                        {
                            Utils.ErrorHandler.Error("Internal error");
                        }

                        PulseUser user = new PulseUser();
                        user.AccessRights = UserAccessRights.DEFAULT_RIGHTS;
                        if( API.api.SessionUser != null )
                        {
                            user.IDSite = API.api.SessionUser.IDSite;
                        }
                        

                        for (int i = 0; i < tokens.Length; i++)
                        {
                            switch( headers[i].ToLower() )
                            {
                                case FirstName: user.FirstName = tokens[i]; break;
                                case LastName: user.LastName = tokens[i]; break;
                                case "password": user.EncryptedPassword = Utils.Security.Crypto.EncryptString(tokens[i]); break;
                                case Email: user.Email = tokens[i]; break;
    
                                // Other unused AD fields
                                case "name": break;
                                case "samaccountname": break;
                                case "description": break;
                                case "department": break;
                                case "employeeid": break;
                                case "path": break;
                                case "enabled": break;
                                case "passwordneverexpires": break;
                                default: break;
                            }
                        }
                        users.Add(user);
                        line = reader.ReadLine();
                    }
                }
                return users;
            }
            return null;
        }

        public void ExportUserList(List<PulseUser> users)
        {
            using (StreamWriter writer = new StreamWriter("/temp.csv",false) )
            {
                writer.WriteLine(FirstName + "," + LastName + "," + Email + "," + "Name,SamAccountName,Description,Department,EmployeeId,Path,Enabled,PasswordNeverExpires");
                foreach( PulseUser user in users )
                {
                    writer.WriteLine(user.FirstName + "," + user.LastName + "," + user.Email + ",,,,,,,,");
                }
            }
        }

        public void Dispose()
        {
            stream.Dispose();
        }
    }
}
