﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using PulseWeb.Model;
using PulseWeb.Utils;
using System;
using System.Collections.Generic;
using System.Timers;
using System.Web;

namespace PulseWeb.API.RTLS
{
    public class AssetLocation
    {
        public string assetId { get; set; }
        public int departmentId { get; set; }
        public double Left { get; set; }
        public double Top { get; set; }
    }


    public class RTLSPacket
    {
        public int AreaId { get; set; }
        public int AssetId{ get; set; }
        public int SiteId { get; set; }

        public RTLSPacket()
        {

        }

        // Warning duplicate code!!! when My code is merged with Arlow, remove this!!!
        protected string GetQueryStringValue(string ParmName, HttpRequest request)
        {
            string value = string.Empty;
            try
            {
                value = request.QueryString[ParmName].ToString();
            }
            catch { }

            return value;
        }

        public bool Create(HttpRequest request)
        {
            AreaId = Misc.SafeConvertToInt(GetQueryStringValue("Beacon1Minor", request));
            //Id = Misc.SafeConvertToInt(GetQueryStringValue("SitID", request));
            SiteId = 1;
            AssetId = Misc.SafeConvertToInt(GetQueryStringValue("EmTagId", request));
            return false;
        }
    }

    // an RTLS site contains a list of all RTLS assets that have moved
    class RTLSSite
    {
        public List<RTLSPacket> mMovedAssetPackets = new List<RTLSPacket>();
    }

    public class Broadcaster
    {
        private readonly static Lazy<Broadcaster> _instance = new Lazy<Broadcaster>(() => new Broadcaster());
        private readonly IHubContext _hubContext;
        private List<RTLSSite> AllSistes = new List<RTLSSite>();
        private Timer aTimer = new System.Timers.Timer(1000);
        private bool test = false;

        public Broadcaster()
        {
            // Save our area context so we can easily use it 
            // to send to its connected clients
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<MoveAssetHub>();
            //_tester = new Tester();
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Enabled = false;

        }

        void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            lock(this)
            {
                BroadcastAllMovedAssets();
            }
        }


        public void BroadcastAllMovedAssets()
        {
            foreach( Site site in PulseGlobals.Sites )
            {
                List<string> connectionIDs = ConnectionManager.GetConnectionIds( site.Id );
                if (connectionIDs != null)
                {
                    PulseGlobals.Lock();

                    foreach( string id in connectionIDs)
                    {
                        _hubContext.Clients.Client(id).updateAsset(site.UpdatedAssets());
                    }
                    site.ClearUpdatedAssets();

                    PulseGlobals.UnLock();
                }
                else
                {
                    PulseWeb.API.RTLS.RTLSLog.Log("No connections :(");
                }
            }
        }

        public static Broadcaster Instance
        {
            get
            {
                return _instance.Value;
            }
        }
    }
}