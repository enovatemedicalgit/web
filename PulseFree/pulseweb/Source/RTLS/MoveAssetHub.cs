﻿using System;
using System.Threading;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Timers;
using System.Linq;

namespace PulseWeb.API.RTLS
{

    public static class ConnectionManager
    {
        class KeyValuePair
        {
            public string Key;
            public int Value;
        }

        private static List<KeyValuePair> _connections = new List<KeyValuePair>();

        public static void Add(string connectionId)
        {
            _connections.Add(new KeyValuePair { Key = connectionId,Value = -1} ); 
        }

        public static void Remove(string connectionId)
        {
            _connections.Remove(new KeyValuePair { Key = connectionId, Value = -1 });
        }

        public static bool AssociateFacility(string connectionId,int siteID)
        {
            KeyValuePair kvp = _connections.Find(s => s.Key == connectionId);
            if (kvp != null)
            {
                kvp.Value = siteID;
                return true;
            }
           
            return false;
        }

        // returns a list of connection ids (key) from the value
        public static List<string> GetConnectionIds(int value)
        {
            List<KeyValuePair> kvp = _connections.FindAll(s => s.Value == value);
            if (kvp != null)
            {
                return kvp.Select(p=>p.Key).ToList();
            }
            return null;
        }

    }

    public class MoveAssetHub : Hub
    {
        // Is set via the constructor on each creation
        private Broadcaster _broadcaster;
        public MoveAssetHub() : this(Broadcaster.Instance)
        {
            
        }
        public MoveAssetHub(Broadcaster broadcaster)
        {
            _broadcaster = broadcaster;
        }

        public override Task OnConnected()
        {
            ConnectionManager.Add(Context.ConnectionId); 
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            ConnectionManager.Remove(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        // Here is where we associate site id with a connection id
        public void RegisterClientFacility(string clientFaclity)
        {
            //RTLSLog.Log(clientFaclity);
            ConnectionManager.AssociateFacility(Context.ConnectionId, Convert.ToInt32(clientFaclity));
        }
    }


}
