﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;
using PulseWeb.Utils.Database;
using PulseWeb.Model;
using System.Diagnostics;
using PulseWeb.Source.Utils;
using System.IO;


namespace PulseWeb.API.RTLS
{
    public class RTLSLog
    {
        public static void Log(string s)
        {
            using (StreamWriter textOutput = new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\rtls.log",
                                               FileMode.Append,
                                               FileAccess.Write)))
            {
                textOutput.WriteLine(DateTime.Now.ToString());
                textOutput.WriteLine("------------------------------------------------------------------------------");
                textOutput.WriteLine(s);
                textOutput.Close();
            }
        }
    }

    public class RTLS
    {
        static Point2 davesOffice = new Point2(72, 313);

        static List<PulseArea> hubList = new List<PulseArea>(new[] {

            // Enovate office
            new PulseArea { Description = "Dave Office", Id = 1, Location = new Point2(65,315), },
            new PulseArea { Description = "Hospital Room", Id = 2, Location = new Point2(1425,768), },
            new PulseArea { Description = "Engineering Room", Id = 3, Location = new Point2(484,767), },
            new PulseArea { Description = "Training Room", Id = 4, Location = new Point2(946,806), },
            new PulseArea { Description = "Office Area", Id = 5, Location = new Point2(1405,380), },
            new PulseArea { Description = "Board Room", Id = 6, Location = new Point2(541,221), },
            new PulseArea { Description = "Hospital Bed", Id = 7, Location = new Point2(1425,835), },
            new PulseArea { Description = "Main Office", Id = 8, Location = new Point2(970,299), },
            new PulseArea { Description = "Plant", Id = 9, Location = new Point2(1208,954), },
            new PulseArea { Description = "ExecOfficeArea", Id = 10, Location = new Point2(329,381), },

            // EM Himss Booth
            new PulseArea { Description = "EMBooth", Id = 11, Location = new Point2(65,315), },

            // Himss floor
            new PulseArea { Description = "HIMMSEMBOOTH", Id = 16, Location = new Point2(908,662), },
            new PulseArea { Description = "CORTEKSERVICES", Id = 17, Location = new Point2(1120,556), },
            new PulseArea { Description = "AVAYA", Id = 18, Location = new Point2(1162,646), },
            new PulseArea { Description = "POSIFLEX", Id = 19, Location = new Point2(497,707), },
            new PulseArea { Description = "HONEYWELL", Id = 20, Location = new Point2(567,524), },
            new PulseArea { Description = "PCCONNECTION", Id = 21, Location = new Point2(495,255), },

            // Extra floor areas
            new PulseArea { Description = "ExtraSpot1", Id = 22, Location = new Point2(890,180), },
            new PulseArea { Description = "ExtraSpot2", Id = 23, Location = new Point2(1043,289), },
            new PulseArea { Description = "ExtraSpot3", Id = 24, Location = new Point2(422,487), },

            // outside enovate booth
            new PulseArea { Description = "EMOutside1", Id = 25, Location = new Point2(908,662), },
            new PulseArea { Description = "EMOutside2", Id = 26, Location = new Point2(908,662), },
            new PulseArea { Description = "EMOutside3", Id = 27, Location = new Point2(908,662), },
            new PulseArea { Description = "EMOutside4", Id = 28, Location = new Point2(908,662), },


        });

        private static void rotate(double radians, ref double x, ref double y)
        {
            double newX = x * Math.Cos(radians) - y * Math.Sin(radians);
            double newY = x * Math.Sin(radians) + y * Math.Cos(radians);

            x = newX;
            y = newY;
        }

        public static void InitAssetLocations(Site f)
        {
            bool bHackInPlace = false;
            //List<Dictionary<string, string>> locations = null;

            //using (PulseDBInterface db = new PulseDBInterface(new Utils.Database.SQLDatabase("Kokoro")))
            //{
            //    locations = db.Query("SELECT * FROM AssetLocation");
            //    db.Dispose();
            //}
            if( f.Areas.Count <= 0 )
            {
                f.Areas = hubList;
                bHackInPlace = true;
            }

            int i = 0;
            using (PulseDBInterface db = new PulseDBInterface(new Utils.Database.SQLDatabase("PulseConnection")))
            {
                var result = db.Query("SELECT IDAssetType, AssetDescription, MinorOfEmTag, IDArea, SiteID, ImageFilename FROM viewTaggedAssets");

                if (result != null)
                {
                    // initialize the area scatter
                    f.Areas.ForEach(p => p.InitScatter());

                    //List<Asset> newList = new list
                    foreach (Dictionary<string, string> cur in result)
                    {
                        //int emTagId = Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(cur, "EmtagID"));
                        //if (emTagId == 0)
                        //    emTagId = -1;
                        //Asset asset = f.Assets.Find(a => a.EmTagID == emTagId);
                        //if (asset == null)
                        //{
                        //    if (i >= f.Assets.Count)
                        //        continue;
                        //    asset = f.Assets[i++];
                        //    //continue;
                        //}
                        if (i >= f.Assets.Count)
                            continue;
                        Asset asset = f.Assets[i++];
                        asset.XCoord = (int)davesOffice.x;
                        asset.YCoord = (int)davesOffice.y;
                        switch (Utils.Misc.SafeGetKey(cur, "IDAssetType"))
                        {
                            case "1": asset.Type = Asset.AssetType.BATTERY; break;
                            case "2": asset.Type = Asset.AssetType.WORKSTATION; break;
                            case "3": asset.Type = Asset.AssetType.CHARGER; break;
                            case "4": asset.Type = Asset.AssetType.EMPLOYEE; break;
                            case "5": asset.Type = Asset.AssetType.PATIENT; break;
                            default: asset.Type = Asset.AssetType.UNKNOWN; break;
                        }
                        asset.Description = Utils.Misc.SafeGetKey(cur, "AssetDescription");
                        asset.SerialNumber = Utils.Misc.SafeGetKey(cur, "MinorOfEmTag");
                        asset.CurrentArea = Convert.ToInt32(Utils.Misc.SafeGetKey(cur, "IDArea"));
                        asset.ImageFilename = Utils.Misc.SafeGetKey(cur, "ImageFilename").Replace(".gif", ".png");
                        if (bHackInPlace)
                            asset.DepartmentId = 5275;

                        //if( asset.Image == null )
                        //{
                        //    byte[] imageBytes = db.QueryBinary(string.Format("SELECT Image FROM viewTaggedAssets WHERE AssetDescription='{0}'", asset.Description));

                        //    if (imageBytes != null)
                        //        asset.Image = GetImage(imageBytes);
                        //}

                        PulseArea area = f.Areas.Find(p => p.Id == asset.CurrentArea);

                        if (area != null)
                        {
                            asset.XCoord = (int)area.Location.x;
                            asset.YCoord = (int)area.Location.y;

                            // Create a scatter pattern that is essentially increasing orbits of 'assetsPerOrbit' number of assets in each orbit
                            // Each orbit is a multiple of radius in size
                            double x = 0;
                            double y = area.scatterRadius;
                            rotate((2 * Math.PI / area.assetsPerOrbit) * area.numAssetsInHub, ref x, ref y);

                            asset.OffsetX = (int)(x + .5f);
                            asset.OffsetY = (int)(y + .5f);
                            asset.XCoord += asset.OffsetX;
                            asset.YCoord += asset.OffsetY;

                            area.numAssetsInHub++;
                            if (area.numAssetsInHub >= area.assetsPerOrbit)
                            {
                                // each orbit has roughly twice the amount as the previous
                                area.assetsPerOrbit *= 2;
                                area.scatterRadius += area.kScatterRadius;
                                area.numAssetsInHub = 0;
                            }
                        }

                    }
                    db.Dispose();
                }
            }
        }
    }
}