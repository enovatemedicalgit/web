﻿using System;
using System.Collections.Generic;
using System.Web;

namespace PulseWeb.Model
{
    public class Customer : PulseEntity
    {

#region properties
        public List<Site> Sites { get; set; }
        public int SiteCount { get { return Sites.Count; } }

#endregion

        public Customer(Dictionary<string, string> dbIdnRow) : base(dbIdnRow,"CustomerName","IDCustomer")
        {
            foreach (var col in dbIdnRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "SFCustomerID":  break;
                    case "SytelineCustomerNum": break;
                    case "CASTBUID": break;
                    case "SytelineCustomerSeq": break;
                    default: Utils.ErrorHandler.WarnMissingDBField(col.Key, this); break;
                }
            }
        }

    }
}