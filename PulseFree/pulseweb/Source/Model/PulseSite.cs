﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PulseWeb.Utils;
using System.Web.Script.Serialization;
using PulseWeb.API;

namespace PulseWeb.Model
{
    class AssetListJson
    {
        public int facilityID { get; set; }
        public List<AssetJSON> assets { set; get; }
    }

    class AssetJSON
    {
        public int x { get; set; }
        public int y { get; set; }
        public string color { get; set; }
        public string type { get; set; }
        public string serialNumber { get; set; }
        public string description { get; set; }
        public string state { get; set; }
        public string imgName { get; set; }
        public string bloodPressure { get; set; }
        public string heartRate { get; set; }
        public string temperature { get; set; }


        public AssetJSON(int x, int y, PulseAsset assetRef)
        {
            this.x = x;
            this.y = y;
            color = TempUtils.GetAssetColor(assetRef.Type);
            type = assetRef.Type.ToString();
            serialNumber = assetRef.SerialNumber;
            description = assetRef.Description;
            state = assetRef.State;
            imgName = assetRef.Image;

            switch (assetRef.SerialNumber)
            {
                case "1": bloodPressure = "122/81"; heartRate = "76"; temperature = "99"; break;
                case "2": bloodPressure = "110/72"; heartRate = "64"; temperature = "98"; break;
                case "3": bloodPressure = "117/72"; heartRate = "60"; temperature = "98"; break;
                case "5": bloodPressure = "140/107"; heartRate = "71"; temperature = "98"; break;
                case "6": bloodPressure = "134/77"; heartRate = "67"; temperature = "97"; break;
                case "8": bloodPressure = "158/120"; heartRate = "129"; temperature = "102"; break;
                default: bloodPressure = "122/81"; heartRate = "81"; temperature = "98"; break;
            }



        }

    }

    public class PulseSite
    {
        private List<PulseAsset> _assets;
        private PulseAsset mFilteredAsset;

        public string Name { get; set; }
        public int Id { get; set; }
        public string SiteDescription { get; set; }
        public string CustomerID { get; set; }
           public string POCEmail { get; set; }
        public List<PulseAsset> Assets
        {
            get
            {
                if (_assets == null)
                {
                    // TODO dependency alert
                    using (NewPulseDB pulseDB = new NewPulseDB())
                    {
                        _assets = pulseDB.InitializeAssetsForSite(this);
                    }
                }
                return _assets;
            }
            set
            {
                _assets = value;
            }
        }

        public List<PulseDepartment> Departments;
        public List<PulseArea> Areas;

        public PulseSite(Dictionary<string, string> dbRow)
        {
            foreach (var col in dbRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "IDSite": Id = Convert.ToInt32(colValue); break;
                    case "SiteName": Name = colValue; break;
                    case "SiteDescription": SiteDescription = colValue; break;
                    case "CustomerID": CustomerID = colValue; break;
                         case "POCEmail": CustomerID = colValue; break;
                }
            }

        }

        public PulseAsset FindAsset( string serialNum )
        {
            return Assets.Find(s => s.SerialNumber == serialNum);
        }

        public string AssetsToJson()
        {
            AssetListJson list = new AssetListJson();
            list.facilityID = this.Id;
            list.assets = new List<AssetJSON>();

            if (mFilteredAsset != null )
            {
                list.assets.Add(new AssetJSON(mFilteredAsset.XCoord, mFilteredAsset.YCoord, mFilteredAsset));
            }
            else
            {
                foreach (PulseAsset asset in Assets)
                {
                    list.assets.Add(new AssetJSON(asset.XCoord, asset.YCoord, asset));
                    if (list.assets.Count > 100)
                        break;
                }
            }

            var js = new JavaScriptSerializer();



            return js.Serialize(list);
        }
        
        public void SetSearchAsset(PulseAsset a)
        {
            this.mFilteredAsset = a;
        }

    }
}