﻿using System;
using System.Collections.Generic;
using System.Web;
using PulseWeb.Utils;

namespace PulseWeb.Model
{
    public class Alert
    {
        //public Asset Asset { get; set; }  // the asset the alert is associated with or null
        public string DeviceSN { get; set; }
        public string Description { get; set; }
        public string BusinessUnitID { get; set; }
        public string CreatedDate { get; set; }

        public Alert( Dictionary<string, string> dbRow)
        {
            CreatedDate = Misc.SafeGetKey(dbRow, "CreatedDateUTC");
            Description = Misc.SafeGetKey(dbRow, "Description");
            DeviceSN = Misc.SafeGetKey(dbRow, "DeviceSerialNumber");
        }

    }
}