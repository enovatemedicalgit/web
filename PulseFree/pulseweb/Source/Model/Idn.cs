﻿using System;
using System.Collections.Generic;
using System.Web;

namespace PulseWeb.Model
{
    public class Idn : FacilityInfo
    {

#region properties

        public List<Facility> Facilities { get; set; }
        public int FacilityCount { get { return Facilities.Count; } }

#endregion

        public Idn(Dictionary<string, string> dbIdnRow,string nameKey,string idKey ) : base(dbIdnRow,nameKey,idKey)
        {
        }

    }
}