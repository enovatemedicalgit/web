﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PulseWeb.Model
{
    public class PulseDepartment
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string SiteID { get; set; }

        public PulseDepartment()
        {

        }

        public PulseDepartment(Dictionary<string, string> dbRow)
        {
            foreach (var col in dbRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "DepartmentID": Id = Convert.ToInt32(colValue); break;
                    case "Description": Description = colValue; break;
                    case "SiteID": SiteID = colValue; break;
                    default: break;
                }
            }
        }
    }
}