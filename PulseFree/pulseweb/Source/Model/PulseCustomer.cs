﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PulseWeb.Utils;

namespace PulseWeb.Model
{
    public class PulseCustomer
    {
        public string Name {get;set;}
        public int Id {get;set;}
        public string SytelineCustomerNum {get;set;}
        public string CASTBUID {get;set;}
        public string SytelineCustomerSeq {get;set;}

        public List<PulseSite> Sites;

        public PulseCustomer(Dictionary<string, string> dbRow )
        {
            foreach (var col in dbRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "IDCustomer": Id = Convert.ToInt32(colValue); break;
                    case "CustomerName": Name = colValue; break;
                    case "SytelineCustomerNum": SytelineCustomerNum = colValue; break;
                    case "CASTBUID": CASTBUID = colValue; break;
                    case "SytelineCustomerSeq": SytelineCustomerSeq = colValue; break;
                }
            }
        }
    }
}