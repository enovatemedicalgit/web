﻿using PulseWeb.API;
using PulseWeb.Utils.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace PulseWeb.Model
{


    public class Site : PulseEntity
    {
        public const int ENOVATE_HQ_CUSTOMER_ID = 4;

        #region Privates
        private List<PulseUser> _users;
        private List<Asset> _assets;
        int mAssetTypes = 0;
        #endregion

        #region Properties
        public String SiteDescription {get;set;}
        public int CustomerID {get;set;}
        public String IPAddr { get; set; }
        public int AssetCount { get { return Assets.Count; } }
        public List<Department> Departments { get; set; }
        public List<PulseArea> Areas { get; set; }
        public int DepartmentCount { get { return Departments.Count; } }
        #endregion


        public List<Asset> Assets
        {
            get
            {
                if (_assets == null)
                {
                    // TODO dependency alert
                    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                    {
                        if( Asset.assetPropertyTable == null )
                        {
                            Asset.assetPropertyTable = enovateDB.InitializeAssetProperties();
                        }
                        _assets = enovateDB.InitializeAssetListView(this);
                    }
                }
                return _assets;
            }
            set
            {
                _assets = value;
            }

        }



        #region JSON TEMP
        public class AssetRTLSUpdate : AssetJSON
        {
            public AssetRTLSUpdate(Asset assetRef) : base(assetRef)
            {

            }
        }


        public class AssetJSON
        {
            public int x { get; set; }
            public int y { get; set; }
            public string color { get; set; }
            public string type { get; set; }
            public string serialNumber { get; set; }
            public string description { get; set; }
            public string state { get; set; }
            public string imgName { get; set; }
            public string bloodPressure {get;set; }
            public string heartRate {get;set; }
            public string temperature { get; set; }
            public int departmentIndex { get; set; }

            public AssetJSON()
            {

            }
            public AssetJSON(Asset assetRef)
            {
                this.x = assetRef.XCoord;
                this.y = assetRef.YCoord;
                this.departmentIndex = assetRef.DepartmentId;
                // TODO move this to the client
                color = assetRef.GetAssetColor(assetRef.Type);
                type = assetRef.Type.ToString();
                serialNumber = assetRef.SerialNumber;
                description = assetRef.Description;
                state = assetRef.State;
                imgName = assetRef.ImageFilename;

                switch( assetRef.SerialNumber)
                {
                    case "1": bloodPressure = "122/81"; heartRate = "76"; temperature = "99"; break;
                    case "2": bloodPressure = "110/72"; heartRate = "64"; temperature = "98"; break;
                    case "3": bloodPressure = "117/72"; heartRate = "60"; temperature = "98"; break;
                    case "5": bloodPressure = "140/107"; heartRate = "71"; temperature = "98"; break;
                    case "6": bloodPressure = "134/77"; heartRate = "67"; temperature = "97"; break;
                    case "8": bloodPressure = "158/120"; heartRate = "129"; temperature = "102"; break;
                    default: bloodPressure = "122/81"; heartRate = "81"; temperature = "98"; break;
                }
            }

        }

        #endregion 

        protected Site()
        {
        }

        public Site(Dictionary<string, string> dbIdnRow ) : base(dbIdnRow,"SiteName", "IDSite")
        {
            foreach (var col in dbIdnRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "SiteDescription": this.SiteDescription = colValue; break;
                    case "CustomerID": this.CustomerID = Convert.ToInt32(colValue); break;
                    case "IPAddr": this.IPAddr = colValue; break;
                    default: Utils.ErrorHandler.WarnMissingDBField(col.Key,this); break;
                }
            }
                
        }


        public bool IsRTLSEnabled()
        {
            return true;
        }



        public List<AssetJSON> AssetsToJson()
        {
            if (IsRTLSEnabled() )
            {
                List<AssetJSON> assets = new List<AssetJSON>();
                List<Asset> curList =  this.Assets;
                foreach (Asset asset in curList)
                {
                    int departmentIndex = Convert.ToInt32(asset.DepartmentId);
                    if (departmentIndex >= 0)
                    {
                        assets.Add(new AssetJSON(asset));
                        if (assets.Count > 100)
                            break;
                    }
                }
                return assets;
            }

            return null;
        }

        // returns a list of RTLS assets that have been updated
        public List<AssetRTLSUpdate> UpdatedAssets()
        {
            if (IsRTLSEnabled())
            {
                List<AssetRTLSUpdate> assets = new List<AssetRTLSUpdate>();
                foreach (Asset asset in this.Assets)
                {
                    if( asset.RTLSUpdated || true )
                    {
                        assets.Add(new AssetRTLSUpdate(asset));
                    }
                }
                return assets;
            }

            return null;
        }

        public void ClearUpdatedAssets()
        {
            this.Assets.ForEach(asset => asset.RTLSUpdated = false);
        }

        public Department DepartmentFromName(string name)
        {
            return Departments.Find(x => x.Name == name);
        }

        internal void UpdateRTLSAsset(int assetId,int areaId,string state )
        {
            Asset asset = this.Assets.Find(a => a.IDAsset == assetId);
            if( asset != null && asset.CurrentArea != areaId )
            {
                asset.CurrentArea = areaId ;
                asset.State = state;
                asset.RTLSUpdated = true;
            }
        }
    }

}