﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using PulseWeb.Utils.Database;
using PulseWeb.Model;
using PulseWeb.Utils;

namespace PulseWeb.API
{
    //
    // This class provides an interface layer to the Pulse DB.  This is where the DB layout is abstracted
    //
    public class PulseDBInterface : IDisposable
    {
        private SQLDatabase _database;
        private int kUnknownUserId = -1; // MDJ TODO should we check for this being used and output an error?

        // Constructor that takes a SQLDatabase instance 
        public PulseDBInterface(SQLDatabase database)
        {
            _database = database;
        }

        public PulseDBInterface()
        {
            _database = new SQLDatabase();
        }

        public void Dispose()
        {
            if (_database != null)
            {
                _database.Dispose();
            }
        }

        // 
        // Queries the DB for a list of IDN's associate with the given buid.  Fills and returns this list
        //
        public List<Customer> InitializeCustomerList(Int32 customerId)
        {
            //DataTable dtBusinessUnit = Common.GetDataTable(string.Format("select * from BusinessUnit where Id = {0}", hidBusinessUnitID.Value));
            List<Customer> list = new List<Customer>();

            string commandText = "select * from Customers where IDCustomer = @customerId";

            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@customerId",
                customerId
            } };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);

            // Fill in session variables from the result
            foreach (var row in dbList)
            {
                list.Add(new Customer(row));
            }

            return list;
        }

        public List<Site> InitializeSiteList(int siteId)
        {
            List<Site> list = new List<Site>();

            string commandText = string.Format("select * from Sites where IDSite = {0}", siteId);

            // Execute the query
            var dbList = _database.Query(commandText, null);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new Site(dbRow));
            }

            return list;
        }

        //
        // Queries the DB for all facilities associated with a given buid and returns these in a List
        //
        public List<Site> InitializeSiteList(Customer customer)
        {
            List<Site> list = new List<Site>();

            string commandText;
            int businessUnitId = customer.Id;
            // If this is an customer, get all the child facilities, otherwise get the single site associated with the id
            commandText = "select * from Sites where CustomerID = @businessUnitId";

            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                {
                    "@businessUnitId",
                    businessUnitId
                }
            };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new Site(dbRow));
            }

            return list;
        }

        public List<PulseArea> InitializeAreaList(Site site)
        {
            List<PulseArea> list = new List<PulseArea>();
            string commandText = "select * from Area where IDSite = @siteId";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@siteId",
                site.Id
            } };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new PulseArea(dbRow, site));
            }
            return list;
        }

        //
        // Queries the DB for departments in a given site and returns them in a list.  
        //  
        //  siteID is the uid of the site in which the departments belong
        //
        public List<Department> InitializeDepartmentList(Site site)
        {
            List<Department> list = new List<Department>();

            string commandText = "select * from Departments where SiteID = @siteId";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@siteId",
                site.Id
            } };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new Department(dbRow));
            }

            return list;
        }

        public List<Asset.AssetProperties> InitializeAssetProperties()
        {
            List<Asset.AssetProperties> list = new List<Asset.AssetProperties>();

            string commandText = "select * from AssetType";

            // Execute the query
            var dbList = _database.Query(commandText, null);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new Asset.AssetProperties(dbRow));
            }

            return list;
        }

        public List<Asset> InitializeAssetList(Site site)
        {
            List<Asset> list = new List<Asset>();

            string commandText =
                "select  IDAsset,isnull(dept.Description, '') as ActualDepartment, s.SiteName as SiteName, assets.Retired as Retired, isnull(nullif([dbo].[fnGetBatteryCapacity_Int] (assets.SerialNo,assets.FullChargeCapacity),0),0) as 'Capacity Health',assets.Notes,assets.IDAssetType, assets.Description,assets.SourceIPAddress,assets.IP, SerialNo,vwDC.ProductSerialNumber as DCMonitorSerial,vwAIO.ProductSerialNumber as AIOSerial,vwCart.ProductSerialNumber as CartSerial,assets.SiteID,FullChargeCapacity,Temperature,assets.LastPostDateUTC,CycleCount,ChargeLevel,assets.DepartmentID,AssetStatusID,AssetNumber,assets.Other, assets.Floor,assets.Wing from assets left join vwProductAIO vwAIO on vwAIO.DeviceSerialNo = assets.SerialNo left join vwProductCart vwCart on vwCart.DeviceSerialNo = assets.SerialNo left join vwProductDCMonitor vwDC on vwDC.DeviceSerialNo = assets.SerialNo left join AccessPoint ap on assets.AccessPointID = ap.ROW_ID left join departments dept on dept.IDDepartment = ap.DepartmentID  and dept.SiteID=ap.SiteID left join Sites s on s.IDSite = assets.SiteID  where assets.SiteID = @siteId  order by Retired asc,LastPostDateUTC desc";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@siteId",
                site.Id
            } };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new Asset(dbRow));
            }

            return list;
        }

        public List<Asset> InitializeAssetListView(Site site)
        {
            List<Asset> list = new List<Asset>();

            //string commandText = "select  IDAsset,CurrentArea,Description,SerialNo,IDAssetType, Retired, VendorID,SiteID,FullChargeCapacity,Temperature,LastPostDateUTC,CycleCount,ChargeLevel,EmTagID,DepartmentID,AssetStatusID,AssetNumber,Floor,Wing from viewSiteAssets where SiteID = @siteId";
            string commandText =
                "select  IDAsset,isnull(dept.Description, '') as ActualDepartment,isnull(nullif([dbo].[fnGetBatteryCapacity_Int] (assets.SerialNo,assets.FullChargeCapacity),0),0) as 'Capacity Health', assets.Notes,s.SiteName as SiteName, assets.Retired as Retired, assets.Description,assets.SourceIPAddress,assets.IP, SerialNo,vwDC.ProductSerialNumber as DCMonitorSerial,vwAIO.ProductSerialNumber as AIOSerial,vwCart.ProductSerialNumber as CartSerial,IDAssetType,assets.SiteID,FullChargeCapacity,Temperature,assets.LastPostDateUTC,CycleCount,ChargeLevel,assets.DepartmentID,AssetStatusID,AssetNumber,assets.Other,assets.Floor,assets.Wing from assets left join vwProductAIO vwAIO on vwAIO.DeviceSerialNo = assets.SerialNo left join vwProductCart vwCart on vwCart.DeviceSerialNo = assets.SerialNo left join vwProductDCMonitor vwDC on vwDC.DeviceSerialNo = assets.SerialNo left join AccessPoint ap on assets.AccessPointID = ap.ROW_ID left join departments dept on dept.IDDepartment = ap.DepartmentID  and dept.SiteID=ap.SiteID left join Sites s on s.IDSite = assets.SiteID where assets.SiteID = @siteId order by Retired asc,LastPostDateUTC desc";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@siteId",
                site.Id
            } };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);

            // Fill in session variables from the result
            foreach (var dbRow in dbList)
            {
                list.Add(new Asset(dbRow));
            }

            return list;
        }

        public UserAccessRights GetUserAccessRights(PulseUser user)
        {
            // Set the user option properties
            string commandText = "SELECT * FROM UserOptions WHERE ( User_ROW_ID = @rowid )";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@rowid",
                user.Id
            } };

            var rows = _database.Query(commandText, parameters);
            //user.AccessRights = AccessRightsHelper.Convert(rows[0]);

            UserAccessRights rights = 0;
            foreach (var row in rows)
            {
                rights = AccessRightsHelper.AddRight(rights,
                    AccessRightsHelper.Convert(row["OptionKey"].ToString(), row["OptionValue"].ToString()));
            }
            return rights;
        }

        public PulseUser GetSessionUser(string userName, string password)
        {
            string commandText = "select top 1 * from [User] where UserName = @username and Password = @password";

            Dictionary<string, object> parameters = new Dictionary<string, object>()
            {
                {
                    "@username",
                    userName
                },
                {
                    "@password",
                    password
                },
            };

            // Execute the query
            var rows = _database.Query(commandText, parameters);

            // Query failed?
            if (rows.Count != 1)
            {
                return null;
            }
            // Fill in session variables from the result
            PulseUser user = new PulseUser(rows[0]);

            user.AccessRights = GetUserAccessRights(user);

            return user;
        }

        public List<PulseUser> GetUsersForSite(int siteId)
        {
            List<PulseUser> list = new List<PulseUser>();

            // string commandText = "SELECT *,s.POCEmail  FROM [User] u left join sites s on s.IDSite = u.IDSite where u.IDSite = @siteId";
            
            string commandText = "SELECT  isnull(s.POCEmail,(select top 1 POCEmail from dbo.sites where IDSite in (select idsite from sites where customerid = s2.idcustomer) and POCEmail is not null)) as POCEmail, u.CreatedDate , u.CreatedDateUTC , u.UserName ,u.Password ,u.FirstName ,u.LastName ,u.Email ,u.Title ,u.AccessLevel ,u.IDSite ,u.PrimaryPhone , " +
                                 " u.OtherPhone ,u.Notes ,u.UTC_Offset ,u.CreatedUserID ,u.ModifiedDateUTC ,u.IDUser ,u.LastLoginDateUTC ,u.UserTypeID ,u.RegionID ,u.SFAcctOwnerID " +
                                 " FROM dbo.[User] u left join dbo.sites s on s.IDSite = u.IDSite  left join dbo.Customers s2 on s2.IDCustomer = u.IDSite where u.IDSite = @siteId";
            // string commandText = "SELECT *,s.POCEmail  FROM [User] u where u.IDSite = @siteId";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
            {
                "@siteId",
                siteId
            } };

            // Execute the query
            var dbList = _database.Query(commandText, parameters);
            foreach (var rows in dbList)
            {
                PulseUser user = new PulseUser(rows);

                user.AccessRights = GetUserAccessRights(user);

                list.Add(user);
            }
            return list;
        }

        //private string BuildUserOptionSQL(RadGrid grid, int UserID)
        //{//syntax: values (_,_,_), (_,_,_)....etc DC

        //    CheckBoxList cbl = ((CheckBoxList)Common.FindControlRecursively(grid, "cblUserOptions"));
        //    string strSQL = string.Format("Delete from [UserOptions] where User_ROW_ID = {0}", UserID);
        //    strSQL += " Insert into [UserOptions] (User_ROW_ID, OptionKey, OptionValue ) Values ";
        //    string Key = "";
        //    string Value = "";

        //    for (int i = 0; i < cbl.Items.Count; i++)
        //    {
        //        strSQL += i != 0 ? ", ( " : " (";
        //        ListItem li = (ListItem)cbl.Items[i];
        //        Key = li.Value;
        //        Value = li.Selected ? "1" : "0";
        //        strSQL += string.Format("{0},'{1}',{2}) ", UserID, Key, Value);
        //    }

        //    return strSQL;
        //}

        private string BuildUserOptionSQL(PulseUser user)
        {
            //syntax: values (_,_,_), (_,_,_)....etc DC
            int userID = Convert.ToInt32(user.Id);
            string strSQL = string.Format("Delete from [UserOptions] where User_ROW_ID = {0}", userID);
            strSQL += " Insert into [UserOptions] (User_ROW_ID, OptionKey, OptionValue ) Values ";

            for (int i = 0; i <= (int)UserAccessRights.NUM_FLAGS; i++)
            {
                string value = "0";

                UserAccessRights cur = (UserAccessRights)(1 << i);
                if (user.AccessRights.HasFlag(cur))
                {
                    value = "1";
                    strSQL += string.Format("({0},'{1}',{2}), ", userID, cur.ToString(), value);
                }
            }
            strSQL = strSQL.Remove(strSQL.Length - 2, 2);
            return strSQL;
        }

        private int GetNewUserID(string email)
        {
            string strSQL = string.Format("Select IDUser from [User] where Email = '{0}'", email);

            int ID = Convert.ToInt32(_database.QueryValue(strSQL, null));
            return ID;
        }

        private void DeleteUserInternal(string user)
        {
            string strSQL = "Delete from [User] where IDUser = (select IDUser from [User] where username = '" + user + "')";
            _database.Execute(strSQL, null);
            PulseSession.Instance.User.CoUsers = null;
            api.GetCoUsers();
        }

        private void AddOrUpdateUserInternal(PulseUser user)
        {
            string strSQL = " ";

            //check for optionals - not verified on the ValidUser function            
            string title = !string.IsNullOrWhiteSpace(user.Title) ? user.Title : " ";
            string phone1 = !string.IsNullOrWhiteSpace(user.Phone1) ? user.Phone1 : " ";
            string phone2 = !string.IsNullOrWhiteSpace(user.Phone2) ? user.Phone2 : " ";
            
            int activeUserID = api.SessionUser != null ? api.SessionUser.Id : kUnknownUserId;

            // is this a new user?
            if (!user.HasValidId())
            {
                System.TimeSpan temp = System.DateTime.Now - System.DateTime.UtcNow;
                string s =
                    "Insert into [User] (UserName, [Password], FirstName, LastName, Email, Title, AccessLevel, IDSite, PrimaryPhone, OtherPhone";
                s +=
                    ", CreatedUserID,CreatedDate,CreatedDateUTC)  values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6}, '{7}','{8}', '{9}', {10},'{11}','{12}')";
                // strSQL = string.Format(s, user.Email, user.EncryptedPassword, user.FirstName, user.LastName, user.Email, title, user.IDSite, phone1, phone2, activeUserID);
                strSQL = string.Format(s, user.Email, user.EncryptedPassword, user.FirstName, user.LastName, user.Email,
                    title, "' '", api.SessionUser.IDSite, phone1, phone2, activeUserID, DateTime.Now, DateTime.UtcNow);
                strSQL = strSQL.Replace("\"", "'");
            }
            else
            {
                //update User Table
                string s =
                    "Update [User] set UserName = '{0}', FirstName = '{1}', LastName = '{2}', Email = '{3}', Title = '{4}', ";
                s +=
                    "PrimaryPhone = '{5}', OtherPhone = '{6}', ModifiedDateUTC = GETUTCDATE(), Password = '{7}', IDSite = '{9}' Where IDUser = {8}";
                strSQL = string.Format(s, user.Email, user.FirstName, user.LastName, user.Email, title, phone1, phone2,
                    user.EncryptedPassword, Convert.ToInt32(user.Id), user.IDSite);
                strSQL = strSQL.Replace("\"", "'");
                // update site table
                s =
                    " ; Update [Sites] set POCEmail = '{0}' where IDSite = {1}  ";
                strSQL += string.Format(s, user.POCEmail, Convert.ToInt32(user.IDSite),Convert.ToInt32(user.IDSite));
                       s =
                    " ; Update [Sites] set POCEmail = '{0}' where CustomerID = {2} and (len(POCEmail) < 2 or POCEmail is null)";
                strSQL += string.Format(s, user.POCEmail, Convert.ToInt32(user.IDSite),Convert.ToInt32(user.IDSite));
                strSQL = strSQL.Replace("\"", "'");

            }

            if (_database.Execute(strSQL, null) != 0)
            {
                if (!user.HasValidId())
                {
                    user.Id = GetNewUserID(user.Email);
                }
                // Update the user options
                string cmd = BuildUserOptionSQL(user);
                _database.Execute(cmd, null);
                PulseSession.Instance.User.CoUsers = null;

                api.GetCoUsers();
            }
            //
            // Now we update user options
        }

        public void SendWelcomeEmail(PulseUser user)
        {
            string message =
                string.Format(@"<p><b>Welcome to Pulse!</b><br/><br/> To login to Pulse, you will need your email address and password.</p><p>Your
                                               Pulse password is: <b>{0}",
                    Utils.Security.Crypto.DecryptString(user.EncryptedPassword));
            message +=
                "</b></p><p>Use the link below to login:<br><a href=\"https://pulse.myenovate.com\">https://pulse.myenovate.com</a></p><p>";
            //message += "Once you login to Pulse, you may change your password at any time by clicking the <b>My Data </b>button, then clicking <b>Options</b>.</p>";
            SendEmail(user.Email, "Welcome to Pulse", message);
        }

        public void SendEmail(string email, string subject, string message)
        {
            string strSQL =
                string.Format(
                    @"Insert into EmailQueue (Subject, ToAddresses, Body, DateCreatedUTC, FromAddress, FromName) Values 
                                        ('{0}', '{1}', '{2}', GETUTCDATE(), 'Pulse@EnovateMedical.com', 'Pulse Notification')",
                    subject, email, message);

            using (SQLDatabase db = new SQLDatabase())
            {
                db.Execute(strSQL, null);
            }
        }

        public void SendForgotPWEmail(string userEmail)
        {
            var rows = this._database.Query(string.Format("select * from [User] where Email = '{0}'", userEmail), null);
            if (rows.Count > 0)
            {
                string EncryptedPassword = rows[0]["Password"].ToString();
                string UnencryptedPassword = Utils.Security.Crypto.DecryptStringAES(EncryptedPassword, "C0mp@ssion");

                string sMsg =
                    string.Format(
                        "<p>Your Pulse password is: <b>{0}</b></p><p>Once you login to Pulse, you may change your password at any time by clicking the settings menu</p>",
                        UnencryptedPassword);

                SendEmail(userEmail, "Pulse Login Assistance", sMsg);
                ErrorHandler.PopUpMessage(
                    string.Format(
                        "An e-mail has been sent to {0} with login instructions.  It may take a few minutes to arrive.",
                        userEmail));
            }
            else
            {
                ErrorHandler.PopUpMessage(
                    "That email address was not found in Pulse. Please make sure you are using the email address that is registered for you in Pulse, then try again.");
            }
        }

        public void AddNewUser(PulseUser user)
        {
            AddOrUpdateUserInternal(user);

            SendWelcomeEmail(user);
        }

        public void DeleteUser(string user)
        {
            DeleteUserInternal(user);
        }

        public void UpdateExistingUser(PulseUser user)
        {
            AddOrUpdateUserInternal(user);
        }

        public bool DoesEmailAlreadyExist(string email)
        {
            object val = _database.QueryValue(string.Format("Select IDUser from [User] where Email = '{0}'", email),
                null);
            if (val != null)
                return true;
            return false;
        }

        public List<Alert> GetAlertsForFacilities(List<Site> facilities, bool bUnreadOnly)
        {
            //string query = string.Format( "select top 10 DeviceSerialNumber, Description, CreatedDateUTC from [AlertMessage] where IDSite={0}",user.IDSite);
            // viewgetsfalerts
            List<Alert> alertList = new List<Alert>();
            foreach (Site site in facilities)
            {
                try
                {
                    string query =
                        string.Format(
                            @"select top 10 DeviceSerialNumber, Description, CreatedDateUTC from [AlertMessage] where SiteID={0} and Priority=911 and DeviceSerialNumber not like '%00000%'",
                            site.Id);
                    //string query = string.Format("select TOP 10 * from viewGetSFAlerts where CustomerID ={0}", site.Id);
                    var rows = _database.Query(query, null);
                    foreach (var dbRow in rows)
                    {
                        //Asset asset = ownedAssets.Find(x => x.SerialNumber == dbRow["DeviceSN"]);
                        alertList.Add(new Alert(dbRow));
                    }
                }
                catch (Exception ex)
                {
                    Utils.ErrorHandler.Error(ex.Message);
                }
            }

            return alertList;
        }

        private void GetVals(Dictionary<string, string> d, ref int active, ref int available, ref int offline)
        {
            if (d.ContainsKey("ActiveWorkstationCount"))
                active += Convert.ToInt32(d["ActiveWorkstationCount"]);
            if (d.ContainsKey("AvailableWorkstationCount"))
                available += Convert.ToInt32(d["AvailableWorkstationCount"]);
            if (d.ContainsKey("OfflineWorkstationCount"))
                offline += Convert.ToInt32(d["OfflineWorkstationCount"]);
        }

        public Dictionary<string, double> AssetSummary(Asset.AssetType type, string dateToSummarize, Site site,
            List<string> departmentList, string[] keys, ref int count, ref int NonZeroCount, out string date)
        {
            Dictionary<string, double> retVal = new Dictionary<string, double>();
            foreach (string key in keys)
            {
                retVal.Add(key, 0.0);
            }
            count = 0;
            NonZeroCount = 0;
            date = "";

            switch (type)
            {
                case Asset.AssetType.BATTERY:
                case Asset.AssetType.WORKSTATION:
                    {
                        DateTime dt = Convert.ToDateTime(dateToSummarize).AddDays(-1);

                        string query = string.Format("exec spSummary_GetSiteDepartmentList '{0}', {1}", dateToSummarize,
                            site.Id);
                        // string query = string.Format("exec spSummary_GetSiteDepartmentList '{0}', {1}", dt.ToString("yyyy-MM-dd"), site.Id);

                        var rows = _database.Query(query, null);

                        foreach (Dictionary<string, string> d in rows)
                        {
                            if (string.IsNullOrEmpty(date))
                            {
                                date = Utils.Misc.SafeShortDate(Utils.Misc.SafeGetKey(d, "Date"));
                            }

                            if (d.ContainsKey("IDDepartment"))
                            {
                                if (departmentList == null ||
                                    !string.IsNullOrEmpty(departmentList.Find(t => t == d["IDDepartment"])))
                                {
                                    count++;

                                    if (d["RemainingCapacity"] != null)
                                    {
                                        int iRC = Convert.ToInt32(Convert.ToDouble(Misc.SafeGetKey(d, "RemainingCapacity")));
                                        if (iRC > 1)
                                        {
                                            NonZeroCount++;
                                        }
                                    }

                                    foreach (string key in keys)
                                    {
                                        retVal[key] += Convert.ToDouble(Misc.SafeGetKey(d, key));
                                    }
                                }
                            }
                        }
                        if (type == Asset.AssetType.BATTERY || type == Asset.AssetType.CHARGER)
                        {
                            //int total = site.Assets.Where(s => s.GetCategory() == Asset.AssetCategory.BATTERY).ToList().Count;
                            if (departmentList == null)
                            {
                                query =
                                    string.Format(
                                        "select IDDepartment, BatteryCount,ActiveWorkstationCount, ActiveBatteryCount,BatteryInChargerCount,BatteryInWorkstationCount,CountFullBatteries,DormantBatteryCount,VacantChargerBays,ActiveChargerBays,InServiceWorkstationCount,AvailableWorkstationCount,DormantWorkstationCount,BayCount from SummarySiteAssets where IDSite = '{0}'",
                                        site.Id);
                            }
                            else
                            {
                                string departmentListString = "";
                                foreach (string sdep in departmentList)
                                {
                                    departmentListString += sdep + ",";
                                }
                                departmentListString = departmentListString.Remove(departmentListString.Length - 1, 1);
                                query =
                                    string.Format(
                                                  "select IDDepartment, BatteryCount,ActiveWorkstationCount, ActiveBatteryCount,BatteryInChargerCount,BatteryInWorkstationCount,CountFullBatteries,DormantBatteryCount,VacantChargerBays,ActiveChargerBays,InServiceWorkstationCount,AvailableWorkstationCount,DormantWorkstationCount,BayCount from SummarySiteAssets where IDSite = '{0}'" +
                                                  " and IDDepartment in ( " + departmentListString + ")", site.Id);
                            }
                            rows = _database.Query(query, null);
                            if (rows != null)
                            {
                                retVal["AvailableWorkstationCount"] = 0;
                                retVal["ActiveWorkstationCount"] = 0;
                                retVal["OfflineWorkstationCount"] = 0;
                                retVal["DormantBatteryCount"] = 0;
                                retVal["CountFullBatteries"] = 0;
                                retVal["BatteryInWorkstationCount"] = 0;
                                retVal["BatteryInChargerCount"] = 0;
                                retVal["BayCount"] = 0;
                                retVal["VacantChargerBays"] = 0;
                                retVal["ActiveChargerBays"] = 0;
                                retVal["InServiceWorkstationCount"] = 0;
                                retVal["DormantWorkstationCount"] = 0;
                                foreach (Dictionary<string, string> d in rows)
                                {
                                    if (departmentList == null ||
                                        !string.IsNullOrEmpty(departmentList.Find(t => t == d["IDDepartment"])))
                                    {
                                        //?retVal["AvailableWorkstationCount"] += Convert.ToDouble(Misc.SafeGetKey(d, "BatteryCount"));
                                        retVal["ActiveWorkstationCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "ActiveWorkstationCount"));
                                        retVal["ActiveBatteryCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "ActiveBatteryCount"));
                                        //retVal["AvailableWorkstationCount"] += Convert.ToDouble(Misc.SafeGetKey(d, "ActiveBatteryCount"));
                                        retVal["DormantBatteryCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "DormantBatteryCount"));
                                        retVal["CountFullBatteries"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "CountFullBatteries"));
                                        retVal["BatteryInWorkstationCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "BatteryInWorkstationCount"));
                                        retVal["BatteryInChargerCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "BatteryInChargerCount"));
                                        retVal["OfflineWorkstationCount"] = retVal["AvailableWorkstationCount"] -
                                                                            retVal["ActiveWorkstationCount"];
                                        retVal["BayCount"] += Convert.ToDouble(Misc.SafeGetKey(d, "BayCount"));
                                        retVal["VacantChargerBays"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "VacantChargerBays"));
                                        retVal["ActiveChargerBays"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "ActiveChargerBays"));
                                        retVal["InServiceWorkstationCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "InServiceWorkstationCount"));
                                        retVal["AvailableWorkstationCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "AvailableWorkstationCount"));
                                        retVal["DormantWorkstationCount"] +=
                                            Convert.ToDouble(Misc.SafeGetKey(d, "DormantWorkstationCount"));
                                    }
                                }
                                // retVal["OfflineWorkstationCount"] =  retVal["AvailableWorkstationCount"] - retVal["ActiveWorkstationCount"];
                            }
                        }
                        return retVal;
                    }

                case Asset.AssetType.CHARGER:
                    {
                        int c =
                            site.Assets.Count(
                                p => p.Status == Asset.AssetStatus.AVAILABLE || p.Status == Asset.AssetStatus.IN_SERVICE);
                        string query = string.Format("exec spChargerStatusCounts_GetBusinessUnitDepartmentList {0}", site.Id);
                        var rows = _database.Query(query, null);
                        retVal["AvailableWorkstationCount"] = 0;
                        retVal["ActiveWorkstationCount"] = 0;
                        retVal["OfflineWorkstationCount"] = 0;
                        retVal["InService"] = 0;
                        retVal["Available"] = 0;
                        foreach (Dictionary<string, string> d in rows)
                        {
                            //if (d["SiteID"] == site.Name)
                            {
                                if (d.ContainsKey("Department"))
                                {
                                    if (departmentList == null ||
                                        !string.IsNullOrEmpty(departmentList.Find(t => t == d["Department"])))
                                    {
                                        count++;

                                        foreach (string key in keys)
                                        {
                                            if (key == "VacantChargerBays" || key == "ActiveChargerBays" ||
                                                key == "BayCount")
                                            {
                                                retVal[key] = retVal[key];
                                            }
                                            else
                                            {
                                                retVal[key] += Convert.ToDouble(Misc.SafeGetKey(d, key, false));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
            return retVal;
        }

        public string GetOrCreateDepartmentID(string BusinessUnitID, string Description)
        {
            string retval = "0";
            try
            {
                string query = string.Format("exec spGetOrCreateDepartmentID '{0}', '{1}'", BusinessUnitID, Description);
                var rows = _database.Query(query, null);
                if (rows != null && rows.Count == 1)
                {
                    retval = Misc.SafeGetKey(rows[0], "IDDepartment");
                }
                else
                {
                    ErrorHandler.Error("System error");
                }
            }
            finally
            {
            }
            return retval;
        }

        public int UpdateUser(PulseUser user)
        {
            return 0;
        }

        public int AddAsset(Asset asset)
        {
            string cmdText =
                string.Format("Insert into [Assets] (SerialNo, Description, DepartmentID)  values ('{0}',{1}, '{2}')",
                    asset.SerialNumber, asset.Description, asset.DepartmentId);
            return 0;
        }

        public int UpdateAsset(Asset asset)
        {
            string cmdText =
                string.Format(
                    "update Assets set Description = '{0}', IDAssetType = {1}, DepartmentID = {2}, AssetNumber = '{3}', Wing = '{4}', Floor = '{5}', SiteID = {6}, Notes = '{7}', Other = '{8}' where IDAsset = {9}",
                    asset.Description, (int)asset.Type, asset.DepartmentId, asset.AssetNumber, asset.Wing, asset.Floor,
                    asset.SiteID, asset.Notes, asset.Other, asset.IDAsset);
            int retVal;
            try
            {
                retVal = _database.Execute(cmdText, null);
            }
            catch (Exception x)
            {
                if (x.Message.ToString() ==
                    "Cannot insert duplicate key row in object 'dbo.Device' with unique index 'IX_Device_SerialNumber'.\r\nThe statement has been terminated.")
                {
                    Utils.ErrorHandler.PopUpMessage("Serial number ( " + asset.SerialNumber +
                                                    " ) is already in use. Each serial number must be unique, not yet assigned to any other device.");
                }
                return -1;
            }
            cmdText = string.Format("select * from AssetProduct where DeviceSerialNumber = '{0}' ", asset.SerialNumber);
            var output = _database.Query(cmdText, null);
            if (output.Count > 0)
            {
                cmdText =
                    string.Format(
                        "update AssetProduct set ProductSerialNumber = '{0}' where DeviceSerialNumber = '{1}' and ProductTypeID = 1",
                        asset.AIOSerial, asset.SerialNumber);
                retVal = _database.Execute(cmdText, null);
                cmdText =
                    string.Format(
                        "update AssetProduct set ProductSerialNumber = '{0}' where DeviceSerialNumber = '{1}' and ProductTypeID = 3",
                        asset.CartSerial, asset.SerialNumber);
                retVal = _database.Execute(cmdText, null);
                cmdText =
                    string.Format(
                        "update AssetProduct set ProductSerialNumber = '{0}' where DeviceSerialNumber = '{1}' and ProductTypeID = 2",
                        asset.DCMonitorSerial, asset.SerialNumber);
                retVal = _database.Execute(cmdText, null);
                return retVal;
            }
            else
            {
                cmdText =
                    string.Format(
                        "insert into AssetProduct (DeviceSerialNumber, ProductSerialNumber, ProductTypeID,Active,CreatedUserID) values ('{0}','{1}',1,1,{2})",
                        asset.SerialNumber, asset.AIOSerial, PulseSession.Instance.User.Id);
                retVal = _database.Execute(cmdText, null);
                cmdText =
                    string.Format(
                        "insert into AssetProduct (DeviceSerialNumber, ProductSerialNumber, ProductTypeID,Active,CreatedUserID) values ('{0}','{1}',2,1,{2})",
                        asset.SerialNumber, asset.DCMonitorSerial, PulseSession.Instance.User.Id);
                retVal = _database.Execute(cmdText, null);
                cmdText =
                    string.Format(
                        "insert into AssetProduct (DeviceSerialNumber, ProductSerialNumber, ProductTypeID,Active,CreatedUserID) values ('{0}','{1}',3,1,{2})",
                        asset.SerialNumber, asset.CartSerial, PulseSession.Instance.User.Id);
                retVal = _database.Execute(cmdText, null);
                return retVal;
            }

            return -1;
        }

        // TODO create a SP that does this on the DB side.
        public List<Dictionary<string, string>> AssetsExpiring(Site whichSite, DateTime begDate, DateTime endDate)
        {
            List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();
            try
            {
                string query = string.Format("exec spBatteriesPastWarrantyDateBySite '{0}'", whichSite.Id.ToString());
                var output = _database.Query(query, null);
               
                foreach (Dictionary<string, string> cur in output)
                {
                    Asset asset = whichSite.Assets.Find(a => a.SerialNumber == cur["SerialNo"]);
                    if (asset != null)
                    {
                        list.Add(cur);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error("DB failure: " + ex.Message);
            }
            return list;
        }

        public List<Dictionary<string, string>> Query(string commandText)
        {
            List<Dictionary<string, string>> data = null;
            try
            {
                data = _database.Query(commandText, null);
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error("DB failure: " + ex.Message);
            }

            return data;
        }

        public byte[] QueryBinary(string commandText)
        {
            object data = null;
            try
            {
                data = _database.QueryValue(commandText, null);
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error("DB failure: " + ex.Message);
            }

            return data as byte[];
        }

        public int UpdateDepartment(Department department)
        {
            string cmdText =
                string.Format("update Departments set Description = '{0}', SiteID = '{1}'  WHERE IDDepartment={2}",
                    department.Name, department.SiteID, department.Id);

            try
            {
                int retVal = _database.Execute(cmdText, null);
                return retVal;
            }
            catch (Exception x)
            {
                Utils.ErrorHandler.Error(x.Message);
            }
            return -1;
        }

        public int DeleteDepartment(int department)
        {
            string cmdText = string.Format("delete from  Departments WHERE IDDepartment={0}", department);

            try
            {
                int retVal = _database.Execute(cmdText, null);
                return retVal;
            }
            catch (Exception x)
            {
                Utils.ErrorHandler.Error(x.Message);
            }
            return -1;
        }

        public List<Dictionary<string, string>> GetAllFacilities()
        {
            string commandText = "select SiteDescription, IDSite from Sites";

            // Execute the query
            return _database.Query(commandText, null);
        }

        public List<Dictionary<string, string>> GetAllCustomers()
        {
            string commandText = "select CustomerName, IDCustomer from Customers";

            // Execute the query
            return _database.Query(commandText, null);
        }

        public List<Dictionary<string, string>> GetAllCustomersBySiteOrSfAcctOwnerID(String SFAccountOwnerID, int SiteID, PulseUser oUser)
        {
            string commandText = "select CustomerName, IDCustomer from Customers";
            if (SFAccountOwnerID == null || SFAccountOwnerID == "")
            {
                if (SiteID == 4 || oUser.UserTypeID == 3)
                {
                    commandText = "select CustomerName, IDCustomer from Customers";
                }
            }
            else
            {
                commandText = "select CustomerName, IDCustomer from Customers where IDCustomer in (select CustomerID from sites where SfAcctOwnerId like '%" + SFAccountOwnerID.Substring(0, SFAccountOwnerID.Length - 5) + "%') or IDCustomer=4";
            }
            // Execute the query
            return _database.Query(commandText, null);
        }

        public DataTable QuerySp(string storedProc, Dictionary<string, object> parms)
        {
            return _database.QuerySp(storedProc, parms);
        }

        public DataTable QuerySp2(string storedProc, Dictionary<string, object> parms)
        {
            return _database.QuerySp2(storedProc, parms);
        }

        public List<Dictionary<string, string>> QuerySp1(string storedProc, Dictionary<string, object> parms)
        {
            return _database.Query(storedProc, parms);
        }
    }
}