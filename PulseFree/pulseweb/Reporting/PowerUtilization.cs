namespace PulseWeb.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using PulseWeb.API;
    using PulseWeb.Model;
    using System.Linq;

    /// <summary>
    /// Summary description for PowerUtilization.
    /// </summary>
    public partial class PowerUtilization : ReportBase
    {
        public PowerUtilization()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            SetTitle("Power Consumption Report by Workstation");
        }
        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title;
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            base.OnNeedSiteDataSource(report, site);
            string s = @"select top 100 Date, SerialNo, AssetNumber, IDDepartment, abs(AvgAmpDraw) as AvgAmpDraw from SummaryWorkstation where IDSite = {0} and Date = '{1}' and AvgAmpDraw < 0 order by AvgAmpDraw ASC";
            DateTime date = (DateTime)report.Parameters["ReportDate"].Value;
            string query = string.Format(s,site.Id, ReformatDate(date.AddDays(-1)));
            using (PulseDBInterface db = new PulseDBInterface())
            {
                //db.repo
                var temp = db.Query(query);
                if (temp != null)
                {
                    // We need to place into a structure that has filed names (DeviceSN, etc) that the data grid is expecting
                    report.DataSource = temp.Select(x => new
                    {
                        Date = Utils.Misc.SafeGetKey(x, "Date"),
                        SerialNo = Utils.Misc.SafeGetKey(x,"SerialNo"),
                        AssetNumber = Utils.Misc.SafeGetKey(x, "AssetNumber"),
                        Department = api.GetDepartmentName(Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(x, "IDDepartment"))),
                        AveAmpDraw = Utils.Misc.SafeConvertToDouble(Utils.Misc.SafeGetKey(x, "AvgAmpDraw")),
                    }).ToList();
                }
            }

        }
    }
}