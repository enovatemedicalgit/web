﻿using PulseWeb.API;
using PulseWeb.Model;
using System;
using System.Web;
using Telerik.Reporting;

namespace PulseWeb.Reporting
{
    public class ReportBase : Telerik.Reporting.Report
    {
        public virtual void SetTitle(string s){}
        public virtual void SetSite(string s){}
        public virtual void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report,Site site){}

        protected ObjectDataSource SiteObjectSource;

        public ReportBase()
        {
            this.SiteObjectSource = new Telerik.Reporting.ObjectDataSource();
            this.SiteObjectSource.DataMember = "SiteName";
            this.SiteObjectSource.Name = "FacilityObjectSource";

            this.NeedDataSource += OnNeedDataSource;
           

            if (HttpContext.Current != null && api.SessionUser != null)
            {
                this.SiteObjectSource.DataSource = api.SessionUser.Sites;
            }
            
        }

        protected override void OnNeedDataSource(object sender, EventArgs e)
        {
            Telerik.Reporting.Processing.Report report = (Telerik.Reporting.Processing.Report)sender;
            Site site = api.GetSite(Convert.ToInt32(report.Parameters["BusinessUnit"].Value));

            OnNeedSiteDataSource(report, site);
            this.SetSite(site.Name);
        }

        public static int GetDefaultSite()
        {
            try
            {
                return api.GetSitesForUser()[0].Id;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static string ReformatDate(System.DateTime input)
        {
            return string.Format("{0}/{1:D2}/{2}", input.Year, input.Month, input.Day);
        }

        public string MapStatus(string statusIdStr)
        {
            int statusId = Utils.Misc.SafeConvertToInt(statusIdStr);
            return Asset.StatusString((Asset.AssetStatus)statusId);
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // ReportBase
            // 
            this.Name = "ReportBase";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(6D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

    }
}