﻿<%@ Page Language="C#"MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" ClientIDMode="Predictable" CodeBehind="Reports.aspx.cs" Inherits="PulseWeb.Reporting.Reports" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
     <style type="text/css">           
        html#html, body#body, form#form1, div#content
        {  
            height: 100%;
        }
    </style>
   <div id="content">
 
 
       <telerik:ReportViewer ID="ReportViewer1"  runat="server" Height="100%"
           Width="100%"
           ShowDocumentMapButton="False"
           ShowHistoryButtons="False"
           ShowRefreshButton="False" ShowZoomSelect="True" ViewMode="PrintPreview"
           ShowNavigationGroup="false"
           ShowPrintPreviewButton="true"> runat="server">

       </telerik:ReportViewer>
 
   </div>
</asp:Content>
 


