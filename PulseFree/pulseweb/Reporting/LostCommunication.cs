namespace PulseWeb.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using PulseWeb.API;
    using PulseWeb.Model;
    using System.Linq;

    /// <summary>
    /// Summary description for LostCommunication.
    /// </summary>
    public partial class LostCommunication : ReportBase
    {
        public LostCommunication()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            SetTitle("Lost Communication Report");
        }
        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title;
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            base.OnNeedSiteDataSource(report, site);
            string query = string.Format(@"exec prcLostAssetComm {0}", site.Id);

            using (PulseDBInterface db = new PulseDBInterface())
            {
                //db.repo
                var temp = db.Query(query);
                if (temp != null)
                {
                    // We need to place into a structure that has filed names (DeviceSN, etc) that the data grid is expecting
                    report.DataSource = temp.Select(x => new
                    {
                        SerialNo = Utils.Misc.SafeGetKey(x, "SerialNo"),
                        Type = Utils.Misc.SafeGetKey(x, "Description"),
                        Department = Utils.Misc.SafeGetKey(x, "DeptDesc"),
                        LastPostDateUTC = Utils.Misc.SafeGetKey(x, "LastPostDateUTC"),
                        AssetDescription = Utils.Misc.SafeGetKey(x, "LongDescription"),
                   
                    }).ToList();
                }
            }

        }
    }
}