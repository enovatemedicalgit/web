namespace PulseWeb.Reporting
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using PulseWeb.API;
    using PulseWeb.Model;
    using System.Linq;

    /// <summary>
    /// Summary description for LostCommunication.
    /// </summary>
    public partial class WorkstationUtilization : ReportBase
    {
        public WorkstationUtilization()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            SetTitle("Workstation Utilization");
        }
        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title;
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            base.OnNeedSiteDataSource(report, site);
            string query = string.Format(@"exec spTrendingUtilization_WorkstationDetailTwoWeeks {0}", site.Id);

            using (PulseDBInterface db = new PulseDBInterface())
            {
                //db.repo
                var temp = db.Query(query);
                if (temp != null)
                {
                    // We need to place into a structure that has filed names (DeviceSN, etc) that the data grid is expecting
                    report.DataSource = temp.Select(x => new
                    {
                        SerialNo  = Utils.Misc.SafeGetKey(x, "SerialNo"),
                    
                        Department = api.GetDepartmentName(Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(x, "DepartmentID"))),
                        Date = Utils.Misc.SafeGetKey(x, "Date"),
                        AssetNumber = Utils.Misc.SafeGetKey(x, "AssetNumber"),
                        Wing = Utils.Misc.SafeGetKey(x, "Wing"),
                        Floor = Utils.Misc.SafeGetKey(x, "Floor"),
                        Other = Utils.Misc.SafeGetKey(x, "Other"),
                        Status = Utils.Misc.SafeGetKey(x, "Status"),
                        LoChargeInsertsCount = Utils.Misc.SafeGetKey(x, "LoChargeInsertsCount"),
                         HiChargeRemovalsCount = Utils.Misc.SafeGetKey(x, "HiChargeRemovalsCount"),
                         AvgRunRate = Utils.Misc.SafeGetKey(x, "AvgRunRate"),
                         AvgAmpDraw = Utils.Misc.SafeGetKey(x, "AvgAmpDraw"),
                    }).ToList();
                }
            }

        }
    }
}