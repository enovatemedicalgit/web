namespace PulseWeb.Reporting
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Telerik.Reporting;
    using PulseWeb.API;
    using PulseWeb.Model;
    using System.Linq;

    /// <summary>
    /// Summary description for BatteryWarrantyExpiring.
    /// </summary>
    public partial class BatteryWarrantyExpiring : ReportBase
    {
        public BatteryWarrantyExpiring()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            SetTitle("Batteries Expiring in the next 6 months");
        }

        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title;
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        
        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            using (PulseDBInterface db = new PulseDBInterface())
            {
                System.DateTime now = Utils.Time.Now;
                System.DateTime inSixMonths = now + new System.TimeSpan(30 * 6, 0, 0, 0);

                var result = db.AssetsExpiring(site, now, inSixMonths);
                report.DataSource = result.Select(x => new
                {
                    SerialNumber = Utils.Misc.SafeGetKey(x, "SerialNo"),
                    Description = Utils.Misc.SafeGetKey(x,"Description"),
                    Department = api.GetDepartmentName(Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(x, "DepartmentID"))),
                    ExpirationDate = Utils.Misc.SafeShortDate(Utils.Misc.SafeGetKey(x, "WarrantyExpDate")),
                }).ToList();

            }

        }

    }
}