namespace PulseWeb.Reporting
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Telerik.Reporting;
    using PulseWeb.API;
    using PulseWeb.Model;
    using System.Linq;

    /// <summary>
    /// Summary description for BatteryNotifications.
    /// </summary>
    public partial class BatteryNotifications : ReportBase
    {
        string mQuery = "select top 200 DeviceSerialNumber, BatterySerialNumber, Description, FriendlyDescription, AlertStatus, CreatedDateUTC from AlertMessage where SiteID = {0} and CreatedDateUTC > '{1}' and Priority=911 order by CreatedDateUTC desc";

        public BatteryNotifications()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            SetTitle("Assets With Notifications within the Last 30 Days");
        }

        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title;
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        protected string MapAlertStatus(string alertStatus)
        {
            // Pull this from DB
            switch (alertStatus)
            {
                case "0": return "Discovered";
                case "10": return "In Progress";
                case "20": return "Resolved";
                case "30": return "Case Created";
                default: return alertStatus;
            }
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            using (PulseDBInterface db = new PulseDBInterface())
            {
                var temp = db.Query(string.Format(mQuery, site.Id, Utils.Time.Now.AddDays(-30)));
                if (temp != null )
                {
                    // We need to place into a structure that has filed names (DeviceSN, etc) that the data grid is expecting
                    report.DataSource = temp.Select(x => new
                    {
                        DeviceSN = Utils.Misc.SafeGetKey(x, "DeviceSerialNumber"),
                        BatterySN = Utils.Misc.SafeGetKey(x, "BatterySerialNumber"),
                        Description = Utils.Misc.SafeGetKey(x, "Description"),
                        AlertStatus = this.MapAlertStatus(Utils.Misc.SafeGetKey(x, "AlertStatus")),
                        FriendlyDescription = Utils.Misc.SafeGetKey(x, "FriendlyDescription"),
                        Date = Utils.Misc.SafeGetKey(x, "CreatedDateUTC"),
                    }).ToList();

                }
            }

        }


    }
}