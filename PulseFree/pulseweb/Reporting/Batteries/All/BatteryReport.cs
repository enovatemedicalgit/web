namespace PulseWeb.Reporting
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Telerik.Reporting;
    using PulseWeb.API;
    using PulseWeb.Model;
    using System.Linq;

    /// <summary>
    /// Summary description for BatteryNotifications.
    /// </summary>
    public partial class BatteryReport : ReportBase
    {
        string mQuery = "exec prcAssetRpt '{0}', 5";

        public BatteryReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();
            SetTitle("Battery report");
        }

        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title;
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            base.OnNeedSiteDataSource(report, site);

            using (PulseDBInterface db = new PulseDBInterface())
            {
                var temp = db.Query(string.Format(mQuery, site.Id));

                if( temp !=  null )
                {
                    // We need to place into a structure that has filed names (DeviceSN, etc) that the data grid is expecting
                    report.DataSource = temp.Select(x => new
                    {
                        SerialNumber = Utils.Misc.SafeGetKey(x, "SerialNo"),
                        Type = Utils.Misc.SafeGetKey(x, "Description"),
                        ChargeLevel = Utils.Misc.SafeGetKey(x, "ChargeLevel"),
                        FullChargeCapacity = Utils.Misc.SafeGetKey(x, "FullChargeCapacity"),
                        CycleCount = Utils.Misc.SafeGetKey(x, "CycleCount"),
                        // not included
                        Description = Utils.Misc.SafeGetKey(x, "AssetNumber",false,""),
                        AssetStatus = MapStatus(Utils.Misc.SafeGetKey(x, "AssetStatusID")),
                        Department = api.GetDepartmentName(Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(x, "DepartmentID"))),
                        Wing = Utils.Misc.SafeGetKey(x, "Wing",false,""),
                        Floor = Utils.Misc.SafeGetKey(x, "Floor", false, ""),

                        //UtilizationLvl = Utils.Misc.SafeGetKey(x, "UtilizationLvl"),
                        //HiChargeRemovals = Utils.Misc.SafeGetKey(x, "HiChargeRemovals"),
                        //LoChargeInserts = Utils.Misc.SafeGetKey(x, "LoChargeInserts"),

                    }).ToList();

                }
            }

        }


    }
}