using System.Collections.Generic;
using Telerik.Reporting;


namespace PulseWeb.Reporting
{
    partial class ChargerReport
    {


        #region Component Designer generated code
        /// <summary>
        /// Required method for Telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.Drawing.FormattingRule formattingRule1 = new Telerik.Reporting.Drawing.FormattingRule();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChargerReport));
            Telerik.Reporting.ReportParameter siteParam = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
            this.shape5 = new Telerik.Reporting.Shape();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.SiteNameText = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.ReportNameText = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.7000001072883606D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1});
            this.detail.Name = "detail";
            // 
            // panel1
            // 
            formattingRule1.Filters.Add(new Telerik.Reporting.Filter("= RowNumber()%2", Telerik.Reporting.FilterOperator.Equal, "1"));
            formattingRule1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.panel1.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule1});
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.textBox14,
            this.textBox4,
            this.textBox7,
            this.textBox3,
            this.textBox2,
            this.textBox11,
            this.textBox12,
            this.textBox10,
            this.textBox9,
            this.textBox8,
            this.textBox6,
            this.textBox5,
            this.textBox1});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.3040475845336914D), Telerik.Reporting.Drawing.Unit.Inch(0.69996070861816406D));
            this.panel1.Style.BackgroundColor = System.Drawing.Color.Empty;
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80008035898208618D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox15.Style.Font.Italic = false;
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox15.Value = "=Fields.Status";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49992117285728455D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox14.Value = "Status:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.097877345979213715D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5936712026596069D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox4.Style.Font.Italic = false;
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox4.Value = "=Fields.SerialNumber";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.09375D), Telerik.Reporting.Drawing.Unit.Inch(0.097877345979213715D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4061315059661865D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox7.Style.Font.Italic = false;
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox7.Value = "=Fields.Type";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.097877345979213715D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99996054172515869D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox3.Value = "Serial Number:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.59375D), Telerik.Reporting.Drawing.Unit.Inch(0.097877345979213715D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42287737131118774D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox2.Value = "Type:";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.097877345979213715D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70420807600021362D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox11.Value = "Number:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2042479515075684D), Telerik.Reporting.Drawing.Unit.Inch(0.097877345979213715D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0956730842590332D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox12.Style.Font.Italic = false;
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox12.Value = "=Fields.AssetNumber";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6979165077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999200820922852D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox10.Style.Font.Italic = false;
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox10.Value = "=Fields.Floor";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1979165077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49992117285728455D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox9.Value = "Floor:";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.09375D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49992117285728455D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox8.Value = "Wing:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.59375D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox6.Style.Font.Italic = false;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox6.Value = "=Fields.Wing";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.90003949403762817D), Telerik.Reporting.Drawing.Unit.Inch(0.39791679382324219D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1228775978088379D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.textBox5.Style.Font.Italic = false;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox5.Value = "=Fields.Department";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.39996066689491272D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89996063709259033D), Telerik.Reporting.Drawing.Unit.Inch(0.29996061325073242D));
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox1.Value = "Department:";
            // 
            // reportHeaderSection1
            // 
            this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1D);
            this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape5,
            this.textBox13,
            this.SiteNameText,
            this.pictureBox1,
            this.ReportNameText});
            this.reportHeaderSection1.Name = "reportHeaderSection1";
            // 
            // shape5
            // 
            this.shape5.Anchoring = ((Telerik.Reporting.AnchoringStyles)((Telerik.Reporting.AnchoringStyles.Left | Telerik.Reporting.AnchoringStyles.Right)));
            this.shape5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.90000009536743164D));
            this.shape5.Name = "shape5";
            this.shape5.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2999601364135742D), Telerik.Reporting.Drawing.Unit.Point(3.75D));
            this.shape5.Stretch = true;
            this.shape5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.shape5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.shape5.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.shape5.Style.Color = System.Drawing.Color.Transparent;
            // 
            // textBox13
            // 
            this.textBox13.Format = "{0:d}";
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999210596084595D), Telerik.Reporting.Drawing.Unit.Inch(0.18488501012325287D));
            this.textBox13.Style.Font.Italic = false;
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox13.Value = "= Now()";
            // 
            // SiteNameText
            // 
            this.SiteNameText.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.002083420753479D), Telerik.Reporting.Drawing.Unit.Inch(0.50208330154418945D));
            this.SiteNameText.Name = "SiteNameText";
            this.SiteNameText.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.100001335144043D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.SiteNameText.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.SiteNameText.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.SiteNameText.Style.Font.Bold = false;
            this.SiteNameText.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.SiteNameText.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.SiteNameText.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.SiteNameText.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.SiteNameText.Value = "Site Placeholder";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.095833458006381989D), Telerik.Reporting.Drawing.Unit.Inch(0.095833301544189453D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5000394582748413D), Telerik.Reporting.Drawing.Unit.Inch(0.39992138743400574D));
            this.pictureBox1.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.pictureBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // ReportNameText
            // 
            this.ReportNameText.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.595833420753479D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.ReportNameText.Name = "ReportNameText";
            this.ReportNameText.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.0000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.ReportNameText.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.ReportNameText.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.ReportNameText.Style.Font.Bold = false;
            this.ReportNameText.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.ReportNameText.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.ReportNameText.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.ReportNameText.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.ReportNameText.Value = "Title Placeholder";
            // 
            // ChargerReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.reportHeaderSection1});
            this.Name = "ListBoundReport";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
                 siteParam.AutoRefresh = true;
            siteParam.AvailableValues.DataSource = this.SiteObjectSource;
            siteParam.AvailableValues.DisplayMember = "= Fields.Name";
            siteParam.AvailableValues.ValueMember = "=Fields.Id";
            siteParam.Name = "BusinessUnit";
            siteParam.Text = "Select a Site";
            siteParam.Type = Telerik.Reporting.ReportParameterType.Integer;
            siteParam.Value = "= PulseWeb.Reporting.ReportBase.GetDefaultSite()";
            siteParam.Visible = true;
            this.ReportParameters.Add(siteParam);
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector("Header")});
            styleRule1.Style.Font.Bold = true;
            styleRule1.Style.Font.Name = "Segoe UI Light";
            styleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(25D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule2.Style.Font.Name = "Segoe UI Light";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.3000001907348633D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DetailSection detail;
        private ReportHeaderSection reportHeaderSection1;
        private Shape shape5;
        private TextBox textBox13;
        private TextBox SiteNameText;
        private PictureBox pictureBox1;
        private TextBox ReportNameText;
        private Panel panel1;
        private TextBox textBox15;
        private TextBox textBox14;
        private TextBox textBox4;
        private TextBox textBox7;
        private TextBox textBox3;
        private TextBox textBox2;
        private TextBox textBox11;
        private TextBox textBox12;
        private TextBox textBox10;
        private TextBox textBox9;
        private TextBox textBox8;
        private TextBox textBox6;
        private TextBox textBox5;
        private TextBox textBox1;


    }
}
