namespace PulseWeb.Reporting
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Telerik.Reporting;
    using System.Collections.Generic;
    using System.Linq;
    using PulseWeb.API;
    using PulseWeb.Model;

    /// <summary>
    /// Demonstrates binding to an IList.
    /// </summary>
    public partial class ChargerReport : ReportBase
    {
        // workstation = 1, charger = 2, battery = 5, 
        string mQuery = "exec prcAssetRpt '{0}', 2";

        public ChargerReport()
        {
            /// <summary>
            /// Required for Telerik Reporting designer support
            /// </summary>
            InitializeComponent();
            SetTitle("Charger Report");
        }


        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title; 
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            base.OnNeedSiteDataSource(report, site);

            using (PulseDBInterface db = new PulseDBInterface())
            {
                var temp = db.Query(string.Format(mQuery, site.Id));
                if (temp != null )
                {
                    // We need to place into a structure that has filed names (DeviceSN, etc) that the data grid is expecting
                    report.DataSource = temp.Select(x => new
                    {
                        SerialNumber = Utils.Misc.SafeGetKey(x, "SerialNo"),
                        Type = Utils.Misc.SafeGetKey(x, "Description"),
                        AssetNumber = Utils.Misc.SafeGetKey(x, "AssetNumber", false, ""),
                        Department = api.GetDepartmentName(Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(x, "DepartmentID"))),
                        Wing = Utils.Misc.SafeGetKey(x, "Wing", false, ""),
                        Floor = Utils.Misc.SafeGetKey(x, "Floor", false, ""),
                        Status = MapStatus(Utils.Misc.SafeGetKey(x, "AssetStatusID")),

                    }).ToList();

                }
            }

        }
 
    }
}