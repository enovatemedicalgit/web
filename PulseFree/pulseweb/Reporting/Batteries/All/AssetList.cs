namespace PulseWeb.Reporting
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using Telerik.Reporting;
    using PulseWeb.Model;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Demonstrates binding to an IList.
    /// </summary>
    public partial class AssetList : ReportBase
    {
        Asset.AssetCategory ReportType { get; set; }

        public AssetList(Asset.AssetCategory reportType)
        {
            /// <summary>
            /// Required for Telerik Reporting designer support
            /// </summary>
            InitializeComponent();
            ReportType = reportType;
            if (ReportType == Asset.AssetCategory.UNKNOWN)
            {
                SetTitle("Asset List");
            }
            else
            {
                char[] c = ReportType.ToString().ToLower().ToCharArray();
                c[0] = Char.ToUpper(c[0]);
                SetTitle(new string(c) + " List");

            }
        }


        public override void SetTitle(string title)
        {
            this.ReportNameText.Value = title; 
        }

        public override void SetSite(string s)
        {
            this.SiteNameText.Value = s;
        }

        public override void OnNeedSiteDataSource(Telerik.Reporting.Processing.Report report, Site site)
        {
            base.OnNeedSiteDataSource(report, site);

            if (this.ReportType == Asset.AssetCategory.UNKNOWN)
                report.DataSource = site.Assets;
            else
                report.DataSource = site.Assets.Where(s => s.GetCategory() == this.ReportType );
        }
 
    }
}