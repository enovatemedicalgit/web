﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Reporting;
using Telerik.ReportViewer;
using PulseWeb.Reporting;
using System.Collections.Specialized;
using PulseWeb.Model;
using System.Configuration;
using System.Data.SqlClient;
using PulseWeb.API;
using System.Data;

namespace PulseWeb.Reporting
{

    public partial class Reports : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                NameValueCollection qscoll = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                if (Array.FindIndex(qscoll.AllKeys, p => p == "site") >= 0 && Array.FindIndex(qscoll.AllKeys, p => p == "queryid") >= 0)
                {
                    ReportBase report = null;
                    Site selectedSite = PulseWeb.API.api.GetSite(qscoll["site"]);
                    if (selectedSite == null && api.GetSitesForUser().Count > 0)
                        selectedSite = api.GetSitesForUser()[0];
                    int reportId = Convert.ToInt32(qscoll["queryid"]);
                    switch (reportId)
                    {
                        default:
                        case 0: report = new BatteryNotifications(); break;
                        case 1: report = new BatteryWarrantyExpiring(); break;
                        case 2: report = new BatteryReport(); break;
                        //case 2: report = new AssetReport(Asset.AssetCategory.BATTERY); break;
                        case 3: report = new WorkstationReport(); break;
                        case 4: report = new ChargerReport(); break;
                        case 5: report = new AssetReport( ); break;
                        case 6: report = new PowerUtilization(); break;
                        case 7: report = new LostCommunication(); break;
                             case 8: report = new WorkstationUtilization(); break;
                    }
                    // Setup the report
                    report.SetSite(selectedSite.Name);

                    var instanceReportSource = new Telerik.Reporting.InstanceReportSource();
                    instanceReportSource.ReportDocument = report;
                    this.ReportViewer1.ReportSource = instanceReportSource;
                    this.ReportViewer1.ShowNavigationGroup = true;
                    this.ReportViewer1.ShowPrintPreviewButton = false;
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(2000);
        }

        protected void RadDropDownList1_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            string s = "=Fields." + e.Text;
            ((this.ReportViewer1.ReportSource as Telerik.Reporting.InstanceReportSource).ReportDocument as AssetReport).Sortings.Clear();
            ((this.ReportViewer1.ReportSource as Telerik.Reporting.InstanceReportSource).ReportDocument as AssetReport).Sortings.Add(new Telerik.Reporting.Sorting(s, Telerik.Reporting.SortDirection.Desc));
        }
    }
}
