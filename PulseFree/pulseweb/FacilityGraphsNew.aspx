﻿<%@ Page Language="C#"MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" ClientIDMode="Predictable" CodeBehind="FacilityGraphsNew.aspx.cs" Inherits="PulseWeb.FacilityGraphsNew" %>
<%@ Register Src="~/modules/header.ascx" TagPrefix="uc1" TagName="header" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content runat="server" ContentPlaceHolderID="head">

<style type="text/css">
        body {
          overflow:auto;
       
        }
        /*div{
            zoom: 90%;
        }*/
        .Chart1
        {
            position:relative;
            left:0px;
            top:1px;
        }
           .Chart2
        {
            position:relative;
            left:10px;
            top:140px;
        }
        .zoom {
         zoom: .9;
            }
     


</style>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">


    <uc1:header runat="server" id="header1" /> 

     <div id="wrapper" style="zoom: 90%">
         
 <div class="facilityheader" >
                <div class="row">
                    <div class="companyLeft">
                        CustomerNew
                       <img src="Images/arrow.png" alt=">" width="10" height="15" />
                    </div>
                    <div id="radios" class="companyRight">
                        <div style="position:relative;top:7px; left:8px;">
                            <asp:RadioButtonList ID="CustomerRadioButtonList" runat="server" AutoPostBack="true"  CssClass="pulseRadioButtonList" OnSelectedIndexChanged="rdlSlelectAll_SelectedIndexChanged" >
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="radios1" class="facilityLeft">
                        <div class="normalText">Facility &nbsp;&nbsp;<img src="Images/arrow.png" alt=">" width="10" height="15" /></div>
                        <div>
                            <asp:RadioButtonList ID="rdlSlelectAll" runat="server" AutoPostBack="true" CssClass="radioButtonListSelectAll" OnSelectedIndexChanged="rdlSlelectAll_SelectedIndexChanged" Visible="false">
                                <asp:ListItem Value="1" Text="Select All" />
                                <asp:ListItem Value="0" Text="Clear All" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="radios2" class="facilityRight">
                        <div style="position:relative;left:8px;top:-8px;min-width:1600px">
                        <asp:CheckBoxList ID="chkFacilities" runat="server"  Height="32" RepeatDirection="Vertical" CssClass="pulseCheckboxList" RepeatLayout="Table" RepeatColumns="7" AutoPostBack="True" OnSelectedIndexChanged="chkFacilities_SelectedIndexChanged" Font-Size="Smaller" OnDataBinding="FacilitytList_DataBinding" CellPadding="1" CellSpacing="1">
                            <asp:ListItem Value="0" Text="Facility number one" />
                            <asp:ListItem Value="1" Text="Facility number two" />
                            <asp:ListItem Value="2" Text="Facility number three" />
                            <asp:ListItem Value="3" Text="Facility number four" />
                            <asp:ListItem Value="4" Text="Facility number five" />
                            <asp:ListItem Value="5" Text="Facility number six" />
                            <asp:ListItem Value="6" Text="Facility number seven" />
                            <asp:ListItem Value="7" Text="Facility number eight" />
                            <asp:ListItem Value="8" Text="Facility number nine" />
                            <asp:ListItem Value="9" Text="Facility number ten" />
                            <asp:ListItem Value="10" Text="Facility number 10" />
                            <asp:ListItem Value="11" Text="Facility number 11" />
                            <asp:ListItem Value="12" Text="Facility number 12" />
                            <asp:ListItem Value="13" Text="Facility number 13" />
                            <asp:ListItem Value="14" Text="Facility number 14" />
                            <asp:ListItem Value="15" Text="Facility number 15" />
                            <asp:ListItem Value="16" Text="Facility number 16" />
                            <asp:ListItem Value="17" Text="Facility number 17" />
                            <asp:ListItem Value="18" Text="Facility number 18" />
                            <asp:ListItem Value="19" Text="Facility number 19" />
                            <asp:ListItem Value="20" Text="Facility number 20" />
                        </asp:CheckBoxList>
                    </div>
                    </div>
                </div>
     </div>
 <div class="container-graph" style="zoom: 90%">
              <telerik:RadListView ID="rptReports" runat="server" OnItemDataBound="rptReports_ItemDataBound" ClientIDMode="Predictable" >
                    <ItemTemplate>
                        <div class="row">
                            <div class="facilityGraphSecionHeader">
                                <div class="facilityTitle">
                                    <asp:Label ID="FacilityID" CssClass="heading" Visible="false" runat="server" Text='<%#Eval("key")%>' /><asp:Label ID="facilityName" CssClass="heading" runat="server" Text='<%# this.FacilityName(Eval("key"))%>' /><br />
                                    <div class="departments">
                                        <asp:Label ID="Departments" runat="server" Text="Departments" />
                                        <img src="Images/arrow.png" alt=">" width="10" height="15" />
                                        <telerik:RadComboBox ID="DepartmentList" runat="server" CheckBoxes="true" CssClass="departmentDDL" AutoPostBack="True" 
                                            OnDataBinding="DepartmentList_DataBinding" EnableCheckAllItemsCheckBox="true" OnInit="DepartmentComboOnInit"  DropDownAutoWidth="Enabled">
                                        </telerik:RadComboBox>
                                        <telerik:RadButton ID="RadButton1" runat="server" Text="Get Checked Items" OnClick="Button1_Click" />
                                    </div>

                                </div>
                                <div class="assetTracking">
                                    <asp:Button ID="Button1"  Enabled="true" CssClass="btnAssetTracking" runat="server" Visible="false" Text="Asset Tracking Map" OnClick="BtnClickAssetTracking" CommandName='<%# this.FacilityName( Eval("key"))%>' ></asp:Button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="firstRowRight">

                                <div class="charts">

                                    <div style="float: left; text-align: right; width: 55%">
                                        <div style="float: left; text-align: right; width: 80%; vertical-align: central">
                                            <div class="bigDonut" ondblclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',3)">
                                                <div class="battery">
                                                    <div class="chartTitleLeft">
                                                        <img src="Images/workstation_icon.png" alt="battery" style="position:relative; top:64px;"/>
                                                    </div>
                                                    <div class="chartTitleRight" style="white-space:nowrap;" onclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',3)" >Workstations</div>
                                                    <div class="infoIcon">
                                                        <img src="Images/icon.png" alt="Workstations" style="position:relative; top:80px;" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',3)"/>
                                                    </div>
                                                </div>
                                                <div id="graphBig0" runat="server" class="graphBig" onclick="$(this).clicked(this);"></div>

                                                <div class="legends" ondblclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',3)" >
                                                    <div class="legendTotals" >
                                                        <asp:Label ID="lblBig0Total"  runat="server" Text="630"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/7.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig0Label1" runat="server" Text="In Service"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/8.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig0Label2" runat="server" Text="Charging"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/10.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig0Label3" runat="server" Text="Dormant"></asp:Label>
                                                </div>
                                                      <%--        <div class="legendLabels">
                                                        <img src="Images/10.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig0Label4" runat="server" Text="Not Used"></asp:Label>
                                                    </div>--%>
                                                </div>
                                            </div>
                                            <div class="bigDonut" ondblclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',2)">
                                                <div class="battery">
                                                    <div class="chartTitleLeft">
                                                        <img src="Images/charger_icon.png" alt="battery"  style="position:relative; top:78px;"/>
                                                    </div>
                                                    <div class="chartTitleRight"  style="white-space:nowrap;">Charger Bays</div>
                                                    
                                                    <div class="infoIcon">
                                                        <img src="Images/icon.png" alt="Chargers" style="position:relative; top:84px;" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',2)"/>
                                                    </div>
                                                </div>
                                                <div id="graphBig1" runat="server" class="graphBig" onclick="$(this).clicked(this);"></div>

                                                <div class="legends"  ondblclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',2)">
                                                    <div class="legendTotals">
                                                        <asp:Label ID="lblBig1Total" runat="server" Text="630"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/4.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig1Label1" runat="server" Text="Charging"></asp:Label>
                                                    </div>
                                                 <div class="legendLabels">
                                                        <img src="Images/5.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig1Label2" runat="server" Text="Vacant"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/10.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig1Label3" runat="server" Text="Offline"></asp:Label>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="bigDonut" ondblclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',1)">
                                                <div class="battery">
                                                    <div class="chartTitleLeft">
                                                        <img src="Images/battery_icon.png" alt="battery" style="position:relative; top:76px;"/>
                                                    </div>
                                                    <div class="chartTitleRight" style="white-space:nowrap;"> Batteries</div>
                                                    <div class="infoIcon"><img src="Images/icon.png" alt="Batteries" style="position:relative; top:80px;" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',1)"/></div>
                                                </div>
                                                <div id="graphBig2" runat="server" class="graphBig"  onclick="$(this).clicked(this);"></div>

                                                <div class="legends" ondblclick="$(this).clickCircleButton(this,'<%#Eval("value")%>',1)">
                                                    <div class="legendTotals">
                                                        <asp:Label ID="lblBig2Total" runat="server" Text="630"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/1.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig2Label1" runat="server" Text="In Use"></asp:Label>
                                                    </div>
                                                    <div class="legendLabels">
                                                        <img src="Images/2.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig2Label2" runat="server" Text="Available"></asp:Label>
                                                    </div>
                                                      
                                                    <div class="legendLabels">
                                                        <img src="Images/3.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig2Label3" runat="server" Text="Offline"></asp:Label>
                                                    </div>
                                                     <div class="legendLabels">
                                                        <img src="Images/10.png" alt=">" width="10" height="15" />
                                                        <asp:Label ID="lblBig2Label4" runat="server" Text="Dormant"></asp:Label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="graphDrilDownA" runat="server" style="float: right; width: 480px; padding-top: 30px; display: inline-block">

                                        <div id="graphSmall00" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall0Total0" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall0Label0" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div id="info1a" class="infoIconR"><img src="Images/icon.png" alt="Workstation Utilization" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',10)"/></div>

                                        <div id="graphSmall01" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals"> 
                                                <asp:Label ID="lblSmall0Total1" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall0Label1" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                         <div id="info1b"  class="infoIconR"><img src="Images/icon.png" alt="Workstations Moving" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',11)"/></div>


                                        <div id="graphSmall02" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall0Total2" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall0Label2" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                         <div id="info1c"  class="infoIconR"><img src="Images/icon.png" alt="Avg PC Utilization" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',12)"/></div>
                                    </div>



                                    <div id="graphDrilDownB" runat="server" style="float: right; width: 480px; padding-top: 30px; display: none">
                                        <div id="graphSmall10" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall1Total0" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall1Label0" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div id="info2a"  class="infoIconR"><img src="Images/icon.png" alt="Fully Charged Batteries" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',7)"/></div>
                                        <div id="graphSmall11" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall1Total1" runat="server" Text="70%"></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall1Label1" runat="server" Text="Avg charge"></asp:Label>
                                            </div>
                                        </div>
                                        <div id="info2b"  class="infoIconR"><img src="Images/icon.png" alt="Low Charge Removals" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',8)"/></div>

                                        <div id="graphSmall12" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall1Total2" runat="server" Text="11.3hrs"></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall1Label2" runat="server" Text="Avg run time"></asp:Label>
                                            </div>
                                        </div>
                                        <div id="info2c"  class="infoIconR"><img src="Images/icon.png" alt="Average Link Quality" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',9)"/></div>
                                    </div>




                                    <div id="graphDrilDownC" runat="server" style="float: right; width: 480px; padding-top: 30px; display: none">
                                        <div id="graphSmall20" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall2Total0" runat="server" Text="63%"></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall2Label0" runat="server" Text="Avg charge"></asp:Label>
                                            </div>
                                        </div>
                                          <div id="info3a"  class="infoIconR"><img src="Images/icon.png" alt="Original Capacity" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',4)"/></div>

                                        <div id="graphSmall21" runat="server" class="graph"></div>
                                        <div class="legendsSmall">
                                            <div class="legendTotals">
                                                <asp:Label ID="lblSmall2Total1" runat="server" Text="70%"></asp:Label>
                                            </div>
                                            <div class="legendLabelsSmall">
                                                <asp:Label ID="lblSmall2Label1" runat="server" Text="Avg charge"></asp:Label>
                                            </div>
                                        </div>
                                          <div id="info3b"  class="infoIconR"><img src="Images/icon.png" alt="Avg Current Draw" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',5)"/></div>
                                       
                                    <div id="graphSmall22"  runat="server" class="graph"></div>
                                          <div   class="legendsSmall">
                                            <div   class="legendTotals">
                                                <asp:Label  ID="lblSmall2Total2" runat="server" Text="11.3hrs"></asp:Label>
                                            </div>
                                            <div  class="legendLabelsSmall">
                                                <asp:Label  ID="lblSmall2Label2" runat="server" Text="Avg run time"></asp:Label>
                                            </div>
                                        </div>
                                          <div  id="info3c"  class="infoIconR"><img src="Images/icon.png" alt="Run Rate" onclick="$(this).clickInfoButton(this,'<%#Eval("value")%>',6)"/></div>
                                    </div>
                                </div>
                                <div id="hzRule" runat="server" class="hzrule"></div>
                               
                                    <div id="vtRule" runat="server" class="vtrule"></div>
                                 <div id="hzRuleR" runat="server" class="hzruleR"></div>
                                    <div id="vtRule2" runat="server" class="vtrule2"></div>
                                
                            </div>

<%--                            <div id="testChart_batteries" runat="server"  style="display:normal">
                                <asp:Image  runat="server" ImageUrl='<%# this.GetChartURL( Eval("key"),"batteries" )%>'  CssClass="Chart1" AlternateText="chart"/>
                            </div>
                            <div id="testChart_chargers"  runat="server" style="display:none">
                                <asp:Image  runat="server" ImageUrl='<%# this.GetChartURL( Eval("key"),"chargers" )%>'  CssClass="Chart1" AlternateText="chart"/>
                            </div>

                            <div id="testChart_workstations" runat="server" style="display:normal">
                                <asp:Image  runat="server" ImageUrl='<%# this.GetChartURL( Eval("key"),"workstations" )%>'  CssClass="Chart1" AlternateText="chart"/>
                            </div>
                        <div class="update_label" runat="server">
                            <asp:Label ID="Label1" runat="server" Text="Last trending analysis update: "></asp:Label>
                        </div>--%>
                            
                            <div id="testChart_workstations" runat="server" style="display:normal">
                                <asp:Image  runat="server" ImageUrl='<%# this.GetChartURL( Eval("key"),"workstations" )%>'  CssClass="Chart1" AlternateText="chart"/>
                               <%-- <telerik:RadPageLayout ID="RadPageLayout1" runat="server" GridType="Fluid">
                                    <Rows>
                                        <telerik:LayoutRow>
                                            <Columns>
                                                <telerik:CompositeLayoutColumn Span="1">
                                                    <Rows>
                                                        <telerik:LayoutRow>
                                                            <Content>additional content 1</Content>
                                                        </telerik:LayoutRow>
                                                        <telerik:LayoutRow>
                                                            <Content>additional content 2</Content>
                                                        </telerik:LayoutRow>
                                                    </Rows>

                                                </telerik:CompositeLayoutColumn>
                                            </Columns>
                                        </telerik:LayoutRow>
                                    </Rows>
                                </telerik:RadPageLayout>    --%>
                               <%-- <asp:Image  runat="server" ImageUrl='Images/BatteryHealthPie.png' Width="420" Height="400" CssClass="Chart1"  AlternateText=" "/>--%>
                            </div>
                           
                            </div>
                    </ItemTemplate>
                
              </telerik:RadListView>
            </div>

         </div>
</asp:Content>
