﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using PulseWeb.API;
using PulseWeb.Model;
using PulseWeb.Utils;

namespace PulseWeb
{
    class DepartmentGrid
    {
        public string Id { get; set; }
        public string Department { get; set; }
        public string Facility { get; set; }
        public int NumberOfWorkstations { get; set; }
              public int NumberOfChargers { get; set; }
              public int NumberOfBatteries { get; set; }
    };

    public partial class ViewDepartments : BasePage
    {
        // ONLY VALID UNTIL THE NEXT POSTBACK!!!
        bool bIsInsert = false;
        Department SelectedDepartment
        {
            get { return HttpContext.Current.Session["SelectedDepartment"] as Department; }
            set { HttpContext.Current.Session["SelectedDepartment"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void RadGrid1_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            List<DepartmentGrid> list = new List<DepartmentGrid>();

            foreach (Site site in api.GetSitesForUser())
            {
                foreach (Department department in site.Departments)
                {
                    int count = site.Assets.Count(p => (p.DepartmentId == department.Id) && p.GetCategory() == Asset.AssetCategory.WORKSTATION);
                     int countch = site.Assets.Count(p => (p.DepartmentId == department.Id) && p.GetCategory() == Asset.AssetCategory.CHARGER);
                     int countbatt = site.Assets.Count(p => (p.DepartmentId == department.Id) && p.GetCategory() == Asset.AssetCategory.BATTERY);
                    list.Add(new DepartmentGrid { Id = department.Id.ToString(), Department = department.Name, Facility = site.Name, NumberOfWorkstations = count, NumberOfChargers=countch,NumberOfBatteries=countbatt });
                }
            }
            RadGrid1.DataSource  = list;
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            RadGrid1.MasterTableView.ExportToCSV();
        }

        protected void RadGrid1_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            //GridBoundColumn boundColumn = e.Column as GridBoundColumn;
            //if (boundColumn != null)
            //{
            //    boundColumn.ReadOnly = false;
            //}
            ////Asset
            switch (e.Column.UniqueName)
            {
                case "Id": e.Column.Visible = false; break;
            //    case "SerialNumber": e.Column.Visible = true; e.Column.HeaderStyle.Width = 100; break;
            //    case "AssetNumber": e.Column.Visible = true; break;
            //    case "Description": e.Column.Visible = true; break;
            //    case "Department": e.Column.Visible = true; break;
            //    case "SiteID": e.Column.Visible = true; break;
            //    case "NumberOfWorkstations": e.Column.Visible = true; break;
            //    default: e.Column.Visible = false; break;
            }
        }


        protected void rgrdParent_ItemCommand(object sender, GridCommandEventArgs e)
        {
            bIsInsert = false;
            switch (e.CommandName)
            {
                case "PerformCancel":
                    {
                        RadGrid rg = (sender as RadGrid);
                        rg.MasterTableView.IsItemInserted = false;
                        rg.MasterTableView.ClearEditItems();    // this line required for closing when editing an existing item
                        rg.MasterTableView.Rebind();

                    }
                    break;
                case RadGrid.EditCommandName:
                    {
                        int index = e.Item.ItemIndex;
                        if (index < e.Item.OwnerTableView.DataKeyValues.Count && index >= 0)
                        {
                            var departmentId = e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"] as string;
                            if (departmentId != null)
                            {
                                int id = Convert.ToInt32(departmentId);

                                this.SelectedDepartment = api.GetDepartment(id);
                            }
                        }
                        break;
                    }
                case RadGrid.InitInsertCommandName:
                    bIsInsert = true; ;
                    break;
            }
        }


        void AddOrUpdate(GridItem item, bool bInsert)
        {
            RadTextBox dptNameText = item.FindControl("RadTextBox1") as RadTextBox;
            RadComboBox combo = item.FindControl("ddlSite") as RadComboBox;

            if (dptNameText != null && combo != null )
            {
                using (PulseDBInterface enovateDB = new PulseDBInterface())
                {
                    Site selectedFacility = api.GetSite(combo.SelectedItem.Text.Trim());
                    string departmentName = dptNameText.Text.Trim();
                    // Get the department that was selected
                    Department selectedDept = null;
                    if (!bInsert)
                    {
                        int index = item.ItemIndex;
                        if (index < item.OwnerTableView.DataKeyValues.Count && index >= 0)
                        {
                            var departmentId = item.OwnerTableView.DataKeyValues[item.ItemIndex]["Id"] as string;
                            if (departmentId != null)
                            {
                                int id = Convert.ToInt32(departmentId);

                                selectedDept = api.GetDepartment(id);
                                int oldDeptBUID = selectedDept.SiteID;
                                selectedDept.Name = departmentName;
                                selectedDept.SiteID = selectedFacility.Id;

                                if (enovateDB.UpdateDepartment(selectedDept) < 0)
                                {
                                    Utils.ErrorHandler.Error("Internal error: UpdateDepartment failed");
                                }
                                else
                                {
                                    // the department is owned by the site so we need to update the old and new facilties
                                    if (oldDeptBUID != selectedDept.SiteID)
                                    {
                                        Site oldFacility = api.GetSite(Convert.ToInt32(oldDeptBUID));
                                        if( oldFacility != null )
                                            oldFacility.Departments = enovateDB.InitializeDepartmentList(oldFacility);
                                    }
                                }

                            }
                        }
                    }
                    else
                    {

                        enovateDB.GetOrCreateDepartmentID(selectedFacility.Id.ToString(), departmentName);
                    }
                    // refresh the department list from the DB
                    selectedFacility.Departments = enovateDB.InitializeDepartmentList(selectedFacility);

                }
            }
            item.OwnerTableView.ClearEditItems();
            item.OwnerTableView.Rebind();
        }
          void Delete(GridItem item, bool bInsert)
        {
            Site oldFacility = PulseSession.Instance.SelectedSite;
            using (PulseDBInterface enovateDB = new PulseDBInterface())
            {
             
                if (!bInsert)
                {
                    int index = item.ItemIndex;
                    if (index < item.OwnerTableView.DataKeyValues.Count && index >= 0)
                    {
                        int departmentId = -1;
                         departmentId = Convert.ToInt32(item.OwnerTableView.DataKeyValues[item.ItemIndex]["Id"].ToString());
                        if (departmentId != -1)
                        {
                          
                            if (enovateDB.DeleteDepartment(departmentId) < 0)
                            {
                                Utils.ErrorHandler.Error("Internal error: Delete Department failed");
                            }
                            else
                            {
                            
                        oldFacility.Departments = enovateDB.InitializeDepartmentList(PulseSession.Instance.SelectedSite);
                                 
                            }

                        }
                    }
                }
                else
                {

                   
                }
  
                        oldFacility.Departments = enovateDB.InitializeDepartmentList(PulseSession.Instance.SelectedSite);

            }
            item.OwnerTableView.ClearEditItems();
            item.OwnerTableView.Rebind();
        }

        protected void RadGrid1_InsertCommand(object source, GridCommandEventArgs e)
        {
            AddOrUpdate(e.Item, true);
        }
        protected void rgrdParent_UpdateCommand(object source, GridCommandEventArgs e)
        {
            AddOrUpdate(e.Item, (e.Item as GridEditFormInsertItem) != null ? true : false);
        }

        protected void SiteCombo_DataBinding(object sender, EventArgs e)
        {
            RadComboBox drop = (sender as RadComboBox);
            if (drop != null)
            {
                int deptBUID = this.SelectedDepartment != null ? this.SelectedDepartment.SiteID : -1;
                foreach (Site facility in PulseWeb.API.api.SessionUser.Sites)
                {
                    drop.Items.Add(new RadComboBoxItem { Text = facility.Name, Value = facility.Id.ToString(), Selected = deptBUID == facility.Id });
                }
            }
        }

        protected void panel1_PreRender(object sender, EventArgs e)
        {
            Control c = (sender as Panel).FindControl("FacilityRow");
            if (c != null)
            {
                c.Visible = bIsInsert ? true : false;
            }
        }

        protected void rdgrdParent_DeleteCommand(object sender, GridCommandEventArgs e)
        {
             Delete(e.Item, (e.Item as GridEditFormInsertItem) != null ? true : false);
        }


    }
}