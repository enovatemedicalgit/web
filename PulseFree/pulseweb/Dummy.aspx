﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dummy.aspx.cs" Inherits="Dummy" %>  
  
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>  
  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  
  
<html xmlns="http://www.w3.org/1999/xhtml">  
<head runat="server">  
    <title></title>  
</head>  
<body>  
    <form id="form1" runat="server">  
    <div>  
         <asp:Chart ID="ScoreChart" Width="600px" Height="320px" runat="server"   
            BackColor="Silver" BackGradientStyle="LeftRight" BorderlineWidth="2"   
            TabIndex="2">  
      <Titles>  
      <asp:Title Text="Runs" />  
      </Titles>  
        <Series>  
          <asp:Series Name="RunSeries" MarkerSize="1" CustomProperties="PointWidth=0.4" IsXValueIndexed="True">  
          
          </asp:Series>  
        </Series>  
        <ChartAreas>  
          <asp:ChartArea Name="ChartArea1">  
          <Area3DStyle Enable3D="true"  WallWidth="10" />  
              <AxisY Title="Runs">  
              </AxisY>  
              <AxisX Title="Players Name">  
              </AxisX>  
<Area3DStyle Enable3D="True" WallWidth="10"></Area3DStyle>  
          </asp:ChartArea>  
        </ChartAreas>  
      </asp:Chart>  

 
    </div>  
    </form>  
</body>  
</html>  