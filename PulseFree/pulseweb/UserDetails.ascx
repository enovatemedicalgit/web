﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.ascx.cs" Inherits="PulseWeb.UserDetails" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/adduser.css" rel="stylesheet" type="text/css" />
</asp:Content>--%>
   
 <div class="main_fom_box2" >
        <div class="top_id">
            <div class="lt_top">
                <img src="Images/id_icon.png" alt="" style="height: 30px; width: 26px"/><span></span></div>
            <div class="rt_top"><span>Update User</span></div>
        </div>
        <div class="main_fom">
            <div class="main_fom_box">
                <div class="fom_st">
                    <label>First Name</label>
                     <telerik:RadTextBox Text='<%#Bind("FirstName") %>'   ID="FirstName" runat="server" Width="230px" TabIndex="1" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div> 
                <div class="fom_st">
                    <label>Email</label>
                     <telerik:RadTextBox Text='<%#Bind("EMail") %>' ID="EmailText"   runat="server" Width="230px" TabIndex="4" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div> 
                <div class="fom_st">
                    <label>Password</label>
                    <telerik:RadTextBox Text='<%#Bind("Password") %>' TextMode="Password" ID="Password" runat="server" TabIndex="7" Width="230px" CssClass="lt"></telerik:RadTextBox>
                    <a href="#" class="rt"></a>
                </div>
            </div>
            <div class="main_fom_box">
                <div class="fom_st">
                    <label>Last Name </label>
                    <telerik:RadTextBox Text='<%#Bind("LastName") %>' ID="LastName" runat="server" Width="230px" TabIndex="2" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div>
                <div class="fom_st">
                    <label>Primary Phone </label>
                    <telerik:RadTextBox Text='<%#Bind("PrimaryPhone") %>' ID="HomePhoneBox" runat="server" Width="230px" TabIndex="5" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div>
                  <div class="fom_st">
                    <label>Site Notification Email</label>
                    <telerik:RadTextBox  ID="txtPOCEmail" Text='<%#Bind("POCEmail") %>' runat="server" Width="230px" TabIndex="99" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div>
                <div class="fom_st_2">
                    <label>Facility/IDN</label>
                    <telerik:RadComboBox ID="ddlSite" runat="server" DataTextField="SiteDescription" Width="63%" EnableEmbeddedSkins="true" ReadOnly="true"  OnDataBinding="ddlSite_DataBinding" CssClass="adduser_text_edit" > 

                    </telerik:RadComboBox>
                </div>
            </div>
            <div class="main_fom_box">
                <div class="fom_st">
                    <label>Title</label>
                    <telerik:RadTextBox Text='<%#Bind("Title") %>'  ID="TitleText" runat="server" Width="230px" TabIndex="3" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div>
                <div class="fom_st">
                    <label>Other Phone</label>
                    <telerik:RadTextBox Text='<%#Bind("SecondaryPhone") %>' ID="OtherPhoneBox" runat="server" Width="230px" TabIndex="6" CssClass="adduser_text_edit"></telerik:RadTextBox>
                </div>
                 <asp:Button ID="cmdCancel" runat="server"  CssClass="adduser_save_btn" Text="Cancel" CommandName="PerformCancel" CausesValidation="false"/>
          <asp:Button ID="cmdDelete" runat="server"  CssClass="Deleteuser_save_btn" Text="Delete User" CommandName="PerformDelete" CausesValidation="false"/>
        <asp:Button ID="cmdSave" runat="server" CssClass="adduser_save_btn" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'></asp:Button>
            </div>
        </div>
        <div class="user_box">
            <div class="user_left_box">
                <div class="user_head">User Settings</div>
                <div class="user_lt_bot">
                    <div id="radios1" class="facilityRight">
                        <asp:CheckBoxList ID="UserOptionsCheck" runat="server" RepeatDirection="Vertical" CssClass="radioButtonList" 
                            RepeatLayout="Table" RepeatColumns="1" 
                            OnDataBound="cblUserOptions_DataBound">
                        </asp:CheckBoxList>
                    </div>
            <%-- Some example pins, hardcoded to demo 
                    <div class="user_opction">
                        <input type="checkbox" id="c1" name="cc" class="css-checkbox"/>
                        <label for="c1">Log into Pulse</label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c2" class="css-checkbox"/>
                        <label for="c2">Maintain Devices </label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c3" class="css-checkbox"/> 
                        <label for="c3">Maintain Facility Networks</label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c4" class="css-checkbox"/> 
                        <label for="c4">Maintain Users</label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c5" class="css-checkbox"/>
                        <label for="c5">Receive Notifications </label>
                    </div>
--%>

                </div>
            </div>
            <div class="user_home_box">
                <div class="user_home_head">Set Home Page</div>
                <div class="user_home_opction"><span><a href="#">Facility graphs</a></span></div>
            </div>
        </div>
    </div>
    <div >
       <%-- <asp:Button ID="cmdCancel" runat="server"  CssClass="adduser_save_btn" Text="Cancel" CommandName="PerformCancel" CausesValidation="false"/>
          <asp:Button ID="cmdDelete" runat="server"  CssClass="Deleteuser_save_btn" Text="Delete User" CommandName="PerformDelete" CausesValidation="false"/>
        <asp:Button ID="cmdSave" runat="server" CssClass="adduser_save_btn" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'></asp:Button>--%>
    </div>
