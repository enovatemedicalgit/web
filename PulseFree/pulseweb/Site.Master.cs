﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using PulseWeb.Model;
using PulseWeb.API;

namespace PulseWeb
{
    public partial class Master : System.Web.UI.MasterPage, System.Web.UI.ICallbackEventHandler
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String csname1 = "PopupScript";

            if (!Page.ClientScript.IsClientScriptBlockRegistered("ServerInitMessages"))
            {
                String cbReference = Page.ClientScript.GetCallbackEventReference(this, "arg", "InitMessages", "context", true);
                String callbackScript = "function ServerInitMessages(arg, context)" + "{ " + cbReference + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ServerInitMessages", callbackScript, true);
            }
            if (!Page.ClientScript.IsClientScriptBlockRegistered(csname1))
            {
                String cstext1 = "<script type=\"text/javascript\">" +
                     "ServerInitMessages(null,null);</" + "script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), csname1, cstext1);
            }

            if (api.SessionUser != null)
            {
                lblUser.Text = api.SessionUser.Email;
            }
          if (PulseSession.Instance.User.IDSite == 1000000){
            pulselogo.ImageUrl = "/Images/satchel_black_longsmall.png";
              navbarheaderdiv.Attributes.Add("style","background-color:#000000");
            navbarheaderdiv.Attributes.Add("bgcolor", "#000000");   
            //NEED TO SET BACKGROUND TO BLACK
        }
        else {
          navbarheaderdiv.Attributes.Add("bgcolor", "#82acD4");
         navbarheaderdiv.Attributes.Add("style","background-color:#82acD4");
           }
            }

        public void RaiseCallbackEvent(String eventArgument)
        {
        }

        public String GetCallbackResult()
        {
            if (api.SessionUser != null)
            {

                var js = new JavaScriptSerializer();
                List<Alert> alerts = api.GetAlertsForUser(api.SessionUser);
                List<string> stringList = new List<string>();
                string[] colors = new string[] { "#feeeee", "#ffffff" };
                int i = 0;
                foreach (Alert alert in alerts)
                {
                    string s = string.Format(@"
                        <li class=""notif unread""style=""background:{2};"" onclick=""$(this).notificationClick(this)"">
                        <div class=""imageblock""> 
                        <img src=""/Images/charger_icon.png"" class=""notifimage""/> 
                        </div> 
                        <div class=""messageblock""> 
                        <div class=""message""> {0}
                        </div>
                        <div class=""message""> {1}
                        </div> </div></li>", alert.DeviceSN, alert.Description, colors[i & 1]);
                    stringList.Add(s);
                    i++;
                }
                return js.Serialize(stringList);
            }
            return "";
        }

    }
}