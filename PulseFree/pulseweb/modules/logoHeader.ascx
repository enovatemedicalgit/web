﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="logoHeader.ascx.cs" Inherits="pulse.modules.logoHeader" %>
  <link rel="stylesheet" href="//ajax.aspnetcdn.com/ajax/bootstrap/3.2.0/css/bootstrap.min.css" />
  <link href="css/agilametrics.css" rel="stylesheet" />

  <div class="header">
    <div class="topbar">
      <div class="container-fluid">
        <ul class="notificationbar pull-right">
          <li><a href="#" class="no-underline"><i class="icon-bat-2bar"></i>35%</a></li>
          <li><a href="#"><i class="icon-wifi"></i></a></li>
        </ul>
      </div>
    </div>

    <div class="navbar navbar-default" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">
            <img src="images/pulse-logo-small.jpg" alt="pulse" />
          </a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#"><i class="icon-line-mail"></i><span class="badge badge-notification">4</span></a></li>
            <li><a href="#"><i class="icon-line-globe"></i><span class="badge badge-notification">6</span></a></li>
            <li><a href="#"><i class="icon-line-cog"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
