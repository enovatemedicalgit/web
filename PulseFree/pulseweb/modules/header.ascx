﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header.ascx.cs" Inherits="pulse.modules.header" %>

<meta charset="utf-8">
    <link rel="stylesheet" href="css/nucleon.css" />
    <link rel="stylesheet" type="text/css" href="css/customRadioButton.css" />

    <!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <%--charts--%>
    <script src="scripts/chart/raphael-min.js"></script>
    <script src="scripts/chart/morris.js?p1=1"></script>
    <script src="scripts/chart/nucleon.js"></script>
    <link rel="stylesheet" href="css/nucleonCharts.css">
    <link rel="stylesheet" href="chart/morris.css">
    <%--HTML5 convas connected to C#- Sandeep Thakur--%>
    <script>
        function makeChart(dv, v1, v2, v3, c1, c2, c3, highlighted) {
            jQuery(document).ready(function () {
                var donut = Morris.Donut({
                    element: dv,
                    data: [
                      { value: v1, label: 'Active', formatted: 'at least 70%',color: c1 },
                      { value: v2, label: 'Available', formatted: 'approx. 15%',color: c2 },
                      { value: v3, label: 'Offline', formatted: 'approx. 10%', color: c3 }
                    ],
                    formatter: function (x, data) { return data.value + "%"; },
                    colors: ['#E5E5E5', '#95BBD7', '#B0CCE1', '#095791', '#095085', '#083E67', '#052C48', '#042135'],
                    radius: 12,
                    isSelected: true,
                });
                $(document.getElementById(dv.id)).data("donut", donut);
                if(highlighted != "True")
                {
                    donut.deselectEntire();
                }
                else {
                    donut.selectEntire();
                    $(document.getElementById(dv.id)).data("selected", donut);
                }
            });
        }
        function makeChartBattery(dv, v1, v2, v3,v4, c1, c2, c3,c4, highlighted) {
            jQuery(document).ready(function () {
                var donut = Morris.Donut({
                    element: dv,
                    data: [
                      { value: v1, label: 'Active', formatted: 'at least 70%', color: c1 },
                      { value: v2, label: 'Available', formatted: 'approx. 15%', color: c2 },
                      { value: v3, label: 'Offline', formatted: 'approx. 10%', color: c3 },
                       { value: v4, label: 'Out Of Service Space', formatted: 'approx. 10%', color: c4 }
                    ],
                    formatter: function (x, data) { return data.value + "%"; },
                    colors: ['#E5E5E5', '#95BBD7', '#B0CCE1', '#095791', '#095085', '#083E67', '#052C48', '#042135'],
                    radius: 12,
                    
                    resize: true,
                    isSelected: true,
                });
                $(document.getElementById(dv.id)).data("donut", donut);
                if (highlighted != "True") {
                    donut.deselectEntire();
                }
                else {
                    donut.selectEntire();
                    $(document.getElementById(dv.id)).data("selected", donut);
                }
            });
        }
        function makeChartWorkstation(dv, v1, v2, v3, v4, c1, c2, c3, c4, highlighted) {
            jQuery(document).ready(function () {
                var donut = Morris.Donut({
                    element: dv,
                    data: [
                      { value: v1, label: 'Active', formatted: 'at least 70%', color: c1 },
                      { value: v2, label: 'Available', formatted: 'approx. 15%', color: c2 },
                      { value: v3, label: 'Offline', formatted: 'approx. 10%', color: c3 },
                       { value: v4, label: 'Out Of Service Space', formatted: 'approx. 10%', color: c4 }
                    ],
                    formatter: function (x, data) { return data.value + "%"; },
                    colors: ['#E5E5E5', '#95BBD7', '#B0CCE1', '#095791', '#095085', '#083E67', '#052C48', '#042135'],
                    radius: 12,

                    resize: true,
                    isSelected: true,
                });
                $(document.getElementById(dv.id)).data("donut", donut);
                if (highlighted != "True") {
                    donut.deselectEntire();
                }
                else {
                    donut.selectEntire();
                    $(document.getElementById(dv.id)).data("selected", donut);
                }
            });
        }
        function makeChartSmall(dv, v1, v2, c1, c2) {
            jQuery(document).ready(function () {
                Morris.Donut({
                    element: dv,
                    data: [
                      { value: v1, label: 'Active', formatted: 'at least 70%', color: c1 },
                      { value: v2, label: 'Available', formatted: 'approx. 15%', color: c2 }
                    ],
                    formatter: function (x, data) { return data.value + "%"; },
                    colors: ['#E5E5E5', '#95BBD7', '#B0CCE1', '#095791', '#095085', '#083E67', '#052C48', '#042135'],
                    radius: 12,
                    isSelected: true,
                });
            });
        }
        function makeChartSmall2(dv, v1, v2, c1, c2) {
            jQuery(document).ready(function () {
                Morris.Donut({
                    element: dv,
                    data: [
                      { value: v1, label: 'Active', formatted: 'at least 70%', color: c1 },
                      { value: v2, label: 'Available', formatted: 'approx. 15%', color: c2 }
                    ],
                    formatter: function (x, data) { return data.value + "%"; },
                    colors: ['#E5E5E5', '#95BBD7', '#B0CCE1', '#095791', '#095085', '#083E67', '#052C48', '#042135'],
                    radius: 12,
                    isSelected: true,
                });
            });
        }
        function DisplayDrillDown(visible, dvf1, dvf2) {
            $(visible).show();
            $(dvf1).hide();
            $(dvf2).hide();
        }

        $(document).ready(function () {


            $.fn.clicked = function (item) {

                var A0 = '#MainContent_rptReports_graphDrilDownA_0';
                var B0 = '#MainContent_rptReports_graphDrilDownB_0';
                var C0 = '#MainContent_rptReports_graphDrilDownC_0';


                var A1 = '#MainContent_rptReports_graphDrilDownA_1';
                var B1 = '#MainContent_rptReports_graphDrilDownB_1';
                var C1 = '#MainContent_rptReports_graphDrilDownC_1';

                var A2 = '#MainContent_rptReports_graphDrilDownA_2';
                var B2 = '#MainContent_rptReports_graphDrilDownB_2';
                var C2 = '#MainContent_rptReports_graphDrilDownC_2';

                var A3 = '#MainContent_rptReports_graphDrilDownA_3';
                var B3 = '#MainContent_rptReports_graphDrilDownB_3';
                var C3 = '#MainContent_rptReports_graphDrilDownC_3';

                var A4 = '#MainContent_rptReports_graphDrilDownA_4';
                var B4 = '#MainContent_rptReports_graphDrilDownB_4';
                var C4 = '#MainContent_rptReports_graphDrilDownC_4';

                var A5 = '#MainContent_rptReports_graphDrilDownA_5';
                var B5 = '#MainContent_rptReports_graphDrilDownB_5';
                var C5 = '#MainContent_rptReports_graphDrilDownC_5';

                var A6 = '#MainContent_rptReports_graphDrilDownA_6';
                var B6 = '#MainContent_rptReports_graphDrilDownB_6';
                var C6 = '#MainContent_rptReports_graphDrilDownC_6';

                var A7 = '#MainContent_rptReports_graphDrilDownA_7';
                var B7 = '#MainContent_rptReports_graphDrilDownB_7';
                var C7 = '#MainContent_rptReports_graphDrilDownC_7';

                var A8 = '#MainContent_rptReports_graphDrilDownA_8';
                var B8 = '#MainContent_rptReports_graphDrilDownB_8';
                var C8 = '#MainContent_rptReports_graphDrilDownC_8';

                var A9 = '#MainContent_rptReports_graphDrilDownA_9';
                var B9 = '#MainContent_rptReports_graphDrilDownB_9';
                var C9 = '#MainContent_rptReports_graphDrilDownC_9';

                var A10 = '#MainContent_rptReports_graphDrilDownA_10';
                var B10 = '#MainContent_rptReports_graphDrilDownB_10';
                var C10 = '#MainContent_rptReports_graphDrilDownC_10';

                //alert($(item).attr("id"));
                var s = $(item).attr("id").trim();
                var lastDash = s.lastIndexOf("_");
                var index = parseInt(s.substring(lastDash + 1, s.length));
                var graphNum = parseInt(s.substring(lastDash - 1, lastDash));

                var el = $(document.getElementById(s))
                var elSelected = $(document.getElementById("MainContent_rptReports_graphBig" + "0_" + index));

                var donut = el.data("donut");
                var curSelected = elSelected.data("selected");

                donut.selectEntire();
                if (curSelected != null && curSelected != donut)
                {
                    curSelected.deselectEntire();
                }
                elSelected.data("selected",donut);


                switch (s) {
                    case 'MainContent_rptReports_graphBig0_0':
                        DisplayDrillDown(A0, B0, C0);
                        $(MainContent_rptReports_hzRule_0).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_0':
                        DisplayDrillDown(B0, C0, A0);
                        $(B0).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_0).css("margin-top", "-107px");

                        break;
                    case 'MainContent_rptReports_graphBig2_0':
                        DisplayDrillDown(C0, A0, B0);
                        $(C0).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_0).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_1':
                        DisplayDrillDown(A1, B1, C1);
                        $(MainContent_rptReports_hzRule_1).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_1':
                        DisplayDrillDown(B1, C1, A1);
                        $(B1).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_1).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_1':
                        DisplayDrillDown(C1, A1, B1);
                        $(C1).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_1).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_2':
                        DisplayDrillDown(A2, B2, C2);
                        $(MainContent_rptReports_hzRule_2).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_2':
                        DisplayDrillDown(B2, C2, A2);
                        $(B2).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_2).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_2':
                        DisplayDrillDown(C2, A2, B2);
                        $(C2).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_2).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_3':
                        DisplayDrillDown(A3, B3, C3);
                        $(MainContent_rptReports_hzRule_3).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_3':
                        DisplayDrillDown(B3, C3, A3);
                        $(B3).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_3).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_3':
                        DisplayDrillDown(C3, A3, B3);
                        $(C3).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_3).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_4':
                        DisplayDrillDown(A4, B4, C4);
                        $(MainContent_rptReports_hzRule_4).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_4':
                        DisplayDrillDown(B4, C4, A4);
                        $(B4).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_4).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_4':
                        DisplayDrillDown(C4, A4, B4);
                        $(C4).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_4).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_5':
                        DisplayDrillDown(A5, B5, C5);
                        $(MainContent_rptReports_hzRule_5).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_5':
                        DisplayDrillDown(B5, C5, A5);
                        $(B5).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_5).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_5':
                        DisplayDrillDown(C5, A5, B5);
                        $(C5).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_5).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_6':
                        DisplayDrillDown(A6, B6, C6);
                        $(MainContent_rptReports_hzRule_6).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_6':
                        DisplayDrillDown(B6, C6, A6);
                        $(B6).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_6).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_6':
                        DisplayDrillDown(C6, A6, B6);
                        $(C6).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_6).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_7':
                        DisplayDrillDown(A7, B7, C7);
                        $(MainContent_rptReports_hzRule_7).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_7':
                        DisplayDrillDown(B7, C7, A7);
                        $(B7).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_7).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_7':
                        DisplayDrillDown(C7, A7, B7);
                        $(C7).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_7).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_8':
                        DisplayDrillDown(A8, B8, C8);
                        $(MainContent_rptReports_hzRule_8).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_8':
                        DisplayDrillDown(B8, C8, A8);
                        $(B8).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_8).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_8':
                        DisplayDrillDown(C8, A8, B8);
                        $(C8).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_8).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_9':
                        DisplayDrillDown(A9, B9, C9);
                        $(MainContent_rptReports_hzRule_9).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_9':
                        DisplayDrillDown(B9, C9, A9);
                        $(B9).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_9).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_9':
                        DisplayDrillDown(C9, A9, B9);
                        $(C9).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_9).css("margin-top", "93px");
                        break;
                    case 'MainContent_rptReports_graphBig0_10':
                        DisplayDrillDown(A10, B10, C10);
                        $(MainContent_rptReports_hzRule_10).css("margin-top", "-307px");
                        break;
                    case 'MainContent_rptReports_graphBig1_10':
                        DisplayDrillDown(B10, C10, A10);
                        $(B10).css("margin-top", "200px");
                        $(MainContent_rptReports_hzRule_10).css("margin-top", "-107px");
                        break;
                    case 'MainContent_rptReports_graphBig2_10':
                        DisplayDrillDown(C10, A10, B10);
                        $(C10).css("margin-top", "400px");
                        $(MainContent_rptReports_hzRule_10).css("margin-top", "93px");
                        break;

                }
            }
        });

    </script>
    <%--charts--%>