﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="PulseWeb.Account.AddUser" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

<script type="text/javascript">
    document.body.onload = function () {
        changecss('radios1');
    }
</script>
    <script >
        function changecss(idname) {

            var divs = document.getElementById(idname).getElementsByTagName("input");

            for (var i = 0; i < divs.length; i++) {
                var _id = divs[i].id;
                document.getElementById(divs[i].id).setAttribute
                ("class", "css-checkbox");
            }
        }

</script> 

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/adduser.css" />
    <link rel="stylesheet" type="text/css" href="css/customRadioButton.css" />
    <div class="main_fom_box2">
        <div class="top_id">
            <div class="lt_top">
                <img src="Images/id_icon.jpg" alt=""/><span>ID 1009</span></div>
            <div class="rt_top"><span><a href="#">Add New User</a></span></div>
        </div>
        <div>

        </div>
        <div class="main_fom"  align="left">
            <div class="main_fom_box">
                <div class="fom_st" >
                    <label>First Name</label>
                     <asp:TextBox ID="FirstName" runat="server" Width="100px" TabIndex="1"></asp:TextBox>
                </div> 
                <div class="fom_st" >
                    <label>Email</label>
                     <asp:TextBox ID="EmailText" runat="server" Width="100px" TabIndex="4"></asp:TextBox>
                </div> 
                <div class="fom_st" >
                    <label>Password</label>
                    <asp:TextBox  ID="Password" runat="server" Width="230px" TabIndex="9"></asp:TextBox>
                </div>
            </div>
            <div class="main_fom_box">
                <div class="fom_st">
                    <label>Last Name </label>
                    <asp:TextBox ID="LastName" runat="server" Width="230px" TabIndex="2"></asp:TextBox>
                </div>
                <div class="fom_st">
                    <label>Primary Phone </label>
                    <asp:TextBox ID="HomePhoneBox" runat="server" Width="230px" TabIndex="5"></asp:TextBox>
                </div>
            </div>
            <div class="main_fom_box">
                <div class="fom_st">
                    <label>Title</label>
                    <asp:TextBox ID="TitleText" runat="server" Width="230px" TabIndex="3"></asp:TextBox>
                </div>
                <div class="fom_st">
                    <label>Other Phone</label>
                    <asp:TextBox ID="OtherPhoneBox" runat="server" Width="230px" TabIndex="6"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="user_box">
            <div class="user_left_box">
                <div class="user_head">User Settings</div>
                <div class="user_lt_bot">
                    <div id="radios1" class="facilityRight">
                        <asp:CheckBoxList ID="UserOptionsCheck" runat="server" RepeatDirection="Vertical" CssClass="radioButtonList" RepeatLayout="Table" RepeatColumns="1" >
                            <asp:ListItem Value="CAN_LOGIN_TO_PULSE" Text="Log into Pulse" />
                            <asp:ListItem Text="Maintain Devices" />
                            <asp:ListItem Text="Maintain Facility Networks" />
                            <asp:ListItem Text="Receive Notifications" />
                        </asp:CheckBoxList>
                    </div>
            <%-- Some example pins, hardcoded to demo 
                    <div class="user_opction">
                        <input type="checkbox" id="c1" name="cc" class="css-checkbox"/>
                        <label for="c1">Log into Pulse</label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c2" class="css-checkbox"/>
                        <label for="c2">Maintain Devices </label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c3" class="css-checkbox"/> 
                        <label for="c3">Maintain Facility Networks</label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c4" class="css-checkbox"/> 
                        <label for="c4">Maintain Users</label>
                    </div>
                    <div class="user_opction">
                        <input name="" type="checkbox" value="" id="c5" class="css-checkbox"/>
                        <label for="c5">Receive Notifications </label>
                    </div>
--%>

                </div>
            </div>
            <div class="user_home_box">
                <div class="user_home_head">Set Home Page</div>
                <div class="user_home_opction"><span><a href="#">Facility Graphs</a></span></div>
            </div>
        </div>
        <div class="user_box_bot">
            <asp:Button ID="ButtonClose" Text="Close" runat="server" OnClick="BtnClose" CssClass="close" />
        </div>
        <div class="user_box_bot">
            <asp:Button ID="Button2" Text="Save" runat="server" OnClick="BtnClick" CssClass="submit" />
        </div>
    </div>
</asp:Content>