﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Telerik.Web.UI;

using PulseWeb.Utils;
using PulseWeb.Utils.Security;
using PulseWeb.Utils.Database;
using PulseWeb.Model;
using PulseWeb.API;

namespace PulseWeb
{
    class UserValidationException : Exception
    {
        public Control mBadTextBox = null;

        public UserValidationException(Control box, string message)
            : base(message)
        {
            mBadTextBox = box;
        }
    }

    public partial class UserDetails : System.Web.UI.UserControl
    {
        public PulseUser EditUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ShowUserMessage(Control txtBox, string message)
        {
            ErrorHandler.PopUpMessage(message);
            txtBox.Focus();
        }

        private void ValidateNotNull(RadTextBox txtBox, string errorMessage)
        {
            if (String.IsNullOrEmpty(txtBox.Text))
            {
                throw new UserValidationException(txtBox, "A valid " + errorMessage + " is needed");
            }
        }

        private void ValidateFacility()
        {
        }

        private void ValidEmail(RadTextBox mail, bool bCheckForExisting)
        {
            ValidateNotNull(mail, "Email");
            if (!Regex.IsMatch(mail.Text, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                throw new UserValidationException(mail, "Invalid e-mail format");
            }
            if( bCheckForExisting )
            {
                PulseDBInterface db = new PulseDBInterface(new SQLDatabase());
                if (db.DoesEmailAlreadyExist(mail.Text))
                {
                    throw new UserValidationException(mail, string.Format("The email address: {0} is already in use.", mail.Text));
                }

            }

        }
        public bool ValidateNewInfo()
        {

            try
            {
                ValidateNotNull(this.FirstName, "First Name");
                ValidateNotNull(this.LastName, "Last Name");
                ValidEmail(this.EmailText,true);
                ValidateNotNull(this.Password, "Password");
                ValidateFacility();
            }
            catch (UserValidationException ex)
            {
                ShowUserMessage(ex.mBadTextBox, ex.Message);
                return false;
            }

            return true;
        }

        public bool ValidateUpdateUserInfo(bool bNewUser)
        {
            try
            {
                ValidateNotNull(this.FirstName, "First Name");
                ValidateNotNull(this.LastName, "Last Name");
                ValidEmail(this.EmailText, bNewUser ? true : false);
                ValidateFacility();
                if (bNewUser)
                {
                    ValidateNotNull(this.Password, "Password");
                }
            }
            catch (UserValidationException ex)
            {
                ShowUserMessage(ex.mBadTextBox, ex.Message);
                return false;
            }

            return true;
        }

        public PulseUser UpdateUserFromForm(PulseUser user)
        {
           
                user.UserName = this.EmailText.Text;
              
                user.FirstName = this.FirstName.Text;
                user.LastName = this.LastName.Text;
                user.Title = this.TitleText.Text;
                user.Phone1 = this.HomePhoneBox.Text;
                user.Phone2 = this.OtherPhoneBox.Text;

                if (!string.IsNullOrEmpty(this.Password.Text))
                {
                    user.EncryptedPassword = Crypto.EncryptString(this.Password.Text);
                }
              
                user.Email = this.EmailText.Text;
                       
                user.POCEmail = this.txtPOCEmail.Text;
                user.IDSite = Convert.ToInt32(this.ddlSite.SelectedValue);
                //            user.Id = "";                // Will be set when added to the DB

                UserAccessRights rights = 0;
                foreach (ListItem item in this.UserOptionsCheck.Items)
                {
                    rights = AccessRightsHelper.AddRight(rights, item.Value, item.Selected ? "1" : "0");
                }
                user.AccessRights = rights;
            
            return user;
    }

        protected void cblUserOptions_DataBound(object source, EventArgs e)
        {
            CheckBoxList cblUserOptions = (CheckBoxList)source;
            if( EditUser != null )
            {
                foreach( ListItem oCheckbox in cblUserOptions.Items )
                {
                    UserAccessRights val = (UserAccessRights)Enum.Parse(typeof(UserAccessRights) , oCheckbox.Value);

                    oCheckbox.Selected = EditUser.HasRight(val);

                }

            }

        }
           private string GetControlText(GridItem item, string ctlName)
        {
            TextBox tb = item.FindControl(ctlName) as TextBox;
            return (tb != null) ? tb.Text :  "";
        }
        protected void ddlSite_DataBinding(object sender, EventArgs e)
        {
            RadComboBox drop = (sender as RadComboBox);
            if (drop != null)
            {
                if (PulseWeb.API.api.SessionUser.IsHQUser)
                {
                    Customer cust = PulseWeb.API.api.SessionUser.ParentCustomer;
                    drop.Items.Add(new RadComboBoxItem { Text = cust.Name, Value = cust.Id.ToString(), Selected = false });
                }
                foreach (Site site in PulseWeb.API.api.SessionUser.Sites)
                {
                    drop.Items.Add(new RadComboBoxItem { Text = site.Name, Value = site.Id.ToString(), Selected = EditUser.IDSite == site.Id ? true : false });
                }
            }

        }

    }
}