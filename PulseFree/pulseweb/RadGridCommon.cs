﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI;
using System.Web.UI.WebControls;

namespace PulseWeb
{
    public class RadGridCommon
    {

        public static void SaveGroupsExpandedState(RadGrid grid)
        {
            GridItem[] groupItems = grid.MasterTableView.GetItems(GridItemType.GroupHeader);
            if (groupItems.Length > 0)
            {
                List<ListItem> expandedIndexes = new List<ListItem>();
                foreach (GridItem item in groupItems)
                {
                    if (item.Expanded)
                    {
                        GridGroupHeaderItem groupHeader = (GridGroupHeaderItem)item;
                        ListItem li = new ListItem(groupHeader.DataCell.Text, groupHeader.GroupIndex);
                        expandedIndexes.Add(li);
                    }
                }
                string SessionKey = "groupExpandedState_" + grid.UniqueID.ToString();
                HttpContext.Current.Session[SessionKey] = expandedIndexes;
            }
        }

        public static void LoadGroupsExpandedState(RadGrid grid)
        {
            string SessionKey = "groupExpandedState_" + grid.UniqueID.ToString();
            List<ListItem> expandedIndexes = HttpContext.Current.Session[SessionKey] as List<ListItem>;
            if (expandedIndexes != null)
            {
                foreach (GridItem item in grid.MasterTableView.GetItems(GridItemType.GroupHeader))
                {
                    GridGroupHeaderItem groupHeader = (GridGroupHeaderItem)item;
                    ListItem li = new ListItem(groupHeader.DataCell.Text, groupHeader.GroupIndex);
                    if (expandedIndexes.Contains(li))
                    {
                        item.Expanded = true;
                    }
                }
            }
        }


        public static bool TraverseGridGroup(GridGroupHeaderItem _headerItem)
        {
            bool HasChildHeaderItems = false;
            GridItem[] children = _headerItem.GetChildItems();
            foreach (GridItem item in children)
            {
                if (item.Expanded || item.Edit)
                {
                    HasChildHeaderItems = true;
                }
                else
                {
                    if (item is GridGroupHeaderItem)
                    {
                        GridGroupHeaderItem GroupHeaderItem = (item as GridGroupHeaderItem);
                        GroupHeaderItem.Expanded = TraverseGridGroup(GroupHeaderItem);
                        HasChildHeaderItems = true;
                    }
                }
            }
            return HasChildHeaderItems;
        }

        public static bool ColumnDisplayIf(string SelectedDeviceType, bool FOR_WORKSTATIONS, bool FOR_CHARGERS, bool FOR_ALL)
        {
            switch (SelectedDeviceType)
            {
                case "Workstation":
                    return FOR_WORKSTATIONS;
                case "Charger":
                    return FOR_CHARGERS;
                default:
                    return FOR_ALL;
            }
        }

        public static bool ColumnVisibleIf(string SelectedDeviceType, bool STINGER_ONLY, bool FOR_WORKSTATIONS, bool FOR_CHARGERS, bool FOR_ALL)
        {
            //bool IsStingerUser = Convert.ToBoolean(HttpContext.Current.Session["IsStinger"].ToString());
            //if (STINGER_ONLY == true && IsStingerUser == false)
            //{
            //    return false;
            //}
            //else
            //{
                switch (SelectedDeviceType)
                {
                    case "Workstation":
                        return FOR_WORKSTATIONS;
                    case "Charger":
                        return FOR_CHARGERS;
                    default:
                        return FOR_ALL;
                }
            //}
        }


        public static void ConfigureColumns(RadGrid rg, string SelectedDeviceType)
        {
            if (rg.ID == "rgrdSessions")
            {
                // do nothing
            }
            else
            {
                GridColumn gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, false, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("FullChargeCapacity");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("xxFullChargeCapacity");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("CycleCount");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MaxCycleCount");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("VoltageCell1");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("VoltageCell2");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("VoltageCell3");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("FETStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("RemainingTime");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatteryName");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Notes");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Efficiency");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ControlBoardRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("LCDRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("HolsterChargerRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCBoardOneRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCBoardTwoRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("WifiFirmwareRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BayWirelessRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay1ChargerRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay2ChargerRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay3ChargerRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay4ChargerRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardRevision");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BackupBatteryVoltage");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BackupBatteryStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("IsAC");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit1AVolts");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit1ACurrent");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit1BVolts");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit1BCurrent");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit2AVolts");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit2ACurrent");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit2BVolts");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DCUnit2BCurrent");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, false, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("XValue");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("XMax");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("YValue");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("YMax");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ZValue");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ZMax");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Move");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatteryStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SafetyStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("PermanentFailureStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("PermanentFailureAlert");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatteryChargeStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatterySafetyAlert");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatteryOpStatus");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatteryMode");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DC1Error");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DC1Status");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DC2Error");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DC2Status");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MouseFailureNotification");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("KeyboardFailureNotification");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("WindowsShutdownNotification");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("LinkQuality");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("IP");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DeviceMAC");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("APMAC");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("AccessPointDescription");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, true, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                }

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("LastTransmissionStatus");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("AuthType");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ChannelNumber");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("PortNumber");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DHCP");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("WEPKey");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("PassCode");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                //}

                //gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("StaticIP");
                //if (gc != null)
                //{
                //    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                //    gc.Visible = ColumnVisibleIf(SelectedDeviceType, false, true, true, true);
                //}

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SSID");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true); //stinger-only, per Gary via email "CAST notes", 4-19-2011 1:21 AM
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SparePinSwitch");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ControlBoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("HolsterBoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("LCDBoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DC1BoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("DC2BoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BayWirelessBoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay1BoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay2BoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay3BoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("Bay4BoardSerialNumber");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardDrawerOpenTime");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardDrawerCount");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardMotorUp");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardMotorDown");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardUnlockCount");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardAdminPin");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardNarcPin");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardUserPin1");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardUserPin2");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardUserPin3");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardUserPin4");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MedBoardUserPin5");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SessionRecordType");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ExaminedDate");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SourceIPAddress");    // IP Address used for sending the packet
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }


                // SiteDescription_Radar, SiteDescription_Owner and SiteDescription_AP added 2012-08-17 by Danny Bates
                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SiteDescription_Radar");    // Facility where the devices appears to be reporting
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }
                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SiteDescription_Owner");    // who owns the device, according to MAS system
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }
                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SiteDescription_AP");    // where the access point appears to be
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, true, true);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }




                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("QueryStringID");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("SessionID");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("CommandCode");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("ROW_ID");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, true, true);
                }

                // MeasuredVoltage and BatteryErrorCode columns added 10-29-2012 by Danny Bates
                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("MeasuredVoltage");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }

                gc = (GridColumn)rg.Columns.FindByUniqueNameSafe("BatteryErrorCode");
                if (gc != null)
                {
                    gc.Display = ColumnDisplayIf(SelectedDeviceType, false, false, false);
                    gc.Visible = ColumnVisibleIf(SelectedDeviceType, true, true, false, true);
                }
            }
        }
    }
}