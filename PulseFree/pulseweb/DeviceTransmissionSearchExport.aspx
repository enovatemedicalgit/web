﻿<%@ Page Title=""  Language="C#" AutoEventWireup="true" EnableEventValidation="false" ClientIDMode="Predictable" CodeBehind="DeviceTransmissionSearchExport.aspx.cs" Inherits="PulseWeb.DeviceTransmissionSearchExport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmMain" runat="server">
        <div id="divDisplay" runat="server" visible="true">
            <asp:GridView runat="server" ID="grdDisplay" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="true"></asp:GridView>
        </div>
        <div id="divError" runat="server" visible="false">
        </div>
    </form>
</body>
</html>
