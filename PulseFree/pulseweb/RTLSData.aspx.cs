﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using PulseWeb;
using PulseWeb.API.RTLS;
using PulseWeb.Model;
using PulseWeb.API;

public partial class RTLSData : System.Web.UI.Page
{
    static Timer test = new Timer();

    bool HandleSignalR()
    {
        if (Request.QueryString.Count > 0)
        {
            Broadcaster.Instance.BroadcastAllMovedAssets();
            //lock( test )
            //{
            //    //BeaconID1 -68 to -104
            //    RTLSPacket packet = new RTLSPacket();
            //    if (packet.Create(Request))
            //    {
            //        PulseGlobals.Lock();
            //        SiteID site = PulseGlobals.GetSite(packet.Id); // adds the site if it doesn't exist
            //        site.UpdateRTLSAsset(packet.AssetId,packet.AreaId,"HEALTHY");
            //        PulseGlobals.UnLock();


            //        //PulseWeb.API.RTLS.RTLSLog.Log("HandleSignalR:2");
            //        //Broadcaster.Instance.BroadcastAssetMoved(packet);
            //        return false;
            //    }

            //}
        }
        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //HandleSignalR();
        //return;

        // http://capture.stingermedical.com/capturedata.aspx

        StreamWriter textOut;
        string sResponse = "0";
        string[] hexValuesSplit;
        if (Request.QueryString.Count > 0)
        {
            //string connstring = ConfigurationManager.ConnectionStrings["StingerCAST"].ConnectionString;
            string PulseConnectionString = ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString;
            string KokoroConnectionString = ConfigurationManager.ConnectionStrings["Kokoro"].ConnectionString;
          //  string CaptureConnectionString = ConfigurationManager.ConnectionStrings["Capture"].ConnectionString;
            Kokoro.QueryString qs = new Kokoro.QueryString(KokoroConnectionString);
            qs.Query = Request.QueryString.ToString();
            // get device serial number
           // string csn = string.Empty;
           // csn = "12345"; //GetQueryStringValue("EmTagID");
           // qs.csn = csn;
            string RTLSPkt = string.Empty;
            RTLSPkt = string.Empty;
            RTLSPkt = GetQueryStringValue("RTLSPkt");
            qs.RTLSPkt = RTLSPkt;
         
            string hexValues = RTLSPkt.ToCharArray().Aggregate("", (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 3 == 0) ? " " : "") + c.ToString());
          hexValuesSplit = hexValues.Split(' ');
            
            string EmTagID = string.Empty;
            string EmTagIDMajor = string.Empty;
            string EventCode = string.Empty;
            string Beacon1TX = "150";
            string Beacon1Major = string.Empty;
            string Beacon1Minor = string.Empty;
            string Beacon1BatteryLvl = string.Empty;
            string Beacon2TX = "150";
            string Beacon2Major = string.Empty;
            string Beacon2Minor = string.Empty;
            string Beacon2BatteryLvl = string.Empty;
            string EmTagTX = string.Empty;
            string EmTagName = string.Empty;

            //StreamWriter textOut1 = new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\RTLSParseError.log",
            //                             FileMode.Append,
            //                             FileAccess.Write));
           // textOut1.WriteLine("HexValuesSplitLength = " + hexValuesSplit.Length);
           // textOut1.WriteLine("HexValues = " + hexValues);
            //67 13 ff 59 00 02 15 00 0f be 00 04 b9 00 00 00 04 80 03 e9 00 c6
            // 1 2  3   4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 
            // 6713ff59000215000fbe0004b9000000048003e900c6
            //0123456789
//index 00 - 1 byte - Number of bytes that follow in first AD structure
//index 01 - 1 byte - Flags AD type    
//index 02 - 1 byte - Flags value    
//index 03 - 1 byte - Number of bytes that follow in next AD structure
//index 04 - 1 byte - Complete Local Name AD Structure 0x09
//index 05 - 5 byte - Name "EmTag"     
//index 10 - 1 byte - Number of bytes that follow in next AD structure 
//index 11 - 1 byte - Manufacturer specific data AD type 0xFF
//index 12 - 2 byte - 0x5900 # Company identifier code (0x0059 = Nordic)
//index 14 - 1 byte - 0x002 # Byte 0 of iBeacon advertisement indicator
//index 15 - 1 byte - 0x15 # Byte 1 of iBeacon advertisement indicator
//index 16 - 2 byte – ClosestBeaconMinor1
//index 18 - 1 byte – ClosestBeaconMinor1’s RSSI
//index 19 - 2 byte – ClosestBeaconMinor2
//index 21 - 1 byte – ClosestBeaconMinor2’s RSSI
//index 25 - 2 byte - major - hard coded for now 1152 or 0x0480
//index 27 - 2 byte - minor - hard coded for now 1001 or 0x03E9
//index 29 - 1 byte - The 2's complement of the calibrated Tx Power
         
            int i = 8;
            string stringValue = "";
            EmTagID = Convert.ToInt32(hexValuesSplit[18] + hexValuesSplit[19], 16).ToString();
            EmTagIDMajor = Convert.ToInt32(hexValuesSplit[16] + hexValuesSplit[17], 16).ToString();
            //EmTagID = Convert.ToDecimal(EmTagID).ToString();
            Beacon1Major = "1152";
            Beacon2Major = "1152";
            Beacon2Minor = Convert.ToInt32(hexValuesSplit[10] + hexValuesSplit[11], 16).ToString();
            Beacon1Minor = Convert.ToInt32(hexValuesSplit[7] + hexValuesSplit[8], 16).ToString();
            Beacon1TX = Convert.ToSByte(hexValuesSplit[9], 16).ToString();
            Beacon2TX = Convert.ToSByte(hexValuesSplit[12], 16).ToString();

            Beacon1BatteryLvl = "97";
            Beacon2BatteryLvl = "92";
            EventCode = "0";
            //Change room/area beacon minor to the minor of the Beacon that has the TX that is closest to zero
            if (Beacon2Minor == "0") {
                Beacon2Minor = Beacon1Minor;
            }
            if (Math.Abs(Convert.ToDecimal(Beacon1TX)) > Math.Abs(Convert.ToDecimal(Beacon2TX)))
            {
                Beacon1Minor = Beacon2Minor;
            }
            if (Math.Abs(Convert.ToDecimal(Beacon2TX)) > Math.Abs(Convert.ToDecimal(Beacon1TX)))
            {
                Beacon2Minor = Beacon1Minor;
            }
            if (Beacon1Minor == "1071" || Beacon2Minor == "1071")
                Beacon1Minor = Beacon2Minor = "1071";
            else if (Beacon1Minor == "1072" || Beacon2Minor == "1072")
                Beacon1Minor = Beacon2Minor = "1072";
            else if (Beacon1Minor == "1073" || Beacon2Minor == "1073")
                Beacon1Minor = Beacon2Minor = "1073";
            else if (Beacon1Minor == "1074" || Beacon2Minor == "1074")
                Beacon1Minor = Beacon2Minor = "1074";

            //textOut1.WriteLine("Beacon1Minor = " + Beacon1Minor);
           // textOut1.WriteLine("Beacon2Minor = " + Beacon2Minor);
           // textOut1.WriteLine("EmTagName = " + EmTagName);
           // textOut1.WriteLine("EmTagID (EmTagMinor) = " + EmTagID);
           // textOut1.WriteLine("EmTagID Major= " + Convert.ToInt32(hexValuesSplit[16] + hexValuesSplit[17], 16).ToString());
           // textOut1.Close();

            foreach (String hex in hexValuesSplit)
            {
            }
    
qs.EmTagID = EmTagID;
EventCode = "0"; // GetQueryStringValue("EventCode");
qs.EventCode = EventCode;
qs.Beacon1TX = Math.Abs(Convert.ToDecimal(Beacon1TX)).ToString();
qs.Beacon1Major = Beacon1Major;
qs.Beacon1Minor = Beacon1Minor;
qs.Beacon1BatteryLvl = Beacon1BatteryLvl;
qs.Beacon2TX = Math.Abs(Convert.ToDecimal(Beacon2TX)).ToString();
qs.Beacon2Major = Beacon2Major;
qs.Beacon2Minor = Beacon2Minor;
qs.Beacon2BatteryLvl = Beacon2BatteryLvl;
qs.SiteID = LookupIP("DMZIP");

            // get IP address of the remote connection
           // qs.SourceIPAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();

            try {
                qs.Insert();
            }
            catch (Exception ex)
            {
                Response.Write("CommandCode=" + sResponse + "Error "  + Request.QueryString.ToString() + ex.Message.ToString());
                StreamWriter textOutput = new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\SQLError.log",
                                                   FileMode.Append,
                                                   FileAccess.Write));
                textOutput.WriteLine(DateTime.Now.ToString());
                textOutput.WriteLine("------------------------------------------------------------------------------");
                textOutput.Close();
            }
            finally {
                qs = null;
            }

            if (EmTagID != string.Empty)
            {
                #region "fyi"
                // if type = "0"
                //      check for rows on command queue table
                //      where:
                //          serial number = csn
                //          and sentdate is null
                //          order by created date asc (process oldest first)
                //
                //      *** can only process one command at a time (one per connection) ***
                //
                //      for found row:
                //          return the command code to the device (response.write)
                //          set SentDateUTC to getutcdate() so we know when the command was sent to the device
                //
                // if type = "8"
                //      and SentDateUTC is not null
                //      and CompleteDateUTC is null
                //      and CommandCode = cmc - 1000
                //  then set CompleteDateUTC = getutcdate()
                //  for acknowledgements (type=8), response to device should be "0"

                // when type = "0", check command queue for this device (serial number)
                #endregion

                string sAction = "0"; //GetQueryStringValue("a");
                try
                {
                    //Kokoro.CommandQueue oCommandQueue = new Kokoro.CommandQueue(PulseConnectionString);
                    //switch (sAction)
                    //{
                    //    case "0":   // check command queue for any awaiting commands for this device
                    //        if (oCommandQueue.Select("select top 1 * from CommandQueue where EmTagID = '" + EmTagID + "' and CreatedDateUTC = '01/01/1900' order by ROW_ID"))
                    //        {
                    //            sResponse = oCommandQueue.CommandText;
                    //            oCommandQueue.SentDateUTC = DateTime.UtcNow.ToString();
                    //            oCommandQueue.Update("ROW_ID", oCommandQueue.ROW_ID.ToString());
                    //        }
                    //        break;
                    //    case "8":   // acknowledgement - update command queue for this device/command combination
                    //        string cmc = GetQueryStringValue("cmc");
                    //        int icmc = Convert.ToInt16(cmc) - 1000;
                    //        if (oCommandQueue.Select("select top 1 * from CommandQueue where SerialNumber = '" + EmTagID + "' and LEFT(CommandText, 3) = '" + icmc.ToString() + "' and SentDateUTC <> '01/01/1900' and CompleteDateUTC = '01/01/1900' order by ROW_ID"))
                    //        {
                    //            // response will be "0" (as set above)
                    //            oCommandQueue.CompleteDateUTC = DateTime.UtcNow.ToString();
                    //            oCommandQueue.Update("ROW_ID", oCommandQueue.ROW_ID.ToString());
                    //        }
                    //        break;
                 //   }
              
                   // oCommandQueue = null;
                }
                catch (Exception ex)
                {
                Response.Write("CommandCode=" + sResponse + "Error "  + Request.QueryString.ToString());
                textOut = new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\QParseError.log", FileMode.Append, FileAccess.Write));
                textOut.WriteLine("CommandCode=MF ERROR - " + ex.Message.ToString() + Request.QueryString.ToString());

                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
                }
            }
        }
        Response.Write("CommandCode=" + sResponse + DateTime.UtcNow.ToString()  + Request.QueryString.ToString());
        textOut = new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\QParseError.log" , FileMode.Append, FileAccess.Write));
        textOut.WriteLine("CommandCode=" + sResponse);

        textOut.WriteLine("------------------------------------------------------------------------------");
        textOut.Close();

    }

    public string LookupIP(string IpAddr)
    {
        //Should return site from site table matching DMZAddress
        return "2";
    }
    protected string GetQueryStringValue(string ParmName) {
        string value = string.Empty;
        try
        {
            value = Request.QueryString[ParmName].ToString();
        }
        catch { }

        return value;
    }

}

namespace Kokoro          /* Change namespace to match your project */
{
    public class CommandQueue
    {
        //private string connStr = @"server = (local); " +
        //                         @"Persist Security Info = False;" +
        //                         @"Integrated Security = SSPI; " +
        //                         @"Connect Timeout = 30; " +
        //                         @"Initial Catalog = StingerCAST;";

        private string connStr = "";
        private string SQLSTMT = "";         /* SQL Statement */
        private bool GOTERROR = false;     /* error flag */
        private DataTable dt;

        private bool _OKtoWrite = true;    /* read-only if false */
        private int _SQLTimeout = 120;    /* SQL timeout in seconds */
        private bool _LogErrors = false;   /* Log Format Exceptions */


        #region CommandQueueVariables
        /* Variables for CommandQueue */

        private string _CreatedDateUTC = DateTime.UtcNow.ToShortDateString();              /* 8   DATETIME  NOT NULL */
        private string _SerialNumber = String.Empty;                         /* 50  VARCHAR   NOT NULL */
        private string _CommandText = String.Empty;                          /* 50  VARCHAR   NOT NULL */
        private string _SentDateUTC = DateTime.UtcNow.ToShortDateString();                 /* 8   DATETIME  NOT NULL */
        private string _CompleteDateUTC = DateTime.UtcNow.ToShortDateString();             /* 8   DATETIME  NOT NULL */
        private int _NotificationConditionID = 0;                /* 4   INT       NOT NULL */
        private int _ROW_ID = 0;                                 /* 4   IDENTITY  NOT NULL */
        #endregion





        #region CommandQueueProperties
        /* Properties for CommandQueue */

        public string CreatedDateUTC { get { return this._CreatedDateUTC; } set { this._CreatedDateUTC = value; } }
        public string SerialNumber
        {
            get { return this._SerialNumber; }
            set
            {
                if (value.Length > 50)
                    this._SerialNumber = value.Substring(0, 50);
                else
                    this._SerialNumber = value;
            }
        }
        public string CommandText
        {
            get { return this._CommandText; }
            set
            {
                if (value.Length > 50)
                    this._CommandText = value.Substring(0, 50);
                else
                    this._CommandText = value;
            }
        }
        public string SentDateUTC { get { return this._SentDateUTC; } set { this._SentDateUTC = value; } }
        public string CompleteDateUTC { get { return this._CompleteDateUTC; } set { this._CompleteDateUTC = value; } }
        public int NotificationConditionID { get { return this._NotificationConditionID; } set { this._NotificationConditionID = value; } }
        public int ROW_ID { get { return this._ROW_ID; } set { this._ROW_ID = value; } }

        public bool OKToWrite { get { return this._OKtoWrite; } set { this._OKtoWrite = value; } }
        public bool GotError { get { return this.GOTERROR; } set { this.GOTERROR = value; } }
        public string SQLStmt { get { return this.SQLSTMT; } set { this.SQLSTMT = value; } }
        public int SQLTimeout { get { return this._SQLTimeout; } set { this._SQLTimeout = value; } }
        #endregion





        #region CommandQueue Constructor
        /****************************************************************************
          CommandQueue                                           1/17/2012
        Description: Class Constructor
         Parameters: SQL connection string
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// CommandQueue Constructor
        /// </summary>
        /// <param name="s">SQL Connection String</param>
        /// <returns>None</returns>

        public CommandQueue(string s)
        {
            connStr = s;
            dt = new DataTable();
        }
        /*--- end of CommandQueue() -----------------------------------------------*/
        #endregion




        #region CaptureError
        /****************************************************************************
          CaptureError                                           1/17/2012
        Description: Writes all errors to a text file.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private void CaptureError(string errmsg, string SQLStmt)
        {
            try
            {
                StreamWriter textOut =
                   new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\SQLError.log",
                                                     FileMode.Append,
                                                     FileAccess.Write));

                textOut.WriteLine(DateTime.Now.ToString());
                textOut.WriteLine(SQLStmt);
                textOut.WriteLine(errmsg);
                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
            }
            catch (IOException iox)
            {
                string errfptr = "Could not write to SQLError.log. Error: " + iox.ToString();
            }

            //GOTERROR = false;

            return;
        }
        /*--- end of CaptureError() -----------------------------------------------*/
        #endregion




        #region CleanField
        /****************************************************************************
          CleanField                                             1/17/2012
        Description: Check for ' in strings.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: Replaces a single quote with two single quotes.
      ****************************************************************************/

        private string CleanField(string stest)
        {
            string sTemp = "";

            if (stest.Contains("''"))   // already done 
                return (stest);

            if (stest.Contains("'"))
            {
                for (int i = 0; i < stest.Length; i++)
                {
                    if (stest[i] == '\'')
                    {
                        sTemp = sTemp + '\'';
                    }
                    sTemp = sTemp + stest[i];
                }
                return (sTemp);
            }

            return (stest);
        }
        /*--- end of CleanField() -------------------------------------------------*/
        #endregion




        #region ReaderToTable
        /****************************************************************************
          ReaderToTable                                          1/17/2012
        Description: Load a Data Reader into a Data Table
         Parameters: 
       Return Value: 
         Processing: 
          Called By: SelectMulti()
           Comments: 
      ****************************************************************************/

        private DataTable ReaderToTable(SqlDataReader reader)
        {
            DataTable newTable = new DataTable();
            DataColumn col = null;
            DataRow row = null;
            int i = 0;

            for (i = 0; i < reader.FieldCount; i++)
            {
                col = new DataColumn();
                col.ColumnName = reader.GetName(i);
                col.DataType = reader.GetFieldType(i);

                newTable.Columns.Add(col);
            }

            while (reader.Read())
            {
                row = newTable.NewRow();
                for (i = 0; i < reader.FieldCount; i++)
                {
                    row[i] = reader[i];
                }

                newTable.Rows.Add(row);
            }

            return newTable;
        }
        /*--- end of ReaderToTable() ----------------------------------------------*/
        #endregion




        #region GetLastDEX
        /****************************************************************************
          GetLastDEX                                          1/17/2012
        Description: Gets the last ROW_ID
         Parameters: 
       Return Value: Returns the ROW_ID of the last inserted record
         Processing: 
          Called By: 
           Comments: Can be used with any Identity field.
      ****************************************************************************/

        /// <summary>
        /// Gets the last ROW_ID from the table
        /// </summary>
        /// <returns>int - the last ROW_ID</returns>

        public int GetLastDEX()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLStmt = "SELECT MAX(ROW_ID) AS LastDEX FROM CommandQueue";
            int LASTDEX = 0;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLStmt, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)      // need one row back
                {
                    reader.Read();
                    LASTDEX = (int)reader[0];
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLStmt);
            }
            catch (Exception ex)
            {
                string errfptr = "Failed to Select from CommandQueue. Unknown Error: " + ex.ToString();
                CaptureError(errfptr, SQLStmt);
            }
            finally
            {
                sqlConn.Close();
            }

            return (LASTDEX);
        }
        /*--- end of GetLastDEX() -------------------------------------------------*/
        #endregion




        #region Clear
        /****************************************************************************
          Clear                                                  1/17/2012
        Description: Clears the variables for a new insert
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Clear() - Sets all class variables to a default value.
        /// </summary>
        /// <param name="none">No parameters</param>
        /// <returns>void</returns>

        public void Clear()
        {
            _CreatedDateUTC = DateTime.UtcNow.ToShortDateString();              /* 8   DATETIME  NOT NULL */
            _SerialNumber = String.Empty;                         /* 50  VARCHAR   NOT NULL */
            _CommandText = String.Empty;                          /* 50  VARCHAR   NOT NULL */
            _SentDateUTC = DateTime.UtcNow.ToShortDateString();                 /* 8   DATETIME  NOT NULL */
            _CompleteDateUTC = DateTime.UtcNow.ToShortDateString();             /* 8   DATETIME  NOT NULL */
            _NotificationConditionID = 0;                /* 4   INT       NOT NULL */
            _ROW_ID = 0;                                 /* 4   IDENTITY  NOT NULL */

            GOTERROR = false;                            /* error flag */

            return;
        }
        /*--- end of Clear() ------------------------------------------------------*/
        #endregion




        #region Insert
        /****************************************************************************
          Insert                                                 1/17/2012
        Description: Insert a new record.
         Parameters: 
       Return Value: 1
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Inserts a new record into the CommandQueue table
        /// </summary>
        /// <returns>int- the number of records inserted.</returns>

        public int Insert()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            int RecCnt = 0;

            /*  Put Field Modifications Here:  */


            SQLSTMT = "INSERT INTO CommandQueue ( " +
            "CreatedDateUTC, SerialNumber, CommandText, SentDateUTC, CompleteDateUTC" +
            ", NotificationConditionID ) VALUES ( " +
            "@CREATEDDATEUTC, @SERIALNUMBER, @COMMANDTEXT, @SENTDATEUTC, @COMPLETEDATEUTC" +
            ", @NOTIFICATIONCONDITIONID )";

            /* ZZZZZZZZ */
            /* You may need to remove IDENTITY fields and/or trailing comma's! */

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;

                command.Parameters.Add("@CREATEDDATEUTC", SqlDbType.DateTime).Value = _CreatedDateUTC;
                command.Parameters.Add("@SERIALNUMBER", SqlDbType.VarChar).Value = _SerialNumber;
                command.Parameters.Add("@COMMANDTEXT", SqlDbType.VarChar).Value = _CommandText;
                command.Parameters.Add("@SENTDATEUTC", SqlDbType.DateTime).Value = _SentDateUTC;
                command.Parameters.Add("@COMPLETEDATEUTC", SqlDbType.DateTime).Value = _CompleteDateUTC;
                command.Parameters.Add("@NOTIFICATIONCONDITIONID", SqlDbType.Int).Value = _NotificationConditionID;

                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Insert into CommandQueue 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Insert() -----------------------------------------------------*/
        #endregion




        #region Insert
        /****************************************************************************
          Insert                                                 1/17/2012
        Description: Insert a new record.
         Parameters: SQL Statement
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Inserts a new record into the CommandQueue table
        /// </summary>
        /// <param name="psql">string - the SQL statement that will perform the insert</param>
        /// <returns>int - the number of records inserted.</returns>

        public int Insert(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();      // execute query
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Insert into CommandQueue 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Insert() -----------------------------------------------------*/
        #endregion




        #region SetSQLStatement
        /****************************************************************************
          SetSQLStatement                                        1/17/2012
        Description: Develops the SQL statement for updating
         Parameters: whereclause - complete WHERE clause
                               i - just needed to make this overload unique
       Return Value: Complete SQL statement
         Processing: 
          Called By: Update()
           Comments: 
      ****************************************************************************/

        private string SetSQLStatement(string whereclause, int i)
        {
            /*  Put Field Modifications Here:  */


            string SQLS = "UPDATE CommandQueue SET " +
                             "CreatedDateUTC = @CREATEDDATEUTC" +
                             ", SerialNumber = @SERIALNUMBER" +
                             ", CommandText = @COMMANDTEXT" +
                             ", SentDateUTC = @SENTDATEUTC" +
                             ", CompleteDateUTC = @COMPLETEDATEUTC" +
                             ", NotificationConditionID = @NOTIFICATIONCONDITIONID" +
                             " WHERE ( " + whereclause + " )";

            /* ZZZZZZZZ */
            /* You may need to remove IDENTITY fields and/or trailing comma's! */

            return (SQLS);
        }
        /*--- end of SetSQLStatement() --------------------------------------------*/
        #endregion




        #region Update
        /****************************************************************************
          Update                                                 1/17/2012
        Description: Updates the record specified.
         Parameters: whereclause - complete WHERE clause
                               i - just needed to make this overload unique
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Updates a record in the CommandQueue table
        /// </summary>
        /// <param name="where">the SQL WHERE clause</param>
        /// <param name="i">int - a dummy parameter, not used</param>
        /// <returns>int - the number of records updated</returns>

        public int Update(string where, int i)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            int RecCnt = 0;

            SQLSTMT = SetSQLStatement(where, i);

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;

                command.Parameters.Add("@CREATEDDATEUTC", SqlDbType.DateTime).Value = _CreatedDateUTC;
                command.Parameters.Add("@SERIALNUMBER", SqlDbType.VarChar).Value = _SerialNumber;
                command.Parameters.Add("@COMMANDTEXT", SqlDbType.VarChar).Value = _CommandText;
                command.Parameters.Add("@SENTDATEUTC", SqlDbType.DateTime).Value = _SentDateUTC;
                command.Parameters.Add("@COMPLETEDATEUTC", SqlDbType.DateTime).Value = _CompleteDateUTC;
                command.Parameters.Add("@NOTIFICATIONCONDITIONID", SqlDbType.Int).Value = _NotificationConditionID;

                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Update CommandQueue #" + i.ToString() + ". Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Update() -----------------------------------------------------*/
        #endregion




        #region SetSQLStatement
        /****************************************************************************
          SetSQLStatement                                        1/17/2012
        Description: Develops the SQL statement for updating
         Parameters: 
       Return Value: 
         Processing: 
          Called By: Update()
           Comments: 
      ****************************************************************************/

        private string SetSQLStatement(string field, string value)
        {
            /*  Put Field Modifications Here:  */


            string SQLS = "UPDATE CommandQueue SET " +
                             "CreatedDateUTC = @CREATEDDATEUTC" +
                             ", SerialNumber = @SERIALNUMBER" +
                             ", CommandText = @COMMANDTEXT" +
                             ", SentDateUTC = @SENTDATEUTC" +
                             ", CompleteDateUTC = @COMPLETEDATEUTC" +
                             ", NotificationConditionID = @NOTIFICATIONCONDITIONID" +
                             " WHERE ( " + field + " = '" + value + "' )";

            /* ZZZZZZZZ */
            /* You may need to remove IDENTITY fields and/or trailing comma's! */

            return (SQLS);
        }
        /*--- end of SetSQLStatement() --------------------------------------------*/
        #endregion




        #region Update
        /****************************************************************************
          Update                                                 1/17/2012
        Description: Updates the record specified.
         Parameters: Field and Value to update
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Updates a record in the CommandQueue table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>int - the number of records updated</returns>

        public int Update(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            int RecCnt = 0;

            SQLSTMT = SetSQLStatement(field, value);

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;

                command.Parameters.Add("@CREATEDDATEUTC", SqlDbType.DateTime).Value = _CreatedDateUTC;
                command.Parameters.Add("@SERIALNUMBER", SqlDbType.VarChar).Value = _SerialNumber;
                command.Parameters.Add("@COMMANDTEXT", SqlDbType.VarChar).Value = _CommandText;
                command.Parameters.Add("@SENTDATEUTC", SqlDbType.DateTime).Value = _SentDateUTC;
                command.Parameters.Add("@COMPLETEDATEUTC", SqlDbType.DateTime).Value = _CompleteDateUTC;
                command.Parameters.Add("@NOTIFICATIONCONDITIONID", SqlDbType.Int).Value = _NotificationConditionID;

                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Update CommandQueue 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Update() -----------------------------------------------------*/
        #endregion




        #region Update
        /****************************************************************************
          Update                                                 1/17/2012
        Description: Updates a record.
         Parameters: SQL Statement
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: SQL Stmt must be set before calling this function.
      ****************************************************************************/

        /// <summary>
        /// Updates a record in the CommandQueue table
        /// </summary>
        /// <param name="psql">string - the SQL statement that performs the update</param>
        /// <returns>int - the number of records updated</returns>

        public int Update(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();      // execute query
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Update CommandQueue 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Update() -----------------------------------------------------*/
        #endregion




        #region Delete
        /****************************************************************************
          Delete                                                 1/17/2012
        Description: Deletes the record specified.
         Parameters: 
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Deletes a record from the CommandQueue table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>int - the number of records deleted.</returns>

        public int Delete(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "DELETE FROM CommandQueue WHERE ( " + field + " = '" + value + "' )";
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();     // delete record
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Delete From CommandQueue 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Delete() -----------------------------------------------------*/
        #endregion




        #region Delete
        /****************************************************************************
          Delete                                                 1/17/2012
        Description: Deletes the record specified.
         Parameters: SQL statement
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Deletes a record from the CommandQueue table
        /// </summary>
        /// <param name="psql">string - the SLQ statement that performs the delete</param>
        /// <returns>int - the number of records deleted.</returns>

        public int Delete(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();     // delete record
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Delete From CommandQueue 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Delete() -----------------------------------------------------*/
        #endregion




        #region LoadVariables
        /****************************************************************************
          LoadVariables                                          1/17/2012
        Description: Load variables from the dataset.
         Parameters: SqlDataReader
       Return Value: 
         Processing: 
          Called By: Select(), Requery()
           Comments: 
      ****************************************************************************/

        private void LoadVariables(SqlDataReader reader)
        {
            string stmp = "";

            try
            {
                _CreatedDateUTC = reader["CreatedDateUTC"].ToString();               /* 8   DATETIME  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CreatedDateUTC");
                _CreatedDateUTC = "";
            }

            try
            {
                _SerialNumber = reader["SerialNumber"].ToString();                   /* 50  VARCHAR   NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _SerialNumber");
                _SerialNumber = "";
            }

            try
            {
                _CommandText = reader["CommandText"].ToString();                     /* 50  VARCHAR   NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CommandText");
                _CommandText = "";
            }

            try
            {
                _SentDateUTC = reader["SentDateUTC"].ToString();                     /* 8   DATETIME  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _SentDateUTC");
                _SentDateUTC = "";
            }

            try
            {
                _CompleteDateUTC = reader["CompleteDateUTC"].ToString();             /* 8   DATETIME  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CompleteDateUTC");
                _CompleteDateUTC = "";
            }

            try
            {
                stmp = reader["NotificationConditionID"].ToString();
                _NotificationConditionID = Convert.ToInt32((stmp.Length > 0) ? stmp : "0"); /* 4   INT       NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _NotificationConditionID");
                _NotificationConditionID = 0;
            }

            try
            {
                stmp = reader["ROW_ID"].ToString();
                _ROW_ID = Convert.ToInt32((stmp.Length > 0) ? stmp : "0");         /* 4   IDENTITY  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _ROW_ID");
                _ROW_ID = 0;
            }

            return;
        }
        /*--- end of LoadVariables() ----------------------------------------------------*/
        #endregion




        #region LoadVariables
        /****************************************************************************
          LoadVariables                                          1/17/2012
        Description: Load variables from the dataset.
         Parameters: DataRow
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private void LoadVariables(DataRow dRow)
        {
            string stmp = "";

            try
            {
                _CreatedDateUTC = dRow["CreatedDateUTC"].ToString();               /* 8   DATETIME  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CreatedDateUTC");
                _CreatedDateUTC = "";
            }

            try
            {
                _SerialNumber = dRow["SerialNumber"].ToString();                     /* 50  VARCHAR   NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _SerialNumber");
                _SerialNumber = "";
            }

            try
            {
                _CommandText = dRow["CommandText"].ToString();                       /* 50  VARCHAR   NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CommandText");
                _CommandText = "";
            }

            try
            {
                _SentDateUTC = dRow["SentDateUTC"].ToString();                     /* 8   DATETIME  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _SentDateUTC");
                _SentDateUTC = "";
            }

            try
            {
                _CompleteDateUTC = dRow["CompleteDateUTC"].ToString();             /* 8   DATETIME  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CompleteDateUTC");
                _CompleteDateUTC = "";
            }

            try
            {
                stmp = dRow["NotificationConditionID"].ToString();
                _NotificationConditionID = Convert.ToInt32((stmp.Length > 0) ? stmp : "0"); /* 4   INT       NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _NotificationConditionID");
                _NotificationConditionID = 0;
            }

            try
            {
                stmp = dRow["ROW_ID"].ToString();
                _ROW_ID = Convert.ToInt32((stmp.Length > 0) ? stmp : "0");         /* 4   IDENTITY  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _ROW_ID");
                _ROW_ID = 0;
            }

            return;
        }
        /*--- end of LoadVariables() ----------------------------------------------------*/
        #endregion




        #region Select
        /****************************************************************************
          Select                                                 1/17/2012
        Description: Gets a single record from the table.
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: SQLSTMT must be set before calling this function.
      ****************************************************************************/

        /// <summary>
        /// Selects a record from the CommandQueue table
        /// The SQL statement must be composed before calling this function.
        /// Uses the global SQLSTMT.
        /// </summary>
        /// <param name="none">none</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Select()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Select() -----------------------------------------------------*/
        #endregion




        #region Select
        /****************************************************************************
          Select                                                 1/17/2012
        Description: Get the record for the supplied SQL statement
         Parameters: SQL Statement
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Selects a record from the CommandQueue table
        /// </summary>
        /// <param name="psql">string - the SQL statement that selects the desired record.</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Select(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Select() -----------------------------------------------------*/
        #endregion




        #region Select
        /****************************************************************************
          Select                                                 1/17/2012
        Description: Get the record specified.
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Selects a record from the CommandQueue table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Select(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM CommandQueue WHERE ( " + field + " = '" + value + "' )";
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 3. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Select() -----------------------------------------------------*/
        #endregion




        #region Exists
        /****************************************************************************
          Exists                                                 1/17/2012
        Description: See if the specified record exists
         Parameters: SQL Statement
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Determines if a record exists in the CommandQueue table
        /// </summary>
        /// <param name="psql">string - the SQL statement to select a record.</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Exists(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;
                    try
                    {
                        _ROW_ID = Convert.ToInt32(reader["ROW_ID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: ROW_ID");
                        _ROW_ID = 0;
                    }
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 4. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Exists() -----------------------------------------------------*/
        #endregion




        #region Exists
        /****************************************************************************
          Exists                                                 1/17/2012
        Description: See if the specified record exists
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Determines if a record exists in the CommandQueue table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Exists(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM CommandQueue WHERE ( " + field + " = '" + value + "' )";
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;
                    try
                    {
                        _ROW_ID = Convert.ToInt32(reader["ROW_ID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: ROW_ID");
                        _ROW_ID = 0;
                    }
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 5. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Exists() -----------------------------------------------------*/
        #endregion




        #region Requery
        /****************************************************************************
          Requery                                                1/17/2012
        Description: ReGets the current record
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Re-reads a record from the CommandQueue table based on its DEX_ROW_ID
        /// </summary>
        /// <param name="none">none</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Requery()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM CommandQueue WHERE ( ROW_ID = " + _ROW_ID + " )";
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 6. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Requery() ---------------------------------------------------*/
        #endregion




        #region SelectMulti
        /****************************************************************************
          SelectMulti                                            1/17/2012
        Description: Use this function when your query could return
                     more than one row.
         Parameters: 
       Return Value: datatable
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Returns a DataTable containing all matching rows from the SELECT statement.
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>DataTable</returns>

        public DataTable SelectMulti(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM CommandQueue WHERE ( " + field + " = '" + value + "' )";

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                dt = ReaderToTable(reader);
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 7. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
                return (null);
            }
            finally
            {
                sqlConn.Close();
            }

            return (dt);
        }
        /*--- end of SelectMulti() ------------------------------------------------*/
        #endregion




        #region SelectMulti
        /****************************************************************************
          SelectMulti                                            1/17/2012
        Description: Use this function when your query could return
                     more than one row.
         Parameters: 
       Return Value: datatable
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Returns a DataTable containing all matching rows from the SELECT statement.
        /// </summary>
        /// <param name="psql">string - The SQL statement that performs the SELECT function.</param>
        /// <returns>DataTable</returns>

        public DataTable SelectMulti(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                dt = ReaderToTable(reader);
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from CommandQueue 8. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
                return (null);
            }
            finally
            {
                sqlConn.Close();
            }

            return (dt);
        }
        /*--- end of SelectMulti() ------------------------------------------------*/
        #endregion








    }
}
/*--- end of CommandQueue.cs ----------------------------------------------------*/

namespace Kokoro          /* Change namespace to match your project */
{
    public class QueryString
    {
        //private string connStr = @"server = (local); " +
        //                         @"Persist Security Info = False;" +
        //                         @"Integrated Security = SSPI; " +
        //                         @"Connect Timeout = 30; " +
        //                         @"Initial Catalog = StingerCAST;";

        private string connStr = "";
        private string SQLSTMT = "";         /* SQL Statement */
        private bool GOTERROR = false;     /* error flag */
        private DataTable dt;

        private bool _OKtoWrite = true;    /* read-only if false */
        private int _SQLTimeout = 120;    /* SQL timeout in seconds */
        private bool _LogErrors = false;   /* Log Format Exceptions */


        #region QueryStringVariables
        /* Variables for QueryString */

        private string _DeviceSerialNumber = String.Empty;                   /* 50  VARCHAR   NULL     */
        private string _RTLSPkt = String.Empty;
        private string _EmTagID = String.Empty; 
        private string _Beacon1Minor = String.Empty;
        private string _Beacon1Major = String.Empty;
        private string _Beacon1TX = String.Empty;
        private string _Beacon2Minor = String.Empty;
        private string _Beacon2Major = String.Empty;
        private string _Beacon2TX = String.Empty;
        private string _Beacon1BatteryLvl = String.Empty;
        private string _Beacon2BatteryLvl = String.Empty;
        private string _EventCode = String.Empty;
        private string _SiteID = String.Empty;
        private string _Query = String.Empty;                                /* 2000 VARCHAR   NULL     */
        private string _SourceIPAddress = string.Empty;                      /* 20 VARCHAR NULL     */
        private bool _NineParsed = false;                         /* 1   BIT       NULL     */
        private int _ROW_ID = 0;                                 /* 4   IDENTITY  NOT NULL */
        private string _CreatedDateUTC = DateTime.UtcNow.ToShortDateString();              /* 8   DATETIME  NULL     */
        #endregion





        #region QueryStringProperties
        /* Properties for QueryString */

        public string DeviceSerialNumber
        {
            get { return this._DeviceSerialNumber; }
            set
            {
                if (value.Length > 50)
                    this._DeviceSerialNumber = value.Substring(0, 50);
                else
                    this._DeviceSerialNumber = value;
            }
        }
        public string EmTagID
        {
            get { return this._EmTagID; }
            set
            {
                if (value.Length > 50)
                    this._EmTagID = value.Substring(0, 50);
                else
                    this._EmTagID = value;
            }
        }
        public string SiteID
        {
            get { return this._SiteID; }
            set
            {
                if (value.Length > 50)
                    this._SiteID = value.Substring(0, 50);
                else
                    this._SiteID = value;
            }
        }
        public string RTLSPkt
        {
            get { return this._RTLSPkt; }
            set
            {
                if (value.Length > 90)
                    this._RTLSPkt = value.Substring(0, 90);
                else
                    this._RTLSPkt = value;
            }
        }
   
        public string Beacon1TX
        {
            get { return this._Beacon1TX; }
            set
            {
                if (value.Length > 22)
                    this._Beacon1TX = value.Substring(0, 22);
                else
                    this._Beacon1TX = value;
            }
        }
        public string Beacon1Major
        {
            get { return this._Beacon1Major; }
            set
            {
                if (value.Length > 22)
                    this._Beacon1Major = value.Substring(0, 22);
                else
                    this._Beacon1Major = value;
            }
        }
        public string Beacon1Minor
        {
            get { return this._Beacon1Minor; }
            set
            {
                if (value.Length > 22)
                    this._Beacon1Minor = value.Substring(0, 22);
                else
                    this._Beacon1Minor = value;
            }
        }
        public string Beacon1BatteryLvl
        {
            get { return this._Beacon1BatteryLvl; }
            set
            {
                if (value.Length > 22)
                    this._Beacon1BatteryLvl = value.Substring(0, 22);
                else
                    this._Beacon1BatteryLvl = value;
            }
        }


        public string Beacon2TX
        {
            get { return this._Beacon2TX; }
            set
            {
                if (value.Length > 22)
                    this._Beacon2TX = value.Substring(0, 22);
                else
                    this._Beacon2TX = value;
            }
        }
        public string Beacon2Major
        {
            get { return this._Beacon2Major; }
            set
            {
                if (value.Length > 22)
                    this._Beacon2Major = value.Substring(0, 22);
                else
                    this._Beacon2Major = value;
            }
        }
        public string Beacon2Minor
        {
            get { return this._Beacon2Minor; }
            set
            {
                if (value.Length > 22)
                    this._Beacon2Minor = value.Substring(0, 22);
                else
                    this._Beacon2Minor = value;
            }
        }

        public string Beacon2BatteryLvl
        {
            get { return this._Beacon2BatteryLvl; }
            set
            {
                if (value.Length > 22)
                    this._Beacon2BatteryLvl = value.Substring(0, 22);
                else
                    this._Beacon2BatteryLvl = value;
            }
        }
        public string EventCode
        {
            get { return this._EventCode; }
            set
            {
                if (value.Length > 22)
                    this._EventCode = value.Substring(0, 22);
                else
                    this._EventCode = value;
            }
        }
       

        public string Query
        {
            get { return this._Query; }
            set
            {
                if (value.Length > 2000)
                    this._Query = value.Substring(0, 2000);
                else
                    this._Query = value;
            }
        }
        public string SourceIPAddress
        {
            get { return this._SourceIPAddress; }
            set
            {
                if (value.Length > 20)
                    this._SourceIPAddress = value.Substring(0, 20);
                else
                    this._SourceIPAddress = value;
            }
        }
        public bool NineParsed { get { return this._NineParsed; } set { this._NineParsed = value; } }
        public int ROW_ID { get { return this._ROW_ID; } set { this._ROW_ID = value; } }
        public string CreatedDateUTC { get { return this._CreatedDateUTC; } set { this._CreatedDateUTC = value; } }

        public bool OKToWrite { get { return this._OKtoWrite; } set { this._OKtoWrite = value; } }
        public bool GotError { get { return this.GOTERROR; } set { this.GOTERROR = value; } }
        public string SQLStmt { get { return this.SQLSTMT; } set { this.SQLSTMT = value; } }
        public int SQLTimeout { get { return this._SQLTimeout; } set { this._SQLTimeout = value; } }
        #endregion





        #region QueryString Constructor
        /****************************************************************************
          QueryString                                            1/17/2012
        Description: Class Constructor
         Parameters: SQL connection string
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// QueryString Constructor
        /// </summary>
        /// <param name="s">SQL Connection String</param>
        /// <returns>None</returns>

        public QueryString(string s)
        {
            connStr = s;
            dt = new DataTable();
        }
        /*--- end of QueryString() ------------------------------------------------*/
        #endregion




        #region CaptureError
        /****************************************************************************
          CaptureError                                           1/17/2012
        Description: Writes all errors to a text file.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private void CaptureError(string errmsg, string SQLStmt)
        {
            try
            {
                StreamWriter textOut =
                   new StreamWriter(new FileStream("C:\\inetpub\\logs\\logfiles\\SQLError.log",
                                                     FileMode.Append,
                                                     FileAccess.Write));

                textOut.WriteLine(DateTime.Now.ToString());
                textOut.WriteLine(SQLStmt);
                textOut.WriteLine(errmsg);
                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
            }
            catch (IOException iox)
            {
                string errfptr = "Could not write to SQLError.log. Error: " + iox.ToString();
            }

            //GOTERROR = false;

            return;
        }
        /*--- end of CaptureError() -----------------------------------------------*/
        #endregion




        #region CleanField
        /****************************************************************************
          CleanField                                             1/17/2012
        Description: Check for ' in strings.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: Replaces a single quote with two single quotes.
      ****************************************************************************/

        private string CleanField(string stest)
        {
            string sTemp = "";

            if (stest.Contains("''"))   // already done 
                return (stest);

            if (stest.Contains("'"))
            {
                for (int i = 0; i < stest.Length; i++)
                {
                    if (stest[i] == '\'')
                    {
                        sTemp = sTemp + '\'';
                    }
                    sTemp = sTemp + stest[i];
                }
                return (sTemp);
            }

            return (stest);
        }
        /*--- end of CleanField() -------------------------------------------------*/
        #endregion




        #region ReaderToTable
        /****************************************************************************
          ReaderToTable                                          1/17/2012
        Description: Load a Data Reader into a Data Table
         Parameters: 
       Return Value: 
         Processing: 
          Called By: SelectMulti()
           Comments: 
      ****************************************************************************/

        private DataTable ReaderToTable(SqlDataReader reader)
        {
            DataTable newTable = new DataTable();
            DataColumn col = null;
            DataRow row = null;
            int i = 0;

            for (i = 0; i < reader.FieldCount; i++)
            {
                col = new DataColumn();
                col.ColumnName = reader.GetName(i);
                col.DataType = reader.GetFieldType(i);

                newTable.Columns.Add(col);
            }

            while (reader.Read())
            {
                row = newTable.NewRow();
                for (i = 0; i < reader.FieldCount; i++)
                {
                    row[i] = reader[i];
                }

                newTable.Rows.Add(row);
            }

            return newTable;
        }
        /*--- end of ReaderToTable() ----------------------------------------------*/
        #endregion




        #region GetLastDEX
        /****************************************************************************
          GetLastDEX                                          1/17/2012
        Description: Gets the last ROW_ID
         Parameters: 
       Return Value: Returns the ROW_ID of the last inserted record
         Processing: 
          Called By: 
           Comments: Can be used with any Identity field.
      ****************************************************************************/

        /// <summary>
        /// Gets the last ROW_ID from the table
        /// </summary>
        /// <returns>int - the last ROW_ID</returns>

        public int GetLastDEX()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLStmt = "SELECT MAX(ROW_ID) AS LastDEX FROM QueryString";
            int LASTDEX = 0;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLStmt, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)      // need one row back
                {
                    reader.Read();
                    LASTDEX = (int)reader[0];
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLStmt);
            }
            catch (Exception ex)
            {
                string errfptr = "Failed to Select from QueryString. Unknown Error: " + ex.ToString();
                CaptureError(errfptr, SQLStmt);
            }
            finally
            {
                sqlConn.Close();
            }

            return (LASTDEX);
        }
        /*--- end of GetLastDEX() -------------------------------------------------*/
        #endregion




        #region Clear
        /****************************************************************************
          Clear                                                  1/17/2012
        Description: Clears the variables for a new insert
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Clear() - Sets all class variables to a default value.
        /// </summary>
        /// <param name="none">No parameters</param>
        /// <returns>void</returns>

        public void Clear()
        {
            _DeviceSerialNumber = String.Empty;                   /* 50  VARCHAR   NULL     */
            _Query = String.Empty;                                /* 2000 VARCHAR   NULL     */
            _SourceIPAddress = string.Empty;
            _NineParsed = false;                         /* 1   BIT       NULL     */
            _ROW_ID = 0;                                 /* 4   IDENTITY  NOT NULL */
            _CreatedDateUTC = DateTime.UtcNow.ToShortDateString();              /* 8   DATETIME  NULL     */

            GOTERROR = false;                            /* error flag */

            return;
        }
        /*--- end of Clear() ------------------------------------------------------*/
        #endregion




        #region Insert
        /****************************************************************************
          Insert                                                 1/17/2012
        Description: Insert a new record.
         Parameters: 
       Return Value: 1
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Inserts a new record into the QueryString table
        /// </summary>
        /// <returns>int- the number of records inserted.</returns>

        public int Insert()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            int RecCnt = 0;

            /*  Put Field Modifications Here:  */



            SQLSTMT = "spAssetLocation_Insert";  // changed to point to new stored proc which requires timestamp to be passed from the web server
            //Data Parameter Structure:
            //EmTagID=####&Beacon1TX=####&Beacon1Minor=####&Beacon1Major=####&Beacon1BatteryLvl=90&Beacon2TX=####&Beacon2Minor=####&Beacon2Major=####&Beacon2BatteryLvl=90&EventCode=0

            /* You may need to remove IDENTITY fields and/or trailing comma's! */
            char pad = '0';
            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandType = CommandType.StoredProcedure;  // added 6-15-2012 by Danny Bates (so we can modify insert behavior without having to recompile this code)
                command.CommandTimeout = _SQLTimeout;

                command.Parameters.Add("@EMTAGID", SqlDbType.Int).Value = Convert.ToInt32(_EmTagID);
                command.Parameters.Add("@EVENTCODE", SqlDbType.VarChar).Value = Convert.ToInt32(_EventCode);
                command.Parameters.Add("@BEACON1MINOR", SqlDbType.VarChar).Value = _Beacon1Minor;
                command.Parameters.Add("@BEACON1MAJOR", SqlDbType.VarChar).Value = _Beacon1Major;
                command.Parameters.Add("@BEACON1TX", SqlDbType.VarChar).Value = _Beacon1TX;     
                command.Parameters.Add("@BEACON2MINOR", SqlDbType.VarChar).Value = _Beacon2Minor;
                command.Parameters.Add("@BEACON2MAJOR", SqlDbType.VarChar).Value = _Beacon2Major;
                command.Parameters.Add("@BEACON2TX", SqlDbType.VarChar).Value = _Beacon2TX;

                command.Parameters.Add("@BEACON1BATTERYLVL", SqlDbType.VarChar).Value = _Beacon1BatteryLvl;
                command.Parameters.Add("@BEACON2BATTERYLVL", SqlDbType.VarChar).Value = _Beacon2BatteryLvl;
                command.Parameters.Add("@SiteID", SqlDbType.VarChar).Value = Convert.ToInt32(_SiteID);
                
                //  command.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = _Query;
                //  command.Parameters.Add("@NINEPARSED", SqlDbType.Bit).Value = _NineParsed;
                //  command.Parameters.Add("@SOURCEIPADDRESS", SqlDbType.VarChar).Value = _SourceIPAddress;
                command.Parameters.Add("@CREATEDDATEUTC", SqlDbType.VarChar).Value = DateTime.UtcNow.Year.ToString() + '-' + DateTime.UtcNow.Month.ToString().PadLeft(2, pad) + '-' + DateTime.UtcNow.Day.ToString().PadLeft(2, pad) + ' ' + DateTime.UtcNow.TimeOfDay.ToString().Substring(0, 12);
                if (_Beacon1Major == "000" && Beacon2Major == "000")
                {

                }
                else
                {

                    if (_OKtoWrite)
                        RecCnt = command.ExecuteNonQuery();
                }
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Insert " + _EmTagID + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Insert() -----------------------------------------------------*/
        #endregion




        #region Insert
        /****************************************************************************
          Insert                                                 1/17/2012
        Description: Insert a new record.
         Parameters: SQL Statement
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Inserts a new record into the QueryString table
        /// </summary>
        /// <param name="psql">string - the SQL statement that will perform the insert</param>
        /// <returns>int - the number of records inserted.</returns>

        public int Insert(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();      // execute query
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Insert into QueryString 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Insert() -----------------------------------------------------*/
        #endregion




        #region SetSQLStatement
        /****************************************************************************
          SetSQLStatement                                        1/17/2012
        Description: Develops the SQL statement for updating
         Parameters: whereclause - complete WHERE clause
                               i - just needed to make this overload unique
       Return Value: Complete SQL statement
         Processing: 
          Called By: Update()
           Comments: 
      ****************************************************************************/

        private string SetSQLStatement(string whereclause, int i)
        {
            /*  Put Field Modifications Here:  */


            string SQLS = "UPDATE QueryString SET " +
                             "DeviceSerialNumber = @DEVICESERIALNUMBER" +
                             ", Query = @QUERY" +
                             ", NineParsed = @NINEPARSED" +
                             ", SourceIPAddress = @SOURCEIPADDRESS" +
                             ", CreatedDateUTC = @CREATEDDATEUTC" +
                             " WHERE ( " + whereclause + " )";

            /* ZZZZZZZZ */
            /* You may need to remove IDENTITY fields and/or trailing comma's! */

            return (SQLS);
        }
        /*--- end of SetSQLStatement() --------------------------------------------*/
        #endregion




        #region Update
        /****************************************************************************
          Update                                                 1/17/2012
        Description: Updates the record specified.
         Parameters: whereclause - complete WHERE clause
                               i - just needed to make this overload unique
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Updates a record in the QueryString table
        /// </summary>
        /// <param name="where">the SQL WHERE clause</param>
        /// <param name="i">int - a dummy parameter, not used</param>
        /// <returns>int - the number of records updated</returns>

        public int Update(string where, int i)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            int RecCnt = 0;

            SQLSTMT = SetSQLStatement(where, i);

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;

                command.Parameters.Add("@DEVICESERIALNUMBER", SqlDbType.VarChar).Value = _DeviceSerialNumber;
                command.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = _Query;
                command.Parameters.Add("@NINEPARSED", SqlDbType.Bit).Value = _NineParsed;
                command.Parameters.Add("@SOURCEIPADDRESS", SqlDbType.VarChar).Value = _SourceIPAddress;
                command.Parameters.Add("@CREATEDDATEUTC", SqlDbType.DateTime).Value = _CreatedDateUTC;

                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Update QueryString #" + i.ToString() + ". Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Update() -----------------------------------------------------*/
        #endregion




        #region SetSQLStatement
        /****************************************************************************
          SetSQLStatement                                        1/17/2012
        Description: Develops the SQL statement for updating
         Parameters: 
       Return Value: 
         Processing: 
          Called By: Update()
           Comments: 
      ****************************************************************************/

        private string SetSQLStatement(string field, string value)
        {
            /*  Put Field Modifications Here:  */


            string SQLS = "UPDATE QueryString SET " +
                             "DeviceSerialNumber = @DEVICESERIALNUMBER" +
                             ", Query = @QUERY" +
                             ", NineParsed = @NINEPARSED" +
                             ", SourceIPAddress = @SOURCEIPADDRESS" +
                             ", CreatedDateUTC = @CREATEDDATEUTC" +
                             " WHERE ( " + field + " = '" + value + "' )";

            /* ZZZZZZZZ */
            /* You may need to remove IDENTITY fields and/or trailing comma's! */

            return (SQLS);
        }
        /*--- end of SetSQLStatement() --------------------------------------------*/
        #endregion




        #region Update
        /****************************************************************************
          Update                                                 1/17/2012
        Description: Updates the record specified.
         Parameters: Field and Value to update
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Updates a record in the QueryString table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>int - the number of records updated</returns>

        public int Update(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            int RecCnt = 0;

            SQLSTMT = SetSQLStatement(field, value);

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;

                command.Parameters.Add("@DEVICESERIALNUMBER", SqlDbType.VarChar).Value = _DeviceSerialNumber;
                command.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = _Query;
                command.Parameters.Add("@NINEPARSED", SqlDbType.Bit).Value = _NineParsed;
                command.Parameters.Add("@SOURCEIPADDRESS", SqlDbType.VarChar).Value = _SourceIPAddress;
                command.Parameters.Add("@CREATEDDATEUTC", SqlDbType.DateTime).Value = _CreatedDateUTC;

                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Update QueryString 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Update() -----------------------------------------------------*/
        #endregion




        #region Update
        /****************************************************************************
          Update                                                 1/17/2012
        Description: Updates a record.
         Parameters: SQL Statement
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: SQL Stmt must be set before calling this function.
      ****************************************************************************/

        /// <summary>
        /// Updates a record in the QueryString table
        /// </summary>
        /// <param name="psql">string - the SQL statement that performs the update</param>
        /// <returns>int - the number of records updated</returns>

        public int Update(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();      // execute query
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Update QueryString 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Update() -----------------------------------------------------*/
        #endregion




        #region Delete
        /****************************************************************************
          Delete                                                 1/17/2012
        Description: Deletes the record specified.
         Parameters: 
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Deletes a record from the QueryString table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>int - the number of records deleted.</returns>

        public int Delete(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "DELETE FROM QueryString WHERE ( " + field + " = '" + value + "' )";
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();     // delete record
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Delete From QueryString 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Delete() -----------------------------------------------------*/
        #endregion




        #region Delete
        /****************************************************************************
          Delete                                                 1/17/2012
        Description: Deletes the record specified.
         Parameters: SQL statement
       Return Value: # of records affected
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Deletes a record from the QueryString table
        /// </summary>
        /// <param name="psql">string - the SLQ statement that performs the delete</param>
        /// <returns>int - the number of records deleted.</returns>

        public int Delete(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            int RecCnt = 0;

            try
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                if (_OKtoWrite)
                    RecCnt = command.ExecuteNonQuery();     // delete record
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Delete From QueryString 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (RecCnt);
        }
        /*--- end of Delete() -----------------------------------------------------*/
        #endregion




        #region LoadVariables
        /****************************************************************************
          LoadVariables                                          1/17/2012
        Description: Load variables from the dataset.
         Parameters: SqlDataReader
       Return Value: 
         Processing: 
          Called By: Select(), Requery()
           Comments: 
      ****************************************************************************/

        private void LoadVariables(SqlDataReader reader)
        {
            string stmp = "";

            try
            {
                _DeviceSerialNumber = reader["DeviceSerialNumber"].ToString();       /* 50  VARCHAR   NULL     */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _DeviceSerialNumber");
                _DeviceSerialNumber = "";
            }

            try
            {
                _Query = reader["Query"].ToString();                                 /* 2000 VARCHAR   NULL     */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _Query");
                _Query = "";
            }

            try
            {
                stmp = reader["NineParsed"].ToString().ToUpper();                     /* 1   BIT       NULL     */
                if (stmp == "TRUE")
                    _NineParsed = true;
                else
                    _NineParsed = false;
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _NineParsed");
                _NineParsed = false;
            }

            try
            {
                _SourceIPAddress = reader["SourceIPAddress"].ToString();
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _SourceIPAddress");
                _SourceIPAddress = string.Empty;
            }

            try
            {
                stmp = reader["ROW_ID"].ToString();
                _ROW_ID = Convert.ToInt32((stmp.Length > 0) ? stmp : "0");         /* 4   IDENTITY  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _ROW_ID");
                _ROW_ID = 0;
            }

            try
            {
                _CreatedDateUTC = reader["CreatedDateUTC"].ToString();               /* 8   DATETIME  NULL     */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CreatedDateUTC");
                _CreatedDateUTC = "";
            }

            return;
        }
        /*--- end of LoadVariables() ----------------------------------------------------*/
        #endregion




        #region LoadVariables
        /****************************************************************************
          LoadVariables                                          1/17/2012
        Description: Load variables from the dataset.
         Parameters: DataRow
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private void LoadVariables(DataRow dRow)
        {
            string stmp = "";

            try
            {
                _DeviceSerialNumber = dRow["DeviceSerialNumber"].ToString();         /* 50  VARCHAR   NULL     */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _DeviceSerialNumber");
                _DeviceSerialNumber = "";
            }

            try
            {
                _Query = dRow["Query"].ToString();                                   /* 2000 VARCHAR   NULL     */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _Query");
                _Query = "";
            }

            try
            {
                stmp = dRow["NineParsed"].ToString().ToUpper();                     /* 1   BIT       NULL     */
                if (stmp == "TRUE")
                    _NineParsed = true;
                else
                    _NineParsed = false;
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _NineParsed");
                _NineParsed = false;
            }

            try
            {
                _SourceIPAddress = dRow["SourceIPAddress"].ToString();
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _SourceIPAddress");
                _SourceIPAddress = string.Empty;
            }

            try
            {
                stmp = dRow["ROW_ID"].ToString();
                _ROW_ID = Convert.ToInt32((stmp.Length > 0) ? stmp : "0");         /* 4   IDENTITY  NOT NULL */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _ROW_ID");
                _ROW_ID = 0;
            }

            try
            {
                _CreatedDateUTC = dRow["CreatedDateUTC"].ToString();               /* 8   DATETIME  NULL     */
            }
            catch (Exception ex)
            {
                if (_LogErrors)
                    CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: _CreatedDateUTC");
                _CreatedDateUTC = "";
            }

            return;
        }
        /*--- end of LoadVariables() ----------------------------------------------------*/
        #endregion




        #region Select
        /****************************************************************************
          Select                                                 1/17/2012
        Description: Gets a single record from the table.
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: SQLSTMT must be set before calling this function.
      ****************************************************************************/

        /// <summary>
        /// Selects a record from the QueryString table
        /// The SQL statement must be composed before calling this function.
        /// Uses the global SQLSTMT.
        /// </summary>
        /// <param name="none">none</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Select()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 1. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Select() -----------------------------------------------------*/
        #endregion




        #region Select
        /****************************************************************************
          Select                                                 1/17/2012
        Description: Get the record for the supplied SQL statement
         Parameters: SQL Statement
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Selects a record from the QueryString table
        /// </summary>
        /// <param name="psql">string - the SQL statement that selects the desired record.</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Select(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 2. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Select() -----------------------------------------------------*/
        #endregion




        #region Select
        /****************************************************************************
          Select                                                 1/17/2012
        Description: Get the record specified.
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Selects a record from the QueryString table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Select(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM QueryString WHERE ( " + field + " = '" + value + "' )";
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 3. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Select() -----------------------------------------------------*/
        #endregion




        #region Exists
        /****************************************************************************
          Exists                                                 1/17/2012
        Description: See if the specified record exists
         Parameters: SQL Statement
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Determines if a record exists in the QueryString table
        /// </summary>
        /// <param name="psql">string - the SQL statement to select a record.</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Exists(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;
                    try
                    {
                        _ROW_ID = Convert.ToInt32(reader["ROW_ID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: ROW_ID");
                        _ROW_ID = 0;
                    }
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 4. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Exists() -----------------------------------------------------*/
        #endregion




        #region Exists
        /****************************************************************************
          Exists                                                 1/17/2012
        Description: See if the specified record exists
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Determines if a record exists in the QueryString table
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Exists(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM QueryString WHERE ( " + field + " = '" + value + "' )";
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;
                    try
                    {
                        _ROW_ID = Convert.ToInt32(reader["ROW_ID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        CaptureError("Exception Error: " + ex.ToString(), "Format Exception Error: ROW_ID");
                        _ROW_ID = 0;
                    }
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 5. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Exists() -----------------------------------------------------*/
        #endregion




        #region Requery
        /****************************************************************************
          Requery                                                1/17/2012
        Description: ReGets the current record
         Parameters: 
       Return Value: true if a record was found, false otherwise
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Re-reads a record from the QueryString table based on its DEX_ROW_ID
        /// </summary>
        /// <param name="none">none</param>
        /// <returns>bool - true if found, false if not found. </returns>

        public bool Requery()
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM QueryString WHERE ( ROW_ID = " + _ROW_ID + " )";
            bool rtn = false;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)       // need one row back
                {
                    reader.Read();
                    rtn = true;

                    LoadVariables(reader);
                }
                reader.Close();
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 6. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
            }
            finally
            {
                sqlConn.Close();
            }

            return (rtn);
        }
        /*--- end of Requery() ---------------------------------------------------*/
        #endregion




        #region SelectMulti
        /****************************************************************************
          SelectMulti                                            1/17/2012
        Description: Use this function when your query could return
                     more than one row.
         Parameters: 
       Return Value: datatable
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Returns a DataTable containing all matching rows from the SELECT statement.
        /// </summary>
        /// <param name="field">string - the field to match in the SQL statement</param>
        /// <param name="value">string - the value to match</param>
        /// <returns>DataTable</returns>

        public DataTable SelectMulti(string field, string value)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = "SELECT * FROM QueryString WHERE ( " + field + " = '" + value + "' )";

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                dt = ReaderToTable(reader);
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 7. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
                return (null);
            }
            finally
            {
                sqlConn.Close();
            }

            return (dt);
        }
        /*--- end of SelectMulti() ------------------------------------------------*/
        #endregion




        #region SelectMulti
        /****************************************************************************
          SelectMulti                                            1/17/2012
        Description: Use this function when your query could return
                     more than one row.
         Parameters: 
       Return Value: datatable
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        /// <summary>
        /// Returns a DataTable containing all matching rows from the SELECT statement.
        /// </summary>
        /// <param name="psql">string - The SQL statement that performs the SELECT function.</param>
        /// <returns>DataTable</returns>

        public DataTable SelectMulti(string psql)
        {
            SqlConnection sqlConn = new SqlConnection(connStr);
            SQLSTMT = psql;

            try
            {
                sqlConn.Open();

                SqlCommand command = new SqlCommand(SQLSTMT, sqlConn);
                command.CommandTimeout = _SQLTimeout;
                SqlDataReader reader = command.ExecuteReader();

                dt = ReaderToTable(reader);
            }
            catch (SqlException sqe)
            {
                string errfptr = "Failed to Select from QueryString 8. Error: " + sqe.ToString();
                CaptureError(errfptr, SQLSTMT);
                GOTERROR = true;
                return (null);
            }
            finally
            {
                sqlConn.Close();
            }

            return (dt);
        }
        /*--- end of SelectMulti() ------------------------------------------------*/
        #endregion

    }
}
/*--- end of QueryString.cs -----------------------------------------------------*/


