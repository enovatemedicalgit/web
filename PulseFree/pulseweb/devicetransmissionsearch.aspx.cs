﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Data;
using System.Web.UI;
using Telerik.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using PulseWeb.SearchObjects;
using PulseWeb.API;
using PulseWeb.Model;
using PulseWeb.Utils;
using PulseWeb.Utils.Security;
using PulseWeb.Utils.Database;
/**********************************
 * Controls that postback:
 * btnSearch ...self implied
 * ddlDeptWFOA - Location info
 * ddlSites - facility options
 * chkSelect - select all / unselect all feature
 * btnExportToExcel ...yes and no see js function
 * onRequestStart (telerik work around)
 * Telerik standard... DC
 * ****************************************************/

namespace PulseWeb
{
    public partial class devicetransmissionsearch : System.Web.UI.Page
    {
        bool SearchClicked = false;
        protected void Page_Load(object sender, EventArgs e)
        {
              string name = Request.QueryString["MessageID"];
            if (!string.IsNullOrEmpty(name)  )
            {
                int id = Convert.ToInt32(name);
             
            }
            if (!Page.IsPostBack)
            {
                ResetGridSettings();
                SetDefaultDatesAndTimes();

                string ActivityDescription = "Search";
                
              //  ActivityLog.Log(api.SessionUser.UserName.ToString(), ActivityDescription);
                btnExcelExport.Attributes.Add("onclick", "window.open('./DeviceTransmissionSearchExport.aspx');");
            }
             
            datetimeEnd.SelectedDate = DateTime.Now.AddHours(1);
        }

        #region HelperFunctions

        private bool Valid()
        {
            if (datetimeStart.SelectedDate > DateTime.UtcNow)
            {
                ShowMessage("A valid start date is required to search");
            }
            if (datetimeEnd.SelectedDate > DateTime.UtcNow)
            {
                ShowMessage("A valid end date is required to search");
            }
            return true;
        }

        private void ShowMessage(string x)
        {
            lblFeedback.Text = x;
            lblFeedback.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFCC00");
            lblFeedback.Visible = true;
        }

        private void DoSearch()
        {
            SearchClicked = true;
            string ActivityDescription = "Search button clicked (Source: " + ddlTransmissionType.SelectedValue + ")";
          //  ActivityLog.Log(Session["UserID"].ToString(), ActivityDescription);
          //  SaveToSearchLog();

            hidEquipmentType.Value = ddlDeviceType.SelectedValue;
            hidSource.Value = ddlTransmissionType.SelectedValue;

            //TAKES SESSION SEARCH PATH                   
            if (ddlTransmissionType.SelectedValue.ToUpper() == "SESSION")
            {
                LoadGridSettings(rgrdSessions, GetGridSettingKey());
                btnPersistSettings.Visible = true;
                rgrdSessions.Visible = false;
                rgrdSessions.DataSource = null;
                rgrdSessions.Rebind();
                rgrdParent.Visible = ddlTransmissionType.SelectedValue != "Session";
                rgrdSessions.Visible = ddlTransmissionType.SelectedValue == "Session";
            }
            else
            {//TAKES SEARCH PATH
                LoadGridSettings(rgrdParent, GetGridSettingKey());
                btnPersistSettings.Visible = true;
                rgrdParent.Visible = false;
                rgrdParent.DataSource = null;
                rgrdParent.Rebind();
                rgrdParent.Visible = ddlTransmissionType.SelectedValue != "Session";
                rgrdSessions.Visible = ddlTransmissionType.SelectedValue == "Session";
            }

        }

        private void ShowLocationInfo()
        {
            lblFeedback.Text = string.Empty;
            lblFeedback.Visible = false;

            if ((ddlDeptWFOA.SelectedValue.ToUpper() != "ALL") && (ddlSite.SelectedValue != "0"))
            {
                ExecuteDeptWFOA();
            }
            else
            {
                ClearDept();
            }
        }

        private void ClearDept()
        {
            cblDeptWFOA.DataSource = new Object[0];
            cblDeptWFOA.DataBind();
            cblDeptWFOA.Visible = false;
            chkSelect.Visible = false;
        }

        #region initialLoads

        private string GetMediaExtension()
        {
            return Session["MediaType"].ToString() == "QuickTime" ? "mp4" : "flv";
        }




        #endregion

        #region timeFunctions

        private void SetDefaultDatesAndTimes()
        {
            DateTime dtLocalNow = LocalDateTime(DateTime.UtcNow);
            //datetimeStart.DbSelectedDate = dtLocalNow.AddHours(Common.GetDefaultSearchDurationHours(true)).ToString();
            datetimeStart.DbSelectedDate = dtLocalNow.AddHours(-12);
        }

        protected string HoursAndMinutes(string value)
        {
            return Common.HoursAndMinutes(value);
        }

        protected DateTime LocalDateTime(DateTime value)
        {
            return Common.LocalDateTime(value);
        }

        public static DateTime LocalToUTC(DateTime value)
        {
            return Common.LocalToUTC(value);
        }

        #endregion

        #region excelExport

        private DataTable formatExportSessions(DataTable dtIn)
        { //to force export to dump what is actually shown. DC 8/15/13   
            //- null table checked before call  

            DataColumn dc = new DataColumn("SessLen");
            dc.DataType = typeof(string);
            dtIn.Columns.Add(dc);
            foreach (DataRow dr in dtIn.Rows)
            {
                var st = dr["StartDateUTC"].ToString();
                dr["StartDateUTC"] = LocalDateTime(Convert.ToDateTime(st)).ToString();
                var et = dr["EndDateUTC"].ToString();
                dr["EndDateUTC"] = LocalDateTime(Convert.ToDateTime(et)).ToString();
                var hm = dr["SessionLengthMinutes"].ToString();
                dr["SessLen"] = HoursAndMinutes(hm).ToString();
            }

            return dtIn;
        }


        private DataTable formatExport(DataTable dtIn)
        {//to force export to dump what is actually shown. DC 8/15/13
            //null table checked before call

            DataColumn dc = new DataColumn("Cap");
            dc.DataType = typeof(string);
            dtIn.Columns.Add(dc);

            foreach (DataRow dr in dtIn.Rows)
            {
                var cd = dr["CreatedDateUTC"].ToString();
                dr["CreatedDateUTC"] = LocalDateTime(Convert.ToDateTime(cd)).ToString();
                var ed = LocalDateTime(Convert.ToDateTime(dr["CreatedDateUTC"].ToString())).AddMinutes(10).ToString() ;
                dr["ExaminedDateUTC"] = LocalDateTime(Convert.ToDateTime(cd)).ToString();


                //var cap = dr["FullChargeCapacity"].ToString();
                //dr["Cap"] = GetPerformanceRating(Convert.ToInt32(cap)).ToString();

                //supressing change and function until the real AH for prefixes is 
                //decided from engineering. I do not want to guess. 
                var cap = dr["FullChargeCapacity"].ToString();
                var battSN = dr["BatterySerialNumber"].ToString();
                dr["Cap"] = Common.GetPerformanceRatingByDevice(Convert.ToInt32(cap), battSN).ToString();

                var mv = dr["MeasuredVoltage"].ToString();
                dr["MeasuredVoltage"] = Common.GetMeasuredVoltage(mv).ToString();
            }

            return dtIn;
        }


        private void ConfigureExport(RadGrid grid)
        {
            grid.ExportSettings.IgnorePaging = true;
            grid.ExportSettings.ExportOnlyData = true;
            grid.ExportSettings.OpenInNewWindow = true;
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            string fileName = string.Format("PulseConnection_SearchResults_{0}", DateTime.UtcNow.ToShortDateString());
            if (rgrdParent.Visible)
            {
                ConfigureExport(rgrdParent);
                rgrdParent.ExportSettings.FileName = fileName;
                rgrdParent.MasterTableView.ExportToExcel();
            }
            if (rgrdSessions.Visible)
            {
                ConfigureExport(rgrdSessions);
                rgrdSessions.ExportSettings.FileName = fileName;
                rgrdSessions.MasterTableView.ExportToExcel();
            }
        }

        #endregion

        #region saveSearchLogs

        private void SaveToSearchLog()
        {
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString);
            //SqlCommand cmd = new SqlCommand("spSearchLog_Save", conn);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("SearchMode", "Advanced");
            //cmd.Parameters.AddWithValue("SearchValue", txtSearchColumn1.Text);
            //cmd.Parameters.AddWithValue("SiteID", Convert.ToInt32(ddlSites.SelectedValue));
            //cmd.Parameters.AddWithValue("StartDate", datetimeStart.SelectedDate.ToString());
            //cmd.Parameters.AddWithValue("EndDate", datetimeEnd.SelectedDate.ToString());
            //cmd.Parameters.AddWithValue("EquipmentType", ddlDeviceType.SelectedValue);
            //cmd.Parameters.AddWithValue("Source", ddlTransmissionType.SelectedValue);
            //cmd.Parameters.AddWithValue("SearchUserID", Convert.ToInt32(Session["UserID"].ToString()));
            //try
            //{
            //    conn.Open();
            //    cmd.ExecuteNonQuery();
            //}
            //catch (Exception ex)
            //{
            //  //  //lblSQLFeedback.Text = ex.ToString();
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        private void SaveSearchHistory(string SQLStatement, SqlConnection conn)
        {
            /*
             *  Not using this yet because we need to store the value of each element 
             *  (since other routines reference controls like rblDeviceType.SelectedValue, etc)
             * 
             */
            string UserID = PulseWeb.API.api.SessionUser.Id.ToString();
            string SiteID = Session["SiteID"].ToString();
            string SQLInsert = string.Format("insert into SearchHistory (UserID, SiteID, SQLStatement) values ({0}, {1}, '{2}')", UserID, SiteID, SQLStatement);
            SqlCommand cmd = new SqlCommand(SQLInsert, conn);
            cmd.ExecuteNonQuery();
        }

        #endregion

        #region persistantSettings

        protected void btnRemoveCustomization_Click(object sender, EventArgs e)
        {
            RadGrid ActiveGrid = new RadGrid();
            string settingkey = GetGridSettingKey();

            if (rgrdParent.Visible == true)
            {
                ActiveGrid = rgrdParent;
            }
            else if (rgrdSessions.Visible == true)
            {
                foreach (GridDataItem item in rgrdSessions.MasterTableView.Items)
                {
                    if (item.Expanded)
                    {
                        ActiveGrid = (RadGrid)Common.FindControlRecursively(item.ChildItem, "rgrdChild");
                        break;
                    }
                }
            }

            if (ActiveGrid.ID != null)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("delete from UserSettings where User_ROW_ID = " + PulseWeb.API.api.SessionUser.Id.ToString() + " and SettingKey = '" + settingkey + "'", conn);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                string SessionKey = "PersistedSettingsLoaded_" + ActiveGrid.ID;
                Session[SessionKey] = "False";
                RadGridCommon.ConfigureColumns(ActiveGrid, ddlDeviceType.SelectedValue);
                btnRemoveCustomization.Visible = false;
            }
        }

        protected void btnPersistSettings_Click(object sender, EventArgs e)
        {
            //Assume same settings should apply to either/all grids which show sessiondata and/or nonsessiondata
            RadGrid ActiveGrid = new RadGrid();
            string settingkey = GetGridSettingKey();

            if (rgrdParent.Visible == true)
            {
                SaveGridSettings(rgrdParent, settingkey);
            }
            else if (rgrdSessions.Visible == true)
            {
                SaveGridSettings(rgrdSessions, settingkey);
                foreach (GridDataItem item in rgrdSessions.MasterTableView.Items)
                {
                    if (item.Expanded)
                    {
                        ActiveGrid = (RadGrid)Common.FindControlRecursively(item.ChildItem, "rgrdChild");
                        if (ActiveGrid.ID != null)
                        {
                            SaveGridSettings(ActiveGrid, settingkey.Replace("_session_", "_sessiondetail_"));
                            break;
                        }
                    }
                }
            }
        }

        protected void SaveGridSettings(RadGrid grid, string SettingKey)
        {
            GridSettingsPersister SavePersister = new GridSettingsPersister(grid);
            string CustomSettings = SavePersister.SaveSettings();

            // save to database with UserID on UserSettings table with SettingsKey of "radgrid_search"
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("spUserSettings_Save", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("User_ROW_ID", PulseWeb.API.api.SessionUser.Id.ToString());
            cmd.Parameters.AddWithValue("SettingKey", SettingKey);
            cmd.Parameters.AddWithValue("SettingValue", CustomSettings.Replace("'", "''"));
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ////lblSQLFeedback.Text = ex.ToString();
            }
            finally
            {
                conn.Close();
            }

            btnRemoveCustomization.Visible = true;
        }

        protected void LoadGridSettings(RadGrid grid, string SettingKey)
        {
            string SessionKey = "PersistedSettingsLoaded_" + grid.ID;
            GridSettingsPersister LoadPersister = new GridSettingsPersister(grid);
            DataTable dt = Common.GetDataTable("select SettingValue from UserSettings where User_ROW_ID = " + PulseWeb.API.api.SessionUser.Id.ToString() + " and SettingKey = '" + SettingKey + "'");
            if (dt.Rows.Count == 1)
            {
                DataRow TheRow = (DataRow)dt.Rows[0];
                string settings = TheRow["SettingValue"].ToString();
                //LoadPersister.LoadSettings(settings);
                btnRemoveCustomization.Visible = true;
            }
            else
            {
                RadGridCommon.ConfigureColumns(grid, ddlDeviceType.SelectedValue);
            }
        }

        private void ResetGridSettings()
        {
            Session["devicetransmissionsearch_cmdText"] = null;
            Session["PersistedSettingsLoaded_rgrdChild"] = "False";
            Session["PersistedSettingsLoaded_rgrdParent"] = "False";
            Session["PersistedSettingsLoaded_rgrdSessions"] = "False";
            Session["devicetransmissionsearch_cmdText_Session"] = null;
        }

        private string GetGridSettingKey()
        {
            string DeviceCategory = ddlDeviceType.SelectedItem.Text;
            if (hidSource.Value != "Session")
            {
                return "radgrid_search_" + hidEquipmentType.Value;
            }
            else
            {
                return "radgrid_search_session_" + hidEquipmentType.Value;
            }
        }

        #endregion

        #endregion

        #region buildingSQLStmt

        public string BuildSearchCmdText()
        {
            // TO SPEED UP THE RESULTS...
            // if dates are the only restrictive criteria - OR - if there are no restrictive criteria at all:
            // then pull up to 1000 rows from each table, combine them, sorted by date, then display the most recent 1000 of the combined results
            //// BUT - if other criteria are applied, pull all matching rows from each table, combine them, sorted by date, then display the most recent 1000 rows
            //    [ 5-6-2011 db ]

            // new logic plugged in 10-11-2011 by db
            bool AutoRecognizeSearchColumn = true;
            string SearchColumn1 = string.Empty;
            if ((txtSearchColumn1.Text.Trim() != string.Empty) & (AutoRecognizeSearchColumn == true))
            {
                SearchColumn1 = ResolveSearchColumn(txtSearchColumn1.Text.Trim());
            }

            string SQLSelect = string.Empty;

            string SQLDropTempTables = "IF OBJECT_ID(N'tempdb..#tempResultsA', N'U') IS NOT NULL begin drop table #tempResultsA end; IF OBJECT_ID(N'tempdb..#tempResultsB', N'U') IS NOT NULL begin drop table #tempResultsB end; ";
            string SQLSelectFromTempTables = "select top 1000 * from (select * from #tempResultsA (NOLOCK) union select * from #tempResultsB (NOLOCK)) alldata ";
            string SQLWhere = " WHERE ";
            string SQLOrderBy = " ORDER BY CreatedDateUTC desc";

            if (ddlSite.SelectedValue != null & ddlSite.SelectedValue != "0" & ddlSite.SelectedValue != "")
            {
                SQLWhere += " (bu.IDSite = " + ddlSite.SelectedValue + ") ";
            }
            else
            {
               // ddlSite.SelectedValue = ddlSite.Items[0].Value;
                SQLWhere += " (bu.IDSite in (select [User].IDSite from [User] (NOLOCK) where IDUser = " +  PulseWeb.API.api.SessionUser.Id.ToString() + ") or d.SiteID  in (select IDSite as IDCustomer from Sites (NOLOCK) where CustomerID = " +  PulseWeb.API.api.SessionUser.ParentCustomer.Id.ToString() + " )) ";
                SQLWhere = SQLWhere.Replace("@UserID", PulseWeb.API.api.SessionUser.Id.ToString());
                SQLWhere = SQLWhere.Replace("@UserIsStinger", "False");
            }


            //if (Convert.ToBoolean(Session["IsStinger"].ToString()) == false)
            //{
                SQLWhere += " AND (sd.Activity <> 10) ";
            //}

            string startDate = string.Empty;
            string endDate = string.Empty;

            DateTime dtStartDate;
            DateTime dtEndDate;


            if (datetimeStart.DateInput.Text != String.Empty)
            {
                startDate = datetimeStart.SelectedDate.Value.ToString();
            }
            if (datetimeEnd.DateInput.Text != String.Empty)
            {
                endDate = datetimeEnd.SelectedDate.Value.ToString();
            }

            if (startDate != string.Empty && endDate != string.Empty)
            {
                dtStartDate = Convert.ToDateTime(startDate);
                dtEndDate = Convert.ToDateTime(endDate);
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (sd.CreatedDateUTC between '" + LocalToUTC(dtStartDate) + "' and '" + LocalToUTC(dtEndDate) + "') ";
            }

            if (startDate == string.Empty && endDate != string.Empty)
            {
                dtEndDate = Convert.ToDateTime(endDate);
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (sd.CreatedDateUTC <= '" + LocalToUTC(dtEndDate) + "') ";
            }

            if (startDate != string.Empty && endDate == string.Empty)
            {
                dtStartDate = Convert.ToDateTime(startDate);
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (sd.CreatedDateUTC >= '" + LocalToUTC(dtStartDate) + "') ";
            }

            if (SearchColumn1 != string.Empty)
            {
                if (SearchColumn1 != "IP")
                {
                    SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (" + SearchColumn1 + " = '" + txtSearchColumn1.Text.Trim() + "') ";

                }
                else
                {
                    // if the value is recognized as an IP address, we need to search two columns: IP and SourceIPAddress
                    SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " ((" + SearchColumn1 + " = '" + txtSearchColumn1.Text.Trim() + "') OR (SourceIPAddress = '" + txtSearchColumn1.Text.Trim() + "')) ";
                }
            }
            else
            {
                if (txtSearchColumn1.Text.Trim() != string.Empty)
                {
                    //SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (" + ddlSearchColumn1.SelectedValue + " = '" + txtSearchColumn1.Text.Trim() + "') ";
                }
            }
            // end of 10-11-2011 modification

            //Adding Search Criteria Department,Floor,Wing,and Other DC 11/30/2012
            SQLWhere = AppendItemsDeptWFOAToSQL(SQLWhere);


            if (ddlDeviceType.SelectedValue == "Workstation")
            {
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (DeviceType in (select IDAssetType from AssetType where Category = 'Workstation')) ";
            }
            else if (ddlDeviceType.SelectedValue == "Charger")
            {
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (DeviceType in (select IDAssetType from AssetType where Category = 'Charger')) ";
            }

            // can limit subsquery select statements, since they are being inserted into temp tables
            string SQLROWLIMIT = "TOP 500";

            // added SiteDescription columns for: Radar, Owner and AccessPoint 2012-08-15 Danny Bates

            string SQLSelect_Session = "select " + SQLROWLIMIT + " case when SessionID = 0 then 'Active' else 'Complete' end as Status, dt.Description as DeviceTypeDescription, bu.SiteDescription, a.ActivityDescription, ap.Description as AccessPointDescription, sd.CreatedDateUTC, sd.DeviceSerialNumber,d.Notes as Notes, d.AssetNumber, d.Floor, d.Wing, d.Other, sd.BatterySerialNumber, sd.DeviceType, sd.Activity, sd.Bay, sd.FullChargeCapacity, sd.Voltage, sd.Amps, sd.Temperature, sd.ChargeLevel, sd.CycleCount, sd.MaxCycleCount, sd.VoltageCell1, sd.VoltageCell2, sd.VoltageCell3, sd.FETStatus, sd.RemainingTime, sd.BatteryName, sd.Efficiency, sd.ControlBoardRevision, sd.LCDRevision, sd.HolsterChargerRevision, sd.DCBoardOneRevision, sd.DCBoardTwoRevision, sd.WifiFirmwareRevision, sd.BayWirelessRevision, sd.Bay1ChargerRevision, sd.Bay2ChargerRevision, sd.Bay3ChargerRevision, sd.Bay4ChargerRevision, sd.MedBoardRevision, sd.BackupBatteryVoltage, sd.BackupBatteryStatus, sd.IsAC, sd.DCUnit1AVolts, sd.DCUnit1ACurrent, sd.DCUnit1BVolts, sd.DCUnit1BCurrent, sd.DCUnit2AVolts, sd.DCUnit2ACurrent, sd.DCUnit2BVolts, sd.DCUnit2BCurrent, sd.XValue, sd.XMax, sd.YValue, sd.YMax, sd.ZValue, sd.ZMax, sd.Move, sd.BatteryStatus, sd.SafetyStatus, sd.PermanentFailureStatus, sd.PermanentFailureAlert, sd.BatteryChargeStatus, sd.BatterySafetyAlert, sd.BatteryOpStatus, sd.BatteryMode, sd.DC1Error, sd.DC1Status, sd.DC2Error, sd.DC2Status, sd.MouseFailureNotification, sd.KeyboardFailureNotification, sd.WindowsShutdownNotification, sd.LinkQuality, sd.IP, sd.DeviceMAC, sd.APMAC, sd.WEPKey, sd.PassCode, sd.SSID, sd.ControlBoardSerialNumber, sd.HolsterBoardSerialNumber, sd.LCDBoardSerialNumber, sd.DC1BoardSerialNumber, sd.DC2BoardSerialNumber, sd.MedBoardSerialNumber, sd.BayWirelessBoardSerialNumber, sd.Bay1BoardSerialNumber, sd.Bay2BoardSerialNumber, sd.Bay3BoardSerialNumber, sd.Bay4BoardSerialNumber, sd.MedBoardDrawerOpenTime, sd.MedBoardDrawerCount, sd.MedBoardMotorUp, sd.MedBoardMotorDown, sd.MedBoardUnlockCount, sd.MedBoardAdminPin, sd.MedBoardNarcPin, sd.MedBoardUserPin1, sd.MedBoardUserPin2, sd.MedBoardUserPin3, sd.MedBoardUserPin4, sd.MedBoardUserPin5, sd.GenericError, sd.SessionRecordType, sd.ExaminedDateUTC, sd.QueryStringID, sd.SessionID, sd.CommandCode, bub.BUBStatusDescription, sd.ROW_ID, aio.ProductSerialNumber as AIOSerialNumber, dcmonitor.ProductSerialNumber as DCMonitorSerialNumber, cart.ProductSerialNumber as CartSerialNumber, bud.Description as Department, sd.SourceIPAddress, sd.SparePinSwitch, buRadar.SiteDescription as SiteDescription_Radar, buOwner.SiteDescription as SiteDescription_Owner, buAccessPoint.SiteDescription as SiteDescription_AP, coalesce(sd.MeasuredVoltage, 0) as MeasuredVoltage, isnull(sd.BatteryErrorCode, 0) as BatteryErrorCode REPLACE_FROM_COMMAND SessionDataCurrent (NOLOCK) sd join Assets (NOLOCK) d on sd.DeviceSerialNumber = d.SerialNo left join Customers (NOLOCK) buC on d.SiteID = buc.IDCustomer left join Departments bud on d.DepartmentID = bud.IDDepartment left join Sites (NOLOCK) bu on d.SiteID = bu.IDSite left join AssetType (NOLOCK) dt on sd.DeviceType = dt.IDAssetType left join vwActivity (NOLOCK) a on sd.Activity = a.ActivityCode left join AccessPoint (NOLOCK) ap on sd.APMAC = ap.MACAddress left join BUBStatus (NOLOCK) bub on d.IDAssetType = bub.AssetTypeID and sd.BackupBatteryStatus = bub.BUBStatusCode and sd.ControlBoardRevision between bub.RevisionLow and bub.RevisionHigh left join vwProductAIO (NOLOCK) aio on d.SerialNo = aio.DeviceSerialNo left join vwProductDCMonitor (NOLOCK) dcmonitor on d.SerialNo = dcmonitor.DeviceSerialNo left join vwProductCart (NOLOCK) cart on d.SerialNo = cart.DeviceSerialNo LEFT JOIN Sites buRadar on d.siteid = buRadar.IDSite LEFT JOIN Sites buOwner on d.SiteID = buOwner.IDSite left join Sites buAccessPoint on ap.SiteID = buAccessPoint.IDSite ";
            string SQLSelect_NonSession = "select " + SQLROWLIMIT + " 'Non-session' as Status, dt.Description as DeviceTypeDescription, bu.SiteDescription, a.ActivityDescription, ap.Description as AccessPointDescription, sd.CreatedDateUTC, sd.DeviceSerialNumber,d.Notes as Notes, d.AssetNumber, d.Floor as Floor, d.Wing as Wing, d.Other as Other, sd.BatterySerialNumber, sd.DeviceType, sd.Activity, sd.Bay, sd.FullChargeCapacity, sd.Voltage, sd.Amps, sd.Temperature, sd.ChargeLevel, sd.CycleCount, sd.MaxCycleCount, sd.VoltageCell1, sd.VoltageCell2, sd.VoltageCell3, sd.FETStatus, sd.RemainingTime, sd.BatteryName, sd.Efficiency, sd.ControlBoardRevision, sd.LCDRevision, sd.HolsterChargerRevision, sd.DCBoardOneRevision, sd.DCBoardTwoRevision, sd.WifiFirmwareRevision, sd.BayWirelessRevision, sd.Bay1ChargerRevision, sd.Bay2ChargerRevision, sd.Bay3ChargerRevision, sd.Bay4ChargerRevision, sd.MedBoardRevision, sd.BackupBatteryVoltage, sd.BackupBatteryStatus, sd.IsAC, sd.DCUnit1AVolts, sd.DCUnit1ACurrent, sd.DCUnit1BVolts, sd.DCUnit1BCurrent, sd.DCUnit2AVolts, sd.DCUnit2ACurrent, sd.DCUnit2BVolts, sd.DCUnit2BCurrent, sd.XValue, sd.XMax, sd.YValue, sd.YMax, sd.ZValue, sd.ZMax, sd.Move, sd.BatteryStatus, sd.SafetyStatus, sd.PermanentFailureStatus, sd.PermanentFailureAlert, sd.BatteryChargeStatus, sd.BatterySafetyAlert, sd.BatteryOpStatus, sd.BatteryMode, sd.DC1Error, sd.DC1Status, sd.DC2Error, sd.DC2Status, sd.MouseFailureNotification, sd.KeyboardFailureNotification, sd.WindowsShutdownNotification, sd.LinkQuality, sd.IP, sd.DeviceMAC, sd.APMAC, sd.WEPKey, sd.PassCode, sd.SSID, sd.ControlBoardSerialNumber, sd.HolsterBoardSerialNumber, sd.LCDBoardSerialNumber, sd.DC1BoardSerialNumber, sd.DC2BoardSerialNumber, sd.MedBoardSerialNumber, sd.BayWirelessBoardSerialNumber, sd.Bay1BoardSerialNumber, sd.Bay2BoardSerialNumber, sd.Bay3BoardSerialNumber, sd.Bay4BoardSerialNumber, sd.MedBoardDrawerOpenTime, sd.MedBoardDrawerCount, sd.MedBoardMotorUp, sd.MedBoardMotorDown, sd.MedBoardUnlockCount, sd.MedBoardAdminPin, sd.MedBoardNarcPin, sd.MedBoardUserPin1, sd.MedBoardUserPin2, sd.MedBoardUserPin3, sd.MedBoardUserPin4, sd.MedBoardUserPin5, sd.GenericError, sd.SessionRecordType, sd.ExaminedDateUTC, sd.QueryStringID, sd.SessionID, sd.CommandCode, bub.BUBStatusDescription, sd.ROW_ID, aio.ProductSerialNumber as AIOSerialNumber, dcmonitor.ProductSerialNumber as DCMonitorSerialNumber, cart.ProductSerialNumber as CartSerialNumber, bud.Description as Department, sd.SourceIPAddress, sd.SparePinSwitch, buRadar.SiteDescription as SiteDescription_Radar, buOwner.SiteDescription as SiteDescription_Owner, buAccessPoint.SiteDescription as SiteDescription_AP, coalesce(sd.MeasuredVoltage, 0) as MeasuredVoltage, isnull(sd.BatteryErrorCode, 0) as BatteryErrorCode REPLACE_FROM_COMMAND NonSessionDataCurrent (NOLOCK) sd join Assets (NOLOCK) d on sd.DeviceSerialNumber = d.SerialNo left join Customers (NOLOCK) buC on d.SiteID = buc.IDCustomer left join Departments bud on d.DepartmentID = bud.IDDepartment left join Sites (NOLOCK) bu on d.SiteID = bu.IDSite left join AssetType (NOLOCK) dt on sd.DeviceType = dt.IDAssetType left join vwActivity (NOLOCK) a on sd.Activity = a.ActivityCode left join AccessPoint (NOLOCK) ap on sd.APMAC = ap.MACAddress left join BUBStatus (NOLOCK) bub on d.IDAssetType = bub.AssetTypeID and sd.BackupBatteryStatus = bub.BUBStatusCode and sd.ControlBoardRevision between bub.RevisionLow and bub.RevisionHigh left join vwProductAIO (NOLOCK) aio on d.SerialNo = aio.DeviceSerialNo left join vwProductDCMonitor dcmonitor (NOLOCK) on d.SerialNo = dcmonitor.DeviceSerialNo left join vwProductCart (NOLOCK) cart on d.SerialNo = cart.DeviceSerialNo LEFT JOIN Sites buRadar on d.siteid = buRadar.IDSite LEFT JOIN Sites buOwner on d.SiteID = buOwner.IDSite left join Sites buAccessPoint on ap.SiteID = buAccessPoint.IDSite ";

            if (ddlTransmissionType.SelectedValue == "Session")
            {
                SQLSelect_Session = "select " + SQLROWLIMIT + " s.ROW_ID, StartDate, EndDate, DeviceType, StartChargeLevel,EndChargeLevel, dt.Description as DeviceTypeDescription, DeviceSerialNumber, BatterySerialNumber, AvgAmpDraw, NumberOfMoves, SessionLengthMinutes, BatteryName, SiteID, bu.SiteDescription, PacketCount from Sessions (NOLOCK) s left join Sites (NOLOCK) bu on s.SiteID = bu.IDSite left join AssetType (NOLOCK) dt on s.DeviceType = dt.IDAssetType ";
                SQLOrderBy = " ORDER BY EndDateUTC desc";
            }


            if (SQLWhere != " WHERE ")  // only proceed if we have search criteria
            {
                if (ddlTransmissionType.SelectedValue == "All")
                {
                    SQLSelect_Session = SQLSelect_Session.Replace("REPLACE_FROM_COMMAND", "INTO #tempResultsA FROM");
                    SQLSelect_NonSession = SQLSelect_NonSession.Replace("REPLACE_FROM_COMMAND", "INTO #tempResultsB FROM");
                    SQLSelect = SQLDropTempTables + " " + SQLSelect_Session + SQLWhere + SQLOrderBy + "; " + SQLSelect_NonSession + SQLWhere + SQLOrderBy + "; " + SQLSelectFromTempTables;
                }
                else if (ddlTransmissionType.SelectedValue == "NonSession")
                {
                    SQLSelect = SQLSelect_NonSession.Replace("REPLACE_FROM_COMMAND", "from") + SQLWhere;
                }

                return SQLSelect + SQLOrderBy;

            }
            else
            {
                return string.Empty;
            }
        }

        public string BuildSessionSearchCmdText()
        {
            // for session data only (since sessions are rolled up, they display different columns, and they are expandable to show the session detail rows)

            string SQLSelect = string.Empty;
            string SQLSelect_Session = "select top 1000 sd.ROW_ID, StartDateUTC, EndDateUTC, DeviceType, dt.Description as DeviceTypeDescription, sd.DeviceSerialNumber,  d.Notes as Notes, d.AssetNumber, d.Wing, d.Floor, d.Other, BatterySerialNumber, AvgAmpDraw, NumberOfMoves, SessionLengthMinutes, BatteryName, sd.SiteID, bu.SiteDescription, PacketCount, StartChargeLevel, EndChargeLevel, aio.ProductSerialNumber as AIOSerialNumber, dcmonitor.ProductSerialNumber as DCMonitorSerialNumber, cart.ProductSerialNumber as CartSerialNumber, bud.Description as Department from Sessions (NOLOCK) sd left join Assets (NOLOCK) d on sd.DeviceSerialNumber = d.SerialNo  left join Sites (NOLOCK) bu on d.SiteID = bu.IDSite left join Customers (NOLOCK) buC on d.SiteID = buc.IDCustomer left join AssetType (NOLOCK) dt on sd.DeviceType = dt.IDAssetType left join Departments bud on d.DepartmentID = bud.IDDepartment left join vwProductAIO (NOLOCK) aio on d.SerialNo = aio.DeviceSerialNo left join vwProductDCMonitor (NOLOCK) dcmonitor on d.SerialNo = dcmonitor.DeviceSerialNo left join vwProductCart (NOLOCK) cart on d.SerialNo = cart.DeviceSerialNo ";
            string SQLOrderBy = " ORDER BY EndDateUTC desc";
            string SQLWhere = " WHERE ";
            bool BothDatesSupplied = false; // logic varies when both a from and to date are supplied

            if (ddlSite.SelectedValue != null & ddlSite.SelectedValue != "0" & ddlSite.SelectedValue != "")
            {
               SQLWhere += " (sd.SiteID = " + ddlSite.SelectedValue + ") ";
            }
            else
            {
                if (Session["IsStinger"].ToString().ToLower() == "false")
                {
                    if (Session["CustomerID"].ToString() == "0" && ddlSite.SelectedValue == "0")
                    {
                        // user is not with Stinger AND they are with an IDN / HQ, so they should see results for all facilities associated with their HQ
                        SQLWhere += " (bu.IDSite in (select SiteID from [User] (NOLOCK)  where IDUser = @UserID) or buc.IDCustomer  in (select SiteID from [User] (NOLOCK)  where IDUser = @UserID)) ";
                    }
                    else
                    {
                        // user is not with Stinger. This line probably will never execute because single selections are accommodated above.
                        SQLWhere += " (bu.IDSite in (select SiteID from [User] (NOLOCK)  where IDUser = @UserID) or buc.IDCustomer  in (select SiteID from [User] (NOLOCK)  where IDUser = @UserID) OR bu.IDSite in (select distinct IDSite from Sites (NOLOCK) WHERE '@UserIsStinger' = 'True')) ";
                    }
                }
                else
                {
                    SQLWhere += " (bu.IDSite in (select SiteID from [User] (NOLOCK)  where IDUser = @UserID) or buC.IDCustomer  in (select SiteID from [User] (NOLOCK)  where IDUser = @UserID) OR bu.IDSite in (select distinct IDSite from Sites (NOLOCK) WHERE '@UserIsStinger' = 'True')) ";
                }

                SQLWhere = SQLWhere.Replace("@UserID", PulseWeb.API.api.SessionUser.Id.ToString());
             //   SQLWhere = SQLWhere.Replace("@UserIsStinger", Session["IsStinger"].ToString());
            }

            string startDate = string.Empty;
            string endDate = string.Empty;
            DateTime dtStartDate;
            DateTime dtEndDate;
            string SQLDateCriteria = string.Empty;

            if (datetimeStart.DateInput.Text != String.Empty)
            {
                startDate = datetimeStart.SelectedDate.Value.ToString();
            }

            if (datetimeEnd.DateInput.Text != String.Empty)
            {
                endDate = datetimeEnd.SelectedDate.Value.ToString();
                BothDatesSupplied = startDate != string.Empty;
            }

            if (BothDatesSupplied)
            {
                dtStartDate = Convert.ToDateTime(startDate);
                dtEndDate = Convert.ToDateTime(endDate);

                SQLDateCriteria = " ((('" + LocalToUTC(dtStartDate) + "' between StartDateUTC and EndDateUTC) OR ('" + LocalToUTC(dtEndDate) + "' between StartDateUTC and EndDateUTC)) ";
                SQLDateCriteria += "OR ((StartDateUTC between '" + LocalToUTC(dtStartDate) + "' and '" + LocalToUTC(dtEndDate) + "') OR (EndDateUTC between '" + LocalToUTC(dtStartDate) + "' and '" + LocalToUTC(dtEndDate) + "'))) ";
            }
            else
            {
                if (startDate != string.Empty || endDate != string.Empty)
                {
                    if (startDate != string.Empty)
                    {
                        dtStartDate = Convert.ToDateTime(startDate);

                        //if Session.StartDate >= from date
                        //OR Session.EndDate >= from date
                        SQLDateCriteria = "((StartDateUTC >= '" + LocalToUTC(dtStartDate) + "') OR (EndDateUTC >= '" + LocalToUTC(dtStartDate) + "')) ";
                    }
                    else
                    {
                        dtEndDate = Convert.ToDateTime(endDate);

                        //if Session.StartDate <= to date
                        //OR Session.EndDate <= to date
                        SQLDateCriteria = "((StartDateUTC <= '" + LocalToUTC(dtEndDate) + "') OR (EndDateUTC <= '" + LocalToUTC(dtEndDate) + "')) ";
                    }
                }
            }

            if (SQLDateCriteria != string.Empty)
            {
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " " + SQLDateCriteria;
            }

            if (txtSearchColumn1.Text.Trim() != string.Empty)
            {
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " ((sd.DeviceSerialNumber = '" + txtSearchColumn1.Text.Trim() + "') OR (BatterySerialNumber = '" + txtSearchColumn1.Text.Trim() + "')) ";
            }


            if (ddlDeviceType.SelectedValue == "Workstation")
            {
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (DeviceType in (select IDAssetType from AssetType where Category = 'Workstation')) ";
            }
            else if (ddlDeviceType.SelectedValue == "Charger")
            {
                SQLWhere += AppendLogicalOperator(SQLWhere, " AND ") + " (DeviceType in (select IDAssetType from AssetType where Category = 'Charger')) ";
            }

            //Adding Search Criteria Department,Floor,Wing,and Other DC 11/30/2012
            SQLWhere = AppendItemsDeptWFOAToSQL(SQLWhere);


            if (SQLWhere != " WHERE ")
            {
                return SQLSelect_Session + SQLWhere + SQLOrderBy;
            }
            else
            {
                return string.Empty;
            }
        }

        public string ResolveSearchColumn(string searchvalue)
        {
            string result = string.Empty;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
            {
                conn.Open();
                //Okay...spSearch_IdentifySearchValue checks initial formats like whether %.%.%.% or '':'':'':'': etc... first off
                //if that doens't catch, if the varchar(30) is numeric, it's a serial number, anything else is asset. 
                SqlCommand cmd = new SqlCommand("spSearch_IdentifySearchValue", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("searchvalue", searchvalue);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    result = dr["ColumnName"].ToString();
                }
                dr = null;
                conn.Close();
            }
            return result;
            //from the db a ColumnName is returned, whether...
            //sd.DeviceMAC 
            //APMAC from AccessPoint
            //**defaults to APMAC if not found in the db
            //IP may be returned
            //sd.DeviceSerialNumber 
            //sd.BatterySerialNumber 
            //or AssetNumber
        }

        private string getPostBackControlName()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control.ID;
        }

        protected string AppendLogicalOperator(string SQLWhere, string LogicalOperator)
        {
            if (SQLWhere == " WHERE ")
            {
                return string.Empty;
            }
            else
            {
                return LogicalOperator;
            }
        }

        #endregion

        #region Department,Wing,Floor,Other,All_Search - 

        protected void ExecuteDeptWFOA()
        {
            //reset the strings
            string strDeptWFOA = string.Empty;
            string strQuery = string.Empty;

            //create the datatable
            DataTable dtDeptWFOA = new DataTable();

            //was the search D W F O or A?
            strDeptWFOA = FindDeptWFOA();

            //fill the query by what was chosen            
            strQuery = BuildWFOAList(strDeptWFOA);

            //fill the datatable by the query
            dtDeptWFOA = FillDeptWFOA(strQuery);

            //add the items to the UI as choices
            AddItemsDeptWFOA(dtDeptWFOA);
        }

        protected string FindDeptWFOA()
        {
            lblFeedback.Text = string.Empty;
            string strDeptWFOA = string.Empty;

            switch (ddlDeptWFOA.SelectedIndex)
            {
                case 1:
                    strDeptWFOA = "Dept";
                    break;
                case 2:
                    strDeptWFOA = "Wing";
                    break;
                case 3:
                    strDeptWFOA = "Floor";
                    break;
                case 4:
                    strDeptWFOA = "Other";
                    break;
                default:
                    if (ddlDeptWFOA.SelectedValue.ToUpper() != "CHOOSE")
                    {
                        ShowMessage("Your search returned no results");
                    }
                    break;
            }
            return strDeptWFOA;
        }

        protected string BuildWFOAList(string strDeptWFOAIn)
        {
            string strQuery = string.Empty;
            if (strDeptWFOAIn != string.Empty)
            {
                if (strDeptWFOAIn == "Dept")
                {//if department
                    strQuery = string.Format("SELECT distinct [Description] FROM Departments where [Description] <> '' and IDDepartment in (select distinct DepartmentID from Assets where SiteID = {0})", ddlSite.SelectedValue);
                }
                else
                {//if wfo = wing, floor, other
                    strQuery = string.Format("Select distinct[{0}] From Assets Where SiteID = {1} and {2} <> ''", strDeptWFOAIn, ddlSite.SelectedValue, strDeptWFOAIn);
                }
            }
            return strQuery;
        }

        protected DataTable FillDeptWFOA(string strQueryIn)
        {
            DataTable dt = new DataTable();

            if (strQueryIn != String.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = new SqlCommand(strQueryIn, conn);
                    try
                    {
                        conn.Open();
                        da.Fill(dt);
                    }
                    catch (Exception x)
                    {
                        //lblSQLFeedback.Text = x.Message.ToString();
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        protected void AddItemsDeptWFOA(DataTable dtIn)
        {
            if (dtIn.Rows.Count > 0)
            {
                chkSelect.Visible = true;
                int type = ddlDeptWFOA.SelectedIndex;
                //Key type: (All of the below is default)
                //1 - Department
                //2 - Wing
                //3 - Floor
                //4 - Other               

                switch (type)
                {
                    case 1:
                        cblDeptWFOA.DataTextField = "Description";
                        cblDeptWFOA.DataValueField = "Description";
                        break;
                    case 2:
                        cblDeptWFOA.DataTextField = "Wing";
                        cblDeptWFOA.DataValueField = "Wing";
                        break;
                    case 3:
                        cblDeptWFOA.DataTextField = "Floor";
                        cblDeptWFOA.DataValueField = "Floor";
                        break;
                    case 4:
                        cblDeptWFOA.DataTextField = "Other";
                        cblDeptWFOA.DataValueField = "Other";
                        break;
                    default:
                        break;
                }

                if (type <= 4)//all other unforseen stragglers...
                {
                    cblDeptWFOA.DataSource = dtIn;
                    cblDeptWFOA.DataBind();
                    cblDeptWFOA.Visible = true;
                }

                //for (int i = cblDeptWFOA.Items.Count - 1; i >= 0; i--)
                //{
                //    if (cblDeptWFOA.Items[i].Value.ToUpper() == "UNALLOCATED")
                //    {
                //        cblDeptWFOA.Items.RemoveAt(i);
                //    }
                //}

                if (cblDeptWFOA.Items.Count > 0)
                {
                    foreach (System.Web.UI.WebControls.ListItem li in cblDeptWFOA.Items)
                    {
                        li.Selected = true;
                    }
                }
            }
            else
            {
                ClearDept();
            }
        }

        protected string AppendItemsDeptWFOAToSQL(string SQLWhereIn)
        {
            //this function appends the sql query with choices of location details
            string a = string.Empty;
            string b = SQLWhereIn.Substring(0, 6);
            string c = SQLWhereIn.Substring(7, SQLWhereIn.Length - 7);

            if ((ddlDeptWFOA.SelectedIndex != 0) && (cblDeptWFOA.SelectedItem != null))
            {
                string d = string.Empty;
                string strColumn = GetTypeDeptWFOA();
                foreach (System.Web.UI.WebControls.ListItem li in cblDeptWFOA.Items)
                {
                    if (li.Selected)
                    {
                        a = d == string.Empty ? a = string.Format(" ({0} ", c) : a += string.Format(" OR ({0} ", c);
                        a += string.Format(" AND ( {0} ) IN ('{1}'))", strColumn, li.Text.Trim().Replace("'", "''"));
                        d = "d";
                    }
                }
                return b + a;
            }
            else
            {
                return SQLWhereIn;
            }
        }

        protected string GetTypeDeptWFOA()
        {
            if (ddlDeptWFOA.SelectedValue == "Department(s)")
            {
                return " bud.Description ";
            }
            else if (ddlDeptWFOA.SelectedValue == "Wing(s)")
            {
                return " d.Wing ";
            }
            else if (ddlDeptWFOA.SelectedValue == "Floor(s)")
            {
                return " d.Floor ";
            }
            else
            {
                return " d.Other ";
            }
        }

        #endregion

        #region pageEvents

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Valid())
            {
                DoSearch();
            }
        }

        protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowLocationInfo();
        }

        protected void ddlDeptWFOA_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowLocationInfo();
        }

        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            string set = chkSelect.Text.ToUpper();
            bool check = false;

            if (set == "UNSELECT ALL")
            {
                check = false;
                chkSelect.Text = "Select All";
            }
            else if (set == "SELECT ALL")
            {
                check = true;
                chkSelect.Text = "UnSelect All";
            }

            for (int i = 0; i < cblDeptWFOA.Items.Count; i++)
            {
                cblDeptWFOA.Items[i].Selected = check;
            }
            chkSelect.Checked = false;
        }

        #endregion

        #region TelerikControlPageEvents

        protected void rgrdSessions_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExpandCollapseCommandName && !e.Item.Expanded)
            {
                GridDataItem parentItem = e.Item as GridDataItem;
                try
                {
                     SearchClicked = true;
                    RadGrid rg = parentItem.ChildItem.FindControl("rgrdChild") as RadGrid;
                    rg.Visible = true;
                    rg.MasterTableView.Columns.FindByUniqueName("AssetNumber").Display = Convert.ToBoolean(Common.CookieOrElse("rgrdParent_Asset Number_Display", "false"));
                    rg.Rebind();
                }
                catch (Exception ex)
                {
                    ////lblSQLFeedback.Text = ex.Message.ToString();
                }
                 SearchClicked = false;
            }
        }

        protected void AnyRadGrid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            RadGrid thisgrid = (RadGrid)sender;
            bool IsStinger = Convert.ToBoolean(Session["IsStinger"]);
            //if (Session["UserID"].ToString() == "813")
            //{
            //    IsStinger = false;
            //}

            if ((e.Item.OwnerTableView as GridTableView).GroupByExpressions.Count == 0)
            {
                if (e.Item is GridPagerItem)
                {
                    RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                    PageSizeCombo.Items.Clear();
                    string[] arPageSizes = new string[] { "12", "24", "48", "96" };
                    for (int i = 0; i < arPageSizes.Length; i++)
                    {
                        RadComboBoxItem cbItem = new RadComboBoxItem(arPageSizes[i]);
                        PageSizeCombo.Items.Add(cbItem);
                        cbItem.Attributes.Add("ownerTableViewId", thisgrid.MasterTableView.ClientID);
                    }

                    try
                    {
                        PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;

                    }
                    catch { }
                }
            }


            if (thisgrid.ClientID.Contains("rgrdParent")) //rgrdParent contain these below
            {
                thisgrid.MasterTableView.Columns.FindByUniqueName("SiteDescription_Radar").Visible = IsStinger;
                thisgrid.MasterTableView.Columns.FindByUniqueName("SiteDescription_Owner").Visible = IsStinger;
                thisgrid.MasterTableView.Columns.FindByUniqueName("SiteDescription_AP").Visible = IsStinger;
                thisgrid.MasterTableView.Columns.FindByUniqueName("SourceIPAddress").Visible = IsStinger;

            }
            //rgrdParent & rgrdChild both contain these below. 
            thisgrid.MasterTableView.Columns.FindByUniqueName("CycleCount").Visible = IsStinger;
            thisgrid.MasterTableView.Columns.FindByUniqueName("MaxCycleCount").Visible = IsStinger;
            thisgrid.MasterTableView.Columns.FindByUniqueName("SparePinSwitch").Visible = IsStinger;
            thisgrid.MasterTableView.Columns.FindByUniqueName("BatteryErrorCode").Visible = IsStinger;

            //hard set to visible
            thisgrid.MasterTableView.Columns.FindByUniqueName("BackupBatteryVoltage").Visible = true;

        }

        //CALLS BUILD SEARCH COMMAND TEXT
        protected void rgrdParent_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (SearchClicked == false)
            {
               
                return;
            }
            SearchClicked = false;
            string cmdText = BuildSearchCmdText();
            if (cmdText != string.Empty)
            {
                LoadSearchSession(cmdText);

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = new SqlCommand(cmdText, conn);
                    DataTable dt = new DataTable();

                    //lblSQLFeedback.Text = string.Empty;
                    try
                    {
                        conn.Open();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            DataTable dtFormat = formatExport(dt);
                            rgrdParent.DataSource = dtFormat;
                            rgrdParent.Visible = true;
                            btnExportToExcel.Visible = false; //not using anymore. Left on page for the time being till new export has been thoroughly tested
                            btnExcelExport.Visible = true;
                        }

                        rgrdParent.DataSource = dt;
                        rgrdParent.Visible = true;
                        btnExportToExcel.Visible = false; //not using anymore. Left on page for the time being till new export has been thoroughly tested
                        btnExcelExport.Visible = dt.Rows.Count > 0;

                    }
                    catch (Exception x)
                    {
                        if (PulseSession.Instance.User.FirstName.Contains("Arlow")){
                       ShowMessage( x.Message.ToString());
                            }
                    }
                    finally
                    {
                        conn.Close();
                    }


                }
            }
        }

        void LoadSearchSession(string sql)
        {
            PulseSearch search = new PulseSearch();
            search.SqlCmdText = sql;

            RadGrid grid;
            if (ddlTransmissionType.SelectedValue.ToUpper() == "SESSION")
            {
                grid = rgrdSessions;
                search.DisplayType = DisplayType.Session;
            }
            else
            {
                grid = rgrdParent;
                search.DisplayType = DisplayType.All;
            }


            foreach (var uCol in grid.Columns)
            {
                var colType = uCol.GetType().ToString();

                dynamic col;

                if (colType.Contains("GridBoundColumn") || colType.Contains("GridTemplateColumn"))
                {

                    if (colType.Contains("GridTemplateColumn"))
                    { col = (GridTemplateColumn)uCol; }
                    else
                    { col = (GridBoundColumn)uCol; }

                    var cv = grid.MasterTableView.GetColumn(col.UniqueName);


                    if (col.Visible && col.Display && cv.OrderIndex > -1)
                    {
                        var c = new DisplayColumn();
                        c.DisplayName = col.HeaderText;
                        c.DataName = col.DataField;
                        c.DisplayOrder = cv.OrderIndex;
                        search.Columns.Add(c);
                    }
                }
            }

            Session["PulseSearchInfo"] = search;
        }

        //CALLS BUILD**SESSION**SEARCH COMMAND TEXT
        protected void rgrdSessions_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            string cmdText = BuildSessionSearchCmdText();
            if (cmdText != string.Empty)
            {
                LoadSearchSession(cmdText);

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString))
                {
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = new SqlCommand(cmdText, conn);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        hidEquipmentType.Value = ddlDeviceType.SelectedValue;
                        hidSource.Value = ddlTransmissionType.SelectedValue;

                        string settingkey = GetGridSettingKey();
                        LoadGridSettings(rgrdSessions, settingkey);
                        btnPersistSettings.Visible = true;

                        if (dt.Rows.Count > 0)
                        {
                            DataTable dtFormat = formatExportSessions(dt);
                            rgrdSessions.DataSource = dtFormat;
                            btnExportToExcel.Visible = false; //not using anymore. Left on page for the time being till new export has been thoroughly tested
                            btnExcelExport.Visible = true;
                        }

                        conn.Close();
                    }
                    catch (Exception x)
                    {
                       if (PulseSession.Instance.User.FirstName.Contains("Arlow")){
                           ShowMessage(x.Message.ToString());
                       }

                       //lblSQLFeedback.Text = x.Message.ToString();
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        //CALLS SP - SHOWS PACKET INFORMATION NESTED IN SESSION DATA
        protected void rgrdChild_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            GridDataItem parentItem = ((sender as RadGrid).NamingContainer as GridNestedViewItem).ParentItem as GridDataItem;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PulseConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("spSessionData_BySessionID_Get", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("SessionID", parentItem.GetDataKeyValue("ROW_ID").ToString());

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
            }
            catch (Exception ex)
            {
              if (PulseSession.Instance.User.FirstName.Contains("Arlow")){
                           ShowMessage(ex.Message.ToString());
                       }
            }
            finally
            {
                conn.Close();
            }

            if (dt.Rows.Count > 0)
            {
                (sender as RadGrid).DataSource = dt;
                // (sender as RadGrid).Skin = getTelerikSkin();
            }
        }

        #region "GroupingNotPaging"
        // taken from http://www.telerik.com/community/forums/aspnet-ajax/grid/290542-grouping-and-paging-on-collapse.aspx

        protected void rgrdParent_GroupsChanging(object source, GridGroupsChangingEventArgs e)
        {
            if (e.Action == GridGroupsChangingAction.Group)
            {
                rgrdParent.MasterTableView.PageSize = rgrdParent.MasterTableView.PageSize * rgrdParent.MasterTableView.PageCount;
            }

            else if (e.Action == GridGroupsChangingAction.Ungroup && e.TableView.GroupByExpressions.Count == 1)
            {
                rgrdParent.MasterTableView.PageSize = 12;
            }

        }

        protected void rgrdParent_PreRender(object sender, EventArgs e)
        {
            // only expands the most recently grouped column. others collapse by default.
            foreach (GridGroupHeaderItem headerItem in rgrdParent.MasterTableView.GetItems(GridItemType.GroupHeader))
            {
                 SearchClicked = true;
                headerItem.Expanded = TraverseGridGroup(headerItem);
            }
            if (rgrdParent.MasterTableView.GroupByExpressions.Count > 0)
            {
                 SearchClicked = true;
                rgrdParent.MasterTableView.PageSize = rgrdParent.MasterTableView.PageSize * rgrdParent.MasterTableView.PageCount;
                rgrdParent.MasterTableView.Rebind();
               
            }
            SearchClicked = false;
        }

        protected void rgrdSessions_PreRender(object sender, EventArgs e)
        {
            foreach (GridDataItem item in rgrdSessions.MasterTableView.Items)
            {
                if (item.Expanded)
                {
                    RadGrid grid = (RadGrid)item.ChildItem.FindControl("rgrdChild");
                    string settingkey = GetGridSettingKey();
                    LoadGridSettings(grid, settingkey.Replace("_session_", "_sessiondetail_"));
                }
            }
        }

        protected bool TraverseGridGroup(GridGroupHeaderItem _headerItem)
        {
            bool HasChildHeaderItems = false;
            GridItem[] children = _headerItem.GetChildItems();
            foreach (GridItem item in children)
            {
                if (item is GridGroupHeaderItem)
                {
                    HasChildHeaderItems = true;
                    GridGroupHeaderItem GroupHeaderItem = (item as GridGroupHeaderItem);
                    GroupHeaderItem.Expanded = TraverseGridGroup(GroupHeaderItem);
                }
            }
            return HasChildHeaderItems;
        }

        #endregion

        protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
        {
      
        }

        #endregion

        protected void ddlSites_PreRender(object sender, EventArgs e)
        {
          //System.Web.UI.WebControls.DropDownList combo = ddlSites;
            //if (ddlSites != null)
            //{
            //    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
            //    {
            //        ddlSites.DataSource = enovateDB.GetAllFacilities();
            //        ddlSites.DataValueField = "IDSite";
             
            //        ddlSites.DataTextField = "SiteDescription";
            //    }

            
            //}
           
             RadDropDownList drop = (sender as RadDropDownList);
            if (drop != null)
            {
                if (drop.Items.Count > 0)
                {
                    return;
                }
                drop.Items.Clear();
                foreach (Site site in PulseWeb.API.api.SessionUser.Sites)
                {
                    drop.Items.Add(new DropDownListItem { Text = site.Name, Value = site.Id.ToString(), Selected = site == PulseSession.Instance.SelectedSite ? true : false });
                }
                 drop.Items.Add(new DropDownListItem { Text = "All Facilities", Value = null}); 
            }
        }

        protected void rgrdParent_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            SearchClicked = true;
        }

        #region Formatting

        /*for 4.0 fill the get functions with db 
         * calls or wherever it is stored ~DC*/

        //private string getFHex()
        //{
        //    //forecolor
        //    return "#FFFFFF";
        //}

        //private string getBHex()
        //{
        //    //backcolor
        //    return "#79BDE8";
        //}

        //private string getTelerikSkin()
        //{
        //    //skin name
        //    return "Black";
        //}

        //private void SetStyles()
        //{
        //    System.Drawing.Color fColor = System.Drawing.ColorTranslator.FromHtml(getFHex());
        //    System.Drawing.Color bColor = System.Drawing.ColorTranslator.FromHtml(getBHex());

        //    ddlDeptWFOA.ForeColor = fColor;
        //    ddlDeptWFOA.BackColor = bColor;
        //    ddlDeviceType.ForeColor = fColor;
        //    ddlDeviceType.BackColor = bColor;
        //    ddlSites.ForeColor = fColor;
        //    ddlSites.BackColor = bColor;
        //    ddlTransmissionType.ForeColor = fColor;
        //    ddlTransmissionType.BackColor = bColor;

        //    //dec1.Skin = getTelerikSkin();
        //    //dec2.Skin = getTelerikSkin();
        //    //dec3.Skin = getTelerikSkin();
        //    //dec4.Skin = getTelerikSkin();

        //    //rgrdParent.Skin = getTelerikSkin();
        //    //rgrdSessions.Skin = getTelerikSkin();
        //    //datetimeEnd.Skin = getTelerikSkin();
        //    //datetimeStart.Skin = getTelerikSkin();
        //    /*rgrdChild is hidden - skin set in needsDataSource*/
        //}

        #endregion

    }//ends class
}//ends namespace