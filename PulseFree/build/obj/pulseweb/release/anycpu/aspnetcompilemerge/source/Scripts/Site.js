﻿$(document).ready(function () {

    $("#menuBar").kendoTooltip({
        filter: "a",
        position: "top"
    });

    //Document Click
    $(document).click(function () {

        $("#notificationContainer").hide();
        $("#adminPopoverContainer").hide();
        //$("userPopoverContainer").hide();
    });

    //Popup Click
    //$("#userPopoverContainer").click(function () {
    //    return false
   // });

    $("#notificationContainer").click(function () {
        return false
    });

    $("#adminPopoverContainer").click(function () {
        return false
    });

//    $("#userLink").click(function () {
//        $("#userPopoverContainer").fadeToggle(300);
//    });

    $("#notificationLink").click(function () {
        $("#adminPopoverContainer").hide();
        $("#notificationContainer").fadeToggle(300);
        $("#notification_count").fadeOut("slow");
        return false;
    });

    $("#adminPopupLink").click(function () {
        $("#notificationContainer").hide();

        $("#adminPopoverContainer").fadeToggle(300);
        return false;
    });

    $.fn.notificationClick = function (object) {
        window.open("https://enovatemedical.force.com/techservices/login");
    }

    $.fn.adminMenuClick = function (object, id, param) {

        $("#adminPopoverContainer").hide();

        switch (id) {
            case 0: window.location.replace("http://" + window.location.host + "/LoginPage.aspx?Logout"); break;
            case 1: window.location.replace("http://" + window.location.host + "/ViewUsers.aspx"); break;
            case 2: window.location.replace("http://" + window.location.host + "/ViewAssets.aspx?" + param); break;
            case 3: window.location.replace("http://" + window.location.host + "/ViewDepartments.aspx"); break;
        }
    }

    var facilityName;
    var reportWindow = $("#window"), openButton = $("#btnReport"), infoWindow = $("#information");

    $.fn.generateReport = function (id) {
        var url = "http://" + window.location.host + "/Reporting/Reports.aspx?site=" + facilityName + "&queryid=" + id;
        window.location.replace(url);
    }

    // create the reports window
    if (!reportWindow.data("kendoWindow")) {
        reportWindow.kendoWindow({
            width: "550px",
            actions: ["Minimize", "Maximize", "Close"],
            title: "Select a report",
            modal: "true",
            close: function () {
            }
        });
    }

    // create the informatin window
    if (!infoWindow.data("kendoWindow")) {
        infoWindow.kendoWindow({
            width: "550px",
            height: "187px",
            actions: ["Minimize", "Maximize", "Close"],
            title: "Information",
            content: "http://" + window.location.host + "/ServerMessage.aspx",
            close: function () {
                openButton.show();
            }
        });
    }

    $.fn.clickReportButtonx = function (facility) {
        //window.data("kendoWindow").wrapper.css("top", e.style.top);
        facilityName = facility;
        reportWindow.data("kendoWindow").center();
        reportWindow.data("kendoWindow").open();
    };

    $.fn.clickInfoButton = function (object, e, messageId) {
        infoWindow.data("kendoWindow").title(object.alt);
        // Set the contents with the results of a web service call
        infoWindow.data("kendoWindow").refresh("http://" + window.location.host + "/ServerMessage.aspx?MessageID=" + messageId);
        infoWindow.data("kendoWindow").center();
        infoWindow.data("kendoWindow").open();
    };

});

function InitMessages(rValue) {
    if (rValue != "") {
        obj = JSON.parse(rValue);

        var div = document.getElementById("hereswheretoinsert");

        if (div != null) {
            for (var i = 0; i < obj.length; i++) {
                var li = document.createElement("li");
                var htmlString = li.innerHTML = obj[i];
                div.appendChild(li);
            }
        }
    }
}

function PopUpShowing(sender, eventArgs) {
    popUp = eventArgs.get_popUp();
    var gridWidth = sender.get_element().offsetWidth;
    var gridHeight = sender.get_element().offsetHeight;
    var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
    var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
    popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
    //popUp.style.top = ((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
    popUp.style.top = "0px";

} 
