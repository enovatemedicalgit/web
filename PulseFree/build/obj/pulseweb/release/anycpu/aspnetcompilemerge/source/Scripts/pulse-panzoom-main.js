﻿var zoomStep = .025;
var minZoom = 0.5;
var maxZoom = 1.5;
var initialZoom = 1.0;
$(function () {

    // initialize panzoom control
    pulse.panzoom.init({
        container: $('#panzoomContainer'),
        zoom: initialZoom, // sets to 1 if not supplied

        // onZoom: this is a callback function that you can supply that gets called after the zoom completes.
        // it returns the zoomLevel (currently between 0.5 and 1, which is the percentage scale of the image)
        // below is an example of how you could remap (update) your pin locations.

        onZoom: function (zoomLevel) {
            // update co-ordinates for each map pin
            $('.location-pin').each(function () {
                var x = parseInt($(this).data('x'));
                var y = parseInt($(this).data('y'));
                var scaledX = Math.round(x * zoomLevel);
                var scaledY = Math.round(y * zoomLevel);
                $(this).css('left', scaledX + 'px');
                $(this).css('top', scaledY + 'px');
            });
        }
    });

    $("#slider-vertical").slider({
        orientation: "vertical",
        min: minZoom,
        max: maxZoom,
        value: initialZoom,
        step: zoomStep,
        change: function (event, ui) {
            pulse.panzoom.setZoom(ui.value);
        }
    });

    // Insert arrow glyph into slider handle
    $('.ui-slider-handle').html('<div class="icon-wrapper"><i class="icon icon-handle icon-zoom-arrow"></i></div>');

    // Wire up zoom buttons to control slider
    $('#btnZoomIn').on('click', function () {
        addSliderIncrements(1);
        return false;
    });
    $('#btnZoomOut').on('click', function () {
        addSliderIncrements(-1);
        return false;
    });


    // Wire up floor animation
    $('.floor').on('click', function () {

        // change panable image
        pulse.panzoom.setFloor($(this).data('floor'));

        // set selected state
        $('.floor.selected').removeClass('selected');
        $(this).attr('class', 'floor selected');

        // now apply transition classes
        var index = 1;
        var prev = $(this).prev();
        while (prev.length > 0) {
            prev.attr('class', 'floor above-' + index);
            prev = prev.prev();
            index++;
        }
        index = 1;
        var next = $(this).next();
        while (next.length > 0) {
            next.attr('class', 'floor below-' + index);
            next = next.next();
            index++;
        }

        var offset = $(this).offset().top;
        $('#floorsContainer').css('perspective', offset + 'px');
    });
});

function addSliderIncrements(increments) {
    var currentValue = $("#slider-vertical").slider("value");
    $("#slider-vertical").slider("value", currentValue + (zoomStep * increments));
}
