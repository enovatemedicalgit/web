﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectADFile.ascx.cs" Inherits="PulseWeb.ImportUsers.SelectADFile" %>
 <script src="script.js" type="text/javascript"></script>

<style>
    .rbPrimaryButton
    {
        position:absolute;
        left:0px;
        top:0px;
    }

</style>

<telerik:RadButton runat="server" ID="SearchButton" Text="Next" OnClick="NextButton_Click" Skin="Silk" CssClass="rbPrimaryButton">
    <Icon PrimaryIconUrl="/images/icon_search.png" PrimaryIconLeft="11" PrimaryIconTop="6" />
</telerik:RadButton>
<br />
<br />
<div class="SearchForm">
    <telerik:RadAsyncUpload  ID="RadUpload1" runat="server" 
        MaxFileInputsCount="1" OverwriteExistingFiles="false"
        OnFileUploaded="AsyncUpload1_FileUploaded"
        ControlObjectsVisibility="RemoveButtons" Skin="Silk">
    </telerik:RadAsyncUpload>
</div>
