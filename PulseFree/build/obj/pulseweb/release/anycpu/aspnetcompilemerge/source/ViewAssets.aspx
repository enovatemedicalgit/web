﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewAssets.aspx.cs" Inherits="PulseWeb.ViewAssets" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/viewAssets.css" rel="stylesheet" type="text/css" />
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="false"/>
    <telerik:RadFormDecorator runat="server" DecorationZoneID="demo" EnableRoundedCorners="true" DecoratedControls="All" />
    <div id="view-assets" class="view-assets-container no-bg">
        <asp:Label runat="server" Font-Size="Large" ID="AssetTypeLabel" ForeColor="#333333" >&nbsp;&nbsp;Workstations</asp:Label>
    <br />
        <div id="demo" class="demo-container no-bg">
        <telerik:RadGrid ID="RadGrid1" runat="server" EnableEmbeddedSkins="true" 
            Skin="Metro" CssClass="RadGrid_Rounded"
            OnNeedDataSource="RadGrid1_NeedDataSource" 
            AllowPaging="True" 
            AllowAutomaticUpdates="True" 
            AllowAutomaticInserts="False"
            AllowAutomaticDeletes="True" 
            AllowSorting="True" 
            GroupPanelPosition="Top" 
            PageSize="40"
            AutoGenerateEditColumn="False" 
            OnItemCommand="rgrdParent_ItemCommand" 
            OnUpdateCommand="rgrdParent_UpdateCommand"
            OnPrerender = "RadGrid1_PreRender"
            OnColumnCreated="RadGrid1_ColumnCreated" AllowFilteringByColumn="True" OnItemDataBound="RadGrid1_ItemDataBound">
            <CommandItemStyle BorderStyle="Solid" />
            <PagerStyle AlwaysVisible="true" />
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="ExcelML" />
                </ExportSettings>
            <PagerStyle AlwaysVisible="true" />

            <ClientSettings>
                <Selecting AllowRowSelect="True"></Selecting>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
            </ClientSettings>
            <MasterTableView AutoGenerateColumns="True"  
                              CommandItemDisplay="Top"
                                AllowSorting="true"
                              
                                DataKeyNames="IDAsset"  
                                EditMode="PopUp"
                                EditFormSettings-PopUpSettings-Height="300"
                                EditFormSettings-PopUpSettings-Width="520"
                                Width="100%">

<PagerStyle AlwaysVisible="True">

</PagerStyle>
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px;">
                                        &nbsp;&nbsp;
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="ExportToExcel"><img style="border:0px;vertical-align:middle;" alt="" src="Images/Excel_XLSX.png"/>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                                    </div>
                                </CommandItemTemplate>
                <CommandItemSettings ShowExportToExcelButton="False" ShowAddNewRecordButton="false" />
                <Columns>
					<telerik:GridTemplateColumn AllowFiltering="false">
						<ItemTemplate>
							<asp:Image runat="server" ID="edit1" ImageUrl="/Images/Edit.png" AlternateText="Edit" Width="12px" Height="12px"/>
							<asp:LinkButton runat="server" ID="linkbuttonedit1" Text="Edit" CommandName="Edit"></asp:LinkButton>
						</ItemTemplate>
					</telerik:GridTemplateColumn>

                </Columns>
                <EditFormSettings  EditFormType="Template" CaptionFormatString="Edit device">
                    <EditColumn ButtonType="ImageButton" />
					<PopUpSettings Modal="true" />
                    <FormTemplate >
                        <asp:Panel runat="server" ID="panel1" CssClass="ViewDepartments_Panel">
                            <div class="df_dept_container">
                                <div class="df_serial_label">
                                    <asp:Label ID="Label1" runat="server" Text="Serial Number" ></asp:Label>
                                </div>
                                <div class="df_serial_input">
                                    <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" Text='<%#Eval("SerialNumber") %>' Width="296px"></asp:TextBox>
                                </div>
                            <%--        <div class="df_facility_label"> Facility </div>
                                <div class="df_facility_combo">
                                    <telerik:RadDropDownList ID="ddlSite" runat="server" DataValueField="ROW_ID" DataTextField="SiteDescription" Width="296" OnPreRender="SiteCombo_PreRender" EnableEmbeddedSkins="true" >
                                    </telerik:RadDropDownList>
                                </div>
                            OnSelectedIndexChanged="ddlSite_SelectedIndexChanged"--%>
                                <div class="df_description_label"> 
                                    <asp:Label ID="Label2" runat="server" Text="Asset Number" ></asp:Label>
                                </div>
                                <div class="df_description_input">
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%#Eval("AssetNumber") %>' Width="294px"></asp:TextBox>
                                </div>
                                <div class="df_dept_label">Department Name</div>
                                <div class="df_dept_name">
                                    <telerik:RadDropDownList ID="rcboDept" runat="server" Text='<%#Eval("DepartmentId") %>' 
                                        Width="296px" DropDownWidth="296px" MaxLength="50" EmptyMessage=""
                                        HighlightTemplatedItems="true" EnableLoadOnDemand="true" Filter="StartsWith"
                                        MarkFirstMatch="true" OnPreRender="DepartmentCombo_ItemsRequested" Height="24px"  >
                                    </telerik:RadDropDownList>
                                </div>
                                <div class="df_dept_btn_save">
                                    <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Update" />
                                 </div>
                                <div class="df_dept_btn_cancel">
                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CommandName="PerformCancel" CausesValidation="false" />
                                 </div>
                            </div>
                        </asp:Panel>
                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
        </div>
    </asp:Content>
