﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewImportedUsers.ascx.cs" Inherits="PulseWeb.ImportUsers.ViewImportedUsers" %>
<style>
    .rbPrimaryButton
    {
        position:absolute;
        left:0px;
        top:0px;
    }

</style>

<telerik:RadButton runat="server" ID="SearchButton" Text="Next" OnClick="NextButton_Click" Skin="Silk" CssClass="rbPrimaryButton">
    <Icon PrimaryIconUrl="/images/icon_search.png" PrimaryIconLeft="11" PrimaryIconTop="6" />
</telerik:RadButton>
<br />
<br />

<div class="ViewImportedUsersForm">
   
    <div class="grid123">
        <telerik:RadGrid ID="RadGrid1" 
            runat="server"
            OnNeedDataSource="RadGrid1_NeedDataSource"
            OnColumnCreated="RadGrid1_ColumnCreated" 
            >

        </telerik:RadGrid>
     </div>
</div>
