﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewDepartments.aspx.cs" Inherits="PulseWeb.ViewDepartments" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/viewAssets.css" rel="stylesheet" type="text/css" />

</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableEmbeddedSkins="false"/>
    <telerik:RadFormDecorator runat="server" DecorationZoneID="demo" EnableRoundedCorners="true" DecoratedControls="All" />
    <div id="view-assets" class="view-assets-container no-bg">
        <asp:Label runat="server" Font-Size="Large" ID="AssetTypeLabel" ForeColor="#333333" >&nbsp;&nbsp;Departments</asp:Label>
    <br />
        <div id="demo" class="demo-container no-bg">
        <telerik:RadGrid ID="RadGrid1" runat="server" EnableEmbeddedSkins="true" 
            Skin="Metro" CssClass="RadGrid_Rounded"
            OnNeedDataSource="RadGrid1_NeedDataSource" 
            AllowPaging="True" 
            AllowAutomaticUpdates="True" 
            AllowAutomaticInserts="True"
            AllowAutomaticDeletes="True" 
            AllowSorting="True" 
            GroupPanelPosition="Top" 
            PageSize="40"
            AutoGenerateEditColumn="False" 
            OnItemCommand="rgrdParent_ItemCommand" 
            OnUpdateCommand="rgrdParent_UpdateCommand"
            OnInsertCommand="RadGrid1_InsertCommand" 

            OnColumnCreated="RadGrid1_ColumnCreated" AllowFilteringByColumn="True" >
            <PagerStyle AlwaysVisible="true" />
            <ExportSettings ExportOnlyData="true" IgnorePaging="true" OpenInNewWindow="true">
                    <Excel Format="ExcelML" />
                </ExportSettings>
            <PagerStyle AlwaysVisible="true" />

            <ClientSettings>
                <Selecting AllowRowSelect="False"></Selecting>
                <ClientEvents OnPopUpShowing="PopUpShowing" />
          </ClientSettings>
            <MasterTableView AutoGenerateColumns="True"  
                              CommandItemDisplay="Top"
                                AllowSorting="true"
                                DataKeyNames="Id"  
                                EditMode="PopUp"
                                EditFormSettings-PopUpSettings-Height="300"
                                EditFormSettings-PopUpSettings-Width="520"
                                 EditFormSettings-PopUpSettings-ShowCaptionInEditForm="true"
                                Width="100%" >
                                <CommandItemTemplate>
                                    <div style="padding: 5px 5px;">
                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="InitInsert" Visible='<%# !RadGrid1.MasterTableView.IsItemInserted %>'><img style="border:0px;vertical-align:middle;" alt="" src="Images/AddRecord.png"/>&nbsp;&nbsp;Add new</asp:LinkButton>&nbsp;&nbsp;
                                        &nbsp;&nbsp;
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="ExportToExcel"><img style="border:0px;vertical-align:middle;" alt="" src="Images/Excel_XLSX.png"/>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                                    </div>
                                </CommandItemTemplate>
                <CommandItemSettings ShowExportToExcelButton="False" ShowAddNewRecordButton="false" />
                <Columns>
					<telerik:GridTemplateColumn AllowFiltering="false">
						<ItemTemplate>
							<asp:Image runat="server" ID="edit1" ImageUrl="/Images/Edit.png" AlternateText="Edit" Width="12px" Height="12px"/>
							<asp:LinkButton runat="server" ID="linkbuttonedit1" Text="Edit" CommandName="Edit"></asp:LinkButton>
						</ItemTemplate>
					</telerik:GridTemplateColumn>

                </Columns>
                <EditFormSettings  EditFormType="Template"  CaptionFormatString="Edit the department">
                    <EditColumn ButtonType="ImageButton" />
					<PopUpSettings Modal="true" />
                    <FormTemplate >
                        <asp:Panel runat="server" ID="panel1" OnPreRender="panel1_PreRender" CssClass="ViewDepartments_Panel">
                            <div class="df_dept_container">
                                <div class="df_facility_label"> Associated facility</div>
                                <div class="df_facility_combo">
                                        <telerik:RadComboBox ID="ddlSite" runat="server" DataValueField="ROW_ID" DataTextField="SiteDescription" Width="211" OnDataBinding="SiteCombo_DataBinding" EnableEmbeddedSkins="true">
                                        </telerik:RadComboBox>
                                </div>
                                <div class="df_dept_label">Department Name</div>
                                <div class="df_dept_name">
                                    <telerik:RadTextBox ID="RadTextBox1" Runat="server" Text='<%#Bind("Department") %>'> </telerik:RadTextBox>
                                </div>
                                <div class="df_dept_btn_save">
                                    <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Update" />
                                 </div>
                                <div class="df_dept_btn_cancel">
                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="false" />
                                 </div>
                            </div>
                        </asp:Panel>

                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>
        </telerik:RadGrid>
</asp:Content>
