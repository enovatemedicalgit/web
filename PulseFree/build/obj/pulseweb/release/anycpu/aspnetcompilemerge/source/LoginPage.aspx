﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="PulseWeb.Account.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta charset="utf-8"/>
<title>Untitled Document</title>
<link rel="stylesheet" href="~/css/login.css"/>
<link href="~/css/font-awesome.min.css" rel="stylesheet"/>
</head>
<body>
    <script>
        function onclick1() {
            document.getElementById("demo").style.color = "red";
        }    </script>
	<div class="si-main">
    	<div class="si-container">
        	<div class="form-container">
            	<div class="form-main">
                	<div class="top-main-text">PULSE</div>
                    <div class="arrow-up"></div>
                	<div class="form-bg">
                        <form enctype="multipart/form-data" runat="server" defaultbutton="login">
                        <asp:Panel ID="Panel1" runat="server">
                            <div class="form-line">
                                <i class="fa fa-user"></i>
                                <asp:TextBox runat="server" id="txtUserName" class="user_name" value="" size="20"/>
                            </div>
                            <div class="form-line">
                                <i class="fa fa-lock"></i>
                                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="user_password"></asp:TextBox>
                            </div>
                            <div class="si-round-design">
                                <div class="roundedTwo">
                                    <asp:CheckBox runat="server" id="roundedTwo"/>
                                    <label for="roundedTwo"></label>
                                    <div class="si-round-text">
                                    	Keep me Logged in
                                    </div> 
                                 </div>
                                 <div class="si-question" >
                                     <asp:ImageButton runat="server" ImageUrl="../images/question-mark.png"  OnClick="ForgotPW_Click"/>
                                     <div style="display:inline;position:relative;top:-8px;left:2px;">
                                        Forgot Password
                                     </div>
                                </div>    
                               
                            </div>    
                           
                            <asp:Button ID="login" Text="Login" runat="server" OnClick="OnClickLogin"  CssClass="btn member-login" />
                            <asp:Button ID="CreateNewUser" Text="Creat New Account" runat="server" OnClick="BtnCreateNewUser" CssClass="btn member-creat-ac" Visible="False" />
                            <div class="arrow-down"></div>
                            </asp:Panel>
                            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
                            <script src="Scripts/modalpopup.js" type="text/javascript"></script>
                            <telerik:RadWindow ID="modalPopup" runat="server" Width="330px" Height="177px" Modal="true" OffsetElementID="main" Visible="True" Title="Please select a facility">
                                <ContentTemplate>
                                    <telerik:RadComboBox ID="ddlSite" runat="server" DataTextField="SiteDescription"  Width="300px" EnableEmbeddedSkins="true" ReadOnly="true" OnPreRender="ddlSite_PreRender"  OnDataBinding="ddlSite_DataBinding" > 
                                    </telerik:RadComboBox>
				                    <asp:Button runat="server" ID="rbSelectFacility" Text="Done" AutoPostBack="true" Height="32px" OnClick="OnSelectFacilityDoneClicked" CssClass="btn member-login2">
                                        </asp:Button>
                                </ContentTemplate>
                                </telerik:RadWindow>
                            <telerik:RadCodeBlock runat="server" ID="rdbScripts">
                                    <script type="text/javascript">
                                        $modalWindowThunk.modalWindowID = "<%=modalPopup.ClientID %>";
                                    </script>
                                </telerik:RadCodeBlock>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
