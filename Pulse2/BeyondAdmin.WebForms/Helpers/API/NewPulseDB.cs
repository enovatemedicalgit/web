﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PulseWeb.Utils.Database;
using PulseWeb.Model;

namespace PulseWeb.API
{

    public class NewPulseDB : IDisposable
    {
        private SQLDatabase _database;


        public NewPulseDB()
        {
            _database = new SQLDatabase("PulseConnection");
        }

        public void Dispose()
        {
            if (_database != null)
            {
                _database.Dispose();
            }
        }

        public List<PulseCustomer> InitializeCustomer(Int32 businessUnitId)
        {
            //DataTable dtBusinessUnit = Common.GetDataTable(string.Format("select * from BusinessUnit where ROW_ID = {0}", hidBusinessUnitID.Value));
            List<PulseCustomer> list = new List<PulseCustomer>();

            try
            {
                string commandText = "select * from Customers where CASTBUID = " + businessUnitId.ToString();

                // Execute the query
                var dbList = _database.Query(commandText, null);

                // Fill in session variables from the result
                foreach (var customer in dbList)
                {
                    list.Add(new PulseCustomer(customer));
                }

            }
            catch(Exception ex)
            {
                Utils.ErrorHandler.Error(ex.Message);
            }

            return list;
        }

        public  List<PulseSite> GetSitesForCustomer(PulseCustomer customer)
        {
            List<PulseSite> sites = new List<PulseSite>();

            try
            {
                string commandText = "select * from Sites where CustomerID = " + customer.Id.ToString();

                // Execute the query
                var dbList = _database.Query(commandText, null);
                // Fill in session variables from the result
                foreach (var site in dbList)
                {
                    sites.Add(new PulseSite(site));
                }
            }
            catch(Exception ex)
            {
                Utils.ErrorHandler.Error(ex.Message);
            }
            return sites;
        }


        public  List<PulseArea> GetAreasForSite(PulseSite site,List<PulseDepartment> departmentList )
        {
            List<PulseArea> areas = new List<PulseArea>();

            try
            {
                string commandText = "select * from Area where IDSite = " + site.Id.ToString();
                // Execute the query
                var dbList = _database.Query(commandText, null);
                // Fill in session variables from the result
                foreach (var area in dbList)
                {
                    areas.Add(new PulseArea(area,departmentList));
                }
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error(ex.Message);
            }
            return areas;

        }

        public List<PulseDepartment> GetDepartmentsForSite(PulseSite site)
        {
            List<PulseDepartment> departments = new List<PulseDepartment>();

            try
            {
                string commandText = "select * from Departments where SiteID = " + site.Id.ToString();
                // Execute the query
                var dbList = _database.Query(commandText, null);
                // Fill in session variables from the result
                foreach (var area in dbList)
                {
                    departments.Add(new PulseDepartment(area));
                }
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error(ex.Message);
            }
            return departments;

        }

        public PulseUser GetSessionUser(string userName, string password)
        {
            PulseUser user = (PulseUser)Activator.CreateInstance(typeof(PulseUser));

            try
            {
                string commandText = "select top 1 * from [User] where WebUsername = '" + userName + "' and WebPassword='" + password + "'";


                // Execute the query
                var rows = _database.Query(commandText, null);

                // Query failed?
                if (rows.Count != 1)
                {
                    return null;
                }
                // Fill in session variables from the result
                user.Initialize(rows[0]);
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error(ex.Message);
            }

            //user.AccessRights = GetUserAccessRights(user);


            return user;

        }

        public List<PulseAsset> InitializeAssetsForSite(PulseSite site)
        {
            List<PulseAsset> list = new List<PulseAsset>();

            try
            {
                string commandText = "SELECT * FROM viewTaggedAssets";

                // Execute the query
                var dbList = _database.Query(commandText, null);

                // Fill in session variables from the result
                foreach (var dbRow in dbList)
                {
                    list.Add(new PulseAsset(dbRow));
                }
            }
            catch (Exception ex)
            {
                Utils.ErrorHandler.Error(ex.Message);
            }

            return list;
        }


    }

}