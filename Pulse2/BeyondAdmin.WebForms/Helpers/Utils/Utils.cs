﻿//#define USE_LOCAL_DATABASE
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Diagnostics;


namespace PulseWeb.Utils
{
    public class ErrorHandler
    {
        // MDJ TODO do something here
        public static void Error(string msg)
        {
            Debug.WriteLine(msg);
        }

        public static void WarnMissingDBField(string field, object obj)
        {
            Debug.WriteLine(obj.GetType().FullName + " undeclared DB member " + field);
        }

        public static void InternalError(string argMessage)
        {

        }

        public static void PopUpMessage(string argMessage)
        {
            string newMsg = argMessage.Replace("\r\n", " ").Replace("\'", "\"");
            string startupScript = "<script>alert('" + newMsg + "');</script>";
            Page ThisPage = HttpContext.Current.Handler as Page;
            ScriptManager.RegisterStartupScript(ThisPage, ThisPage.GetType(), "startup", startupScript, false);
        }
    }

    public class Time
    {
#if USE_LOCAL_DATABASE
        public static System.DateTime Now = new System.DateTime(2015, 4, 20); //System.DateTime.Now;
        public static System.DateTime Today = new System.DateTime(2015, 4, 20, 0, 0, 0); //System.DateTime.Today;
#else
        public static System.DateTime Now = System.DateTime.Now;
        public static System.DateTime Today = System.DateTime.Today;
#endif

        public static DateTime LocalDateTime(DateTime value)
        {
            int OffsetMinutes_Standard = Convert.ToInt16(HttpContext.Current.Request.Cookies["OffsetMinutes_Standard"].Value);
            int OffsetMinutes_DaylightSavings = Convert.ToInt16(HttpContext.Current.Request.Cookies["OffsetMinutes_DaylightSavings"].Value);
            int CurrentOffset;
            if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
            {
                CurrentOffset = OffsetMinutes_DaylightSavings;
            }
            else
            {
                CurrentOffset = OffsetMinutes_Standard;
            }
            return value.AddMinutes(CurrentOffset);
        }
    }

    public class Misc
    {

        static public string SafeGetKey(Dictionary<string, string> dictionary,string key,bool errorIfMissing = true,string defaultStr = "0")
        {

            if (dictionary != null && dictionary.ContainsKey(key) && !string.IsNullOrWhiteSpace(dictionary[key]))
            {
                return dictionary[key];
            }
            if (errorIfMissing)
            {
                ErrorHandler.Error("Null key: " + key);
            }
            return defaultStr;
        }
        
        static public string SafeShortDate(string str)
        {
            try
            {
                return DateTime.Parse(str).ToShortDateString();
            }
            catch( Exception )
            {
                return "";
            }
        }
        static public int SafeConvertToInt(string str)
        {
            int val = 0;
            try{
                if( str == "" || str == "0" || string.IsNullOrEmpty(str) )
                    val = 0;
                else
                    val = Convert.ToInt32(str);
            }
            catch(Exception){

            }
            return val;
        }

        static public double SafeConvertToDouble(string str)
        {
            double val = 0;
            try
            {
                if (str == "" || str == "0" || string.IsNullOrEmpty(str))
                    val = 0;
                else
                    val = Convert.ToDouble(str);
            }
            catch (Exception)
            {

            }
            return val;
        }

    }

}