﻿using System;
using System.Web.UI;
using PulseWeb;

// All pages will derve from this	 
public class BasePage : System.Web.UI.Page
{
	/// <summary>
	/// property vcariable for the URL Property
	/// </summary>
	private static string _redirectUrl;
	 
	/// <summary>
	/// property to hold the redirect url we will
	/// use if the users session is expired or has
	/// timed out.
	/// </summary>
	public static string RedirectUrl
	{
	    get { return _redirectUrl; }
	    set { _redirectUrl = value; }
	}

    public BasePage()
	{
        _redirectUrl = "~/LoginPage.aspx";
	}
	 
	override protected void OnInit(EventArgs e)
	{
	    //initialize our base class (System.Web,UI.Page)
	    base.OnInit(e);

        string fromPage = Page.AppRelativeVirtualPath.ToLower();
        if (fromPage != _redirectUrl.ToLower() && Context.Session != null )
	    {
            if (PulseSession.Instance.User == null && !Page.IsCallback)
            {
                string ClientQueryString = Page.ClientQueryString.ToString();
                string redir = ClientQueryString != string.Empty ? Page.AppRelativeVirtualPath + "?" + ClientQueryString : Page.AppRelativeVirtualPath;

                Response.Redirect(_redirectUrl + "?redir=" + redir);
            }
        }
	}

     protected Control FindControl(Control parent, string name)
    {
        foreach (Control c in parent.Controls)
        {
            if (c.ID == name)
                return c;
            Control ctl = FindControl(c, name);
            if (ctl != null)
                return ctl;
        }
        return null;

    }

}