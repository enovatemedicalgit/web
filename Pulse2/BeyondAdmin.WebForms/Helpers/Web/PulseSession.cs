﻿using System;
using System.Collections.Generic;
using System.Web;
using PulseWeb.Utils.Database;
using PulseWeb.Model;
using PulseWeb.API;

namespace PulseWeb
{
    // PulseSession caches session data needed by the api
    public class PulseSession
    {
        #region Singleton

        static public PulseSession Instance
        {
            get
            {
                if (HttpContext.Current.Session["PulseSession"] == null)
                {
                    HttpContext.Current.Session["PulseSession"] = new PulseSession();
                }
                return HttpContext.Current.Session["PulseSession"] as PulseSession;
            }
        }
        #endregion

        public PulseUser User 
        { 
            set { HttpContext.Current.Session["User"] = value;} 
            get { return HttpContext.Current.Session["User"] as PulseUser; }
        }
        // TODO should this be global and not per instance?
        public List<Customer> Customers 
        {
            set { HttpContext.Current.Session["Customers"] = value; }
            get { return HttpContext.Current != null && HttpContext.Current.Session != null ? HttpContext.Current.Session["Customers"] as List<Customer> : null; }
        }
        public Site SelectedSite
        {
            get { return HttpContext.Current.Session["SelectedFacility"] as Site; }
            set { HttpContext.Current.Session["SelectedFacility"] = value; }
        }
        public int SelectedDepartmentIndex
        {
            get { return Convert.ToInt32(HttpContext.Current.Session["SelectedDept"]); }
            set { HttpContext.Current.Session["SelectedDept"] = value.ToString(); }

        }


        public Customer GetCustomerFromBUID(int buid)
        {
            return Customers.Find(x => x.Id == buid);
        }

        public bool EndCurrentSession()
        {
            User = null;
            Customers = null;
            return true;
        }

        public void InitializeSessionUser()
        {
            if (User != null)
            {
                using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                {
                    //Create a list of customers's associated with the user
                    Customers = enovateDB.InitializeCustomerList(User.IDSite);

                    // MDJ TODO fix this
                    // for now the site id can still be either customer or site.  If not a customer, get the parent
                    if( Customers.Count <= 0 )
                    {
                        List<Site> sites = enovateDB.InitializeSiteList(User.IDSite);

                        if( sites.Count > 0 )
                        {
                            Customers = enovateDB.InitializeCustomerList(sites[0].CustomerID);
                            if (Customers.Count > 0)
                            {
                                Customers[0].Sites = sites;
                                //User.IDSite = Customers[0].Id;
                            }

                        }
                    }

                    // add facilities belonging to this customer
                    foreach (Customer customer in Customers)
                    {
                        if (customer.Sites == null )
                            customer.Sites = enovateDB.InitializeSiteList(customer);

                        // add departments to each site
                        foreach (Site site in customer.Sites)
                        {
                            site.Departments = enovateDB.InitializeDepartmentList(site);
                            site.Areas = enovateDB.InitializeAreaList(site);
                            PulseGlobals.Sites.Add(site);
                        }
                    }

                    // Set the user's properties
                    if (Customers.Count > 0)
                    {
                        User.ParentCustomer = Customers[0];
                        User.Sites = User.ParentCustomer.Sites;
                    }

                    if (Customers.Count <= 0 || User.Sites.Count <= 0)
                        throw new Exception("User not associated with any customers or sites");

                }
            }
        }

        public void StartNewSession(string userName, string password)
        {
            using( PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()) )
            {
                // Initialize the global user
                User = enovateDB.GetSessionUser(userName, password);
                if (User == null)
                {
                    throw new Exception("Invalid username or password");
                }

                System.Web.HttpContext.Current.Session["UserName"] = userName;
                InitializeSessionUser();
            }
        }
    }
}