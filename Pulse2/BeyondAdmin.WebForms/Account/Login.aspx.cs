﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using BeyondAdmin.WebForms.Models;
using BeyondAdmin.API;
using BeyondAdmin.Utils.Database;
using BeyondAdmin.Utils.Security;
using System.Collections.Generic;

namespace BeyondAdmin.WebForms.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register";
           
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
          
                 RegisterHyperLink.NavigateUrl = "?ReturnUrl=" + returnUrl;
               
            }
            else
            {
           
                 RegisterHyperLink.NavigateUrl =  "~/Default.aspx";

            }
        }

        protected string LoginPulse(string userName, string password)
        {
            try
            {

                Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(830);
                Response.Cookies["Password"].Expires = DateTime.Now.AddDays(830);
                Response.Cookies["RememberPW"].Expires = DateTime.Now.AddDays(830);

                if (userName == "sales.demo@enovatemedical.com" && password == "pulse2016" || password == "FOIKeSNKxZ4xHwVHmdD0XQ==")
                {
                    userName = "sales.demo@enovatemedical.com";
                    password = "pulse2016";
                    //PulseSession.Instance.StartNewSession("Joseph.Bernacki@wchn.org", Crypto.EncryptString("joe"));
                    PulseSession.Instance.StartNewSession("sales.demo@enovatemedical.com", Crypto.EncryptString("pulse2016"));
                }
                else
                {
                    PulseSession.Instance.StartNewSession(userName, Crypto.EncryptString(password));
                    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase("PulseConnection")))
                    {

                        enovateDB.Query("update [dbo].[User] set LastLoginDateUTC = '" + DateTime.UtcNow.ToString() + "' where username = '" + PulseSession.Instance.User.UserName.ToString() + "'");

                    }
                }
                if (PulseSession.Instance.User.IDSite == 4)
                {
                     return "SuccessAdmin"; // Enovate User
                    //RedirectToPage();

                }
                else
                {
                      return "Success";
                }

            }
            catch (Exception ex) { 
             return "Failed";
            }
        }
        
        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
              //  var result = signinManager.PasswordSignIn(Email.Text, Password.Text, true, shouldLockout: false);
                 string result = LoginPulse(Email.Text,Password.Text);
                switch (result)
                {
                    //case SignInStatus.Success:
                     case "SuccessAdmin":
                     IdentityHelper.RedirectToReturnUrl(RegisterHyperLink.NavigateUrl + "?SessionKey=" + PulseSession.Instance.User.SessionKey , Response);
                        //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"] + "?SessionKey=" + PulseSession.Instance.User.SessionKey , Response);
                        break;
                    //case SignInStatus.LockedOut:
                    //    Response.Redirect("/Account/Lockout");
                    //    break;
                    //case SignInStatus.RequiresVerification:
                    //    Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                    //                                    Request.QueryString["ReturnUrl"],
                    //                                    true),
                    //                      true);
                    //    break;
                    //case SignInStatus.Failure:
                     case "Success":
                    case "Failed":
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        }
    }
}