﻿function updateAmpDrawSparkline(siteId, divElement) {
    $.support.cors = true;
    getAmpDrawfromAjax(siteId, divElement);
}
function getAmpDrawfromAjax(siteId, divElement) {

   var contentType = 'application/json; charset-utf-8';
    var params = ['SiteID: ' + siteId];
   
    $.ajax({
        url: apiUrl,
        method: 'POST',
        contentType: contentType,
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetBatteryAmpDrawSummary]', paramsStr: params }),
        dataType: 'json',
        success: function (dataJson) {
            makeAmpDrawSparkLineChart(dataJson, divElement);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.prcDashGetBatteryAmpDrawSummary returned error =[' + xhr.responseText + ']');
        }
    });
}

function makeAmpDrawSparkLineChart(jsonData, divElement) {
    var sparklineChartData = mapAmpDrawSparkLineData(jsonData);
    var element = $('#' + divElement);
    //$('#sparklineAmpDraw').sparkline(
    element.sparkline(
        sparklineChartData.barDataset,
        {
            type: 'bar',
            height: 82,
            barColor: '#b0dc81',
            barWidth: '10px',
            width: '100%',
            barSpacing: '5px',
            composite: false,
            fillColor: true
        });

    var sp = element.sparkline(
        sparklineChartData.lineDataset,
      {
          type: 'line',
          //width: '100%',
          composite: true,
          fillColor: false,
          spotColor: '#fafafa',
          minSpotColor: '#fafafa',
          maxSpotColor: '#fafafa',
          spotRadius: 3,
          highlightSpotColor: '#fff',
          lineWidth: 2,
          lineColor: 'white'
      });
    console.log("sparkline test");
};


function mapAmpDrawSparkLineData(rawSparklineData) {
    var barDataset = [];
    var lineDataset = [];
    var whereClause = "";
    if (selectedDepartments !== "") whereClause = "WHERE DepartmentId in (" + selectedDepartments + ")";
    var ampDrawResults = alasql('SELECT ampDayWeek, ampDay, avg(AvgAmpDraw) as ampDraw, SiteID FROM ? ' + whereClause + ' group by ampDayWeek, ampDay, SiteID order by ampDayWeek desc', [rawSparklineData]);
    $.each(ampDrawResults, function () {
        barDataset.push(this.ampDraw.toFixed(2));
    });

    $.each(ampDrawResults, function () {
        lineDataset.push(this.ampDraw.toFixed(2));
    });

    var sparklineData = { barDataset: barDataset, lineDataset: lineDataset };
    return sparklineData;
};
