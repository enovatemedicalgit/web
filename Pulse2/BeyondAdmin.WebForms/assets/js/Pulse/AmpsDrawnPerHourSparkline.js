﻿function updateAmpsDrawnPerHourSparkline(siteId, divElement, avgElement) {
    getAmpsDrawnPerHourfromAjax(siteId, divElement, avgElement);
}
function getAmpsDrawnPerHourfromAjax(siteId, divElement, avgElement) {

    var params = ['SiteID: ' + siteId];
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetAmpsDrawnPerHourSummary]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
       success: function (dataJson) {
           makeAmpsDrawnPerHourSparkLineChart(dataJson, divElement, avgElement);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.prcDashGetAmpsDrawnPerHourSummary returned error=[' + xhr.responseText + ']');
        }
    });
}

function makeAmpsDrawnPerHourSparkLineChart(jsonData, divElement, avgElement) {
    var sparklineChartData = mapAmpsDrawnPerHourSparkLineData(jsonData);
    console.log("Count of packets " + sparklineChartData.lineDataset.length.toString());
    var element = $('#' + divElement);
    
    element.sparkline(
        sparklineChartData.lineDataset,
      {
          type: "line",
          height: "125px",
          width: "100%",
          composite: false,
          fillColor: false,
          spotColor: "#fafafa",
          minSpotColor: "#fafafa",
          maxSpotColor: "#fafafa",
          spotRadius: 2,
          highlightSpotColor: "#ffce55",
          highlightLineColor: "#ffce55",
          lineWidth: 2,
          //lineColor: "themesecondary"
          lineColor: "orange"
      });

   
    

    $("#" + avgElement).text(sparklineChartData.avgDraw.toFixed(2).toString());
    console.log("test");
};



function mapAmpsDrawnPerHourSparkLineData(rawSparklineData) {
    var barDataset = [];
    var lineDataset = [];
    var avgDraw = 0;

    $.each(rawSparklineData, function () {
        barDataset.push(this.ampsDrawnPerHour);
        avgDraw += this.ampsDrawnPerHour;
    });

    $.each(rawSparklineData, function () {
        lineDataset.push(this.ampsDrawnPerHour);
    });

    avgDraw = avgDraw / lineDataset.length;
    var sparklineData = { barDataset: barDataset, lineDataset: lineDataset, avgDraw: avgDraw };
    return sparklineData;
};
