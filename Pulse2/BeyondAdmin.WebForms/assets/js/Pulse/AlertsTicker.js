﻿
function getTicker(siteId) {
    var params = ['SiteID: ' + siteId];
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashAlertMessages]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        success: function (dataJson) {
            buildTicker(dataJson);
            $("ul#ticker01").liScroll();
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.sp_DashAlertMessages returned error ' + xhr.responseText);
        }
    });
}

function buildTicker(jsonData) {
    $("#ticker01").empty();
    var cnt = 0;
    $.each(jsonData, function () {
        cnt++;
        switch (this.AssetDesc.toUpperCase()) {
            case "WKSTN":
                switch (this.PacketType) {
                    case 'Notification':
                        $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-laptop'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
                        break;
                    case 'Info':
                        $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-laptop'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
                        break;
                    case 'Alert':
                        $("#ticker01").append("<li style='color:red;font-weight:bold'><i class='fa fa-laptop'><span>  " + this.AssetDesc + " " + "</span>  <a target='_blank' style='color:red;font-weight:bold' href='https://na32.salesforce.com/" + this.SFCID + "'>" + this.FriendlyDescription + "</a></li>");
                        break;
                }
                break;
            case "BATT":
                switch (this.PacketType) {
                    case 'Notification':
                        $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
                        break;
                    case 'Info':
                        $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
                        break;
                    case 'Alert':
                        $("#ticker01").append("<li style='color:red;font-weight:bold'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a target='_blank' style='color:red;font-weight:bold' href='https://na32.salesforce.com/" + this.SFCID + "'>" + this.FriendlyDescription + "</a></li>");
                        break;
                }
                break;
            case "CHRG":
                switch (this.PacketType) {
                    case 'Notification':
                        $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
                        break;
                    case 'Info':
                        $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
                        break;
                    case 'Alert':
                        $("#ticker01").append("<li style='color:red;font-weight:bold'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a target='_blank' style='color:red;font-weight:bold' href='https://na32.salesforce.com/" + this.SFCID + "'>" + this.FriendlyDescription + "</a></li>");
                        break;
                }
                break;
        }
        //  $("#ticker01").append("<li><span>  " + this.AssetDesc + " " +  "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
    });
    $("#ticker01").append("<li>last...</li>");

}
