﻿function updateBatteriesUsedSparkline(siteId, divElement) {
    getBatteriesUsedfromAjax(siteId, divElement);
}
function getBatteriesUsedfromAjax(siteId, divElement) {

    var params = ['SiteID: ' + siteId];
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetBatteryDailyUsage]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        xhrFields: {
            withCredentials: false
        },
        success: function (dataJson) {
            makeBatteriesUsedSparkLineChart(dataJson, divElement);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('sp_DashGetBatteryDailyUsage returned error =[' +xhr.reponseText + ']');
        }
    });
}

function makeBatteriesUsedSparkLineChart(jsonData, divElement) {
    var sparklineChartData = mapBatteriesUsedSparkLineData(jsonData);
    $('#' +  divElement).sparkline(
        sparklineChartData.barDataset,
        {
            type: 'bar',
            height: 82,
            barColor: '#fb7d64',
            barWidth: '10px',
            width: '100%',
            barSpacing: '5px',
            composite: false,
            fillColor: true
        });

    var sp = $('#' +  divElement).sparkline(
        sparklineChartData.lineDataset,
      {
          type: 'line',
          //width: '100%',
          composite: true,
          fillColor: false,
          spotColor: '#fafafa',
          minSpotColor: '#fafafa',
          maxSpotColor: '#fafafa',
          spotRadius: 3,
          highlightSpotColor: '#fff',
          lineWidth: 2,
          lineColor: 'white'
      });

};


function mapBatteriesUsedSparkLineData(rawSparklineData) {
    var barDataset = [];
    var lineDataset = [];
    $.each(rawSparklineData, function () {
        barDataset.push(this.BatteriesUsed);
    });

    $.each(rawSparklineData, function () {
        lineDataset.push(this.BatteriesUsed);
    });

    var sparklineData = { barDataset: barDataset, lineDataset: lineDataset };
    return sparklineData;
};
