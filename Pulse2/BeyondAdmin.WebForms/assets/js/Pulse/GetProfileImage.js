﻿function GetProfileImage(userId, divElement) {
    $.ajax({
        url: apiImgUrl,
        method: 'Get',
        data: { userId: userId, fieldName: 'ProfileImage' },
        contentType: "application/json; charset-utf-8",
        dataType: 'json',
        success: function (dataJson) {
            profileImg = dataJson;
            $('#' + divElement + " img:last-child").remove();
            $('#' + divElement).html('<img  alt="" class="header-avatar" src="data:image/png;base64,' + profileImg + '" />');
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert("Get profile image returned error " + thrownError);
        }
    });
}