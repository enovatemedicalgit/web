﻿function getSessionInfo() {
    var session = getQueryVariable("sessionId");
    var testKey = "M5INL/Xjsm6XLPe7XNr+RSefaMo89hKdXYJf7PHjUeNE3brqHGmV9ykDBaS+mPmS";
    //sessionId = decodeSessionKey(session);
    sessionId = decodeSessionKey(testKey);
    var params = ['SessionId: ' + sessionId];

    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetSessionInfo]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        success: function(session) {
            sessionStartTime = session.CreatedDateUTC;
            sessionExpireTime = session.ExpirationDateUTC;
            sessionId = session.SessionKey;
        },
        error: function(xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('prcDashGetSessionInfo returned error =[' + xhr.responseText + ']');
        }
    });
}

function checkSessionTime() {
    var now = new Date();
    var sessionTime = new Date(sessionExpireTime);
    if (now > sessionTime) {
        window.location.href = "../Pages/Lock.aspx";
    }
}

function decodeSessionKey(sessionKey) {
    var key = CryptoJS.enc.Utf8.parse('C0mp@ssion');
    var iv = CryptoJS.enc.Utf8.parse('o6806642kbM7c5');
    var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse("It works"), key,
        {
            keySize: 128 / 8,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

    var decrypted = CryptoJS.AES.decrypt(sessionKey, key, {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    //alert('Encrypted :' + encrypted);
    //alert('Key :' + encrypted.key);
    //alert('Salt :' + encrypted.salt);
    //alert('iv :' + encrypted.iv);
    var x = decrypted.toString(CryptoJS.enc.Utf8);
    alert('Decrypted : ' + decrypted);
    alert('utf8 = ' + decrypted.toString(CryptoJS.enc.Utf8));
}