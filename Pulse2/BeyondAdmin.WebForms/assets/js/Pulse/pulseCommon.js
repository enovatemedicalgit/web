﻿var apiUrl = 'http://localhost:49814/api/StoredProc/';
var apiImgUrl = 'http://localhost:49814/api/BiFrost/';
//var apiUrl = 'http://sndrhythm.myenovate.com/api/StoredProc/';
//var apiUrl = 'http://rhythm.myenovate.com/api/StoredProc/';
var siteId = '212';
var totalAssets = 0;
var userId = '223';
var profileImg = "";
var selectedDepartments = "";
var sessionStartTime = "";
var sessionExpireTime = "";
var sessionId = "";


var AssetsReportingdoughnutData = [];

var contentType = 'application/json; charset-utf-8';

function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR has 'withCredentials' property only if it supports CORS
        xhr.open('POST', apiUrl, true);
    } else if (typeof XDomainRequest != "undefined") { // if IE use XDR
        xhr = new XDomainRequest();
        xhr.open('POST', apiUrl);
    } else {
        xhr = null;
    }
    return xhr;
}


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return (false);
}