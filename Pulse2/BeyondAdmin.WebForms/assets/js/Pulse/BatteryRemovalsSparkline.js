﻿function updateBatteryRemovalsSparkline(siteId, divElement) {
    getBatteryRemovalsfromAjax(siteId, divElement);
}
function getBatteryRemovalsfromAjax(siteId, divElement) {
    
    var params = ['SiteID: ' + siteId];
    //$.ajax({
    //    url: apiUrl,
    //    method: 'POST',
    //    contentType: contentType,
    //    data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetBatteryRemovals]', paramsStr: params }),
    //    dataType: 'json',
    //    success: function (dataJson) {
    //        makeBatteryRemovalsSparkLineChart(dataJson, divElement);
    //    },
    //    error: function (xhr, ajaxOption, thrownError) {
    //        console.log(thrownError);
    //        alert('prcDashGetBatteryRemovals returned error =[' + xhr.responseText + ']');
    //    }
    //});
    $.ajax({
        url: apiUrl,
        method: 'POST',
        contentType: contentType,
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetBatteryRemovals]', paramsStr: params }),
        dataType: 'json',
        success: function (dataJson) {
            makeBatteryRemovalsSparkLineChart(dataJson, divElement);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.prcDashGetBatteryRemovals returned error =[' + xhr.responseText + ']');
        }
    });
}

function makeBatteryRemovalsSparkLineChart(jsonData, divElement) {
    var sparklineChartData = mapBatteryRemovalsSparkLineData(jsonData);
    $('#' + divElement).sparkline(
        sparklineChartData.barDataset,
        {
            type: 'bar',
            height: 82,
            barColor: '#fc8973',
            barWidth: '10px',
            width: '100%',
            barSpacing: '5px',
            composite: false,
            fillColor: true
        });

    $('#' + divElement).sparkline(
        sparklineChartData.lineDataset,
      {
          type: 'line',
          //width: '100%',
          composite: true,
          fillColor: false,
          spotColor: '#fafafa',
          minSpotColor: '#fafafa',
          maxSpotColor: '#fafafa',
          spotRadius: 3,
          highlightSpotColor: '#fff',
          lineWidth: 2,
          lineColor: 'white'
      });

};


function mapBatteryRemovalsSparkLineData(rawSparklineData) {
    var barDataset = [];
    var lineDataset = [];
    $.each(rawSparklineData, function () {
        barDataset.push(this.BatteryRemovals);
    });

    $.each(rawSparklineData, function () {
        lineDataset.push(this.BatteryRemovals);
    });

    var sparklineData = { barDataset: barDataset, lineDataset: lineDataset };
    return sparklineData;
};
