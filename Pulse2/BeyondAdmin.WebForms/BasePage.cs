﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BeyondAdmin.API;

namespace BeyondAdmin.WebForms
{
    public class BasePage : System.Web.UI.Page
    {
        private static string _redirectUrl;
      
	public static string RedirectUrl
	{
	    get { return _redirectUrl; }
	    set { _redirectUrl = value; }
	}

    public BasePage()
	{
        _redirectUrl = "~/Account/Login.aspx";
	}
	 
	override protected void OnInit(EventArgs e)
	{
	    //initialize our base class (System.Web,UI.Page)
	    base.OnInit(e);

        string fromPage = Page.AppRelativeVirtualPath.ToLower();
        if (fromPage != _redirectUrl.ToLower() && Context.Session != null )
	    {
            if (PulseSession.Instance.User == null && !Page.IsCallback)
            {
                string ClientQueryString = Page.ClientQueryString.ToString();
                string redir = ClientQueryString != string.Empty ? Page.AppRelativeVirtualPath + "?" + ClientQueryString : Page.AppRelativeVirtualPath;

                Response.Redirect(_redirectUrl + "?redir=" + redir);
            }
        }
	}

     protected Control FindControl(Control parent, string name)
    {
        foreach (Control c in parent.Controls)
        {
            if (c.ID == name)
                return c;
            Control ctl = FindControl(c, name);
            if (ctl != null)
                return ctl;
        }
        return null;

    }
    }
}


