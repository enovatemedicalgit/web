﻿function updatedevicePacketsPerHourSparkline(siteId, divElement, avgElement) {
    getdevicePacketsPerHourfromAjax(siteId, divElement, avgElement);
}
function getdevicePacketsPerHourfromAjax(siteId, divElement, avgElement) {

    var params = ['SiteID: ' + siteId];
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetdevicePacketsPerHourSummary]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        xhrFields: {
            withCredentials: false
        },
        success: function (dataJson) {
            makedevicePacketsPerHourSparkLineChart(dataJson, divElement, avgElement);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.sp_DashGetdevicePacketsPerHourSummary returned error =[' + xhr.responseText + ']');
        }
    });
}

function makedevicePacketsPerHourSparkLineChart(jsonData, divElement, avgElement) {
    var sparklineChartData = mapdevicePacketsPerHourSparkLineData(jsonData);
    $('#' + divElement).sparkline(
        sparklineChartData.lineDataset,
     
      {
          type: 'line',
          width: '100%',
          height: '125px',
          composite: false,
          fillColor: false,
          spotColor: '#fafafa',
          minSpotColor: '#fafafa',
          maxSpotColor: '#8cc474',
          spotRadius: 2,
          highlightSpotColor: '#8cc474',
          highlightLineColor: '#8cc474',
          lineWidth: 2,
          //lineColor: 'themefourthcolor'
          lineColor: 'green'
      });

    $("#" + avgElement).text(sparklineChartData.packetTotals.toFixed(0).toString());
    console.log("test");
};

function mapdevicePacketsPerHourSparkLineData(rawSparklineData) {
    var barDataset = [];
    var lineDataset = [];
    var packetTotals = 0

    $.each(rawSparklineData, function () {
        barDataset.push(this.PacketsPerHour);
        packetTotals += this.PacketsPerHour;
    });

    $.each(rawSparklineData, function () {
        lineDataset.push(this.PacketsPerHour);
    });

    var sparklineData = { barDataset: barDataset, lineDataset: lineDataset, packetTotals: packetTotals };
    return sparklineData;
};
