﻿function updateHeatmap(siteId, type) {
    getheatmapfromAjax(siteId, type);
}
//function createCORSRequest(method, url) {
//    var xhr = new XMLHttpRequest();
//    if ("withCredentials" in xhr) {
//        // XHR has 'withCredentials' property only if it supports CORS
//        xhr.open('POST', apiUrl, true);
//    } else if (typeof XDomainRequest != "undefined") { // if IE use XDR
//        xhr = new XDomainRequest();
//        xhr.open('POST', apiUrl);
//    } else {
//        xhr = null;
//    }
//    return xhr;
//}


function getheatmapfromAjax(siteId, type) {

    var params = ['SiteID: ' + siteId];

    $.ajax({
        url: apiUrl,
        method: 'POST',
        crossDomain: true,
        //beforeSend: function (xmlHttpRequest) {
        //    xmlHttpRequest.withCredentials = true;
        //},
        data: JSON.stringify({ storedProcName: '[spDashboardBatteryChargeLevels_test]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        success: function (dataJson) {
            makeHeatmapChart(dataJson);

        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.spDashboardBatteryChargeLevels_test returned error =[' + xhr.responseText + ']');
        }
    });


  
}


function mapHeatMapData(dataJson) {
    var heatMapData = [];
    var divisor = Math.floor(dataJson.length / 3.27);
    var x = 0;
    var y = 1;
    for (var i = 0; i < dataJson.length - 1; i++) {
        x++;
        if (x > divisor) {
            y++;
            x = 1;
        }
        heatMapData.push({ heat: dataJson[i].heat, symbol: dataJson[i].symbol, x: x, y: y });
    }
    return heatMapData;
}
function makeHeatmapChart(jsonData) {
    var heatMapSource = mapHeatMapData(jsonData);
    var colors = ["#92EE2B", "#AFEF27", "#FFC903", "#FFFC02", "#CEFF01", "#9BFF01", "#67FF00", "#33FF00", "#00FF00"]; // alternatively colorbrewer.YlGnBu[9]

    var dataset = anychart.data.set(heatMapSource);
    var chart = anychart.heatMap(heatMapSource);

    var colorScale = anychart.scales.ordinalColor();
    // set color for all points
    colorScale.ranges([
        { less: 0.35, color: "#FF0000" },
        { from: 0.35, to: 0.50, color: "#FFC903" },
        { from: 0.50, to: 0.55, color: "#FFC903" },
        { from: 0.55, to: 0.60, color: "#FFC903" },
        { from: 0.60, to: 0.65, color: "#FFC903" },
        { from: 0.65, to: 0.70, color: "#FFFC02" },
        { from: 0.70, to: 0.75, color: "#FFFC02" },
        { from: 0.75, to: 0.80, color: "#CEFF01" },
        { from: 0.80, to: 0.85, color: "#9BFF01" },
        { from: 0.85, to: 0.90, color: "#67FF00" },
        { from: 0.90, to: 0.95, color: "#33FF00" },
        { greater: .949, color: "#00FF00" }
    ]);
    chart.colorScale(colorScale);

    // Sets selection mode for single selection
    chart.interactivity().selectionMode("none");


    chart.padding([1, 1, 1, 1])
   .title()
   .useHtml(true)
   .enabled(true)
   .padding([1, 1, 1, 1])
   .align('center')
   .text("Capacity <style='font-size: 10px; color:#B9B9B9'> </span>");

    // Sets chart labels
    chart.labels().enabled(true).maxFontSize(8).textFormatter(function () {
        return this.getDataValue('symbol');
    });

    // Turns off axes
    chart.yAxis(null);
    chart.xAxis(null);

    // Sets chart and hover chart settings
    chart.stroke('#fff');
    chart.hoverStroke('6 #fff');
    chart.hoverFill('#5DADE2');
    chart.hoverLabels().fontColor('#fff');

    // Sets legend
    chart.legend()
        .align('top')
        .position('bottom')
        .itemsLayout('h')
        .padding([2, 5, 0, 5])
        .enabled(false).tooltip().enabled(false);

    // Sets tooltip
    chart.tooltip()
        .titleFormatter(function () {
            var s = '';
            var serialNo = this.getDataValue('symbol');
            //if (this.getDataValue('heat') === 's') s = 'suited';
            //else if (this.getDataValue('symbol') === 'u') s = 'low';
            //return this.x + ',' + this.y + ' ' + serialNo;
            return serialNo + " (" + this.x + ',' + this.y + ")";
        })
        .textFormatter(function () {
            return 'Capacity Level: ' + (this.heat * 100) + '%';
        });



    // create horizontal chart scroller
    //var xScroller = chart.xScroller();
    // scroller settings
    //  xScroller.enabled(true);

    // enables zoom on chart by default
    var xZoom = chart.xZoom();
    // define the number of visible points at the same time
    xZoom.setToPointsCount(900);

    // create vertical chart scroller
    var yScroller = chart.yScroller();
    // scroller settings
    yScroller.enabled(false);

    // enables zoom on chart by default
    var yZoom = chart.yZoom();
    // define the number of visible points at the same time
    yZoom.setToPointsCount(900);

    chart.container("chartheat");
    chart.bounds(0, 5, "99%", "250px");
    chart.draw();



};
