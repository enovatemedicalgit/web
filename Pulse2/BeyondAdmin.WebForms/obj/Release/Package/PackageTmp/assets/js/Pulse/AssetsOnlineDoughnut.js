﻿function updateAssetsOnline(siteId, piechartSource, totalsSource, monitoredSource) {
    getAssetsOnlineDoughnutfromAjax(siteId, totalsSource, monitoredSource);
}

function getAssetsOnlineDoughnutfromAjax(siteId, totalsSource, monitoredSource) {
    var params = ['SiteID: ' + siteId];
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.prcDashGetSiteAssetsSummary', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        success: function (dataJson) {
            makeAssetsOnlineDoughnutChart(dataJson, totalsSource, monitoredSource);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('dbo.sp_DashGetSiteAssetsSummary returned error ' + xhr.responseText);
        }
    });
}


function makeAssetsOnlineDoughnutChart(jsonData, totalsSource, monitoredSource) {
    var doughnutData = mapAssetsOnlineDoughnutData(jsonData);
    //var ctx = document.getElementById('Doughnutchart-area').getContext('2d');
    //window.myDoughnut = new Chart(ctx).Doughnut(doughnutData);

    $("#" + totalsSource).text(totalAssets.toString());
    $("#" + monitoredSource).text("Total Monitored Assets: " + totalAssets.toString());
    var placeholder = $("#dashboard-pie-chart-sources");
    placeholder.unbind();

    $.plot(placeholder, doughnutData, {
        series: {
            pie: {
                innerRadius: .30,
                show: true,
                radius: 3 / 4,
                stroke: {
                    width: 4
                },
                combine: { threshold: 0.1, color: '#999' },
                label: {
                    show: true,
                    radius: 1,
                    threshold: 0.1,
                    formatter: function (label, series) {
                        //return "<div style='font-size:10pt; text-align:center; padding:1px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                        return "<div style='font-size:10pt; font-weight:bold; color=white; text-align:center; padding:1px; '>" + Math.round(series.percent) + "% </div>";
                    },
                    //background: {opacity: 0.5}
                }
            }
        },
        legend: {
            show: true,
            label: {
                show: true,
                labelFormatter: legendFormatter
            },
            container: $('#AssetsOnlineLegend')
        }
    });
}

function legendFormatter(label, series) {
        return "<div style='font-size:14pt font-weight:bold; text-align:center; padding:1px; '>" + label + " = " + Math.round(series.percent) + "%</div>";
}


var randomColorFactor = function () {
    return Math.round(Math.random() * 255);
};
var randomColor = function () {
    return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
}


function mapAssetsOnlineDoughnutData(rawDoughnutData) {
    var data = [],
			series = Math.floor(Math.random() * 6) + 3;

    for (var i = 0; i < series; i++) {
        data[i] = {
            label: "Series" + (i + 1),
            data: Math.floor(Math.random() * 100) + 1
        }
    }
    totalAssets = 0;
    $.each(rawDoughnutData, function() {
        var p = {
            //color: randomColor(),
            //data: {
                label: this.category, data:  this.cnt
                //data: { label: this.category, data:[1, this.cnt]
    //}
};
        AssetsReportingdoughnutData.push(p);
        totalAssets += this.cnt;
    });
    return AssetsReportingdoughnutData;
}



//function getAssetsOnlineDoughnutData(siteId) {
//    var params = ['SiteID: ' + siteId];
//    $.ajax({
//        url: apiUrl,
//        method: 'POST',
//        data: JSON.stringify({ storedProcName: 'dbo.sp_DashGetSiteAssetsSummary', paramsStr: params }),
//        contentType: 'application/json; charset-utf-8',
//        dataType: 'json',
//        success: function (dataJson) {
//            makeAssetsOnlineDoughnutChart(dataJson);
//        },
//        error: function (xhr, ajaxOption, thrownError) {
//            console.log(thrownError);
//            alert('dbo.sp_DashGetSiteAssetsSummary returned error ' + thrownError);
//        }
//    });
//}
