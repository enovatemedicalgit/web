﻿function updateWorkstationsMovedSparkline(siteId, divElement) {
    getWorkstationsMovedfromAjax(siteId);
}
function getWorkstationsMovedfromAjax(siteId, divElement) {

    var params = ['SiteID: ' + siteId];
    $.ajax({
        url: apiUrl,
        method: 'POST',
        data: JSON.stringify({ storedProcName: 'dbo.[prcDashGetWorkstationsMoved]', paramsStr: params }),
        contentType: 'application/json; charset-utf-8',
        dataType: 'json',
        xhrFields: {
            withCredentials: false
        },
        success: function (dataJson) {
            makeWorkstationsMovedSparkLineChart(dataJson, divElement);
        },
        error: function (xhr, ajaxOption, thrownError) {
            console.log(thrownError);
            alert('sp_DashGetWorkstationsMoved returned error =[' + xhr.responseText + ']');
        }
    });
}

function makeWorkstationsMovedSparkLineChart(jsonData, divElement) {
    var sparklineChartData = mapWorkstationsMovedSparkLineData(jsonData);
    $('#' + divElement).sparkline(
        sparklineChartData.barDataset,
        {
            type: 'bar',
            height: 82,
            barColor: '#3bcbef',
            barWidth: '10px',
            width: '100%',
            barSpacing: '5px',
            composite: false,
            fillColor: true
        });

    var sp = $('#' + divElement).sparkline(
        sparklineChartData.lineDataset,
      {
          type: 'line',
          //width: '100%',
          composite: true,
          fillColor: false,
          spotColor: '#fafafa',
          minSpotColor: '#fafafa',
          maxSpotColor: '#fafafa',
          spotRadius: 3,
          highlightSpotColor: '#fff',
          lineWidth: 2,
          lineColor: 'white'
      });

};


function mapWorkstationsMovedSparkLineData(rawSparklineData) {
    var barDataset = [];
    var lineDataset = [];
    $.each(rawSparklineData, function () {
        barDataset.push(this.WorkstationsMoved);
    });

    $.each(rawSparklineData, function () {
        lineDataset.push(this.WorkstationsMoved);
    });

    var sparklineData = { barDataset: barDataset, lineDataset: lineDataset };
    return sparklineData;
};
