﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BeyondAdmin.WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col=lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical">
                        <div class="databox-top bg-palegreen no-padding">
                            <div class="databox-stat white bg-palegreen font-120">
                                <i class="stat-icon fa fa-caret-down icon-xlg"></i>
                            </div>
                            <div class="horizontal-space no-padding space-sm"></div>
                            <div class="databox-sparkline no-margin">
                                <span id="AmpsDrawnSummarySparkline" data-sparkline="compositebar" data-height="82px" data-width="100%"
                                    data-barcolor="#b0dc81"
                                    data-barwidth="10px" data-barspacing="5px"
                                    data-fillcolor="false" data-linecolor="#fff" data-spotradius="3" data-linewidth="2"
                                    data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#fff"
                                    data-highlightspotcolor="#fff" data-highlightlinecolor="#fff"
                                    data-composite="7, 6, 5, 7, 9, 10, 8, 7">8,4,1,2,4,6,2,8
                                </span>
                            </div>
                        </div>
                        <div class="databox-bottom no-padding">
                            <div class="databox-row">
                                <div class="databox-cell cell-10 text-align-left">
                                    <span class="databox-text"><i class="fa fa-flash"></i>Amp Draw Yesterday</span>
                                    <span id="AmpDraw" class="databox-number">2.97 Avg</span>
                                </div>
                                <div class="databox-cell cell-2 text-align-right">
                                    <span class="databox-text">Avg</span>
                                    <span class="databox-number font-70">2.87 </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col=lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical">
                        <div class="databox-top bg-orange no-padding">
                            <div class="databox-stat white bg-orange font-120">
                                <i class="stat-icon fa fa-caret-up icon-xlg"></i>
                            </div>
                            <div class="horizontal-space space-sm"></div>
                            <div class="databox-sparkline no-margin">
                                <%--  data-barcolor="#fb7d64"--%>
                                <span id="BatteriesUsedSparkline" data-sparkline="compositebar" data-height="82px" data-width="100%"
                                    data-barcolor="#fc8973"
                                    data-barwidth="10px" data-barspacing="5px"
                                    data-fillcolor="false" data-linecolor="#fff" data-spotradius="3" data-linewidth="2"
                                    data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#fff"
                                    data-highlightspotcolor="#fff" data-highlightlinecolor="#fff"
                                    data-composite="7, 6, 5, 7, 9, 10, 8, 6">7, 6, 5, 7, 9, 10, 8, 6
                                </span>
                            </div>
                        </div>
                        <div class="databox-bottom no-padding">
                            <div class="databox-row">
                                <div class="databox-cell cell-8 text-align-left">
                                    <span class="databox-text"><i class="fa fa-flash"></i>Used Yesterday</span>
                                    <span id="BatteriesUsed" class="databox-number">132</span>
                                </div>
                                <div class="databox-cell cell-4 text-align-right">
                                    <span class="databox-text">Avg</span>
                                    <span class="databox-number font-70">152</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col=lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical">
                        <div class="databox-top bg-azure">
                            <div class="databox-stat white bg-azure font-120">
                                <i class="stat-icon fa fa-caret-up icon-xlg"></i>
                            </div>
                            <div class="horizontal-space space-sm"></div>
                            <div class="databox-sparkline no-margin">
                                <span id="WorkstationsMovedSparkline" data-sparkline="compositebar" data-height="82px" data-width="100%"
                                    data-barcolor="#3bcbef"
                                    data-barwidth="10px" data-barspacing="5px"
                                    data-fillcolor="false" data-linecolor="#fff" data-spotradius="3" data-linewidth="2"
                                    data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#fff"
                                    data-highlightspotcolor="#fff" data-highlightlinecolor="#fff"
                                    data-composite="48,44,41,42,44,46,42,44,47,48">48,44,41,42,44,46,42,44,48,48
                                </span>
                            </div>
                        </div>
                        <div class="databox-bottom no-padding">
                            <div class="databox-row">
                                <div class="databox-cell cell-10 text-align-left">
                                    <span class="databox-text" ><i class="fa fa-laptop"></i>Moved Yesterday</span>
                                    <span id="WorkstationsMoved" class="databox-number">48</span>
                                </div>
                                <div class="databox-cell cell-2 text-align-left">
                                    <span class="databox-text">Avg</span>
                                    <span class="databox-number font-70">72</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col=lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical">
                        <div class="databox-top bg-orange">
                            <div class="databox-stat white bg-orange font-120">
                                <i class="stat-icon fa fa-caret-up icon-xlg"></i>
                            </div>
                            <div class="horizontal-space space-sm"></div>
                            <div class="databox-sparkline no-margin">
                                <span id="BatteryRemovalsSparkline" data-sparkline="compositebar" data-height="82px" data-width="100%"
                                    data-barcolor="#fc8973"
                                    data-barwidth="10px" data-barspacing="5px"
                                    data-fillcolor="false" data-linecolor="#fff" data-spotradius="3" data-linewidth="2"
                                    data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#fff"
                                    data-highlightspotcolor="#fff" data-highlightlinecolor="#fff"
                                    data-composite="8,4,1,2,6,2,4">7, 6, 5, 7, 9, 10, 8, 7,2,4
                                </span>
                            </div>
                        </div>

                        <div class="databox-bottom no-padding">
                            <div class="databox-row">
                                <i class="stat-icon fa fa-caret-up icon-xlg"></i>
                                <div id="" class="databox-cell cell-10 text-align-left">
                                    <span class="databox-text">Battery Inefficiencies</span>
                                    <span id="highlowinserts" class="databox-number">7985</span>
                                </div>
                                <div class="databox-cell cell-2 text-align-left">
                                    <span class="databox-text">Avg</span>
                                    <span class="databox-number font-50">12</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                    <div class="databox bg-white radius-bordered">
                        <div class="databox-left no-padding">
                            <img src="assets/img/avatars/GORDONWAID.jpg" style="width: 65px; height: 65px;">
                        </div>
                        <div class="databox-right padding-top-20">
                            <div class="databox-stat palegreen">
                                <i class="stat-icon icon-xlg fa fa-phone"></i>615-895-4525
                            </div>
                            <div class="databox-text darkgray">Sales Rep: Doug Gallacher</div>
                            <asp:HyperLink runat="server"> <i class="stat-icon icon-xlg fa fa-phone"></i>Doug.Gallacher@EnovateMedical.com</asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="dashboard-box">
                        <div class="box-header">
                            <div id="totalMonitoredAssets" class="deadline">
                                Total Monitored Assets: 1209
                            </div>
                        </div>
                        <div class="box-progress">
                            <div class="progress-handle">Online</div>
                            <div class="progress progress-xs progress-no-radius bg-whitesmoke">
                                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                </div>
                            </div>
                        </div>
                        <div class="box-tabbs">
                            <div class="tabbable">
                                <ul class="nav nav-tabs tabs-flat nav-justified" id="myTab11">
                                    <li class="active">
                                        <a data-toggle="tab" id="contacttab" href="#bandwidth" onclick="clickBatteryHealth()">Battery Health
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#realtime">Pulse Network
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#sales" onclick="clickAssetStatus()">Asset Status
                                        </a>
                                    </li>

                                    <li>
                                        <a data-toggle="tab" href="#visits">Workstation Usage
                                        </a>
                                    </li>



                                </ul>
                                <div class="tab-content tabs-flat no-padding">
                                    <div id="realtime" class="tab-pane active padding-5 animated fadeInUp">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="dashboard-chart-realtime" class="chart chart-lg no-margin"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="visits" class="tab-pane  animated fadeInUp">
                                        <div class="row">
                                            <div class="col-lg-12 chart-container">
                                                <div id="dashboard-chart-visits" class="chart chart-lg no-margin" style="width: 100%"></div>
                                            </div>
                                        </div>

                                    </div>

                                    <div id="bandwidth" class="tab-pane padding-10 animated fadeInUp">
                                        <div class="control-group">
                                            <div class="radio">
                                                <label>
                                                    <input checked="checked" <%--onclick="rdoBattClicked()"--%> name="batt" id="rdoBattHealth" type="radio" class="colored-blue">
                                                    <span class="text">Health     </span>
                                                </label>
                                                <label>
                                                    <input name="batt" <%--onclick="rdoBattClicked()" --%>id="rdoBattCharge" type="radio" class="colored-danger">
                                                    <span class="text">Charge</span>
                                                </label>
                                            </div>
                                            </div>
                                            <div id="chartheat" style="height: 250px"></div>
                                            <%-- <div class="databox-sparkline bg-themeprimary">
                                           <%-- <span id="dashboard-bandwidth-chart" data-sparkline="compositeline" data-height="250px" data-width="100%" data-linecolor="#fff" data-secondlinecolor="#eee"
                                                data-fillcolor="rgba(255,255,255,.1)" data-secondfillcolor="rgba(255,255,255,.25)"
                                                data-spotradius="0"
                                                data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#ffce55"
                                                data-highlightspotcolor="#fff" data-highlightlinecolor="#fff"
                                                data-linewidth="2" data-secondlinewidth="2"
                                                data-composite="500, 400, 100, 450, 300, 200, 100, 200">300,300,400,300,200,300,300,200
                                            </span>
                                        </div>--%>
                                        </div>
                                        <div id="sales" class="tab-pane animated fadeInUp no-padding-bottom" style="padding: 5px 5px 0 20px;">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="databox databox-xlg databox-vertical databox-inverted databox-shadowed">
                                                        <div class="databox-top">
                                                            <div class="databox-sparkline">
                                                                <span id="AmpsDrawnPerHourSparkline" data-sparkline="line" data-height="125px" data-width="100%" data-fillcolor="false" data-linecolor="themesecondary"
                                                                    data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#ffce55"
                                                                    data-highlightspotcolor="#ffce55" data-highlightlinecolor="#ffce55"
                                                                    data-linewidth="1.5" data-spotradius="2">1.2,2.0,4.0,3.0,5.0,6.0,8.0,7.0,11.0,14.0,11.0,1.9
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="databox-bottom no-padding text-align-center">
                                                            <span id="ampsAvgDrawPerHour" class="databox-number lightcarbon no-margin">224</span>
                                                            <span class="databox-text lightcarbon no-margin">Amps Drawn / Hour</span>

                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="databox databox-xlg databox-vertical databox-inverted databox-shadowed">
                                                        <div class="databox-top">
                                                            <div class="databox-sparkline">
                                                                <span  id="DevicePacketsPerHourSparkLine" data-sparkline="line" data-height="125px" data-width="100%" data-fillcolor="false" data-linecolor="themefourthcolor"
                                                                    data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#8cc474"
                                                                    data-highlightspotcolor="#8cc474" data-highlightlinecolor="#8cc474"
                                                                    data-linewidth="1.5" data-spotradius="2">100,208,450,298,450,776,234,680,1100,1400,1000,1200
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="databox-bottom no-padding text-align-center">
                                                            <span id="devicePacketsTotal" class="databox-number lightcarbon no-margin">7063</span>
                                                            <span class="databox-text lightcarbon no-margin">Device Packets / Hour</span>

                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="databox databox-xlg databox-vertical databox-inverted databox-shadowed">
                                                        <div class="databox-top">
                                                            <div class="databox-piechart">
                                                                <div  id="FullyChargedDoughnut" data-toggle="easypiechart" class="easyPieChart block-center"
                                                                    data-barcolor="themeprimary" data-linecap="butt" data-percent="80" data-animate="500"
                                                                    data-linewidth="8" data-size="125" data-trackcolor="#eee">
                                                                    <span class="font-200"><i class="fa fa-gift themeprimary"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="databox-bottom no-padding text-align-center">
                                                            <span id="FullyChargedDoughnutSummary" class="databox-number lightcarbon no-margin">29</span>
                                                            <span class="databox-text lightcarbon no-margin">Fully Charged Batteries</span>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                    <div class="databox databox-xlg databox-vertical databox-inverted  databox-shadowed">
                                                        <div class="databox-top">
                                                            <div class="databox-piechart">
                                                                <div id="AssetsNeedUpdateDoughnut"  data-toggle="easypiechart" class="easyPieChart block-center"
                                                                    data-barcolor="themethirdcolor" data-linecap="butt" data-percent="40" data-animate="500"
                                                                    data-linewidth="8" data-size="125" data-trackcolor="#eee">
                                                                    <span class="white font-200"><i class="fa fa-tags themethirdcolor"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="databox-bottom no-padding text-align-center">
                                                            <span id="AssetsNeedUpdateDoughnutSummary" class="databox-number lightcarbon no-margin">11</span>
                                                            <span class="databox-text lightcarbon no-margin">Assets Need Firmware Updates</span>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-visits">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-xs-12">
                                        <div class="notification">
                                            <div class="clearfix">
                                                <div class="notification-icon">
                                                    <i class="fa fa-signal palegreen bordered-1 bordered-palegreen"></i>
                                                </div>
                                                <div class="notification-body">
                                                    <span class="title">New Workstation Commissioned</span>
                                                    <span class="description">06:30 pm</span>
                                                </div>
                                                <div class="notification-extra">

                                                    <i class="fa fa-clock-o palegreen"></i>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-xs-12">
                                        <div class="notification">
                                            <div class="clearfix">
                                                <div class="notification-icon">
                                                    <i class="fa fa-signal azure bordered-1 bordered-azure"></i>
                                                </div>
                                                <div class="notification-body">
                                                    <span class="title">Charger 311445420043 Updated</span>
                                                    <span class="description">03:30 pm</span>
                                                </div>
                                                <div class="notification-extra">
                                                    <i class="fa fa-clock-o azure"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="notification">
                                            <div class="clearfix">
                                                <div class="notification-icon">
                                                    <i class="fa fa-phone bordered-1 bordered-orange orange"></i>
                                                </div>
                                                <div class="notification-body">
                                                    <span class="title">Pulse Webinar With Jason Batts</span>
                                                    <span class="description">01:00 pm</span>
                                                </div>
                                                <div class="notification-extra">
                                                    <i class="fa fa-clock-o orange"></i>

                                                </div>
                                            </div>
                                        </div>
                                 
                                    </div>
                                    </div>
                            </div>
                             
                                        <ul id="ticker01" style="width: 70%">
                                            <li>...</li>
                                        </ul>
                              
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="orders-container">
                    <div class="orders-header">
                        <h6>Recent Cases</h6>
                    </div>
                    <ul class="orders-list">

                        <li class="order-item">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                    <div class="item-booker">Backup Battery Disconnected</div>
                                    <div class="item-time">
                                        <i class="fa fa-calendar"></i>
                                        <span>10 minutes ago</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                    <div class="item-price">
                                        <span class="currency">New</span>
                                    </div>
                                </div>
                            </div>
                            <a class="item-more" href="">
                                <i></i>
                            </a>
                        </li>
                        <li class="order-item top">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                    <div class="item-booker">Battery at 72% Capacity</div>
                                    <div class="item-time">
                                        <i class="fa fa-calendar"></i>
                                        <span>2 hours ago</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                    <div class="item-price">
                                        <span class="price">In Process</span>
                                    </div>
                                </div>
                            </div>
                            <a class="item-more" href="">
                                <i></i>
                            </a>
                        </li>
                        <li class="order-item">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                    <div class="item-booker">Battery Cell Imbalance Detected</div>
                                    <div class="item-time">
                                        <i class="fa fa-calendar"></i>
                                        <span>Today 8th July</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                    <div class="item-price">
                                        <span class="price">Resolved</span>
                                    </div>
                                </div>
                            </div>
                            <a class="item-more" href="">
                                <i></i>
                            </a>
                        </li>
                        <li class="order-item">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                    <div class="item-booker">Charger Fan Malfunction</div>
                                    <div class="item-time">
                                        <i class="fa fa-calendar"></i>
                                        <span>July 7th</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                    <div class="item-price">

                                        <span class="price">Resolved</span>
                                    </div>
                                </div>
                            </div>
                            <a class="item-more" href="">
                                <i></i>
                            </a>
                        </li>
                        <li class="order-item">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                    <div class="item-booker">DC1 Short on Workstation</div>
                                    <div class="item-time">
                                        <i class="fa fa-calendar"></i>
                                        <span>April 23rd</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                    <div class="item-price">

                                        <span class="price">Closed</span>
                                    </div>
                                </div>
                            </div>
                            <a class="item-more" href="">
                                <i></i>
                            </a>
                        </li>
                    </ul>
                    <div class="orders-footer">
                        <a class="show-all" href=""><i class="fa fa-angle-down"></i>Show All</a>
                        <div class="help">
                            <a href=""><i class="fa fa-question"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    <div class="row">
        <div class="col-lg-4 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bordered-bottom bordered-themeprimary">
                    <i class="widget-icon fa fa-tasks themeprimary"></i>
                    <span class="widget-caption themeprimary">Recent Notifications</span>
                </div>
                <!--Widget Header-->
                <div class="widget-body no-padding">
                    <div class="task-container">
                        <div class="task-search">
                            <span class="input-icon">
                                <input type="text" class="form-control" placeholder="Search Tasks">
                                <i class="fa fa-search gray"></i>
                            </span>
                        </div>
                        <ul class="tasks-list">
                            <li class="task-item">
                                <div class="task-check">
                                    <label>
                                        <input type="checkbox">
                                        <span class="text"></span>
                                    </label>
                                </div>
                                <div class="task-state">
                                    <span class="label label-yellow">In Progress
                                    </span>
                                </div>
                                <div class="task-time">1 hour ago</div>
                                <div class="task-body">Mobius Backup Battery Depleted or Disconnected</div>
                                <div class="task-creator"><a href="">Pulse Generated</a></div>
                                <div class="task-assignedto">311458678548</div>
                            </li>
                            <li class="task-item">
                                <div class="task-check">
                                    <label>
                                        <input type="checkbox">
                                        <span class="text"></span>
                                    </label>
                                </div>
                                <div class="task-state">
                                    <span class="label label-orange">Active
                                    </span>
                                </div>
                                <div class="task-time">2 hours ago</div>
                                <div class="task-body">4.0 Mobius Battery at 72% Capacity - Consider Replacing Soon</div>
                                <div class="task-creator"><a href="">Pulse Generated</a></div>
                                <div class="task-assignedto">31170135484851</div>
                            </li>
                            <li class="task-item">
                                <div class="task-check">
                                    <label>
                                        <input type="checkbox">
                                        <span class="text"></span>
                                    </label>
                                </div>
                                <div class="task-state">
                                    <span class="label label-palegreen">Approved
                                    </span>
                                </div>
                                <div class="task-time">yesterday</div>
                                <div class="task-body">Mobius 3.5 Battery out of warranty at 74% Capacity</div>
                                <div class="task-creator"><a href="">Pulse Generated</a></div>
                                <div class="task-assignedto">3118013574895</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--Widget Body-->
            </div>

        </div>
        <div class="col-lg-8 col-sm-12 col-xs-12">
            <div class="row">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="widget">
                            <!--Widget Header-->
                            <div class="widget-body  no-padding">
                                <div class="col-lg-12">
                                    <div class="databox databox-xxlg databox-vertical databox-shadowed bg-white radius-bordered padding-5">
                                        <div class="databox-top">
                                            <div class="databox-row row-12">
                                                <div class="databox-cell cell-3 text-center">
                                                    <div  id="AssetsOnlineCount" class="databox-number number-xxlg sonic-silver">1209</div>
                                                    <div class="databox-text storm-cloud">Assets in Inventory</div>
                                                </div>
                                                <div class="databox-cell cell-9 text-align-center">
                                                    <div class="databox-row row-6 text-left">
                                                        <span class="badge badge-palegreen badge-empty margin-left-5"></span>
                                                        <span class="databox-inlinetext uppercase darkgray margin-left-5">Reporting</span>
                                                        <span class="badge badge-yellow badge-empty margin-left-5"></span>
                                                        <span class="databox-inlinetext uppercase darkgray margin-left-5">Not Reporting</span>
                                                    </div>
                                                    <div class="databox-row row-6">
                                                        <div class="progress bg-yellow progress-no-radius">
                                                            <div class="progress-bar progress-bar-palegreen" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="databox-bottom">
                                            <div class="databox-row row-12">
                                                <div class="databox-cell cell-7 text-center  padding-5">
                                                    <div id="dashboard-pie-chart-sources" class="chart"></div>
                                                </div>
                                                <div id="AssetsOnlineLegend" class="databox-cell cell-5 text-center no-padding-left padding-bottom-30">
                                                </div>
                                                <%--<div class="databox-cell cell-5 text-center no-padding-left padding-bottom-30">
                                                    <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                        <span class="databox-text sonic-silver pull-left no-margin">Type</span>
                                                        <span class="databox-text sonic-silver pull-right no-margin uppercase">PCT</span>
                                                    </div>
                                                    <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                        <span class="badge badge-blue badge-empty pull-left margin-5"></span>
                                                        <span class="databox-text darkgray pull-left no-margin hidden-xs">Mobius 4.0 Batteries</span>
                                                        <span class="databox-text darkgray pull-right no-margin uppercase">46%</span>
                                                    </div>
                                                    <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                        <span class="badge badge-orange badge-empty pull-left margin-5"></span>
                                                        <span class="databox-text darkgray pull-left no-margin hidden-xs">Mobius Workstations</span>
                                                        <span class="databox-text darkgray pull-right no-margin uppercase">21%</span>
                                                    </div>
                                                    <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                        <span class="badge badge-pink badge-empty pull-left margin-5"></span>
                                                        <span class="databox-text darkgray pull-left no-margin hidden-xs">PowerVar EMC Workstations</span>
                                                        <span class="databox-text darkgray pull-right no-margin uppercase">12%</span>
                                                    </div>
                                                    <div class="databox-row row-2 bordered-bottom bordered-ivory padding-10">
                                                        <span class="badge badge-palegreen badge-empty pull-left margin-5"></span>
                                                        <span class="databox-text darkgray pull-left no-margin hidden-xs">4 Bay Chargers</span>
                                                        <span class="databox-text darkgray pull-right no-margin uppercase">11%</span>
                                                    </div>
                                                    <div class="databox-row row-2 padding-10">
                                                        <span class="badge badge-yellow badge-empty pull-left margin-5"></span>
                                                        <span class="databox-text darkgray pull-left no-margin hidden-xs">2 Bay Chargers</span>
                                                        <span class="databox-text darkgray pull-right no-margin uppercase">10%</span>
                                                    </div>

                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <%-- <div class="tickets-container">
                                <ul class="tickets-list">
                                    <li class="ticket-item">
                                        <div class="row">
                                            <div class="ticket-user col-lg-2 col-sm-12">
                                                <img src="assets/img/avatars/adam-jansen.jpg" class="user-avatar">
                                                <span class="user-name">Jason Batts</span>
                                                <span class="user-at">at</span>
                                                <span class="user-company">Pulse Administrator</span>
                                            </div>
                                            <div class="ticket-time  col-lg-4 col-sm-6 col-xs-12">
                                                <div class="divider hidden-md hidden-sm hidden-xs"></div>
                                                <i class="fa fa-clock-o"></i>
                                                <span class="time">1 Hours Ago</span>
                                            </div>
                                            <div class="ticket-type  col-lg-2 col-sm-6 col-xs-12">
                                                <span class="divider hidden-xs"></span>
                                                <span class="type">Pulse Webinar Request</span>
                                            </div>
                                            <div class="ticket-state bg-palegreen">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="ticket-item">
                                        <div class="row">
                                            <div class="ticket-user col-lg-6 col-sm-12">
                                                <img src="assets/img/avatars/divyia.jpg" class="user-avatar">
                                                <span class="user-name">Clinton Chaffin</span>
                                                <span class="user-at">at</span>
                                                <span class="user-company">Field Services Manager</span>
                                            </div>
                                            <div class="ticket-time  col-lg-4 col-sm-6 col-xs-12">
                                                <div class="divider hidden-md hidden-sm hidden-xs"></div>
                                                <i class="fa fa-clock-o"></i>
                                                <span class="time">3 Hours Ago</span>
                                            </div>
                                            <div class="ticket-type  col-lg-2 col-sm-6 col-xs-12">
                                                <span class="divider hidden-xs"></span>
                                                <span class="type">Backup Battery Replacement</span>
                                            </div>
                                            <div class="ticket-state bg-palegreen">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="ticket-item">
                                        <div class="row">
                                            <div class="ticket-user col-lg-6 col-sm-12">
                                                <img src="assets/img/avatars/Matt-Cheuvront.jpg" class="user-avatar">
                                                <span class="user-name">Noe Marquez</span>
                                                <span class="user-at"> - </span>
                                                <span class="user-company">Technical Services - AIO Repair Specialist</span>
                                            </div>
                                            <div class="ticket-time  col-lg-4 col-sm-6 col-xs-12">
                                                <div class="divider hidden-md hidden-sm hidden-xs"></div>
                                                <i class="fa fa-clock-o"></i>
                                                <span class="time">18 Hours Ago</span>
                                            </div>
                                            <div class="ticket-type  col-lg-2 col-sm-6 col-xs-12">
                                                <span class="divider hidden-xs"></span>
                                                <span class="type">R6 Hard Drive Replacements</span>
                                            </div>
                                            <div class="ticket-state bg-darkorange">
                                                <i class="fa fa-times"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="ticket-item">
                                        <div class="row">
                                            <div class="ticket-user col-lg-6 col-sm-12">
                                                <img src="assets/img/avatars/Sergey-Azovskiy.jpg" class="user-avatar">
                                                <span class="user-name">James Howell</span>
                                                <span class="user-at">at</span>
                                                <span class="user-company">Technical Services Phone Tech</span>
                                            </div>
                                            <div class="ticket-time  col-lg-4 col-sm-6 col-lg-12">
                                                <div class="divider hidden-md hidden-sm hidden-xs"></div>
                                                <i class="fa fa-clock-o"></i>
                                                <span class="time">2 days Ago</span>
                                            </div>
                                            <div class="ticket-type  col-lg-2 col-sm-6 col-xs-12">
                                                <span class="divider hidden-xs"></span>
                                                <span class="type">EMC Tray Cracked</span>
                                            </div>
                                            <div class="ticket-state bg-palegreen">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="ticket-item">
                                        <div class="row">
                                            <div class="ticket-user col-lg-6 col-sm-12">
                                                <img src="assets/img/avatars/John-Smith.jpg" class="user-avatar">
                                                <span class="user-name">Joe Romines</span>
                                                <span class="user-at">at</span>
                                                <span class="user-company">Technical Services Manager</span>
                                            </div>
                                            <div class="ticket-time  col-lg-4 col-sm-6 col-xs-12">
                                                <div class="divider hidden-md hidden-sm hidden-xs"></div>
                                                <i class="fa fa-clock-o"></i>
                                                <span class="time">2 days Ago</span>
                                            </div>
                                            <div class="ticket-type  col-lg-12 col-sm-6 col-xs-12">
                                                <span class="divider hidden-xs"></span>
                                                <span class="type">Field Tech Scheduling</span>
                                            </div>
                                            <div class="ticket-state bg-yellow">
                                                <i class="fa fa-info"></i>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>--%>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <%--   <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="databox databox-xxlg databox-vertical databox-inverted">
                <div class="databox-top bg-whitesmoke no-padding">
                    <div class="databox-row row-2 bg-orange no-padding">
                        <div class="databox-cell cell-1 text-align-center no-padding padding-top-5">
                            <span class="databox-number white"><i class="fa fa-bar-chart-o no-margin"></i></span>
                        </div>
                        <div class="databox-cell cell-8 no-padding padding-top-5 text-align-left">
                            <span class="databox-number white">Low or High Charge Battery Removals</span>
                        </div>
                        <div class="databox-cell cell-3 text-align-right padding-10">
                            <span class="databox-text white">11 August</span>
                        </div>
                    </div>
                    <div class="databox-row row-4">
                        <div class="databox-cell cell-6 no-padding padding-10 padding-left-20 text-align-left">
                            <span class="databox-number orange no-margin">308</span>
                            <span class="databox-text sky no-margin">LAST 30 DAYS</span>
                        </div>
                        <div class="databox-cell cell-2 no-padding padding-10 text-align-left">
                            <span class="databox-number orange no-margin">129</span>
                            <span class="databox-text darkgray no-margin">THIS WEEK</span>
                        </div>
                        <div class="databox-cell cell-2 no-padding padding-10 text-align-left">
                            <span class="databox-number orange no-margin">29</span>
                            <span class="databox-text darkgray no-margin">YESTERDAY</span>
                        </div>
                        <div class="databox-cell cell-2 no-padding padding-10 text-align-left">
                            <span class="databox-number orange no-margin">15</span>
                            <span class="databox-text darkgray no-margin">TODAY</span>
                        </div>
                    </div>
                    <div class="databox-row row-6 no-padding">
                        <div class="databox-sparkline">
                            <span data-sparkline="line" data-height="126px" data-width="100%" data-fillcolor="#37c2e2" data-linecolor="#37c2e2"
                                data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#ffce55"
                                data-highlightspotcolor="#f5f5f5 " data-highlightlinecolor="#f5f5f5"
                                data-linewidth="2" data-spotradius="0">5,7,6,5,9,4,3,7,2
                            </span>
                        </div>
                    </div>
                </div>
                <div class="databox-bottom bg-sky no-padding">
                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                        <span class="databox-header white">Mon</span>
                    </div>
                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                        <span class="databox-header white">Tues</span>
                    </div>
                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                        <span class="databox-header white">Wed</span>
                    </div>
                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                        <span class="databox-header white">Thu</span>
                    </div>
                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                        <span class="databox-header white">Fri</span>
                    </div>
                    <div class="databox-cell cell-2 text-align-center no-padding padding-top-5">
                        <span class="databox-header white">Sat</span>
                    </div>

                </div>
            </div>
        </div>--%>
        </div>
</asp:Content>

<asp:Content ID="Scripts" ContentPlaceHolderID="PageScriptContent" runat="server">
    <script src="assets/js/jquery.li-scroller.1.0.js"></script>
    <link href="assets/css/liScroll.css" rel="stylesheet" />

    <!--Sparkline Charts Needed Scripts-->
    <script src="assets/js/charts/sparkline/jquery.sparkline.js"></script>
    <script src="assets/js/charts/sparkline/sparkline-init.js"></script>

    <!--Easy Pie Charts Needed Scripts-->
    <script src="assets/js/charts/easypiechart/jquery.easypiechart.js"></script>
    <script src="assets/js/charts/easypiechart/easypiechart-init.js"></script>

    <!--Flot Charts Needed Scripts-->
    <script src="assets/js/charts/flot/jquery.flot.js"></script>
    <script src="assets/js/charts/flot/jquery.flot.resize.js"></script>
    <script src="assets/js/charts/flot/jquery.flot.pie.js"></script>
    <script src="assets/js/charts/flot/jquery.flot.tooltip.js"></script>
    <script src="assets/js/charts/flot/jquery.flot.orderBars.js"></script>
    
    <!--Pulse Needed Scripts-->
    <script src="assets/js/Pulse/pulseCommon.js"></script>
    <script src="assets/js/Pulse/AlertsTicker.js"></script>
    <script src="assets/js/Pulse/AmpDrawSparkline.js"></script>
    <script src="assets/js/Pulse/AmpsDrawnPerHourSparkline.js"></script>
    <script src="assets/js/Pulse/AssetsOnlineDoughnut.js"></script>
    <script src="assets/js/Pulse/AssetsOnlineProgressBar.js"></script>
    <script src="assets/js/Pulse/BatteriesUsedSparkline.js"></script>
    <script src="assets/js/Pulse/BatteryRemovalsSparkline.js"></script>
    <script src="assets/js/Pulse/DevicePacketsPerHourSparkline.js"></script>
    <script src="assets/js/Pulse/FullyChargedBatteriesSparkline.js"></script>
    <script src="assets/js/Pulse/HeatMap.js"></script>
    <script src="assets/js/Pulse/WorkstationsMovedSparkline.js"></script>
    <script src="assets/js/Pulse/AssetsNeedingUpdates.js"></script>

    <!-- D3 Chart -->
    <script src="https://d3js.org/d3.v3.js"></script>


    <script>
        // If you want to draw your charts with Theme colors you must run initiating charts after that current skin is loaded
        $(window).bind("load", function () {

            /*Sets Themed Colors Based on Themes*/
            themeprimary = getThemeColorFromCss('themeprimary');
            themesecondary = getThemeColorFromCss('themesecondary');
            themethirdcolor = getThemeColorFromCss('themethirdcolor');
            themefourthcolor = getThemeColorFromCss('themefourthcolor');
            themefifthcolor = getThemeColorFromCss('themefifthcolor');

            var apiURL = 'http://rhythm.myenovate.com/api/StoredProc/';

            LoadRandomValsForCharts();
            //getTicker();
            //Sets The Hidden Chart Width
            $('#dashboard-bandwidth-chart')
                .data('width', $('.box-tabbs')
                    .width() - 20);

            //-------------------------Visitor Sources Pie Chart----------------------------------------//
            var data = [
                {
                    data: [[1, 21]],
                    color: '#fb6e52'
                },
                {
                    data: [[1, 12]],
                    color: '#e75b8d'
                },
                {
                    data: [[1, 11]],
                    color: '#a0d468'
                },
                {
                    data: [[1, 10]],
                    color: '#ffce55'
                },
                {
                    data: [[1, 46]],
                    color: '#5db2ff'
                }
            ];
            var placeholder = $("#dashboard-pie-chart-sources");
            placeholder.unbind();

            $.plot(placeholder, data, {
                series: {
                    pie: {
                        innerRadius: 0.45,
                        show: true,
                        stroke: {
                            width: 4
                        }
                    }
                }
            });

            //------------------------------Visit Chart------------------------------------------------//
            var data2 = [{
                color: themesecondary,
                label: "Direct Visits",
                data: [[3, 2], [4, 5], [5, 4], [6, 11], [7, 12], [8, 11], [9, 8], [10, 14], [11, 12], [12, 16], [13, 9],
                [14, 10], [15, 14], [16, 15], [17, 9]],

                lines: {
                    show: true,
                    fill: true,
                    lineWidth: .1,
                    fillColor: {
                        colors: [{
                            opacity: 0
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
                {
                    color: themeprimary,
                    label: "Referral Visits",
                    data: [[3, 10], [4, 13], [5, 12], [6, 16], [7, 19], [8, 19], [9, 24], [10, 19], [11, 18], [12, 21], [13, 17],
                    [14, 14], [15, 12], [16, 14], [17, 15]],
                    bars: {
                        order: 1,
                        show: true,
                        borderWidth: 0,
                        barWidth: 0.4,
                        lineWidth: .5,
                        fillColor: {
                            colors: [{
                                opacity: 0.4
                            }, {
                                opacity: 1
                            }]
                        }
                    }
                },
                {
                    color: themethirdcolor,
                    label: "Search Engines",
                    data: [[3, 14], [4, 11], [5, 10], [6, 9], [7, 5], [8, 8], [9, 5], [10, 6], [11, 4], [12, 7], [13, 4],
                    [14, 3], [15, 4], [16, 6], [17, 4]],
                    lines: {
                        show: true,
                        fill: false,
                        fillColor: {
                            colors: [{
                                opacity: 0.3
                            }, {
                                opacity: 0
                            }]
                        }
                    },
                    points: {
                        show: true
                    }
                }
            ];
            var options = {
                legend: {
                    show: false
                },
                xaxis: {
                    tickDecimals: 0,
                    color: '#f3f3f3'
                },
                yaxis: {
                    min: 0,
                    color: '#f3f3f3',
                    tickFormatter: function (val, axis) {
                        return "";
                    },
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 0,
                    aboveData: false,
                    color: '#fbfbfb'

                },
                tooltip: true,
                tooltipOpts: {
                    defaultTheme: false,
                    content: " <b>%x May</b> , <b>%s</b> : <span>%y</span>",
                }
            };
            var placeholder = $("#dashboard-chart-visits");
            var plot = $.plot(placeholder, data2, options);

            //------------------------------Real-Time Chart-------------------------------------------//
            var realTimedata = [],
                realTimedata2 = [],
                totalPoints = 300;

            var getSeriesObj = function () {
                return [
                {
                    data: getRandomData(),
                    lines: {
                        show: true,
                        lineWidth: 1,
                        fill: true,
                        fillColor: {
                            colors: [
                                {
                                    opacity: 0
                                }, {
                                    opacity: 1
                                }
                            ]
                        },
                        steps: false
                    },
                    shadowSize: 0
                }, {
                    data: getRandomData2(),
                    lines: {
                        lineWidth: 0,
                        fill: true,
                        fillColor: {
                            colors: [
                                {
                                    opacity: .5
                                }, {
                                    opacity: 1
                                }
                            ]
                        },
                        steps: false
                    },
                    shadowSize: 0
                }
                ];
            };
            function rdoBattClicked() {
                //if (document.getElementById('rdoBattCharge').checked) {
                //    updateHeatmap('212','charge');
                //    chartheat = anychart.heatMap(heatMapSource);
                //} else if (document.getElementById('rdoBattHealth').checked) {
                //    updateHeatmap('212','health');
                //    chartheat = anychart.heatMap(heatMapSource);
                //}
                //updateHeatmap('212');
                //chartheat = anychart.heatMap(heatMapSource);
            }
            function getRandomData() {
                if (realTimedata.length > 0)
                    realTimedata = realTimedata.slice(1);

                // Do a random walk

                while (realTimedata.length < totalPoints) {

                    var prev = realTimedata.length > 0 ? realTimedata[realTimedata.length - 1] : 50,
                        y = prev + Math.random() * 10 - 5;

                    if (y < 0) {
                        y = 0;
                    } else if (y > 100) {
                        y = 100;
                    }
                    realTimedata.push(y);
                }

                // Zip the generated y values with the x values

                var res = [];
                for (var i = 0; i < realTimedata.length; ++i) {
                    res.push([i, realTimedata[i]]);
                }

                return res;
            }
            function getRandomData2() {
                if (realTimedata2.length > 0)
                    realTimedata2 = realTimedata2.slice(1);

                // Do a random walk

                while (realTimedata2.length < totalPoints) {

                    var prev = realTimedata2.length > 0 ? realTimedata[realTimedata2.length] : 50,
                        y = prev - 25;

                    if (y < 0) {
                        y = 0;
                    } else if (y > 100) {
                        y = 100;
                    }
                    realTimedata2.push(y);
                }


                var res = [];
                for (var i = 0; i < realTimedata2.length; ++i) {
                    res.push([i, realTimedata2[i]]);
                }

                return res;
            }
            // Set up the control widget
            var updateInterval = 1000;
            var plot = $.plot("#dashboard-chart-realtime", getSeriesObj(), {
                yaxis: {
                    color: '#f3f3f3',
                    min: 0,
                    max: 100,
                    tickFormatter: function (val, axis) {
                        return "";
                    }
                },
                xaxis: {
                    color: '#f3f3f3',
                    min: 0,
                    max: 100,
                    tickFormatter: function (val, axis) {
                        return "";
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 0,
                    aboveData: false
                },
                colors: ['#eee', themeprimary],
            });


            function update() {


                plot.setData(getSeriesObj());

                plot.draw();
                setTimeout(update, updateInterval);
            }
            update();


            //-------------------------Initiates Easy Pie Chart instances in page--------------------//
            InitiateEasyPieChart.init();

            //updateHeatmap('212', 'health');
            anychart.licenseKey("enovatemedical-8057aec8-90891fb7");
            //chartheat = anychart.heatMap(heatMapSource);
            //anychart.licenseKey("enovatemedical-8057aec8-90891fb7");
            //var credits = chartheat.credits();
            //credits.enabled(false);
          
            //-------------------------Initiates Sparkline Chart instances in page------------------//
            InitiateSparklineCharts.init();

            var request = createCORSRequest('POST', apiUrl); 
            if (request) {
                request.setRequestHeader("Content-Type", 'application/json; charset-utf-8');

                    request.setRequestHeader("Access-Control-Allow-Origin", '*');
                    request.setRequestHeader("dataType", 'json');
                    request.setRequestHeader("Access-Control-Max-Age", '99999');
                    request.onload = function () {

                        updateHeatmap(siteId, 'health');
                        getTicker(siteId);
                        updateAmpDrawSparkline(siteId, "AmpsDrawnSummarySparkline");
                        updateBatteriesUsedSparkline(siteId, "BatteriesUsedSparkline");
                        updateWorkstationsMovedSparkline(siteId, "WorkstationsMovedSparkline");
                        updateBatteryRemovalsSparkline(siteId, "BatteryRemovalsSparkline");
                        updateAssetsOnline(siteId, "dashboard-pie-chart-sources", "AssetsOnlineCount", "totalMonitoredAssets");
                        //updateAmpsDrawnPerHourSparkline(siteId, "AmpsDrawnPerHourSparkline", "ampsAvgDrawPerHour");
                        //updatedevicePacketsPerHourSparkline(siteId, "DevicePacketsPerHourSparkLine");


                    };
                    request.onerror = function (xhr, ajaxOption, thrownError) {
                        console.log(thrownError);
                        alert('CORS request returned error =[' + xhr.responseText + ' ' + thrownError + ' ' + ajaxOption +  ']');
                    };
                    request.send(data);
            }
        
            
            //
            // redisplay the heatmap 1st - we have to move to another tab and back in order for the refresh to work
            $('.nav-tabs a[href="#visits"]').tab('show');
            $('.nav-tabs a[href="#bandwidth"]').tab('show');


        });
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        function LoadRandomValsForCharts() {
            $('#highlowinserts').text(getRandomInt(22, 24));
            $('#BatteriesUsed').text(getRandomInt(139, 154));
            $('#WorkstationsMoved').text(getRandomInt(72, 82));
            $('#AmpDraw').text(getRandomInt(1, 2) + '.97');
            getRandomInt(22, 24);

        }

        function clickAssetStatus() {
            updateAmpsDrawnPerHourSparkline(siteId, "AmpsDrawnPerHourSparkline", "ampsAvgDrawPerHour");
            updatedevicePacketsPerHourSparkline(siteId, "DevicePacketsPerHourSparkLine", "devicePacketsTotal");
            updateFullyCharged(siteId, "FullyChargedDoughnut", "FullyChargedDoughnutSummary");
            updateAssetsNeedingUpdates(siteId, "AssetsNeedUpdateDoughnut", "AssetsNeedUpdateDoughnutSummary");
        }


        function clickBatteryHealth() {
            updateHeatmap(siteId, 'health');
        }
        //-- Bunch of Heatmap Script DELETE THIS LATER //

        //var heatMapSource = null;
        //var apiURL = 'http://rhythm.myenovate.com/api/StoredProc/';
        //function updateHeatmap(siteId,type) {
        //    getheatmapfromAjax(siteId,type);
        //}
        ////function createCORSRequest(method, url) {
        ////    var xhr = new XMLHttpRequest();
        ////    if ("withCredentials" in xhr) {
        ////        // XHR has 'withCredentials' property only if it supports CORS
        ////        xhr.open('POST', apiURL, true);
        ////    } else if (typeof XDomainRequest != "undefined") { // if IE use XDR
        ////        xhr = new XDomainRequest();
        ////        xhr.open('POST', apiURL);
        ////    } else {
        ////        xhr = null;
        ////    }
        ////    return xhr;
        ////}


        //function getheatmapfromAjax(siteId, type) {

        //     //if (type == 'charge') {
        //    params = ['SiteID: ' + siteId];
        //    // var params = JSON.stringify('SiteID: ' + siteId);

        //    var request = createCORSRequest('POST', apiURL);
        //    if (request) {

        //        // Send request
        //        var data = JSON.stringify({ storedProcName: '[spDashboardBatteryChargeLevels_test]', paramsStr: params });

        //        request.setRequestHeader("Content-Type", 'application/json; charset-utf-8');
        //        request.setRequestHeader("Access-Control-Allow-Origin", '*');
        //        request.setRequestHeader("dataType", 'json');
        //        // dataType: 'json';
        //        // request.withCredentials= true;
        //        request.onload = function (){
        //            $.ajax({
        //                        url: apiURL,
        //                        method: 'POST',
        //                        crossDomain: true,
        //                        //beforeSend: function (xmlHttpRequest) {
        //                        //    xmlHttpRequest.withCredentials = true;
        //                        //},
        //                          data: JSON.stringify({ storedProcName: '[spDashboardBatteryChargeLevels_test]', paramsStr: params }),
        //                        contentType: 'application/json; charset-utf-8',
        //                        dataType: 'json',
        //                        success: function (dataJson) {
        //                            makeHeatmapChart(dataJson);

        //                        },
        //                        error: function (xhr, ajaxOption, thrownError) {
        //                            console.log(thrownError);
        //                            alert('dbo.spDashboardBatteryHealthLevels_test returned error thrownError=[' + thrownError + ']' + ' xhr=[' + xhr.statusText + ']' + ' ajaxOption=[' + ajaxOption + ']');
        //                        }
        //                });

        //        };
        //        request.onerror = function (xhr, ajaxOption, thrownError) {
        //            console.log(thrownError);
        //            alert('dbo.spDashboardBatteryChargeLevels_test returned error thrownError=[' + thrownError + ']' + ' xhr=[' + xhr.statusText + ']' + ' ajaxOption=[' + ajaxOption + ']');
        //        };
        //        request.send(data);

        //    }


        //}

        //function makeHeatmapChart(jsonData) {
        //    heatMapSource = jsonData;
        //    var colors = ["#92EE2B", "#AFEF27", "#FFC903", "#FFFC02", "#CEFF01", "#9BFF01", "#67FF00", "#33FF00", "#00FF00"]; // alternatively colorbrewer.YlGnBu[9]



        //    anychart.licenseKey("enovatemedical-8057aec8-90891fb7");
        //    var dataset = anychart.data.set(heatMapSource);
        //    chart = anychart.heatMap(heatMapSource);
        //    var credits = chart.credits();
        //    credits.enabled(false);
        //    var colorScale = anychart.scales.ordinalColor();
        //    // set color for all points
        //    colorScale.ranges([
        //        { less: 0.35, color: "#FF0000" },
        //        { from: 0.35, to: 0.50, color: "#FFC903" },
        //        { from: 0.50, to: 0.55, color: "#FFC903" },
        //        { from: 0.55, to: 0.60, color: "#FFC903" },
        //        { from: 0.60, to: 0.65, color: "#FFC903" },
        //        { from: 0.65, to: 0.70, color: "#FFFC02" },
        //        { from: 0.70, to: 0.75, color: "#FFFC02" },
        //        { from: 0.75, to: 0.80, color: "#CEFF01" },
        //        { from: 0.80, to: 0.85, color: "#9BFF01" },
        //        { from: 0.85, to: 0.90, color: "#67FF00" },
        //        { from: 0.90, to: 0.95, color: "#33FF00" },
        //        { greater: .949, color: "#00FF00" }
        //    ]);
        //    chart.colorScale(colorScale);

        //    // Sets selection mode for single selection
        //    chart.interactivity().selectionMode("none");

       
        //        chart.padding([1, 1, 1, 1])
        //       .title()
        //       .useHtml(true)
        //       .enabled(true)
        //       .padding([1, 1, 1, 1])
        //       .align('center')
        //       .text("Capacity <style='font-size: 10px; color:#B9B9B9'> </span>");
           
        //        // Sets chart labels
        //        chart.labels().enabled(true).maxFontSize(8).textFormatter(function () {
        //            return this.getDataValue('symbol');
        //        });
          
        //    // Turns off axes
        //    chart.yAxis(null);
        //    chart.xAxis(null);

        //    // Sets chart and hover chart settings
        //    chart.stroke('#fff');
        //    chart.hoverStroke('6 #fff');
        //    chart.hoverFill('#5DADE2');
        //    chart.hoverLabels().fontColor('#fff');

        //    // Sets legend
        //    chart.legend()
        //        .align('top')
        //        .position('bottom')
        //        .itemsLayout('h')
        //        .padding([2, 5, 0, 5])
        //        .enabled(false).tooltip().enabled(false);

        //    // Sets tooltip
        //    chart.tooltip()
        //        .titleFormatter(function () {
        //            var s = '';
        //            var serialNo = this.getDataValue('symbol');
        //            //if (this.getDataValue('heat') === 's') s = 'suited';
        //            //else if (this.getDataValue('symbol') === 'u') s = 'low';
        //            //return this.x + ',' + this.y + ' ' + serialNo;
        //            return serialNo + " (" + this.x + ',' + this.y + ")";
        //        })
        //        .textFormatter(function () {
        //            return 'Capacity Level: ' + (this.heat * 100) + '%';
        //        });



        //    // create horizontal chart scroller
        //    //var xScroller = chart.xScroller();
        //    // scroller settings
        //    //  xScroller.enabled(true);

        //    // enables zoom on chart by default
        //    var xZoom = chart.xZoom();
        //    // define the number of visible points at the same time
        //    xZoom.setToPointsCount(900);

        //    // create vertical chart scroller
        //    var yScroller = chart.yScroller();
        //    // scroller settings
        //    yScroller.enabled(false);

        //    // enables zoom on chart by default
        //    var yZoom = chart.yZoom();
        //    // define the number of visible points at the same time
        //    yZoom.setToPointsCount(900);

        //    chart.container("chartheat");
        //    chart.bounds(0, 5, "99%", "250px");
        //    chart.draw();



          
        //};

        //function getTicker(siteId) {
        //    var params = ['SiteID: ' + '212'];
        //    $.ajax({
        //        url: apiURL,
        //        method: 'POST',
        //        data: JSON.stringify({ storedProcName: 'prcDashAlertMessages', paramsStr: params }),
        //        contentType: 'application/json; charset-utf-8',
        //        dataType: 'json',
        //        success: function (dataJson) {
        //            buildTicker(dataJson);
        //            $("ul#ticker01").liScroll();
        //        },
        //        error: function (xhr, ajaxOption, thrownError) {
        //            console.log(thrownError);
        //            alert('dbo.prcDashAlertMessages returned error ' + thrownError);
        //        }
        //    });
        //}

        //function buildTicker(jsonData) {
        //    $("#ticker01").empty();
        //    var cnt = 0;
        //    $.each(jsonData, function () {
        //        cnt++;
        //        if (this.AssetDesc == 'WKSTN') {
        //            switch (this.PacketType) {
        //                case 'Notification':
        //                    $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-laptop'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //                case 'Info':
        //                    $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-laptop'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //                case 'Alert':
        //                    $("#ticker01").append("<li style='color:red;font-weight:bold'><i class='fa fa-laptop'><span>  " + this.AssetDesc + " " + "</span>  <a target='_blank' style='color:red;font-weight:bold' href='https://na32.salesforce.com/" + this.SFCID + "'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //            }
        //        }
        //        if (this.AssetDesc == 'BATT') {
        //            switch (this.PacketType) {
        //                case 'Notification':
        //                    $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //                case 'Info':
        //                    $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //                case 'Alert':
        //                    $("#ticker01").append("<li style='color:red;font-weight:bold'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a target='_blank' style='color:red;font-weight:bold' href='https://na32.salesforce.com/" + this.SFCID + "'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //            }
        //        }
        //        if (this.AssetDesc == 'CHRG') {
        //            switch (this.PacketType) {
        //                case 'Notification':
        //                    $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //                case 'Info':
        //                    $("#ticker01").append("<li style='color:darkgreen'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //                case 'Alert':
        //                    $("#ticker01").append("<li style='color:red;font-weight:bold'><i class='fa fa-flash'><span>  " + this.AssetDesc + " " + "</span>  <a target='_blank' style='color:red;font-weight:bold' href='https://na32.salesforce.com/" + this.SFCID + "'>" + this.FriendlyDescription + "</a></li>");
        //                    break;
        //            }
        //        }
        //        //  $("#ticker01").append("<li><span>  " + this.AssetDesc + " " +  "</span>  <a href='#'>" + this.FriendlyDescription + "</a></li>");
        //    });
        //    $("#ticker01").append("<li>last...</li>");

        //}




    </script>
</asp:Content>

