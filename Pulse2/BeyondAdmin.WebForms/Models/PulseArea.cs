﻿using BeyondAdmin.Source.Utils;
using BeyondAdmin.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeyondAdmin.Model
{
    public class PulseArea : PulseEntity
    {
        public string Description { get; set; }
        public string Beacon1Major { get; set; }
        public string Beacon1Minor { get; set; }
        public string Beacon1BatteryLVL { get; set; }
        public string Beacon2Major { get; set; }
        public string Beacon2Minor { get; set; }
        public string Beacon2BatteryLVL { get; set; }
        public int IDSite { get; set; }
        public Department Department {get;set;}

        // RTLS
        public Point2 Location { get; set; }
        public double kScatterRadius { get; set; }
        public double kAssetsPerOrbit { get; set; }
        public double scatterRadius;
        public double assetsPerOrbit;
        public int numAssetsInHub;

        public PulseArea()
        {
            Location = new Point2(0, 0);
            kScatterRadius = 15;
            kAssetsPerOrbit = 5;
        }

        public PulseArea(Dictionary<string, string> dbRow, Site site) : base(dbRow, "Description", "AreaID")
        {
            Location = new Point2(0, 0);

            foreach (var col in dbRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "Description": Description = colValue; break;
                    case "Beacon1Major": Beacon1Major = colValue; break;
                    case "Beacon1Minor": Beacon1Minor = colValue; break;
                    case "Beacon1BatteryLVL": Beacon1BatteryLVL = colValue; break;
                    case "Beacon2Major": Beacon2Major = colValue; break;
                    case "Beacon2Minor": Beacon2Minor = colValue; break;
                    case "Beacon2BatteryLVL": Beacon2BatteryLVL = colValue; break;
                    case "IDSite": IDSite = Convert.ToInt32(colValue); break;
                    case "IDDepartment":
                        int departmentId = Convert.ToInt32(colValue);
                        Department = site.Departments != null ? site.Departments.Find(d => d.Id == departmentId) : null; 
                        break;
                    default: Utils.ErrorHandler.WarnMissingDBField(col.Key, this); break;
                }
            }
        }

        public void InitScatter()
        {
            scatterRadius = kScatterRadius;
            assetsPerOrbit = kAssetsPerOrbit;
            numAssetsInHub = 0;
        }

    }
}