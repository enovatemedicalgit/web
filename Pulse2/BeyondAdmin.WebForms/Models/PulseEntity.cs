﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BeyondAdmin.Model
{
    public class PulseEntity
    {
        public Int32 Id { set; get; }
        public String Name { set; get; }

        protected PulseEntity()
        {
            Id = 0;
            Name = "Unknown";
        }

        public PulseEntity(Dictionary<string, string> dbIdnRow, string nameKey, string idKey)
        {
            this.Name = Utils.Misc.SafeGetKey(dbIdnRow, nameKey);
            this.Id = Utils.Misc.SafeConvertToInt(Utils.Misc.SafeGetKey(dbIdnRow, idKey));
        }
    }
}