﻿using BeyondAdmin.API;
using BeyondAdmin.Utils.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace BeyondAdmin.Model
{
    // MDJ TODO move to another module
    //public class PulseEntity
    //{
    //    public Int32 Id { set; get; }
    //    public String Name { set; get; }

    //    protected PulseEntity()
    //    {
    //        Id = 0;
    //        Name = "Unknown";
    //    }

        //public PulseEntity(Dictionary<string, string> dbIdnRow, string nameKey, string idKey)
        //{
        //    this.Name = nameKey != null ? dbIdnRow[nameKey] : "";
        //    this.Id = idKey != null ? Convert.ToInt32(dbIdnRow[idKey]) : -1;
        //}
   // }

    public class FacilityInfo : PulseEntity
    {
        // Type
        public const int IDN = 1;
        public const int SITE = 2;
        public const int ENOVATE_HQ_ID = 4;

#region properties

        public string CreatedDateUTC { set; get; }
        public int BUType { set; get; }
        public string Address1 { set; get; }
        public string Address2 { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public string Zip { set; get; }
        public string SiteDescription { set; get; }
        public string BedCount { set; get; }
        public string IsStinger { set; get; }
        public int ParentBusinessUnitID { get; set; }

#endregion

        protected FacilityInfo()
        {
        }

        public FacilityInfo(Dictionary<string, string> dbIdnRow, string nameKey, string idKey) : base(dbIdnRow,nameKey,idKey)
        {
            foreach( var col in dbIdnRow )
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "CreatedDateUTC":  this.CreatedDateUTC = colValue; break;
                    case "BUType": this.BUType = Convert.ToInt32(colValue);break;
                    case "Address1": this.Address1 = colValue;break;
                    case "Address2": this.Address2 = colValue;break;
                    case "City": this.City = colValue;break;
                    case "State": this.State = colValue;break;
                    case "Zip": this.Zip = colValue;break;
                    case "SiteDescription": this.SiteDescription = colValue;break;
                    case "IsStinger": this.IsStinger = colValue;break;
                    case "ParentBusinessUnit": this.ParentBusinessUnitID = Convert.ToInt32(colValue); break;
                }
            }
        }

    }

    public class Facility : FacilityInfo
    {
        #region Privates
        private List<PulseUser> _users;
        private List<Asset> _assets;
        int mAssetTypes = 0;
        #endregion

        public List<PulseUser> Users
        {
            get
            {
                if (_users == null)
                {
                    // TODO dependency alert
                    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                    {
                        _users = enovateDB.InitializeUserList(this);
                    }
                }
                return _users;
            }
            set
            {
                _users = value;
            }
        }

        public List<Asset> Assets
        {
            get
            {
                if (_assets == null)
                {
                    // TODO dependency alert
                    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase()))
                    {
                        _assets = enovateDB.InitializeAssetList(this);
                    }
                }
                return _assets;
            }
            set
            {
                _assets = value;
            }
        }

        public int AssetCount { get { return Assets.Count; } }
        public List<Department> Departments { get;set; }
        public int DepartmentCount { get { return Departments.Count; } }
        public int AssetTypeCount { get { return mAssetTypes; } }
        public int BUID { get; set; }
        public int FacilityId { get; set; }


        #region JSON TEMP
        class AssetListJson
        {
            public int facilityID { get; set; }
            public List<AssetJSON> assets { set; get; }
        }

        class AssetJSON
        {
            public int x { get; set; }
            public int y { get; set; }
            public string color { get; set; }
            public string type { get; set; }
            public string serialNumber { get; set; }
            public string description { get; set; }
            public string state { get; set; }
            public string imgName { get; set; }
            public string bloodPressure {get;set; }
            public string heartRate {get;set; }
            public string temperature { get; set; }
            public int departmentIndex { get; set; }


            public AssetJSON(int x, int y,int departmentIndex, Asset assetRef)
            {
                this.x = x;
                this.y = y;
                this.departmentIndex = departmentIndex;
                color = TempUtils.GetAssetColor(assetRef.Type);
                type = assetRef.Type.ToString();
                serialNumber = assetRef.SerialNumber;
                description = assetRef.Description;
                state = assetRef.State;
                imgName = assetRef.ImageName;

                switch( assetRef.SerialNumber)
                {
                    case "1": bloodPressure = "122/81"; heartRate = "76"; temperature = "99"; break;
                    case "2": bloodPressure = "110/72"; heartRate = "64"; temperature = "98"; break;
                    case "3": bloodPressure = "117/72"; heartRate = "60"; temperature = "98"; break;
                    case "5": bloodPressure = "140/107"; heartRate = "71"; temperature = "98"; break;
                    case "6": bloodPressure = "134/77"; heartRate = "67"; temperature = "97"; break;
                    case "8": bloodPressure = "158/120"; heartRate = "129"; temperature = "102"; break;
                    default: bloodPressure = "122/81"; heartRate = "81"; temperature = "98"; break;
                }
                
                
                
            }

        }

        protected string mAssetsJsonString;
        List<Asset> mMovedAssets = new List<Asset>();
        bool mTrackingAssets = false;
        public bool TrackingMovedAssets { get { return mTrackingAssets; } set { mTrackingAssets = value; } }
        #endregion 

        protected Facility()
        {
        }

        public Facility(Int32 BUID,Dictionary<string, string> dbIdnRow,string nameKey,string idKey ) : base(dbIdnRow,nameKey,idKey)
        {
            this.BUID = BUID;
            this.FacilityId = Convert.ToInt32(dbIdnRow["ROW_ID"]);
        }

        public void AssetUpdateCallback(Asset asset, int locX, int locY, DateTime time)
        {
            // Don't move too many assets per frame
            if (!TrackingMovedAssets || mMovedAssets.Count > 50 )
                return;
            //Asset asset = Assets.Find(x => x.SerialNumber == assetId);
            if (asset != null)
            {
                asset.XCoord = locX;
                asset.YCoord = locY;
                //asset.UpdateTime = time;
            }
            lock (mMovedAssets)
            {
                // Make sure we aren't added the asset 2 times in the list
                Asset existing = mMovedAssets.Find(f => f.Id == asset.Id);
                if (existing == null)
                {
                    mMovedAssets.Add(asset);
                }
                else
                {
                    existing.XCoord = asset.XCoord;
                    existing.YCoord = asset.YCoord;
                }
            }
        }

        public bool AnyAssetsMoved()
        {
            return mMovedAssets == null ? false : mMovedAssets.Count > 0;
        }

        // Floor 1 and 2 are 
        public bool IsPartOfFloor(Asset asset,int floor)
        {
            int assetAreaId = Convert.ToInt32(asset.Department); 
            switch( floor )
            {
                // enovate section
                case 1: return (assetAreaId >= 12 && assetAreaId <= 15 && asset.siteId == 2) ? true : false;
                // Himss floor
                case 2: return (assetAreaId == 11 && asset.siteId == 2) ? true : false;
                // full temp map
                case 3: return asset.siteId == 1 ?  true : false;
                default: return false;
            }
        }
        
        private int HackGetDepartmentIndex(Asset asset)
        {
            int assetAreaId = Convert.ToInt32(asset.Department);

            // Enovate office
            if (assetAreaId >= 1 && assetAreaId <= 10)
                return 3;
            // Himss Enovate area
            if (assetAreaId >= 12 && assetAreaId <= 15 )
                return 1;

            if (assetAreaId == 11)
                return 1;

            // Himss floor
            if (assetAreaId >= 16 && assetAreaId <= 28)
                return 2;

            return -1;
        }

        public string AssetsToJson(Facility facility,bool bInit,int floor)
        {
            if (string.IsNullOrEmpty(mAssetsJsonString) || AnyAssetsMoved() || bInit == true)
            {
                AssetListJson list = new AssetListJson();
                list.facilityID = facility.FacilityId;
                list.assets = new List<AssetJSON>();
                lock (mMovedAssets)
                {
                    List<Asset> curList = !bInit && AnyAssetsMoved() ? mMovedAssets : this.Assets;
                    foreach (Asset asset in curList)
                    {
                        if (asset.IsRTLS )
                        {
                            int departmentIndex = HackGetDepartmentIndex(asset);
                            if( departmentIndex >= 0 )
                            {
                                list.assets.Add(new AssetJSON(asset.XCoord, asset.YCoord, departmentIndex, asset));
                                if (list.assets.Count > 100)
                                    break;
                            }
                        }
                    }
                }
                var js = new JavaScriptSerializer();
                js.MaxJsonLength = 4000000;
                mAssetsJsonString = js.Serialize(list);
                mMovedAssets.Clear();
            }
            
            return mAssetsJsonString;
        }

        public Department DepartmentFromName(string name)
        {
            return Departments.Find(x => x.Name == name);
        }
    }

    public class UnknownFacility : Facility
    {
        public UnknownFacility()
        {
            this.Name = "Unkown";
            this.Id = 0;
        }
    }
}