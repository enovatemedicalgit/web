﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace BeyondAdmin.Model
{
    public class PulseAsset
    {
        public enum AssetType
        {
            BATTERY,
            WORKSTATION,
            CHARGER,
            EMPLOYEE,
            PATIENT,
            OTHER,
        }

        public static AssetType StringToAssetType(string s)
        {
            switch (s)
            {
                case "1": return AssetType.BATTERY;
                case "2": return AssetType.WORKSTATION;
                case "3": return AssetType.CHARGER;
                case "4": return AssetType.EMPLOYEE;
                case "5": return AssetType.PATIENT;
            }
            return AssetType.OTHER;
        }

        public string Description {get;set;}
        public string SerialNumber { get; set; }
        public AssetType Type { get; set; }
        public int VendorID {get;set;}
        public string SoftwareVersion{get;set;}
        public int SiteID{get;set;}
        public string DepartmentID { get;set; }
        public string Image{get;set;}
        public int IDAsset { get; set; }
        public int Retired { get; set; }
        // Emtag
        public int EmTagID { get; set; }
        public string BatteryChargeLVL { get; set; }
        public string MinorOfTag { get; set; }
        public string MajorOfTag { get; set; }

        // RTLS 
        public int CurrentArea { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public int XCoord { get; set; }
        public int YCoord { get; set; }
        public string State { get; set; }
        public string color { get; set; }


        public PulseAsset(Dictionary<string, string> dbRow)
        {
            //this.Name = Misc.SafeGetKey(dbRow, nameKey);
            //this.Id = Misc.SafeGetKey(dbRow, idKey);

            State = "Healthy";
            foreach (var col in dbRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "CurrentArea": CurrentArea = Convert.ToInt32(colValue); break;
                    case "AssetDescription": Description = colValue; break;
                    case "SerialNo": SerialNumber = colValue; break;
                    case "AssetTypeID": Type = StringToAssetType(colValue); color = ""; break;
                    case "VendorID": VendorID = Convert.ToInt32(colValue); break;
                    case "SoftwareVersion": SoftwareVersion = colValue; break;
                    case "SiteID": SiteID = Convert.ToInt32(colValue); break;
                    case "DepartmentID": DepartmentID = colValue; break;
                    case "IDAsset": IDAsset = Convert.ToInt32(colValue); break;
                    case "Retired": Retired = Convert.ToInt32(colValue); break;
                    case "Image": 
                        //using (MemoryStream mStream = new MemoryStream())
                        //{
                        //    byte[] bytes = BeyondAdmin.Utils.Misc.GetBytes(col.Value);

                        //    mStream.Write(bytes, 0, bytes.Length);
                        //    mStream.Seek(0, SeekOrigin.Begin);

                        //    ImageBmp = new Bitmap(mStream);
                        //}
                    break;
                    case "BatteryChargeLVL": BatteryChargeLVL = colValue; break;
                    case "EmTagID": EmTagID = Convert.ToInt32(colValue); break;
                    case "MinorOfTag": MinorOfTag = colValue; break;
                    case "MajorOfTag": MajorOfTag = colValue; break;
                }
            }

        }
    }
}