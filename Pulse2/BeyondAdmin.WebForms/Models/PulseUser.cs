﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using BeyondAdmin.API;
using BeyondAdmin.Utils.Database;

namespace BeyondAdmin.Model
{

    [Flags]
    public enum UserAccessRights : ulong
    { 
        NONE = 0,
        // MDJ TODO change this when the DB has changed to the new Pulse DB format
        CAN_LOGIN_TO_CAST = 1<<0,
        CAN_LOGIN_TO_PULSE = 1 << 0,
        CAN_MAINTAIN_DEVICES = 1 << 1,
        CAN_MAINTAIN_BUSINESS_UNITS = 1<<2, 
        CAN_MAINTAIN_USERS = 1<<3,
        CAN_RECEIVE_NOTIFICATIONS = 1<<4,
        CAN_AUTHORIZE_QA_RELEASES = 1<<5,
        CAN_AUTHORIZE_QA_RETESTING = 1<<6,
        CAN_MAINTAIN_ALERT_CONDITIONS = 1<<7,
        CAN_MAINTAIN_FACILITY_NETWORKS = 1<<8,
        CAN_MAINTAIN_NEWS_AND_TIPS = 1<<9,
        CAN_MAINTAIN_NOTIFICATIONS = 1<<10,
        CAN_PERFORM_QA_TESTS = 1<<11,
        CAN_RECEIVE_EMAIL_REPORTS = 1<<12,
        CAN_SEARCH_ONLY = 1<<13,
        CAN_SEND_COMMAND_CODES = 1<<14,
        IS_SERVICE_TECHNICIAN = 1<<15,
        RECEIVES_ALERTS = 1<<16,
        RECEIVES_CHARGER_CYCLER_EMAILS = 1<<17,
        RECEIVES_QA_FAILURE_EMAILS = 1 << 18,

        // Make sure to update this if new flags are added
        NUM_FLAGS = 18,
        DEFAULT_RIGHTS = CAN_LOGIN_TO_PULSE|CAN_LOGIN_TO_CAST|CAN_RECEIVE_NOTIFICATIONS,
        // this means somethign went wrong
        UNKNOWN1 = (uint)1 << 31,
    };

    
    public class AccessRightsHelper
    {
        public static UserAccessRights Convert(Dictionary<string,string> dictionary)
        {
            UserAccessRights flags = UserAccessRights.NONE;

            foreach(KeyValuePair<string,string> val in dictionary)
            {
                flags |= Convert(val.Key, val.Value);
            }
            return flags;
        }

        public static UserAccessRights AddRight(UserAccessRights cur,string option, string value)
        {
            return AddRight(cur, Convert(option, value));
        }

        public static UserAccessRights Convert(string option, string value)
        {
            if (value == "0" || value.ToLower() == "false" || !Enum.IsDefined(typeof(UserAccessRights), option) )
                return UserAccessRights.NONE;
            return (UserAccessRights) Enum.Parse(typeof(UserAccessRights), option);  
        }

        public static UserAccessRights AddRight(UserAccessRights cur, UserAccessRights toAdd) { return cur | toAdd; }
        public static bool HasRight(UserAccessRights cur, UserAccessRights toCheck) { return (cur & toCheck) == 0 ? false : true; }
    }

    /// Class that implements the ASP.NET Identity
    /// IUser interface 
    public class PulseUser  : PulseEntity
    {
        private List<PulseUser> _users;
        #region Properties

        /// Unique User ID
        public string UserName { get { return this.Name; } set { this.Name = value; } }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string SessionKey { get; set; }
        public string EncryptedPassword { get; set; }
         public string SFAcctOwnerID { get; set; }
        public string Email { get; set; }
        public int IDSite { get; set; }
           public int UserTypeID { get; set; }
        public UserAccessRights AccessRights { get; set; }
        public bool HasRight(UserAccessRights toCheck) { return AccessRightsHelper.HasRight(AccessRights, toCheck); }
        public bool IsHQUser { get { return ParentCustomer != null ? ParentCustomer.Id == IDSite : false; } }

        #endregion 

        #region Web properties
        public List<Site> Sites{ get; set; }
        public List<Asset> Assets { get; set; }
        public Customer ParentCustomer { get; set; }
        public List<PulseUser> CoUsers
        {
            get
            {
                if (_users == null)
                {
                    _users = new List<PulseUser>();
                    using (PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase("PulseConnection")))
                    {
                        if (IsHQUser)
                            _users.AddRange(enovateDB.GetUsersForSite(ParentCustomer.Id));
                        foreach (Site site in this.Sites)
                        {
                            _users.AddRange(enovateDB.GetUsersForSite(site.Id));
                        }

                    }
                }
                return _users;
            }
            set
            {
                _users = value;
            }
        }
        #endregion

        /// Default constructor 
        public PulseUser()
        {
        }

        /// Constructor that takes user name as argument
        public PulseUser(string userName)
            : this()
        {
            UserName = userName;
        }

        public PulseUser(Dictionary<string, string> dbRow) : base(dbRow,"UserName","IDUser")
        {
            foreach (var col in dbRow)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "FirstName": this.FirstName = colValue; break;
                    case "LastName": this.LastName = colValue; break;
                    case "Title": this.Title = colValue; break;
                    case "PrimaryPhone": this.Phone1 = colValue; break;
                    case "OtherPhone": this.Phone2 = colValue; break;
                    case "IDSite": this.IDSite = Convert.ToInt32(col.Value); break;
                    case "UserTypeID": this.UserTypeID = Convert.ToInt32(col.Value); break;
                    case "Email": this.Email = colValue; break;
                    case "Password": this.EncryptedPassword = colValue; break;
                    case "SFAcctOwnerID": this.SFAcctOwnerID = colValue;  break;
                     case "SessionKey": this.SessionKey = colValue;  break;
                    default: Utils.ErrorHandler.WarnMissingDBField(col.Key, this); break;
                }
            }
            if (PulseSession.Instance.User != null && PulseSession.Instance.User.ParentCustomer != null)
                this.ParentCustomer = PulseSession.Instance.User.ParentCustomer;

        }

        public bool HasValidId() { return Id > 0; }


    }
}