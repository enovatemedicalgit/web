﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BeyondAdmin.Model
{

    public class Department : PulseEntity
    {
        public int SiteID { set; get; }
        public Department()
        {

        }
        public Department(Dictionary<string, string> dbIdnRow) : base(dbIdnRow,"Description","IDDepartment")
        {
            foreach( var col in dbIdnRow )
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    // Handled in base 
                    case "IDDepartment":
                    case "Description": break;

                    case "SiteID": SiteID = Utils.Misc.SafeConvertToInt(colValue); break;
                    default: Utils.ErrorHandler.WarnMissingDBField(col.Key, this); break;
                }
            }
        }
    }
}