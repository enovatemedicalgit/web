﻿using BeyondAdmin.API;
using BeyondAdmin.Utils;
using System;
using System.Collections.Generic;
using System.Web;

namespace BeyondAdmin.Model
{
  
    public class Asset  
    {
        // TODO move this and all colors to client
        const string kUnknownColor = "rgb(78,78,78)";

        #region common properties
        public class AssetProperties
        {
            public AssetType type;
            public AssetCategory category;
            public string description;
            public string htmlColor;

            public AssetProperties(Dictionary<string, string> dbRows)
            {
                type = AssetType.UNKNOWN;
                foreach (var col in dbRows)
                {
                    string colValue = col.Value != null ? col.Value.ToString() : "";
                    switch (col.Key)
                    {
                        case "IDAssetType": type = (AssetType)Convert.ToInt32(colValue); break;
                        case "Description": description = colValue; break;
                        case "Category": Enum.TryParse(colValue.ToUpper(), out category); break;
                        default: Utils.ErrorHandler.WarnMissingDBField(col.Key, this); break;
                    }
                }
                switch(type)
                {
                    case AssetType.UNKNOWN: htmlColor = kUnknownColor; break;
                    case AssetType.BATTERY: htmlColor = "rgb(78,240,93)";  break;
                    case AssetType.WORKSTATION: htmlColor = "rgb(255,150,115)"; break;
                    case AssetType.CHARGER: htmlColor = "rgb(105,244,223)"; break;
                    case AssetType.EMPLOYEE: htmlColor = "rgb(63,72,204)"; break;
                    case AssetType.PATIENT: htmlColor = "rgb(196,15,254)";  break;
                    case AssetType.MOBIUS2_WORKSTATION: htmlColor = "rgb(255,150,115)";  break;
                    case AssetType.MOBIUS2_BAYCHARGER: htmlColor = "rgb(255,150,115)";  break;
                    case AssetType.MED_WORKSTATION: htmlColor = "rgb(255,150,115)";  break;
                    case AssetType.EMPOWER_BATTERY: htmlColor = "rgb(78,240,93)";  break;
                    case AssetType.OTHER: break;
                }
                
            }
        };

        public static List<AssetProperties> assetPropertyTable = null;

        public enum AssetType
        {
            UNKNOWN = 0,
            WORKSTATION = 1,
            CHARGER = 2,
            MOBIUS2_WORKSTATION = 3,
            MOBIUS2_BAYCHARGER = 4,
            BATTERY = 5,
            MED_WORKSTATION = 6,
            PATIENT = 7,
            EMPLOYEE = 8,
            EMPOWER_BATTERY = 9,
            OTHER = 10,
        };

        public enum AssetCategory
        {
            WORKSTATION,
            CHARGER,
            BATTERY,
            PATIENT,
            EMPLOYEE,
            OTHER,
            UNKNOWN,
        }

        public enum AssetStatus
        {
            OFFLINE = 0,
            AVAILABLE = 1,
            IN_SERVICE = 2,
            NOT_COMMISSIONED = 3,
        };
        #region private
        Byte _deviceStatus = (Byte)AssetStatus.NOT_COMMISSIONED;
        Byte _assetTypeID = 0;
        #endregion

        public int IDAsset{get;set;}
        public int CurrentArea {get;set;}
        public string Description{get;set;}
       public string ActualDepartment{get;set;}
               public string CapacityHealth{get;set;}

        public string SerialNumber { get; set; }               // SerialNo
            public string CartSerial { get; set; } 
            public string DCMonitorSerial { get; set; } 
             public string Notes { get; set; } 
                    public string AIOSerial { get; set; } 
        public AssetType Type { get; set; }
        public int VendorID {get;set;}
          public int Retired {get;set;}
        public int SiteID {get;set;}                              // SiteID
        public int EmTagID {get;set;}
        public int DepartmentId { get; set; }                  //DepartmentID
        public string DepartmentName { get; set; }
        public string AssetNumber { get; set; }
         public string Other { get; set; }
        public string ImageFilename {get;set;}
        public string Floor { get; set; }
        public string Wing { get; set; }
        public string ChargeLevel { get; set; }
        public string CycleCount { get; set; }
        public string FullChargeCapacity { get; set;  }
        public string Temperature { get; set;  }
               public string SourceIPAddress { get; set;  }
               public string IP { get; set;  }
               public string SiteName { get; set;  }
        public DateTime LastPostDateUTC { get; set; }
        public AssetStatus Status { get { return (AssetStatus)_deviceStatus; } }

        // MDJ TODO - read this from DB
        static public string StatusString( AssetStatus status )
        {
            switch( status )
            {
                case AssetStatus.AVAILABLE: return "Available";
                case AssetStatus.IN_SERVICE: return "In Service";
                case AssetStatus.NOT_COMMISSIONED: return "Not commissioned";
                case AssetStatus.OFFLINE: return "Offline";
            }
            return "Unknown";
        }


        // RTLS HACK
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public int XCoord { get; set; }
        public int YCoord { get; set; }
        public string State { get; set; }
        public bool RTLSUpdated { get; set; }
        #endregion


        public Asset()
        {
            Type = AssetType.UNKNOWN;
            State = "Healthy";
            SerialNumber = "0";
            Description = "0";
            XCoord = 0;
            YCoord = 0;
            RTLSUpdated = true;
            AssetNumber = "";
        }

        public Asset(Dictionary<string, string> dbRows) 
        {
            State = "Healthy";
            AssetNumber = "";
            foreach (var col in dbRows)
            {
                string colValue = col.Value != null ? col.Value.ToString() : "";
                switch (col.Key)
                {
                    case "IDAsset": IDAsset = Misc.SafeConvertToInt(colValue); break;
                    case "CurrentArea": CurrentArea = Misc.SafeConvertToInt(colValue); break;
                    case "Description": Description = colValue; break;
                    case "ActualDepartment": ActualDepartment = colValue; break;
                                            case "Capacity Health": CapacityHealth = colValue; break;
                          case "Retired": Retired = (Byte)Misc.SafeConvertToInt(colValue); break;
                    case "SerialNo": SerialNumber = colValue; break;
                                      case "SiteName": SiteName = colValue; break;
                             case "SourceIPAddress": SourceIPAddress = colValue; break;
                             case "IP": IP = colValue; break;
                    case "IDAssetType": Type = (Asset.AssetType)Misc.SafeConvertToInt(colValue); break;
                    case "VendorID": VendorID = Misc.SafeConvertToInt(colValue); break;
                    case "SiteID": SiteID = Misc.SafeConvertToInt(colValue); break;
                    case "EmTagID": EmTagID = Misc.SafeConvertToInt(colValue); break;
                    case "DepartmentID": DepartmentId = Misc.SafeConvertToInt(colValue); DepartmentName = api.GetDepartmentName(DepartmentId); break;
                    //case "Image": 
                    case "ImageFilename": ImageFilename = colValue; break;
                    //case "CreatedDateUTC":
                    //case "LegacyCastBUID":
                    case "SerialNumber": this.SerialNumber = colValue; break;
                    case "AssetStatusID": this._deviceStatus = (Byte)Misc.SafeConvertToInt(colValue); break;
                    case "AssetNumber": this.AssetNumber = colValue; break;
                        case "Other": Other = colValue; break;
                    case "Floor": Floor = colValue; break;
                    case "Wing": Wing = colValue; break;
                  case "Notes":  Notes = colValue; break;
                    case "DCMonitorSerial": this.DCMonitorSerial = colValue; break;
                           
                    case "AIOSerial": this.AIOSerial = colValue; break;
                    case "CartSerial": this.CartSerial = colValue; break;
                    case "ChargeLevel": this.ChargeLevel = Convert.ToString(Math.Round((Double)Misc.SafeConvertToDouble(colValue),2)); break;
                    case "FullChargeCapacity": this.FullChargeCapacity = colValue; break;
                    case "CycleCount": this.CycleCount = colValue; break;
                    case "Temperature": this.Temperature = colValue; break;
                    case "LastPostDateUTC": 
                        DateTime dPar;
                        if (DateTime.TryParse(colValue, out  dPar) == true ){
                                    this.LastPostDateUTC = Convert.ToDateTime(colValue) ;
                        }
                        else
                        {
                            this.LastPostDateUTC = DateTime.MinValue;
                        }
                         
                        break;
                    default: Utils.ErrorHandler.WarnMissingDBField(col.Key, this); break;
                }
            }
            SetDescription(Description);
            XCoord = 0;
            YCoord = 0;
            RTLSUpdated = true;
        }

        public void Update(Asset src)
        {
            this.CurrentArea = src.CurrentArea;
            this.DepartmentId = src.DepartmentId;
            this.Description = src.Description;
            this.SiteID = src.SiteID;
            this.ImageFilename = src.ImageFilename;
            this.OffsetX = src.OffsetX;
            this.OffsetY = src.OffsetY;
            this.SerialNumber = src.SerialNumber;
            this.State = src.State;
            this.Type = src.Type;
            this.XCoord = src.XCoord;
            this.YCoord = src.YCoord;
        }

        private void SetDescription(string description)
        {
            if (string.IsNullOrEmpty(description) || description.ToLower() == "unknown device")
            {
                this.Description = assetPropertyTable[0].description;
            }
            else
            {
                this.Description = description;
            }
            
        }

        public AssetCategory GetCategory()
        {
            AssetProperties prop = assetPropertyTable.Find(p => p.type == Type);
            return prop != null ? prop.category : AssetCategory.OTHER;
        }

        public string GetAssetColor(Asset.AssetType type)
        {
            AssetProperties prop = assetPropertyTable.Find(p => p.type == Type);
            return prop != null ? prop.htmlColor : kUnknownColor;
        }
    }

}