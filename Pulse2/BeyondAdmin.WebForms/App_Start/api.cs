﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;
using BeyondAdmin.Utils.Database;
using BeyondAdmin.Model;
    
namespace BeyondAdmin.API
{

    public class api
    {
        #region properties
        static public PulseUser SessionUser { get { return PulseSession.Instance.User; } }
        #endregion

        //
        // Returns a list of IDS for this user instance
        //
        static public List<Customer> GetCustomers()
        {
            return PulseSession.Instance.Customers;
        }

        //
        // Returns a list of facilities for a given user.  Currently assumes a 1:1 relationship between a user and a business unit
        //
        static public List<Site> GetSitesForUser(PulseUser user = null)
        {
            if( user == null )
            {
                user = SessionUser;
            }
            return user.Sites;
        }

        static public Site GetSessionUserSiteByIndex(int which)
        {
            if( SessionUser != null )
            {
                List<Site> facilities = GetSitesForUser(SessionUser);
                if( which >= 0 && which < facilities.Count )
                {
                    return facilities[which];
                }
            }
            return null;
        }

        //
        // Returns a list of facilities for a given user
        //
        static public List<Site> GetSiteListFromBUID(Int32 aBUID)
        {
            return PulseSession.Instance.GetCustomerFromBUID(aBUID).Sites;
        }

        //
        // Returns a list of departments for a given site
        //
        static public List<Department> GetDepartmentList(Site site)
        {
            return site.Departments;
        }



        //
        // Returns a list of assets at a given site
        //
        static public List<Asset> GetAssetList(Site site)
        {
            return site.Assets;
        }

        //
        // Adds a user to the DB
        //
        static public void AddUser(PulseUser user)
        {
            PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase("PulseConnection"));
            enovateDB.AddNewUser(user);
        }
        //
        // Returns a list of facilities for a given user
        //
        static public List<Asset> GetAssetsForBUID(int buid)
        {
            List<Site> facilities = GetSiteListFromBUID(buid);
            List<Asset> allAssets = new List<Asset>();

            foreach (Site site in facilities)
            {
                allAssets.AddRange(GetAssetList(site));
            }
            return allAssets;
        }

        //
        // Retunrs all assets associated with the given user's business unit
        //
        static public List<Asset> GetAssetsForUser(PulseUser user = null )
        {
            if (user == null)
            {
                user = SessionUser;
            }
            if( user.Assets == null )
            {
                user.Assets = GetAssetsForBUID(user.ParentCustomer.Id);
            }
            return user.Assets;
        }

        //
        // Returns the site that owns the given asset or null if not found
        //
        static public Site GetAssetsOwningSite(Asset asset)
        {
            List<Customer> idns = GetCustomers();
            if (idns != null)
            {
                foreach (Customer idn in idns)
                {
                    foreach (Site site in idn.Sites)
                    {
                        Asset found = site.Assets.Find(p => p.SerialNumber == asset.SerialNumber);
                        if (found != null)
                        {
                            return site;
                        }
                    }
                }
            }
            return null;
        }

        // 
        // Returns a list of alerts for the given user
        //
        static public List<Alert> GetAlertsForUser(PulseUser user)
        {
            PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase("PulseConnection"));


            return enovateDB.GetAlertsForFacilities(GetSitesForUser(user), true);
        }

        //
        // Returns a SiteID for a given site name or null if not found
        //
        static public Site GetSite(string siteName)
        {
            foreach (Customer idn in PulseSession.Instance.Customers)
            {
                Site f = idn.Sites.Find(e => e.Name == siteName);
                if (f != null)
                    return f;
            }
            return null;
        }

        //
        // Returns a SiteID for a given site name or null if not found
        //
        static public Site GetSite(int siteId)
        {
            foreach (Customer idn in PulseSession.Instance.Customers)
            {
                Site f = idn.Sites.Find(e => e.Id == siteId);
                if (f != null)
                    return f;
            }
            return null;
        }
        //
        // Returns a department for a given site and department name or null if not found
        //
        static public Department GetDepartment(Site site, string deptName)
        {
            foreach (Department dept in site.Departments)
            {
                if (dept.Name == deptName )
                    return dept;
            }
            return null;
        }
        static public Department GetDepartment(Site site, int deptId)
        {
            foreach (Department dept in site.Departments)
            {
                if (dept.Id== deptId)
                    return dept;
            }
            return null;
        }

        //
        // Returns a list of departments for a give user
        //
        static public Department GetDepartment(int id)
        {
            foreach( Site site in SessionUser.Sites )
            {
                Department dept = site.Departments.Find(d => d.Id == id);
                if( dept != null)
                {
                    return dept;
                }
            }
            return null;
        }
        
        static public string GetDepartmentName(int departmentId)
        {
            Department dept = GetDepartment(departmentId);
            if( dept != null )
            {
                return dept.Name;
            }
            return "";

        }
         
        static public List<PulseUser> GetCoUsers()
        {
            return PulseSession.Instance.User.CoUsers;
        }

           static public void DeleteCoUser(string username)
        {
            PulseDBInterface enovateDB = new PulseDBInterface(new SQLDatabase("PulseConnection"));
            enovateDB.DeleteUser(username);
        }
   
    }
}